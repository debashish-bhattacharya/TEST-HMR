var api = "api";

var serviceUrls=
{
		"udpStatus":api+"/udp/status",
		"httpStatus":api+"/http/status",
		"startHttpServer":api+"/http/start",
		"stopHttpServer":api+"/http/stop",
		"startUdpServer":api+"/udp/start",
		"stopUdpServer":api+"/udp/stop",
		"setAckTrue":api+"/acknowledgement",
		"clearCurrentAlarmById":api+"/clear"
}

var endPoint = 
{
		//"socket":remoteAddr+"/gs-guide-websocket",
		//"socket":"http://localhost:8010/gs-guide-websocket",
		"socket":location.protocol+"//"+location.host+"/gs-guide-websocket"
}


var topics = 
{
		"currentAlarm":"/topic/alarm"	
}
