PGDMP         6    
            w            fault_server    9.6.3    9.6.3     \           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            ]           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            ^           1262    78589    fault_server    DATABASE     �   CREATE DATABASE fault_server WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_India.1252' LC_CTYPE = 'English_India.1252';
    DROP DATABASE fault_server;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            _           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            `           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    78650    clearbyseverityfunc()    FUNCTION     �   CREATE FUNCTION clearbyseverityfunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
   	  delete from current where id = new.id and new.severity = 0;
      RETURN NEW;
   END;

$$;
 ,   DROP FUNCTION public.clearbyseverityfunc();
       public       postgres    false    3    1            �            1255    86758    delete_by_id()    FUNCTION     �   CREATE FUNCTION delete_by_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
		insert into history select * from current where id=old.id;
		update history set severity = 0 where id =  old.id;
      RETURN OLD;
   END;

$$;
 %   DROP FUNCTION public.delete_by_id();
       public       postgres    false    3    1            �            1255    78664    deletefunc()    FUNCTION     
  CREATE FUNCTION deletefunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
insert into history select * from current where component_id=old.component_id and managed_object_id=old.managed_object_id and event_id=old.event_id;
      RETURN OLD;
   END;

$$;
 #   DROP FUNCTION public.deletefunc();
       public       postgres    false    1    3            �            1255    114048    insertfunc()    FUNCTION     �  CREATE FUNCTION insertfunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
 delete from current where component_id=new.component_id and managed_object_id=new.managed_object_id  and event_id=new.event_id;
      INSERT INTO history(id,report,alarm,component_id, component_type, managed_object_id, managed_object_type,event_id, event_type, severity,event_description,generation_time, ip, log_time,acknowledgement) 
      VALUES (new.ID, new.report, new.alarm, new.component_id, new.component_type, new.managed_object_id, new.managed_object_type, new.event_id, new.event_type, new.severity, new.event_description, new.generation_time, new.ip, new.log_time, new.acknowledgement);
      RETURN NEW;
   END;

$$;
 #   DROP FUNCTION public.insertfunc();
       public       postgres    false    3    1            �            1255    78617    updatefunc()    FUNCTION     �  CREATE FUNCTION updatefunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
      INSERT INTO history(id,report,alarm,component_id, component_type, managed_object_id, managed_object_type,event_id, event_type, severity,event_description,generation_time, ip, log_time,acknowledgement) 
      VALUES (new.ID, new.report, new.alarm, new.component_id, new.component_type, new.managed_object_id, new.managed_object_type, new.event_id, new.event_type, new.severity, new.event_description, new.generation_time, new.ip, new.log_time, new.acknowledgement);

	delete from current where id = new.id  and new.severity = 0;
      
      RETURN NEW;
   END;

$$;
 #   DROP FUNCTION public.updatefunc();
       public       postgres    false    1    3            �            1259    78603    current    TABLE       CREATE TABLE current (
    id integer NOT NULL,
    report integer,
    alarm integer,
    component_id character varying(50),
    component_type integer,
    managed_object_id character varying(50),
    managed_object_type integer,
    event_id character varying(50),
    event_type integer,
    severity integer,
    event_description character varying(100),
    generation_time timestamp without time zone,
    ip character varying(24),
    log_time timestamp without time zone DEFAULT now(),
    acknowledgement boolean DEFAULT false
);
    DROP TABLE public.current;
       public         postgres    false    3            �            1259    78601    current_id_seq    SEQUENCE     p   CREATE SEQUENCE current_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.current_id_seq;
       public       postgres    false    186    3            a           0    0    current_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE current_id_seq OWNED BY current.id;
            public       postgres    false    185            �            1259    78609    history    TABLE       CREATE TABLE history (
    id numeric,
    report integer,
    alarm integer,
    component_id character varying(50),
    component_type integer,
    managed_object_id character varying(50),
    managed_object_type integer,
    event_id character varying(50),
    event_type integer,
    severity integer,
    event_description character varying(100),
    generation_time timestamp without time zone,
    ip character varying(24),
    log_time timestamp without time zone,
    acknowledgement boolean DEFAULT false
);
    DROP TABLE public.history;
       public         postgres    false    3            �           2604    78606 
   current id    DEFAULT     Z   ALTER TABLE ONLY current ALTER COLUMN id SET DEFAULT nextval('current_id_seq'::regclass);
 9   ALTER TABLE public.current ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    186    185    186            X          0    78603    current 
   TABLE DATA               �   COPY current (id, report, alarm, component_id, component_type, managed_object_id, managed_object_type, event_id, event_type, severity, event_description, generation_time, ip, log_time, acknowledgement) FROM stdin;
    public       postgres    false    186   �        b           0    0    current_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('current_id_seq', 492, true);
            public       postgres    false    185            Y          0    78609    history 
   TABLE DATA               �   COPY history (id, report, alarm, component_id, component_type, managed_object_id, managed_object_type, event_id, event_type, severity, event_description, generation_time, ip, log_time, acknowledgement) FROM stdin;
    public       postgres    false    187   Z#       �           2620    78657 !   current clear_by_severity_trigger    TRIGGER     w   CREATE TRIGGER clear_by_severity_trigger AFTER INSERT ON current FOR EACH ROW EXECUTE PROCEDURE clearbyseverityfunc();
 :   DROP TRIGGER clear_by_severity_trigger ON public.current;
       public       postgres    false    186    190            �           2620    114049    current insert_trigger    TRIGGER     d   CREATE TRIGGER insert_trigger BEFORE INSERT ON current FOR EACH ROW EXECUTE PROCEDURE insertfunc();
 /   DROP TRIGGER insert_trigger ON public.current;
       public       postgres    false    186    204            �           2620    78619    current update_trigger    TRIGGER     c   CREATE TRIGGER update_trigger AFTER UPDATE ON current FOR EACH ROW EXECUTE PROCEDURE updatefunc();
 /   DROP TRIGGER update_trigger ON public.current;
       public       postgres    false    186    189            X   f  x���Mk�@���_!|j���~B��(m��[��:�K
��?�Y%ב!
#y����������@N������;�K.g�A����F�Be� N��>��L	��R�Bq#!w�Ap9$(U�R�� !<�	�� ���5`�♲;l$�!��v�e�u�7�?nwW�v����������oÇ��W�[�%�8b W��/��#�'D�S.�+G����15Z�+$kR���)��k3��깴<�ta�\�d�0�(��
x����{�H��� T�T�>k`�H��L��y@����L��H��2a�Ih�����g��"!�'WQ$�G�r�i���'_J�<��z�HC=n�X\��챴�R�'4[��}��͓����])z��:��j'Pq�����վݽ��}�wU�HRi�z?*���ɜd��j���5�W�4>W��������^ �����?��en9�ڦR�
o��Fsy\A���H���6/��[���<�C�|w����������d�vt6��6��ڐ�)���k�ux���OѲN��)f�"���N�aE�m���T��,��B���d{�0,�<#�<Rl�ǖ���l���,�      Y      x���M�,9r�ק�eV��ө���
��1���w޸f3���ߊ�ʨ̺�#���K7}�呎���+���}}��>B�����~����>��#}��Q��g�_Z]�yM?���/~��뇣ۏ�7G��^|Z��?>r=��$�ܕW77�x	���Q�����k���+7�K���ƥIn]��/��p��us�t��%�%�&��5��t�.���u�7�	p���g�t���a�.k^�#��]k�������s�.��8�,Ey)8в%�`������������������js����=�CU�UU9�y���^tP=g�5惡�ꏫr�n��:�u�ŗ�R9lC:��s��6���?j==������u�u���Z�x�q9����p��'�T���>+W�7�]W��E����Z�%X�ٞ��\ܺD'G�'�� "���I��,� ��uF|XkZ�Ƈ��2{���N�?�j�ڦ�J>�����xSK̻�\4�mh�G�cj����M��,�����fCO`�[(_��ׁ�̈́��F6�cV�����uw�n��>%��v�LX4Wn�Ĳlj@{�sg�M��Bn٢n�Y���XԖ\�k�hSg��:ߴ��v-6��ױ�fM�\���nݷ�e��[���Kؖ����ִ .�ɲ�r��]޶9yS3ɹ����.oۜ\07�:�~��msr�zR��Rt���msrQ���.�F�aL�7Va��mN��lC�ߚ��
�os���xq��"Z2�R��I[5n�z[���^���t�%�Ҍ�:n��r劰E�:n���RV]��:n��x��븙�A�zq{���K3��r�%G]��2n���}����q��ٖ����*qn����u�V�����7�f���-&��u����/>,j�n���XxϜ^�۫dm�@H]�W��ƪ��ZT�l{��m�`�	9]�W�Z}W9d������b��pӫD�r�����W�Z�v_]�d�6g���zï^,��K[��O�C����.ni����M�㴋[��O�[�x��̴�[��O7�e��_��[��-Mڧ­TQ�F��-MZ���CUg�:�]�Ҥ%���]���Ȼ��I�������]�Ҥ\d�hżdy�]��\���UMz�;�]��̻M��C������g�M�$�� ��g�M����K��wq�3�c�2D�4�.myF�[.��$�����i�g�n�]��	7x�:a�u(���i۹>-A�qe�<#l;6�E�B�E�L�Z�P�i᲋Z���j������Z�� 59
e�2-h�-�Ҳ���%�LKZ���4�S�+��$I绋Z��(굨H�V�E8��Zyh�E���r��e'�]�ʴWÁ��I�&l�"�Ϡ�8y��\;:��M]�����'�X�u��u㢁'��q����[ԝ�<)ʎ�b��нK��9��[���;d�K��G/*�,���I�s�_�"��,6�o�YpUn!�R�9OJ����Y�$`2��9y�e$��L�ܤ�B}���9�;7)wL�<��	F���Mʝ�듓�>���d)]����L��n*+`'	yU��];W��j'g��NE��N���a'�vqnR��EkZ����rQ9�u\x)8��&ynR��!�gqn$BD�� �h�	��#X <�`��<�����X$4�a�#��c��<�����#a�SP����0���S���7@EB� ����뇱���"�z;�G������,�ǰÎ�a,�G����{釱��~��,������;�1���y �x�,�ǰ�o��6�+f�9U8gM��V������.Í��g� �vgl-����JH�`p,�O�)�4�^�?�a���V�#aq������:P�`Oq���	 �N��������w���_��!��}`X�|�"5؎5_�א�u��}cD��c781̽�Mt�) �L�& �)2a���]Q)�"��޶ZY. �sJ/]S��!`%����s���E�w<x�p����B�9���W�Ra�r\X?��=�}j~�W��ǜZԽ 1��O��NQ��(��� &#^���Ǝ���{\��Kz�l������)���Z��������MջE��?f�Y-	���s��1q8����nЩ
����;�4��ќ�^>�t"���D���(��*P�� �Z�>A��4n�BU�\��D	�t#��J \�{�ߒE�^�K\T��ؒ����wlSE�ɢKn�XW�u^��������u�9O��Ʀ���u�)u����U�4[�PY��M�
�ZĽ�T�LMU�*X�W�F�R�|}R]K����[���س�����Y5��*�;nTܓMe���4f�&O5��M՜+8�G��d��n��|We�C�v�JB\��#\�юp�~v��vSU��q��t����8>��۵��	�*�ռT��*��Te��U�7'��J��T���U�v�Rn7U��\VKX�ȍ?�u�~�DA�'�cS!ܓI�1���X��d�v�l�{��;��=�i��2�㻫�GT���s`܃ߠ����7GG�F��h�4�0�^QvT;0߳f��q�:���уp�6O�Jø#�cV"ܓUir1Uٮ"J��j���*mWpbI[���<|##���[`���/�=.�5WqS���
.������� �-��$�U������
�����P��Ê�j \��٢�MWNuPpⷲ��dMV�T��\[�@���S=�~�͊�jL��|�4Gi�L�g��v&�R��a�klr7ۚ`���!.^��Z�����$�W�U�r��n���� ָu[���mO�񏉻��M�f���δn7۠ �q��M�f;$��*x���,�۵� !p�*}�X��mR�8�����Dn7ۧ��%���j	Z*��mT���薤&�%s��Nv�-�d�-���6+0't�9��n7�U����W�-��`����]�8�L��ו0��탐9�,��&�ud��n�Bf��*8��M5Bخ��Ug/v[��݌�P������or7�)\�`�,�.K�v������B-M��z!(8_�nM-MK���^
.�]�4%�A��^��ĉ���Y�(M�B���T��͋�<˃?���F,4�A������'�I�����K�nfwy}��q1��)��1J��_M�!l�\g~�H:��|oj�3B`����������`)^����[
1MuC����H6ɛꆠ�b����oGS�̙���36ɛꆰ�i���s�[&MuCP0�[,=�&yS�v���ӥ0ɛ� `/�mQ����S�T���9ž��+�$o��h����/����M��AQ�_��	��MU	7�@�_)�R�q�Y����+lr7Uմ]��;�����TU�&�f���*��r�_í�[х��6��o���>��M׸qIr?{Z��z�ܒ����}�w��NO�55BԸ�Sz��[S	#����өun������:Σ����ĭ�������v�[.=H����:�z^s�r�w|� qmXȳ�pO���㎻�.�}F���y���.�zF����B�g�;�wƂng�y�.�t���y���.�qƸ�g�{�7w�� q�"X�wp����"\�qp!�-��ܶ��"\�i;�E}�w�e�p�-�E���\�]�qǽ� r�"\�W�pW�0��"\�Qp!?-�Eܴ��b�q'-�=�h��jț�������\��\��p�/����p�c＾V�H���a���H�B�z;M�ڵ;��C'�:-�(פ�7�m�(R��?�硷�I���q�	7�e��V�ip�ߌ_2^�sK�<ao�������	�?�(.o��\R0��XQꥶ�Ij( �M7k�[�$�lL�7�OC<g�Jϸ����q=���j�z�ӠЎ��s�#W/���5q{А�nZ3��`���1��ؘ\U=X�WH�p3�իk昷~	\�5��o��%ouQ\�6����^;�{��⊹�s��uF^o�`�&�!ʕ '  jZ�N�+��V��׎�tou}\a���]�U���`���?��W��rp�V�F�����5.7@�����6v������.r�a����_o��@���6�ů7�]o�x�Yy#��G��������ȗ���Q��v���r7i�/�޼��уf5 7�1i�V�H�2�a5)\8E7r�x#\�:��u�p���"�����ឮ�9����9ie��n0|��Fצ��ʔ���`X�D���yIRV�ͮ�.���{ˮ�d!�S�Q'k��p���e�x+��v����BNC�fI��z+Q�ݱ n�Ӑ�H��(S�)��m�zX�q��wV�L���ain�fi-#>��m��q�ǈUr|���a-	Z�d=t�$�E��0�NKZ�:��# �7�4#�^^޲�����H�a ]_�P�����
i��h�8�h{�c얿=�{�9G}�{ ���
��IN�ӿ���jMjF��MR�y
��|
̻�k��5�y\`j����]C�|��Po� �y�L��;�_o���r5����[������T�꬏��ĭ�2dX��ǒ��-ە�2g�B���35�-ەڭH�ł�m)��[�+����}�k~Sy�lWjx�}K�
4��Z�+����}K\��<X�+=hDp��U�nٮ���F�-��Fҫ�[�+����}˜�4���]�����[�GK�Kw�v�6�o�����-ەt
�����[�+����m+���c�޲]���l�||��9���J�dm`ۜ�߄"�̲]���l�b=�Ywz�%d��_��?l筹����}㯩�%K�voٹ�I:�oa�|���*��K�lm`��C��v,=������q�JYV5K,?�4����U��䭝���[�H��0�ϥv�6�oY�M}e��s������q��=�uv��vm{�kiC���!ܣ�ݲ����c��s�2g~a#�������cR��"-�Ґc�p���|Ǹ�Ϸ�����=)F��(�=����A�t�:�o�޹51y�ݚ=+
������p���*�]%2v�y�B&�o�V��(7ȗ�²]o�wT.�=�X_���'��VѮ��5=;(�P�v-�9;<Z:�,�{{d��a�<ܯC�Ղ����ғ�����[�5����k��$@���$�9����s�`�9KzN9�{r�u�Q�|Ψ��[߃�����{pru�F�~�9�z1�I{����0�~0'Lϗ�m����	9!�p9Y1�}M<x�\GV�7�&�6���3��-K����vq尹#\JKQq�b:��#ĭ�F򹦰�Ɲx+׉�'	L���|���7'}�%�!X1�oW"��84O�<�r���{ng0�a:��qmߺ���YW�0�A�:��~襀`1�[
H/���Cȿ�E��PlO�a�k�,X��o�2j������V��۵�7�������|�:07��j$�&�����O�&n�����B:�����8g��5��jg�
{�U\�x���rU���P`/����J�5�ۮ�Ƹ>-�x����vM7�k2J��.wl߾����ס�o��o�>��r���\��d�'�Б�����7gfT�w p~I�Ü$���Ùk�d�'2_��j���r����v�ڽ��=gyL���ۢ�Aax����P��ҏ����=פ�ݙ��t:�N-\d�'�i�vgd��}��GK�����5������W��6�#\d�'��B4g=7�G�t`�r���v�[T �e�ﺤ�����=q����9&�J��E�{⺗�7�G��g���̥�7�Ͼr��]��|���\.C���$��ɷ[��q�U��Ë����+c�B���R�`������R��n�j�@7=9�ɵs]��z��M�Mdr�ܪJ��{�;��q�%Ν`���]����&��ԙ��g�;&on�;uӫ�թ	룮�ʍ"ןWW�5RU��F�ȶ��vR)k��	M��rI*`�"�����}��k+��Q;�zQ]V���; �ƕO���\S1풛�=p�XM+����C\G�uje�k���Xn�X�W�/�f ��Y�^'�~i7� ��pyfV��n��p��V�+�/]>Y�-Vu��M.n��M	�&���p2�8���7~6��iq^�&�O�p2na5[��Y����Qn5��xuv��m�Zho[g� �4ߞ��]c7�Ú��[�s���&�نI;=l�:�,Ҷ&�(�M���N$󻗫7��y��Py�?���v"��v�4�0�^�篹�-�vpc��q����k�'o��n��|nR,Y'���j�}��s�$��uIj5YA��A[y�A"սNo3�~�6|�W�9Us�BV ���q���A�X�A
1Ԉ�&��*�	����A&1�c�o��y��.�������:m�n��� A��t�Vk��5�O����u�dF�#���K��#6��G��ay�����Mo��7�4?��o�|����M���7�J?���WhO\��
�n��7y|?���ñ�gC;���?<zz�8@=�����vw����	�`f�W������a���`�������)I�I�Y�X��o������q��2{n��ٻ�:�U/�m;�=�>d�%��R�:
��B�݇�F6ߔ�z�~�'��7���ӣ�v�$��>���?���~S/�8�������?�����������o���?�����<���_'{�ph]\��I?�vS�{�&�)�˘|m�oL)��ZG?�6��w�)�#��P�����Q���>7=nq�������M��ר���(��v]�N�7��U\t��{F �>�M�c���ѭEH��������GOz,�������������g�����ţۥ.����C{ΊaH�~hϧ�]CG�բס�Zp�[�ƕ|�.��G��d�F�벻:�s�(��V/u�����f_��Ő�|]R\�i���j�f��𣆥�?�yM���~���a�%�e�F���/l��y1����|]����G=���0$�Z�oL.���~zf�,�{<�6e����O�%h��6��eyZ�C�3*��;=4zF�!��6�?�;�S�4d}�k�ݏ^�ρ!���[��yXؿ,?����h�Cj     