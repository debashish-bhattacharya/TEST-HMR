var viewUdpServerStatus = function()
{
	$.ajax(
			{
				url:serviceUrls.udpStatus,
				data:{},
				success:function(data)
				{
					$('#udpStatus').text(data.STATUS);	 
					data.STATUS=="UP"?$('#udpButton').prop('checked', true):$('#udpButton').prop('checked', false);
				}
			});
}

var viewHttpServerStatus = function()
{
	$.ajax(
			{
				url:serviceUrls.httpStatus,
				data:{},
				success:function(data)
				{
					 $('#httpStatus').text(data.STATUS);
					 data.STATUS=="UP"?$('#httpButton').prop('checked', true):$('#httpButton').prop('checked', false);
				}
			});
}

$(document).ready(function(){ 
	viewHttpServerStatus();
	viewUdpServerStatus();
});


var startAndStopHttpServer = function()
{
        if($('#httpButton').prop("checked") == true){
        	$.ajax(
        			{
        				url:serviceUrls.startHttpServer,
        				data:{},
        				success:function()
        				{
        					 viewHttpServerStatus(); 
        				}
        			});
        }	
        else{
            $.ajax(
            		{
            			url:serviceUrls.stopHttpServer,
            			data:{},
            			success:function()
            			{
            				 viewHttpServerStatus(); 
            			}
            		});
        }
}

var startAndStopUdpServer = function()
{
        if($('#udpButton').prop("checked") == true){
        	$.ajax(
        			{
        				url:serviceUrls.startUdpServer,
        				data:{},
        				success:function()
        				{
        					 viewUdpServerStatus(); 
        				}
        			});
        }	
        else{
            $.ajax(
            		{
            			url:serviceUrls.stopUdpServer,
            			data:{},
            			success:function()
            			{
            				 viewUdpServerStatus(); 
            			}
            		});
        }
}