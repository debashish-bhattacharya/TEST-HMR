var loadAlarms = function()
{
	jQuery("#grid").jqGrid({
	   	url:'api/current',
		datatype: "json",
	   	colNames:['Id', 'Report', 'Alarm', 'Component Id', 'Component Type', 'Managed Object Id', 'Managed Object Type', 'Event Id', 'Event Type', 'Severity', 'Event Description', 'Generation Time', 'IP', 'Log Time', 'Ack', 'Clear'],
	   	colModel:[
	   		{name:'id',index:'id', width:55},
	   		{name:'report',index:'report', width:70},
	   		{name:'alarm',index:'alarm', width:70},
	   		{name:'componentId',index:'componentId', width:70},
	   		{name:'componentType',index:'componentType', width:70},
	   		{name:'managedObjectId',index:'managedObjectId', width:70},
	   		{name:'managedObjectType',index:'managedObjectType', width:70},
	   		{name:'eventId',index:'eventId', width:70},
	   		{name:'eventType',index:'eventType', width:70},
	   		{name:'severity',index:'severity', width:70},
	   		{name:'eventDescription',index:'eventDescription', width:70},
	   		
	   		{name: 'generationTime', 
		   		align: 'center', search: false, width: 200,
		   		formatter: 'date',
		   		formatoptions: { srcformat: "ISO8601Long", newformat: "d/m/Y h:i:s" }
	   		},
		   		
	   		{name:'ip',index:'ip', width:70},
	   	
	   		{name: 'logTime', 
	   			align: 'center', search: false, width: 200,
	   			formatter: 'date',
	   			formatoptions: { srcformat: "ISO8601Long", newformat: "d/m/Y h:i:s" }
	   		},
	   		
	   		{name: 'acknowledgement', index: 'acknowledgement', width: 60, align: 'center',
            formatter: 'checkbox', editoptions: { value: '1:0' },
            formatoptions: { disabled: false },
            },
            
            { name: 'clear', index: 'clear', width: 50,align: 'center', sortable: false, jsonmap: "id",
                formatter:function (cellvalue, options, rowObject) {
                	return "<i style='cursor:pointer;color:red;' title='Delete' class=\"ti-trash\" onclick='javascript:clearCurrentAlarmById("+cellvalue+");'/>";
                }
            }
	   	],
	   	
	   	rowNum:15,
	   	rowList:[10,20,30,40,50,60,70,80,90,100],
	   	pager: '#pager2',
	   	sortname: 'logTime',
	    viewrecords: true,
	    sortorder: "desc",
	    caption:"Current Alarm",
	   
	    beforeSelectRow: function (rowid, e) {
	        var $self = $(this),
	            iCol = $.jgrid.getCellIndex($(e.target).closest("td")[0]),
	            cm = $self.jqGrid("getGridParam", "colModel"),
	            localData = $self.jqGrid("getLocalRow", rowid);
	        if (cm[iCol].name === "acknowledgement" && e.target.tagName.toUpperCase() === "INPUT") {
	            // set local grid data
	       	setAckTrue(rowid);
	        	
	        }
	        
	        return true; // allow selection
	    },
	       
		 loadComplete: function() {
			   var i=getColumnIndexByName('acknowledgement');
			   // nth-child need 1-based index so we use (i+1) below
			   $("tbody > tr.jqgrow > td:nth-child("+(i+1)+") > input",
			      this).click(function(e) {
			        disableIfChecked(this);
			    }).each(function(e) {
			        disableIfChecked(this);
			    });
		 }
	});
}

var setAckTrue = function(id)
{	
	var data = { id: id, acknowledgement: true };
	$.ajax({
	    url:serviceUrls.setAckTrue,
	    type: 'POST',
	    data: JSON.stringify(data),
	    contentType: 'application/json; charset=utf-8',
	    dataType: 'json',
	    async: false,
	    success:function(data)
		{
		}
	});
}

$(document).ready(function(){
	 loadAlarms();
	 connectSocket();
});

var getColumnIndexByName = function(columnName) {
    var cm = $("#grid").jqGrid('getGridParam','colModel'),i=0,l=cm.length;
    for (; i<l; i++) {
        if (cm[i].name===columnName) {
            return i; // return the index
        }
    }
    return -1;
}

var disableIfChecked = function(elem){
    if ($(elem).is(':checked')) {
        $(elem).attr('disabled',true);
    }
}

var clearCurrentAlarmById = function(id)
{	
	$.ajax({
	    url:serviceUrls.clearCurrentAlarmById+"/"+id,
	    success:function(data)
		{
	    	$('#grid').jqGrid('delRowData',id);
		}
	});
}


var connectSocket = function()
{
	var stompClient = null;

	var socket = new SockJS(endPoint.socket);
	
	stompClient = Stomp.over(socket);
	
	stompClient.connect({}, function (frame) 
    {
    		console.log('Connected: ' + frame);
    		
    		stompClient.subscribe(topics.currentAlarm, function (data) 
    	    		{
    	    			if(data.body != "")
    	    			{
    	    				var data = jQuery.parseJSON(data.body);
    	    				//updateCueTable(data,'new');
    	    				jQuery("#grid").addRowData( rowid, data, first);
    	    			}
    	    		});
    });
}