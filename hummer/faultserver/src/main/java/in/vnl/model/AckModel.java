package in.vnl.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AckModel {
	private int id;
	private boolean acknowledgement;
	
	public AckModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*public AckModel(int id, boolean acknowledgement) {
		super();
		this.id = id;
		this.acknowledgement = acknowledgement;
	}*/
	
	@JsonCreator
	public AckModel(@JsonProperty("id") int id, @JsonProperty("acknowledgement") boolean acknowledgement) {
	    this.id = id;
	    this.acknowledgement = acknowledgement;
	}
	
	@Override
	public String toString() {
		return "AckModel [id=" + id + ", acknowledgement=" + acknowledgement + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isAcknowledgement() {
		return acknowledgement;
	}

	public void setAcknowledgement(boolean acknowledgement) {
		this.acknowledgement = acknowledgement;
	}
	
}
