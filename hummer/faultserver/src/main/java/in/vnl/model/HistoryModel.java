package in.vnl.model;

import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "history")
public class HistoryModel{
	@Id
	private int id;
	private int report;
	private int alarm;
	private String componentId;
	private int componentType;
	private String managedObjectId;
	private int managedObjectType;
	private String eventId;
	private int eventType;
	private int severity;
	private String eventDescription;
	private Date generationTime;
	private String ip;
	private Date logTime;
    private boolean acknowledgement;
    
	public HistoryModel(int id,int report, int alarm, String componentId, int componentType, String managedObjectId,
			int managedObjectType, String eventId, int eventType, int severity, String eventDescription,
			Date generationTime, String ip,Date logTime, boolean acknowledgement) {
		super();
		this.id = id;
		this.report = report;
		this.alarm = alarm;
		this.componentId = componentId;
		this.componentType = componentType;
		this.managedObjectId = managedObjectId;
		this.managedObjectType = managedObjectType;
		this.eventId = eventId;
		this.eventType = eventType;
		this.severity = severity;
		this.eventDescription = eventDescription;
		this.generationTime = generationTime;
		this.ip = ip;
		this.logTime = logTime;
		this.acknowledgement=acknowledgement;
	}
	
	@Override
	public String toString() {
		return "AlarmModel [id=" + id + ", idreport=" + report + ", alarm=" + alarm + ", componentId=" + componentId + ", componentType="
				+ componentType + ", managedObjectId=" + managedObjectId + ", managedObjectType=" + managedObjectType
				+ ", eventId=" + eventId + ", eventType=" + eventType + ", severity=" + severity + ", eventDescription="
				+ eventDescription + ", generationTime=" + generationTime + ", ip=" + ip + ", logTime=" + logTime +", acknowledgement=" + acknowledgement + "]";
	}

	public HistoryModel() {
		super();
	}
    
	
	public Date getLogTime() {
		return logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getAcknowledgement() {
		return acknowledgement;
	}

	public void setAcknowledgement(boolean acknowledgement) {
		this.acknowledgement = acknowledgement;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getReport() {
		return report;
	}
	public void setReport(int report) {
		this.report = report;
	}
	public int getAlarm() {
		return alarm;
	}
	public void setAlarm(int alarm) {
		this.alarm = alarm;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public int getComponentType() {
		return componentType;
	}
	public void setComponentType(int componentType) {
		this.componentType = componentType;
	}
	public String getManagedObjectId() {
		return managedObjectId;
	}
	public void setManagedObjectId(String managedObjectId) {
		this.managedObjectId = managedObjectId;
	}
	public int getManagedObjectType() {
		return managedObjectType;
	}
	public void setManagedObjectType(int managedObjectType) {
		this.managedObjectType = managedObjectType;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public int getEventType() {
		return eventType;
	}
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}
	public int getSeverity() {
		return severity;
	}
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	public String getEventDescription() {
		return eventDescription;
	}
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}
	public Date getGenerationTime() {
		return generationTime;
	}
	public void setGenerationTime(Date generationTime) {
		this.generationTime = generationTime;
	}	
}