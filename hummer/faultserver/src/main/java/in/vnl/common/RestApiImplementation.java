package in.vnl.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.vnl.model.AlarmModel;

@Service
public class RestApiImplementation {
	
	
	public void sendAlarmData(AlarmModel alarmModel,String ip, String port, String postUrl)  {
		try {
			
			SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
	        factory.setConnectTimeout(30000);
	        factory.setReadTimeout(30000);
			RestTemplate restTemplate = new RestTemplate(factory);;
		   
			URL url = new URL ("http://"+ip+":"+port+"/"+postUrl);
			String jsonInputString = new ObjectMapper().writeValueAsString(alarmModel);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(jsonInputString,headers);
			
			byte[] imageBytes = new byte[0];
			
			
				imageBytes = restTemplate.postForObject("http://"+ip+":"+port+"/"+postUrl, entity, byte[].class);
		
		}
		catch(MalformedURLException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();	
		}
	}
}
