package in.vnl.common;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import in.vnl.model.AckModel;
import in.vnl.model.AlarmModel;
import in.vnl.model.HistoryModel;
import in.vnl.repository.AlarmRepository;
import in.vnl.repository.HistoryRepository;

@Service
public class AlarmService {
	
	@Autowired
	AlarmRepository alarmRepository;
	
	@Autowired
	HistoryRepository historyRepository;
	
	public void saveAlarmData(AlarmModel alarmModel) {
		alarmRepository.save(alarmModel);
	}
	
	public HashMap<String,Boolean> setAlarmByAck(AckModel ackModel) 
	{
		AlarmModel alarmModel = alarmRepository.findById(ackModel.getId());
		alarmModel.setAcknowledgement(ackModel.isAcknowledgement());
		alarmRepository.save(alarmModel);
		
		HashMap<String,Boolean> hm = new HashMap<String,Boolean>();
		hm.put("SUCCESS", true);
		return hm;
	}
	
	public void clearAlarmData(AlarmModel alarmModel)
	{
		AlarmModel aM= alarmRepository.findByComponentIdAndManagedObjectIdAndEventId(alarmModel.getComponentId(), alarmModel.getManagedObjectId(), alarmModel.getEventId());
		aM.setSeverity(0);
		alarmRepository.save(aM);
		//alarmRepository.delete(aM.getId());
	}
	
	public List<AlarmModel> getCurrentAlarm() {
		return (List<AlarmModel>) alarmRepository.findAllByOrderByLogTimeDesc();
	}
	
	public List<HistoryModel> getCurrentAlarm(Date startDate,Date endDate) {
		return (List<HistoryModel>) alarmRepository.findByLogTimeBetween(startDate,endDate);
	}
	
	public List<HistoryModel> getHistoryAlarmData() {
		return (List<HistoryModel>) historyRepository.findAllByOrderByLogTimeDesc();
	}
	
	public List<HistoryModel> getHistoryAlarm(Date startDate,Date endDate) {
		return (List<HistoryModel>) historyRepository.findByLogTimeBetween(startDate,endDate);
	}
	
	public HashMap<String,Integer> clearById(int id) {
		AlarmModel alarmModel = alarmRepository.findById(id);
		int severity=alarmModel.getSeverity();
		alarmModel.setSeverity(0);
		System.out.print(alarmModel);
		alarmRepository.save(alarmModel);
		HashMap<String,Integer> hm = new HashMap<String,Integer>();
		hm.put("severity", severity);
		return hm;
	}
}
