package in.vnl.common;

import java.io.IOException; 
import java.net.DatagramPacket; 
import java.net.DatagramSocket; 
import java.net.InetAddress; 
import java.util.Scanner; 
  
public class UdpClient 
{ 
    public static void main(String args[]) throws IOException 
    { 
        Scanner sc = new Scanner(System.in); 
  
        // Step 1:Create the socket object for carrying the data. 
        DatagramSocket ds = new DatagramSocket(); 
  
        InetAddress ip = InetAddress.getLocalHost(); 
        //InetAddress ip = InetAddress.getByName("10.100.207.181");
        byte buf[] = null; 
   
        while (true) 
        { 
            String inp = sc.nextLine(); 
  
            // convert the String input into the byte array. 
            buf = inp.getBytes(); 
  
            // Step 2 : Create the datagramPacket for sending the data. 
            DatagramPacket DpSend = new DatagramPacket(buf, buf.length, ip, 8000); 
  
            // Step 3 : invoke the send call to actually send the data.
            ds.send(DpSend); 
        } 
    } 
} 