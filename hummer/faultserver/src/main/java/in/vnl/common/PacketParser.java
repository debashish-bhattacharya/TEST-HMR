package in.vnl.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.vnl.model.AlarmModel;
import in.vnl.repository.AlarmRepository;
import in.vnl.repository.HistoryRepository;

@Service
public class PacketParser extends Thread{
	
	String ip = null;
	String packet = null;
	
	@Autowired
	AlarmRepository alarmRepository;
	
	@Autowired
	RestApiImplementation restClient;
	
	@Autowired
	private Environment env;
	//@Autowired
	//HistoryRepository historyRepository;
	
	public PacketParser() {
		super();
	}
	
	@Override
	public void run() {
		handlePacket();
	}
	
	public void handlePacket() 
	{
		System.out.println("Client:-10");
		AlarmModel alarmModel =new AlarmModel();
		String[] data = this.packet.split(",");
		alarmModel.setReport(Integer.parseInt(data[0]));
		alarmModel.setAlarm(Integer.parseInt(data[1]));
		alarmModel.setComponentId((data[2]));
		alarmModel.setComponentType(Integer.parseInt(data[3]));
		alarmModel.setManagedObjectId(data[4]);
		alarmModel.setManagedObjectType(Integer.parseInt(data[5]));
		alarmModel.setEventId((data[6]));
		alarmModel.setEventType(Integer.parseInt(data[7]));
		alarmModel.setSeverity(Integer.parseInt(data[8]));
		alarmModel.setEventDescription(data[9]);
		Date dd = new Date(Long.parseLong(data[10])+19800000);
		alarmModel.setGenerationTime(dd);
		alarmModel.setIp(ip);
		//alarmModel.setIp("10.100.211.65");
		System.out.println(alarmModel);
		alarmRepository.save(alarmModel);
		String ip = env.getProperty("application.ip");
		String port = env.getProperty("application.port");
		String postUrl = env.getProperty("application.url");
		System.out.println("........."+ip+"..........."+port+"..........."+postUrl);
		restClient.sendAlarmData(alarmModel, ip, port, postUrl);
	}
	
	public List<AlarmModel> readJsonData(String inputFile) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		TypeReference<List<AlarmModel>> typeReference = new TypeReference<List<AlarmModel>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream(inputFile);
		try {
			List<AlarmModel> alarmModel = mapper.readValue(inputStream,typeReference);
			alarmRepository.saveAll(alarmModel);
			return alarmModel;
		} catch (IOException e){
			System.out.println("Unable to save Alarm Data: " + e.getMessage());
		}
		return null;
	}
}
