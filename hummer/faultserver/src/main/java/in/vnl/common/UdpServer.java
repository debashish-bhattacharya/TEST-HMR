package in.vnl.common;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class UdpServer extends Thread
{
	private DatagramSocket socket;
    private boolean running;
    private byte[] receive = new byte[200];
    public static boolean status = false;
    DatagramPacket packet = null;
    
    @Autowired
	private ApplicationContext applicationContext;
    
    @Autowired
    private  PacketParser packetParser;
    public UdpServer(int port) {
	    try {
	    	// Step 1 : Create a socket to listen at port 1234 
		    socket = new DatagramSocket(port); 
	    }
	    catch(Exception e) {
	    	System.out.println(e);
	    }
    }
    
	public void run() 
	{ 
		status = running = true;
		
		while (running) 
		{ 
			try {
		    // Step 2 : create a DatgramPacket to receive the data. 
		    packet = new DatagramPacket(receive, receive.length); 
		    System.out.println("Client:-1");
		    // Step 3 : receive the data in byte buffer. 
		    socket.receive(packet); 
		    System.out.println("Client:-2");
		    System.out.println("Client:-" + data(receive)); 
		    
		    InetAddress address = packet.getAddress();
			int port = packet.getPort();
			System.out.println("Client:-3");
			System.out.println(address);
			//System.out.println(packet.getLength());
			
			String received = new String(receive, 0, packet.getLength());
			System.out.println(received);
			System.out.println("Client:-4");
			
			if(received.equalsIgnoreCase("stop")) 
			{
				status = running = false;
				break;
			}
			System.out.println("Client:-5");
			PacketParser pp = new PacketParser(); 
			
			pp.packet = received;
			pp.ip = address.getHostAddress();
			System.out.println("Client:-6");
			applicationContext.getAutowireCapableBeanFactory().autowireBean(pp);
			System.out.println("Client:-7");
			pp.start();
			System.out.println("Client:-8");
		    // Clear the buffer after every message. 
		    receive = new byte[200]; 
		}
		catch(Exception e) {
			socket.close();
			System.out.println(e);
		}
	}
		if(!status) {
			socket.close();
		}
		
	}
    
	 // A utility method to convert the byte array 
	 // data into a string representation. 
	 public static StringBuilder data(byte[] a) 
	 { 
	     if (a == null) 
	         return null; 
	     StringBuilder ret = new StringBuilder(); 
	     int i = 0; 
	     while (a[i] != 0) 
	     { 
	         ret.append((char) a[i]); 
	         i++; 
	     } 
	     return ret; 
	 } 
} 
