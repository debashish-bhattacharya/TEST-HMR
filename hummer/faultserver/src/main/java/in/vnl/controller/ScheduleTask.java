package in.vnl.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import in.vnl.common.UdpServer;


import org.springframework.context.ApplicationContext;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class ScheduleTask {

	@Autowired
	private Environment env;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	public void scheduleTaskInit() throws IOException {
		Thread th = new UdpServer(Integer.parseInt(env.getProperty("app.udpport")));
		applicationContext.getAutowireCapableBeanFactory().autowireBean(th);
		th.start();
	}
}