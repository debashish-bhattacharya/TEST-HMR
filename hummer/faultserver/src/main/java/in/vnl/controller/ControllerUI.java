package in.vnl.controller;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import in.vnl.common.AlarmService;
import in.vnl.common.PacketParser;

@Controller
public class ControllerUI {
	
	@Autowired
	ScheduleTask scheduleTask;
	
	@Autowired
	PacketParser packetParser;
	
	@Autowired
	AlarmService alarmService;
	
	
	@PostConstruct
	public void scheduleTask() throws IOException {
		scheduleTask.scheduleTaskInit();	
		//System.out.print(packetParser.readJsonData("/data.json"));
	}
	
	@RequestMapping(value = "/status")
	public String serverStatus() {
		return "status";
	}
	
	@RequestMapping(value = "/current")
	public String getAllCurrentAlarm(Model model) {
		model.addAttribute("currentAlarmList", alarmService.getCurrentAlarm());
		return "fault1";
	}
}
