package in.vnl.sms.udpAdapter;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.TriggerData;
import in.vnl.sms.service.DBDataService;


@Service
@PropertySource("classpath:application.properties")
public class TriggerThread2 implements Callable<Boolean> {

	
	@Value("${retry_interval}")
	int retry_interval;
	@Value("${retries}")
	int retries;
	
	String destinationIP;
	DatagramSocket dsocket;
	DBDataService dbserv1;
	TriggerData tData;
	
	public TriggerThread2() {
		// TODO Auto-generated constructor stub
	}
	
	public TriggerThread2(String destinationIP, DatagramSocket serverSocket, TriggerData tData, int retry_interval, int retries) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		this.tData = tData;
		this.retry_interval = retry_interval;
		this.retries=retries;
	}
	@Override
	public Boolean call() throws Exception {
		// TODO Auto-generated method stub

		Thread.currentThread().setName(destinationIP + "SaveUITrigger");
		InetAddress address;
		try {
			address = InetAddress.getLocalHost();

			//DatagramSocket dsocket = new DatagramSocket(2000,address);

			for (int retry=0; retry < retries; retry++)
			{
				try {

					//Send SMSTrigger UDP packet. Receive this data from DB based on IP
					byte[] message = ("SMSTrigger,unit_no:"+tData.getUnit_no() + "\n" +
							" product_name:"+tData.getProduct_name()+"\n" + 
							" model:"+tData.getModel()+"\n" + 
							" mfg_date:"+tData.getMfg_date()+"\n" + 
							" client:"+tData.getClient()+"\n" + 
							" location:"+tData.getLocation()+"\n" + 
							" country:091" + "\n" +
							"").getBytes();


					// INPUT OF HOST FROM UI OR SMS HB
					InetAddress destinationAddress = InetAddress.getByName(destinationIP);

					// Initialize a datagram packet with data and address
					DatagramPacket packet = new DatagramPacket(message, message.length,
							destinationAddress, 252);
					dsocket.send(packet);

					Thread.sleep(retry_interval); 				
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//Ack received
					System.out.println("TriggerThread: Interrupt 1............." + Thread.currentThread().getName());
					if(Thread.currentThread().getName().contains("SaveUITrigger"))
					{
						System.out.println("TriggerThread: Interrupt SaveUITrigger");
						return true;
					}
					break;
					//e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}catch(Exception ae) {
					ae.printStackTrace();
					break;
				}
			}
			
			/*//Wait for next Heartbeat message. If not received after pre-defined period of time, mark it as down.
			Thread.sleep(10000);
			//PENDING-> MARK STATUS AS DOWN IF NO INTERRUPT IS RECEIVED
*/		
			
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}

}
