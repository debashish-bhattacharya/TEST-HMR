package in.vnl.sms.udpAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class JMGPSLocator implements Runnable{

	String timer, serialPort;
	String port = "5142";
	ArrayList<String> comPort;
	int flag = 0;
	//static JDialog jd;
	static SerialPort serialPortObj;
	static BlockingQueue<String> queue;
	
	public JMGPSLocator() {
		queue = new LinkedBlockingQueue<String>();
	}
	
	@Override
	public void run() {

		boolean portAvailable = false;
		while(!portAvailable)
		{
			try {
				Thread.sleep(60000);
				System.out.println("Retry to connect to JM after 1 min ");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			portAvailable = connect();
		}
			startCapture();
	
	}
	
	
	public boolean connect()
	{
		String j = null;
		if(getComPort()){
			for(String val: comPort)
			{
				if(val.contains("Sili")) {
					j=val;
				}
			}
    		if(flag == 1 && j!=null)
    			serialPort = j.substring(j.indexOf("(")+1, j.indexOf(")"));
    		
    		return true;
    	}
    	else{
    		System.out.println("JMCompass : No connection to Serial COM Port ");
    		return false;
    	}
	}
	
	
	public boolean getComPort(){
		String[] portNames = SerialPortList.getPortNames();
		comPort = new ArrayList<String>();
		boolean temp = false;
		flag = 0;
		if(portNames.length != 0){
			try{
				Process p = Runtime.getRuntime().exec("wmic path Win32_SerialPort get name");
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line = "";
				while ((line = reader.readLine())!= null) {
					if(line.contains("COM")){
						comPort.add(line);
						temp = true;
						if(line.contains("Sili"))
							flag = 1;
					}
				}
				reader.close();
			}catch(Exception e){
				System.out.println(e);
				//log.error(e);
			}
			return temp;
		}
		else{
			return false;
		}
		
	}
	
	public void startCapture()
	{
		if(serialPort!=null) {
			serialPortObj = new SerialPort(serialPort);
			
			System.out.println("JMCompass : Start JM Capture");
			
			try {
				serialPortObj.openPort();
				serialPortObj.setParams(921600, 8, 1, 0);
				serialPortObj.addEventListener(new SerialPortReader());
		        Thread.sleep(10);
	        }
			catch (InterruptedException ex) {
				ex.printStackTrace();
	        }
	        catch (SerialPortException e) {
	        	e.printStackTrace();
	        }
			
			try {

				String path;
				String[] packetVal;
				List<String> temp;
				Iterator<String> iter;
				boolean DR_Flag = false;
				
				String prevTime = null;
				HashMap<String, Double> tempGPSVal = new HashMap<String, Double>();
				
				while(true){
					if((path = queue.take()) != null )
					{
						temp = Arrays.asList(path.split("(?=\\$)"));
						iter = temp.iterator();

						while(iter.hasNext()){
							path = iter.next().trim();

							 if(path.contains("GNRMC")){
								if(path.contains("V")){
									DR_Flag = true;
								}
								else if(checksumNmea(path)){
									packetVal = path.split(",");
									double lat = latLon(packetVal[3], packetVal[4]);
									double lon = latLon(packetVal[5], packetVal[6]);
									
									LocalDateTime now = LocalDateTime.now();
									DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
									String formatDateTime = now.format(format);
									 
									System.out.println("JMCompass : Local Time" + formatDateTime +" witn NMEA Lat value "+ lat +" and lon value : " + lon);
									DR_Flag = false;
									
									if(prevTime==null)
									{
										tempGPSVal.put("latitude", lat);
										tempGPSVal.put("longitude", lon);
										prevTime = formatDateTime;
									}else if(prevTime.equalsIgnoreCase(formatDateTime))
									{
										tempGPSVal.put("latitude", lat);
										tempGPSVal.put("longitude", lon);
									}else {
										//@SAHIL SUNIL : SAVE DATA IN DB or perform other action here
										//saveInDB(Inet4Address.getLocalHost().getHostAddress(), prevTime, tempGPSVal.get("latitude"), tempGPSVal.get("longitude"));
										tempGPSVal.put("latitude", lat);
										tempGPSVal.put("longitude", lon);
										prevTime = formatDateTime;
									}
								}
							}
							
							else if(DR_Flag && path.contains("DR")){
								if(path.contains(",F*") && checksum(path)){
									packetVal = path.split(",");
									double lat = Double.parseDouble(packetVal[4]);
									double lon = Double.parseDouble(packetVal[5]);
									
									LocalDate localDate = LocalDate.now();
									String formatDateTime = localDate + " " +packetVal[3].substring(0,2)+":"+packetVal[3].substring(2,4)+":"+packetVal[3].substring(4,6);
									System.out.println("JMCompass : Local Time "+ formatDateTime +" with JM Lat value "+ lat +" and lon value : " + lon);

									if(prevTime==null)
									{
										tempGPSVal.put("latitude", lat);
										tempGPSVal.put("longitude", lon);
										prevTime = formatDateTime;
									}else if(prevTime.equalsIgnoreCase(formatDateTime))
									{
										tempGPSVal.put("latitude", lat);
										tempGPSVal.put("longitude", lon);
									}else {
										//@SAHIL SUNIL : SAVE DATA IN DB or perform other action here
										//saveInDB(Inet4Address.getLocalHost().getHostAddress(), prevTime, tempGPSVal.get("latitude"), tempGPSVal.get("longitude"));
										tempGPSVal.put("latitude", lat);
										tempGPSVal.put("longitude", lon);
										prevTime = formatDateTime;
									}

								}
							}
						}
					}
				}

			}catch (Exception e) {
				System.out.println(e);
			}finally {

				try{
				    serialPortObj.closePort();
					queue.clear();
				}catch(Exception e){
					System.out.println(e);
				}
				
			}
			
		}
	}
	
	
	
	static class SerialPortReader implements SerialPortEventListener {
        public void serialEvent(SerialPortEvent event) {
            try {
                String buffer = serialPortObj.readString(event.getEventValue());
                //if(!buffer.contains("[H")){
                	queue.put(buffer.trim());
                	Thread.sleep(100);
                //}
            }
            catch (SerialPortException ex) {
                System.out.println(ex);
                //log.error(ex);
            } catch (InterruptedException e) {
				System.out.println(e);
				//log.error(e);
			}
        }
    }
	
	
	
	public boolean checksum(String s){
		int j=0;
		try{
			String s1 = s.substring(0,s.lastIndexOf(","));
			for(int i=0;i<s1.length();i++){
				j = j+(int)s1.charAt(i);
			}
			s = s.substring(s.lastIndexOf("*")+1);
			if(Integer.parseInt(s)==j)
				return true;
			else
				return false;
		}catch(Exception e){
			return false;
		}
	}
	
	public boolean checksumNmea(String s){
		try{
			char check = 0;
			String s1 = s.substring(1,s.lastIndexOf("*"));
		    for (int c = 0; c < s1.length(); c++) {
		    	check = (char)(check ^ s1.charAt(c));
		    }
			s = s.substring(s.lastIndexOf("*")+1);
			if(s.equals(Integer.toHexString(check)))
				return true;
			else
				return false;
		}catch(Exception e){
			return false;
		}
	}
	
	private double latLon(String val, String dir){
		int te = val.indexOf(".")-2;
		double lat = Double.parseDouble(val.substring(0,te))+Double.parseDouble(val.substring(te,val.length()))/60;
		if(dir.equals("S") || dir.equals("W"))
			lat = -1*lat;
		return ((int)(lat*1000000))/1000000.0;
	}

}
