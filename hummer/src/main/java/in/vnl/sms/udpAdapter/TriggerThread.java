package in.vnl.sms.udpAdapter;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.TriggerData;
import in.vnl.sms.service.DBDataService;

@Service
@PropertySource("classpath:application.properties")
public class TriggerThread implements Runnable {
	
	@Value("${retry_interval}")
	int retry_interval;
	@Value("${retries}")
	int retries;
	
	String destinationIP;
	DatagramSocket dsocket;
	DBDataService dbserv1;
	TriggerData tData;
	
	public TriggerThread(String destinationIP, DatagramSocket serverSocket, DBDataService dbserv, int retry_interval, int retries) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		dbserv1= dbserv;
		this.retry_interval = retry_interval;
		this.retries=retries;
		//ApplicationContext context=SpringContext
	}
	
	/*public TriggerThread(String destinationIP, DatagramSocket serverSocket, TriggerData tData, int retry_interval) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		this.tData = tData;
		this.retry_interval = retry_interval;
		//ApplicationContext context=SpringContext
	}*/
	
	public TriggerThread() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	private DBDataService dbserv;
	
	public void test()
	{
		System.out.println("Object ------->" + dbserv);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub

		InetAddress address;
		try {
			address = InetAddress.getLocalHost();

			//DatagramSocket dsocket = new DatagramSocket(2000,address);

			for (int retry=0; retry < 3; retry++)
			{
				try {

					/*Map<String, String> dataMap = dbserv.getTriggerData(destinationIP);

				//Send SMSTrigger UDP packet. Receive this data from DB based on IP
				byte[] message = ("Trigger PACKET:\r\n" + 
						"unit_no:"+dataMap.get("unit_no")+"\r\n" + 
						" product_name:"+dataMap.get("Product_name")+"\r\n" + 
						" model:"+dataMap.get("Model")+"\r\n" + 
						" mfg_date:"+dataMap.get("Mfg_date")+"\r\n" + 
						" client:"+dataMap.get("Client")+"\r\n" + 
						" location:"+dataMap.get("Location")+"\r\n" + 
						" country:091\r\n" + 
						"").getBytes();*/

					tData = dbserv1.getTriggerData(destinationIP);
					
					System.out.println("Creating Trigger Message");

					//Send SMSTrigger UDP packet. Receive this data from DB based on IP
					byte[] message = ("SMSTrigger,unit_no:"+tData.getUnit_no() + "\n" +
							" product_name:"+tData.getProduct_name()+"\n" + 
							" model:"+tData.getModel()+"\n" + 
							" mfg_date:"+tData.getMfg_date()+"\n" + 
							" client:"+tData.getClient()+"\n" + 
							" location:"+tData.getLocation()+"\n" + 
							" country:091" + "\n" +
							"").getBytes();



					// INPUT OF HOST FROM UI OR SMS HB

					InetAddress destinationAddress = InetAddress.getByName(destinationIP);

					// Initialize a datagram packet with data and address
					DatagramPacket packet = new DatagramPacket(message, message.length,
							destinationAddress, 252);
					dsocket.send(packet);

					System.out.println("Trigger Data Sent!. Waiting ");
					Thread.sleep(20000); 
					
					
					
					/*// Retry after 5sec if ack is not received.

					if(retry==3)
					{
						System.out.println("DOWN...............");
						//PENDING-> MARK STATUS AS DOWN IF NO INTERRUPT IS RECEIVED
					}*/
					
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					//Ack received
					System.out.println("TriggerThread: Interrupt 1............." + Thread.currentThread().getName());
					break;
					//e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}catch(Exception ae) {
					ae.printStackTrace();
					break;
				}
			}
			
			
			
			/*//Wait for next Heartbeat message. If not received after pre-defined period of time, mark it as down.
			Thread.sleep(10000);
			//PENDING-> MARK STATUS AS DOWN IF NO INTERRUPT IS RECEIVED
*/		
			
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		
		
		/*catch (InterruptedException e) {
			//HB received.
			System.out.println("Interrupt 2................");
		}*/
	}

}
