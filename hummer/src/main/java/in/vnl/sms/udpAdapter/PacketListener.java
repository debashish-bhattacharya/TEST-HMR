package in.vnl.sms.udpAdapter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.service.AutoBackupService;
import in.vnl.sms.service.DBDataService;
import in.vnl.sms.service.DatabaseService;



@Component
@PropertySource("classpath:application.properties")
public class PacketListener {
	
	Logger logger = LoggerFactory.getLogger(PacketListener.class);
	
	public static ArrayList<String> serverAddresses = new ArrayList<String>();
	//private ArrayList<String> triggerThNamesList = new ArrayList<String>();
	//private ArrayList<String> configThNamesList = new ArrayList<String>();
	public static HashMap<String, Boolean> existingState = new HashMap<String, Boolean>();
	public static String startTime;
	
	//ExecutorService triggerThExecutor = Executors.newFixedThreadPool(10);
	//ExecutorService configThExecutor = Executors.newFixedThreadPool(10);
	@Autowired
	public DBDataService dbserv;
	@Autowired
	public DatabaseService databaseService;
	@Autowired
	public AutoBackupService autoBackup;
	
	@Value("${udp_port}")
	int udp_port;
	@Value("${heartbeat_interval}")
	int heartbeat_interval;
	@Value("${retry_interval}")
	int retry_interval;
	@Value("${retries}")
	int retries;
	@Value("${jmRetryTime}")
	int jmRetryTime;
	@Value("${diskCheckRetryTime}")
	int diskCheckRetryTime;
	@Value("${autoRetryTime}")
	int autoRetryTime;
	@Value("${maxSamples}")
	int maxSamples;
	
	DatagramSocket serverSocket;
	byte[] receiveData;
	byte[] sendData;
	ExecutorService executor = Executors.newCachedThreadPool();
	
	public PacketListener(int port) throws IOException
	{
		//dbserv.checkDependency();
		//PENDING-->INPUT SOCKET FROM PROPERTY FILE
		serverSocket = new DatagramSocket(udp_port);
	    receiveData = new byte[1024];
	    sendData = new byte[1024];
	    //service();
	}
	
	public PacketListener() throws IOException  {
		//this(3004);		
	
	}
	
	public DatagramSocket getSocketInstance()
	{
		return serverSocket;
	}
	/*public void createSocketConnection() {
		try {
			serverSocket = new DatagramSocket(3001);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    receiveData = new byte[1024];
	    sendData = new byte[1024];
	}*/
    
    public void service() throws IOException
    {
    	/*System.out.println("PacketListener: Going to listen JM");    	
    	Thread JMThread = new Thread(new JMCompass(dbserv));
    	JMThread.setName("JMThread");
    	JMThread.start();*/
    
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        startTime= dateFormat.format(new Date());
    	
    	dbserv.updateIs_ActiveFalse();
    	
        System.out.println("Fetching required data from db");
        logger.info("Fetching required data from db");
        dbserv.updateAllData();
        
        String antennaIdPort = dbserv.getStruAntennaIdPort();
        System.out.println("Sector Mapping Is:" + antennaIdPort);
        
        //service to start auto backup
        autoBackup.startAutoBackup();
        
        System.out.println("Running Queue to process events");
        logger.info("Running Queue to process events");
        Runnable runnable = new Runnable(){
            public void run(){
            	dbserv.executeQueue();
            }
        };
        Thread executeQueueThread = new Thread(runnable);
        executeQueueThread.start();
        
        System.out.println("Running scheduler to check node status");
        logger.info("Running scheduler to check node status");
        Runnable runnable1 = new Runnable()
        {
            @Override
            public void run()
            {
            	try {
            		dbserv.checkNodeStatus();
            	}catch(Exception e) {
            		logger.error("Error in Node Status Scheduler "+e.getMessage());
            	}
            }
        };
        
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	    scheduler.scheduleAtFixedRate(runnable1, 10, 10, TimeUnit.SECONDS);
	    
	    // commented code for JM scheduler
	    /*System.out.println("Running scheduler to get JM data");
        logger.info("Running scheduler to get JM data");
        Runnable runnable2 = new Runnable()
        {
            @Override
            public void run()
            {
            	try {
            		dbserv.jmNodeData();
            	}catch(Exception e) {
            		logger.error("Error in JM Scheduler "+e.getStackTrace());
            	}
            }
        };
        
        ScheduledExecutorService jmScheduler = Executors.newScheduledThreadPool(1);
	    jmScheduler.scheduleAtFixedRate(runnable2, 0, jmRetryTime, TimeUnit.MINUTES);*/
	    
	    System.out.println("Running scheduler to check available disk space");
        logger.info("Running scheduler to check available disk space");
        Runnable diskCheckRunnable = new Runnable()
        {
            @Override
            public void run()
            {
            	try {
            		dbserv.checkDiskSpace();;
            	}catch(Exception e) {
            		logger.error("Error in Disk Check Space Scheduler "+e.getStackTrace());
            	}
            }
        };
        
        ScheduledExecutorService diskCheckScheduler = Executors.newScheduledThreadPool(1);
        diskCheckScheduler.scheduleAtFixedRate(diskCheckRunnable, 0, diskCheckRetryTime, TimeUnit.MINUTES);
	    
	    System.out.println("Running scheduler to calculate autoEvent");
        logger.info("Running scheduler to calculate autoEvent");
        Runnable runnable3 = new Runnable()
        {
            @Override
            public void run()
            {
            	try {
            		dbserv.findAutoEvent();
            	}catch(Exception e) {
            		dbserv.deleteCache();
            		Writer buffer = new StringWriter();
            		PrintWriter pw = new PrintWriter(buffer);
            		e.printStackTrace(pw);
            		//logger.error("Error in Auto Scheduler ",e);
            		logger.error("Error in Auto Scheduler "+buffer.toString());
            	}
            }
        };
        
        ScheduledExecutorService autoScheduler = Executors.newScheduledThreadPool(1);
	    autoScheduler.scheduleWithFixedDelay(runnable3, 10, autoRetryTime, TimeUnit.SECONDS);
        
    	System.out.println("PacketListener: Going to listen to PT Udp Packets");
    	logger.info("PacketListener: Going to listen to PT Udp Packets");
    	Thread PTZUdpThread = new Thread(new PTZUdpListener(dbserv));
    	PTZUdpThread.setName("PTZUdpThread");
    	System.out.println("--------------------------");
    	PTZUdpThread.start();
    	
    	System.out.println("PacketListener: Going to listen on port..."+udp_port);
    	logger.info("PacketListener: Going to listen on port..."+udp_port);
    	
    	Thread peakThread = new Thread(new PeakDataThreadCopy(dbserv, databaseService));
    	peakThread.start();
    	System.out.println("PacketListener: Going to start thread for peak data...");
    	logger.info("PacketListener: Going to start thread for peak data...");
    	
    	//REposrt Angle 
    	Thread ptzPeakThread = new Thread(new PtzDataThread(dbserv));
    	ptzPeakThread.start();
    	System.out.println("PacketListener: Going to start thread for ptz peak data...");
    	logger.info("PacketListener: Going to start thread for ptz peak data...");
    	
    	serverSocket = new DatagramSocket(udp_port);
	    //receiveData = new byte[33000];
	    sendData = new byte[1024];
    	//this.createSocketConnection();
    	while(true)
        {
    		  receiveData = new byte[33000];
              DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
              serverSocket.receive(receivePacket);
              String sentence = new String(receivePacket.getData()).trim();
              //System.out.println(LocalDateTime.now());
              //System.out.println(Thread.getAllStackTraces().size());
              //System.out.println("PacketListener: RECEIVED: " + sentence);
              logger.debug("PacketListener: RECEIVED: " + sentence);
              //System.out.println("PacketListener: dbs erv obj" + dbserv);
                         
              if(sentence.contains("SMSHeartBeat"))
              {
            	  //added here sajal
        		  dbserv.updateDeviceState(receivePacket.getAddress().getHostAddress(), "Reachable");
        		  System.out.println(sentence);
            	  if(serverAddresses.contains(receivePacket.getAddress().getHostAddress()))
            	  {
            		  //get the running thread and update status
            		  Thread serverHB;
            		  if(sentence.contains("SMSHeartBeat,Configuration Applied")){
    					  
    					  DBDataService.changesAllowed=true;
    			  	  }
            		  if(sentence.contains("SMSHeartBeat,Calibrating") || sentence.contains("SMSHeartBeat,Monitoring") ||sentence.contains("SMSHeartBeat,NORMAL")||(sentence.contains("SMSHeartBeat,DEFAULT")))
            		  {
            			  for (Thread t : Thread.getAllStackTraces().keySet()) {
                			  if (t.getName().equals(receivePacket.getAddress().getHostAddress())) 
                			  {
                				  //boolean threadState = existingState.get(t.getName());

                				  t.interrupt();
                				  
                				  if(sentence.contains("SMSHeartBeat,Calibrating"))
                				  {
                					  String[] hbMessage = sentence.split(",");
                    				  dbserv.updateDeviceState(receivePacket.getAddress().getHostAddress(), "Scanning");
                    				  
                    				  //if(!dbserv.isCalibrating) 
                    				  dbserv.sendCurrentEvent("Scanning");
                    				  dbserv.stopOperations();
                    				  if(sentence.contains("SMSHeartBeat,Calibrating stops"))
                    				  {
                    					  //SMSHeartBeat,Calibrating stops;#40,l1-1,l2-1,l3-1,l4-1,l5-1#
                    					  try {
                    						  System.out.println(sentence);
                    							List<String> recordclb = new ArrayList<String>();
                    							List<String> dltclb = new ArrayList<String>();
                    		            		String[] temp = sentence.split(";")[1].split("#");
                    		            		//changed here sentence.split("=")[0].trim().split("#")[0]
                    		            		for(int i=0; i < temp.length; i++)
                    		            		{
                    		            			String qurey="";
                    		            			String instr="";
                    		            			String antId="";
                    		            			String[] clbData=temp[i].split(",");
                    		            			for(int i1=0;i1<clbData.length;i1++) {
                    		            				
                    		            				if(i1==0) {
                    		            					antId=clbData[0];
                    		            				}
                    		            				else {
                    		            					instr=""+antId+"','"+clbData[i1].split("-")[0]+"','"+clbData[i1].split("-")[1]+"'";
                    		            					qurey="insert into calibration(ip_add,ant_id,ref_level,status) values ('"+receivePacket.getAddress().getHostAddress()+"','"+instr+")";
                    		            					System.out.println(qurey);
                    		            					
                    		            					recordclb.add(qurey);
                    		            					System.out.println(recordclb.size());
                    		            				}
                    		            					
                    		            			}
                    		            			}
                    		            			if(recordclb.size()>0) {
                    		            			String qry="delete from calibration" ; //where ip_add='"+receivePacket.getAddress().getHostAddress()+"'";
            		            					dltclb.add(qry);
               		            					databaseService.executeBatchInsertion(dltclb);
                      		            			databaseService.executeBatchInsertion(recordclb);
                    		            			
                    		            		}
                    		            		
                    						} catch (Exception e) {
                    							// TODO Auto-generated catch block
                    							e.printStackTrace();
                    						}
                    					  
                    				  }
                				  }else if(sentence.contains("SMSHeartBeat,Monitoring")){
                					  
                					  if(dbserv.isCalibrating)
                					  dbserv.sendCurrentEvent("Idle");
                					  String[] hbMessage = sentence.split(",");
                    				  //dbserv.updateDeviceState(receivePacket.getAddress().getHostAddress(), "Monitoring");
                    				  dbserv.startOperations();
                			  	  }
                				  
                				  else if(sentence.contains("SMSHeartBeat,NORMAL")){
                					  try {
										dbserv.cahngeBySpsConfigData ("Normal");
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
                			  	  }else if(sentence.contains("SMSHeartBeat,DEFAULT")){
                					 
                			  		try {
										dbserv.cahngeBySpsConfigData ("Default");
									} catch (ClassNotFoundException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (SQLException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
                			  	  }

                				 /* if(!threadState)
                				  {
                					  //PENDING---> Start SMS Trigger packet exchange
                					  Thread triggerThread = (new Thread(new TriggerThread(receivePacket.getAddress().toString())));
                					  //PENDING-->To make thread unique, give thread name acc to unit no present in udp packet...if multiple SMSTriger packet can be sent.
                					  triggerThread.setName("unit No.-?");
                					  triggerThNamesList.add("unit No.-?");
                					  triggerThread.start();
                				  }else {
                					  //Already in active state. Regular HB message
                				  }*/

                			  }else {
                				  //unable to find thread with that name. Remove it's name from list and create a new one.
                			  }
                		  }
            		  }else {
            			  //This is the case of error condition where HB is received again although the state is active.
            			  for (Thread t : Thread.getAllStackTraces().keySet()) {
                			  if (t.getName().equals(receivePacket.getAddress().getHostAddress())) 
                			  {
                				  boolean threadState = existingState.get(t.getName());

                				  System.out.println("THREAD STATE BEFORE INTERRUPTING : "+threadState);
                				  t.interrupt();
            
                				  if(!threadState)
                				  {
                					  //PENDING---> Start SMS Trigger packet exchange
                					  Thread triggerThread = (new Thread(new TriggerThread(receivePacket.getAddress().getHostAddress(), serverSocket, dbserv, retry_interval, retries)));
                					  //PENDING-->To make thread unique, give thread name acc to unit no present in udp packet...if multiple SMSTriger packet can be sent.
                					  triggerThread.setName(receivePacket.getAddress().getHostAddress() + "Trigger");
                					  //triggerThNamesList.add(receivePacket.getAddress() + "Trigger");
                					  triggerThread.start();
                				  }
                			  }else {
                				  //unable to find thread. Need to check this scenario
                			  }
                		  }
            		  }
            		  

            	  }else {		//CHANGE TEST::::::::: if(!(sentence.contains("SMSHeartBeat,Calibration") || sentence.contains("SMSHeartBeat,Monitoring"))){
            		  
            		  List<DeviceInfo> devices = dbserv.getDevicesList();
            		  List<String> allIPs = new ArrayList<String>();
            		  for(DeviceInfo device : devices)
            		  {
            			  allIPs.add(device.getIp_add());
            		  }
            		  
            		  if(allIPs.contains(receivePacket.getAddress().getHostAddress()))
            		  {
            			  serverAddresses.add(receivePacket.getAddress().getHostAddress());
                		  //start a new thread for this server IP for heartbeat
                		  
                		  if(sentence.contains("SMSHeartBeat,Calibrat") || sentence.contains("SMSHeartBeat,Monitoring")) {
                			  
                			  Thread serverHB = (new Thread(new HBThread(receivePacket.getAddress().getHostAddress(), dbserv, heartbeat_interval)));
                    		  serverHB.setName(receivePacket.getAddress().getHostAddress());
                    		  serverHB.start();
                			  
                		  }else if(sentence.contains("SMSHeartBeat,")) {
                			  String[] hbTimeTemp = sentence.split(",");
                    		  String hbStartTime = hbTimeTemp[1] +" " + (hbTimeTemp[2].split("#"))[0];
                    		  
                    		  HBThread hbObj = new HBThread();
                			  hbObj.setStartTime(hbStartTime);
                			  
                    		  Thread serverHB = (new Thread(new HBThread(receivePacket.getAddress().getHostAddress(), dbserv,hbStartTime, heartbeat_interval)));
                    		  serverHB.setName(receivePacket.getAddress().getHostAddress());
                    		  serverHB.start();
                			  
                		  }else if(sentence.contains("SMSHeartBeat")) {
                			  Thread serverHB = (new Thread(new HBThread(receivePacket.getAddress().getHostAddress(), dbserv, heartbeat_interval)));
                    		  serverHB.setName(receivePacket.getAddress().getHostAddress());
                    		  serverHB.start();
                		  }
                		  /*String[] hbTimeTemp = sentence.split(",");
                		  String hbStartTime = hbTimeTemp[1] +" " + (hbTimeTemp[2].split("#"))[0];*/
                		  
                		  //PENDING------> Make Thread pool
                		  /*Thread serverHB = (new Thread(new HBThread(receivePacket.getAddress().getHostAddress(), serverSocket, dbserv,hbStartTime, heartbeat_interval)));
                		  serverHB.setName(receivePacket.getAddress().getHostAddress());
                		  serverHB.start();*/
                		  System.out.println("In SMSHEARTBEAT: Going to START Trigger Thread");
                		  logger.info("In SMSHEARTBEAT: Going to START Trigger Thread");
                		  Thread triggerThread = (new Thread(new TriggerThread(receivePacket.getAddress().getHostAddress(), serverSocket, dbserv, retry_interval, retries)));
                		  triggerThread.setName(receivePacket.getAddress().getHostAddress() + "Trigger");
                		  //triggerThNamesList.add(receivePacket.getAddress() + "Trigger");
                    	  triggerThread.start();
            		  }

            	  }
              }else if(sentence.contains("SMSTriggerACK")){
            	  //Handling for Ack received for SMSTrigger
            	  for (Thread t : Thread.getAllStackTraces().keySet()) {
        			  //PENDING---> Find unit no in ACK and find that thread from its number.
            		  if (t.getName().equals(receivePacket.getAddress().getHostAddress() + "Trigger") && (!(t.getState().name().equals("TERMINATED")))) 
        			  {
            			  System.out.println("PacketListener: Received Trigger ACK");
            			  logger.info("PacketListener: Received Trigger ACK");
            			  /*String[] hbTimeTemp = sentence.split(",");
                		  String hbStartTime = hbTimeTemp[1] +" " + (hbTimeTemp[2].split("#"))[0];
                		  
            			  HBThread hbObj = new HBThread();
            			  hbObj.setStartTime(hbStartTime);*/
        				  t.interrupt();
        				  
        				//When SMSTrigger response is received, send Config message.
        				  dbserv.systemConfiguration="Configuartion change by System Trigger-Auto";
                    	  Thread configThread = (new Thread(new ConfigThread(receivePacket.getAddress().getHostAddress(), serverSocket, dbserv,retry_interval, retries, maxSamples)));
                    	  configThread.setName(receivePacket.getAddress().getHostAddress() + "Config");
                    	  //configThNamesList.add(receivePacket.getAddress() + "Config");
                    	  configThread.start();
        			  }
            		  if(t.getName().equals(receivePacket.getAddress().getHostAddress() + "SaveUITrigger") && (!(t.getState().name().equals("TERMINATED"))))
            		  {
            			  System.out.println("PacketListener: SaveUITrigger ACK");
            			  logger.info("PacketListener: SaveUITrigger ACK");
            			  t.interrupt();
            		  }
            		  
            	  }
            	  
              }else if(sentence.contains("smsGPSinfo")){
            	  System.out.println(sentence);
            	  //logger.info(sentence);
            	  if(sentence.contains("GOOD")) {
            		  String gps[] = sentence.trim().split(",");
            		  //System.out.println(gps);
            		  dbserv.updateGPSData(receivePacket.getAddress().getHostAddress(), gps[2], gps[3], DBDataService.gpsAccuracy);
            	  }
              }else if(sentence.contains("SMSSettingsACK")){
            	  
    			  System.out.println("PacketListener: Received Config ACK");
    			  logger.info("PacketListener: Received Config ACK");
            	  for (Thread t : Thread.getAllStackTraces().keySet()) {
            		  //PENDING---> Find unit no in ACK and find that thread from its number.
            		  if (t.getName().equals(receivePacket.getAddress().getHostAddress() + "Config") && (!(t.getState().name().equals("TERMINATED")))) 
            		  {
            			  dbserv.updateDeviceState(receivePacket.getAddress().getHostAddress(), "Reachable");
            			  t.interrupt();
            		  }
            		  if(t.getName().equals(receivePacket.getAddress().getHostAddress() + "SaveUIConfig") && (!(t.getState().name().equals("TERMINATED"))))
            		  {
            			  System.out.println("PacketListener: SaveUIConfig ACK");
            			  logger.info("PacketListener: SaveUIConfig ACK");
            			  dbserv.updateDeviceState(receivePacket.getAddress().getHostAddress(), "Reachable");
            			  t.interrupt();
            		  }
            	  }
              }else if(sentence.contains("SMSStopACK")) {
            	  System.out.println("PacketListener: Received Stop ACK");
    			  logger.info("PacketListener: Received Stop ACK");
            	  for (Thread t : Thread.getAllStackTraces().keySet()) {
            		  //PENDING---> Find unit no in ACK and find that thread from its number.
            		  if(t.getName().equals(receivePacket.getAddress().getHostAddress() + "StopUIConfig") && (!(t.getState().name().equals("TERMINATED"))))
            		  {
            			  System.out.println("PacketListener: StopUIConfig ACK");
            			  logger.info("PacketListener: StopUIConfig ACK");
            			  t.interrupt();
            		  }
            	  }
            	  
              }else if(sentence.contains("PeakInfo")){
            	 
            	  try {
            		String[] temp = sentence.split(";")[1].split(",");		//changed here sentence.split("=")[0].trim().split("#")[0]
            		
            		if(temp[3].equals(antennaIdPort))
            			PtzDataThread.queue.put(sentence+"="+receivePacket.getAddress().getHostAddress());
            		
            		
            		else
            			PeakDataThreadCopy.queue.put(sentence+"="+receivePacket.getAddress().getHostAddress());
					//System.out.println("DATA INSERTED IN QUEUE");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	  //executor.execute(new PeakDataThread(dbserv));
            	  /*try {
            		  PeakDetectionInfo peakData = null;
            		  boolean isPTZAntennaData = false;
            		  List<PeakDetectionInfo> peakDataList = new ArrayList<PeakDetectionInfo>();
            		  String[] sentenceNEW = sentence.split("#");
            		  String[] peakPackets = sentenceNEW[0].split(";");
            		  for(int index=1; index < peakPackets.length-1; index++)
            		  {
            			  String[] peakInfoValues = peakPackets[index].split(",");
            			  
            			  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            			  LocalDate localDate = LocalDate.now();
            			  //PENDING--> Need ot handle index out of bounds exception in case packet data is not received in proper structure. We shall ignore that packet.
            			  if(peakInfoValues.length < 5)
            			  {
            				  peakData = dbserv.recordPeakInfo(receivePacket.getAddress().getHostAddress(), peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3],null);
            				  isPTZAntennaData=true;
            			  }else {
            				  peakData = dbserv.recordPeakInfo(receivePacket.getAddress().getHostAddress(), peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3], peakInfoValues[4]);
            			  }
            			  
            			  peakDataList.add(peakData);
            		  }
            		  if(isPTZAntennaData)
            		  {
            			  if(DBDataService.rotatePTZ)
            			  {
            				  dbserv.sendPeakDataToSocket(peakDataList);
            			  }
            		  }else {
            			  dbserv.sendPeakDataToSocket(peakDataList);
            		  }
            	  }catch (Exception e) {
            		  e.printStackTrace();
            		  // TODO: handle exception
            	  }*/
              }else if(sentence.contains("LEDON")){
                	//String[] ledInfoValue = sentence.split("\\\\");
              	  //dbserv.recordLEDInfo(receivePacket.getAddress().getHostAddress(), ledInfoValue[1].replaceAll("\\n", ""));
                	dbserv.recordLEDInfo(receivePacket.getAddress().getHostAddress());
                	//dbserv.sendLEDDataToSocket(receivePacket.getAddress().getHostAddress());

                }else {
                	//JUNK PACKET. DROP IT
                }
             
              
             /* InetAddress IPAddress = receivePacket.getAddress();
              int port = receivePacket.getPort();
              String capitalizedSentence = sentence.toUpperCase();
              sendData = capitalizedSentence.getBytes();
              DatagramPacket sendPacket =
              new DatagramPacket(sendData, sendData.length, IPAddress, port);
              serverSocket.send(sendPacket);*/
              receiveData = null;
              sentence = null;
        }
    }
    
   /* public static void main(String[] args) {
		PacketListener pl;
		try {
			pl = new PacketListener(2000);
			pl.service();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
