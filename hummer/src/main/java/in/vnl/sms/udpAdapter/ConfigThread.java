package in.vnl.sms.udpAdapter;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.converter.SmartMessageConverter;

import in.vnl.sms.model.AuditLog;
import in.vnl.sms.model.ConfigData;
import in.vnl.sms.service.DBDataService;



@PropertySource("classpath:application.properties")
public class ConfigThread implements Runnable {

	@Value("${retries}")
	int retries;
	@Value("${ptz_room}")
	String ptz_room;
	@Value("${maxSamples}")
	int maxSamples;
	
	int retry_interval;
	String destinationIP;
	DatagramSocket dsocket;
	DBDataService dbserv2;
	List<ConfigData> cData;
	String msg;
	int countData=1;
	boolean startCommand = true;
	int df=0;
	Logger logger = LoggerFactory.getLogger(ConfigThread.class);
	
	public ConfigThread(String destinationIP, DatagramSocket serverSocket, DBDataService dbserv, int retry_interval, int retries, int maxSamples) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		this.dbserv2 = dbserv;
		this.retry_interval = retry_interval;
		this.retries=retries;
		this.maxSamples = maxSamples;
	}
	
	public ConfigThread(String destinationIP, DatagramSocket serverSocket, List<ConfigData> cdata, DBDataService dbserv, int retry_interval, int retries, int maxSamples) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		this.dbserv2 = dbserv;
		this.cData = cdata;
		this.retry_interval = retry_interval;
		this.retries=retries;
		this.maxSamples = maxSamples;
	}
	
	public ConfigThread(String destinationIP, DatagramSocket serverSocket, boolean startCommand, String msg, int retry_interval, int retries) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		this.startCommand = startCommand;
		this.msg = msg;
		this.retry_interval = retry_interval;
		this.retries=retries;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	      // INPUT OF HOST FROM UI OR SMS HB
		InetAddress address;
		
		try {
		
		address = InetAddress.getLocalHost();
		//DatagramSocket dsocket = new DatagramSocket(2000,address);
		
		for (int retry=0; retry < retries; retry++)
		{
			try {
				if(startCommand) {
					System.out.println("ConfigThread: Sending Config Data");
					logger.info("ConfigThread: Sending Config Data");
					if(cData==null)
					{
						System.out.println("ConfigThread: CData is null........................");
						logger.info("ConfigThread: CData is null........................");
						
						if(!DBDataService.systemType.equals("standalone")) {						//changed here DBDataService.systemType.equals("integrated")
							cData = dbserv2.getConfigData(destinationIP);
							PeakDataThreadCopy.queue.clear();
						}
						else {
							cData = new ArrayList<ConfigData>();
							
							ConfigData c = dbserv2.getConfigDataByRoom("1");
							
							c.setPreamp_type(dbserv2.getStruAntennaIdPort());
							c.setRoom(String.valueOf(c.getPreamp_type().charAt(0)));
							/*c.setRoom("4");
							c.setPreamp_type("41");*/
							
							cData.add(c);
							
							PtzDataThread.queue.clear();
						}
					}
					
				
					//Send ConfigTrigger UDP packet
					Iterator<ConfigData> iterator = cData.iterator();
					countData=1;
					DBDataService.getbandconfig();
					while(iterator.hasNext()) {
						
						
						ConfigData cdata = iterator.next();
						logger.info("Sending Config Data for "+cdata.getRoom());
						
						if(DBDataService.maximization) {
							if (cdata.getRoom().equalsIgnoreCase("4"))
							{
								df=1;
							 DBDataService.maximization=false;
							}
						}
						
						if(cdata.getRoom().equals("2")) {					//also check in case of standalone
							DBDataService.smmStartFreq = Integer.parseInt(cdata.getStart_freq());
							DBDataService.smmStopFreq = Integer.parseInt(cdata.getStop_freq());
							//logger.error("max samples: "+maxSamples+" "+ptz_room+" "+retries);
							DBDataService.rbwBandwidth = DBDataService.calculateRbwBandwidth(DBDataService.smmStopFreq-DBDataService.smmStartFreq);
						}
						String bsf=null;
//						if(DBDataService.bandFilter) {
//							bsf ="0";
//						}else
//						{
//							bsf=cdata.getBand_specific();
//						}
						DBDataService.checkBSF=cdata.getBand_specific();
						DBDataService.aplliedband=cdata.getBand_filter();
						String cableLength = cdata.getCable_length()+DBDataService.rffeCableLength;
						
						byte[] message = ("floor:"+cdata.getFloor()+"\n" + 
								"room:"+cdata.getRoom()+"\n" + 
								"calibration:"+cdata.getCalibration()+"\n" +
								"calib_timer:"+cdata.getCalib_timer()+"\n" +
								"start_freq:"+cdata.getStart_freq()+"\n" + 
								"stop_freq:"+cdata.getStop_freq()+"\n" + 
								"threshold:"+cdata.getThreshold()+"\n" + 
								"mask_offset:"+cdata.getMask_offset()+"\n" + 
								"use_mask:"+cdata.getUse_mask()+"\n" + 
								"start_time:"+cdata.getStart_time()+"\n" + 
								"stop_time:"+cdata.getStop_time()+"\n" + 
								"cable_length:"+cableLength+"\n" +						//made changes to cable length removed usage of cdata.getCable_length() 
								"preamp_type:"+cdata.getPreamp_type()+"\n" +
								"distance:"+cdata.getDistance()+"\n" +
								"tx_power:"+cdata.getTx_power()+"\n" +
								"fade_margin:"+cdata.getFade_margin()+"\n" +
								"gsm_dl:"+cdata.getGsm_dl()+"\n" + 
								"wcdma_dl:"+cdata.getWcdma_dl()+"\n" + 
								"wifi_band:"+cdata.getWifi_band()+"\n" + 
								"lte_dl:"+cdata.getLte_dl()+"\n" + 
								"band_start1:"+cdata.getBand_start1()+"\n" + 
								"band_stop1:"+cdata.getBand_stop1()+"\n" + 
								"band_en1:"+cdata.getBand_en1()+"\n" + 
								"band_start2:"+cdata.getBand_start2()+"\n" + 
								"band_stop2:"+cdata.getBand_stop2()+"\n" + 
								"band_en2:"+cdata.getBand_en2()+"\n" + 
								"band_start3:"+cdata.getBand_start3()+"\n" + 
								"band_stop3:"+cdata.getBand_stop3()+"\n" + 
								"band_en3:"+cdata.getBand_en3()+"\n" + 
								"band_start4:"+cdata.getBand_start4()+"\n" + 
								"band_stop4:"+cdata.getBand_stop4()+"\n" + 
								"band_en4:"+cdata.getBand_en4()+"\n" + 
								"band_start5:"+cdata.getBand_start5()+"\n" + 
								"band_stop5:"+cdata.getBand_stop5()+"\n" +
								"band_en5:"+cdata.getBand_en5()+"\r\n" + 
								"band_en5:"+cdata.getBand_en5()+"\n" +
								"bsf:"+cdata.getBand_filter()+"\n" +
								"country_code:"+cdata.getCountry_code()+"\n" +
								"df:"+df+"\n" +
								"system_type:"+DBDataService.systemType_ID+"\n" + 
								"band_specific_filter:"+cdata.getBand_specific()+"\n" + 
								//"opr_mode:"+DBDataService.Opr_mode+"\n" + 
								"refLevel:"+cdata.getRef_level()+"\n" +
								"").getBytes();
						
						
						/*byte[] message = (".floor:"+cdata.getFloor()+"." + 
								"room:"+cdata.getRoom()+"." + 
								"calibration:"+cdata.getCalibration()+"." + 
								"start_freq:"+cdata.getStart_freq()+"." + 
								"stop_freq:"+cdata.getStop_freq()+"." + 
								"threshold:"+cdata.getThreshold()+"." + 
								"mask_offset:"+cdata.getMask_offset()+"." + 
								"use_mask:"+cdata.getUse_mask()+"." + 
								"start_time:"+cdata.getStart_time()+"." + 
								"stop_time:"+cdata.getStop_time()+". " + 
								"cable_length:"+cdata.getCable_length()+"." + 
								"preamp_type:"+cdata.getPreamp_type()+"." + 
								"gsm_dl:"+cdata.getGsm_dl()+"." + 
								"wcdma_dl:"+cdata.getWcdma_dl()+"." + 
								"wifi_band:"+cdata.getWifi_band()+"." + 
								"lte_dl:"+cdata.getLte_dl()+"." + 
								"band_start1:"+cdata.getBand_start1()+"." + 
								"band_stop1:"+cdata.getBand_stop1()+"." + 
								"band_en1:"+cdata.getBand_en1()+"." + 
								"band_start2:"+cdata.getBand_start2()+"." + 
								"band_stop2:"+cdata.getBand_stop2()+"." + 
								"band_en2:"+cdata.getBand_en2()+"." + 
								"band_start3:"+cdata.getBand_start3()+"." + 
								"band_stop3:"+cdata.getBand_stop3()+"." + 
								"band_en3:"+cdata.getBand_en3()+"." + 
								"band_start4:"+cdata.getBand_start4()+"." + 
								"band_stop4:"+cdata.getBand_stop4()+"." + 
								"band_en4:"+cdata.getBand_en4()+"." + 
								"band_start5:"+cdata.getBand_start5()+"." + 
								"band_stop5:"+cdata.getBand_stop5()+"." +
								//"band_en5:"+cdata.getBand_en5()+"\r\n" + 
								"band_en5:"+cdata.getBand_en5()+ 
								"").getBytes();*/
		
					      InetAddress destinationAddress = InetAddress.getByName(destinationIP);
					      System.out.println(destinationIP);
					      
					     
					      // Initialize a datagram packet with data and address
					      DatagramPacket packet = new DatagramPacket(message, message.length,
					    		  destinationAddress, 130);
					      dsocket.send(packet);
					     
					      try {
					    	  if(countData==1) {
					    		  countData=2;
					    		  dbserv2.createAuditlog(DBDataService.systemConfiguration, cdata.toString());
						    	  	}
					    	  DBDataService.systemConfiguration="";
					    	 
					    	  
					      }
					      catch (Exception e) {
					    	
					      }
							/* commented by muneesh
							 * cdata.setCalibration("0"); if (!dbserv2.systemType.equals("standalone")) //
							 * systemType.equals("integrated") changed here dbserv2.setupData(cdata, false,
							 * cdata.getFloor(), cdata.getFloor()+cdata.getRoom());
							 * 
							 * else dbserv2.setupStandaloneData(cdata, true, cdata.getFloor(),
							 * cdata.getFloor()+cdata.getRoom());
							 */
					      /*AuditLog auditLog = new AuditLog();
					      auditLog.setLogType("Configuration Applied");
					      auditLog.setDescription(cdata.toString());*/
					}
					//DBDataService.bandFilter=false;
				    Thread.sleep(retry_interval);
				}
				else {
					System.out.println("ConfigThread: Sending Stop Config Data");
					logger.info("ConfigThread: Sending Stop Config Data");
					
					
					
					byte[] message = msg.getBytes();
					
					InetAddress destinationAddress = InetAddress.getByName(destinationIP);
				    System.out.println(destinationIP);
				    // Initialize a datagram packet with data and address
				    DatagramPacket packet = new DatagramPacket(message, message.length,
				    		  destinationAddress, 130);
				    dsocket.send(packet);
				   // dbserv2.createAuditlog("Stop Configuration Applied", msg);
				}
			      
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				//Ack received
				logger.error("Ack Recieved for Config Setting");
				break;
				//e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error(e.toString());
				e.printStackTrace();
			}
		}
		}catch (UnknownHostException e1) {
			// TODO: handle exception
			logger.error(e1.toString());
		}	}

}
