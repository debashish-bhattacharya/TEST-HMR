package in.vnl.sms.udpAdapter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import in.vnl.sms.service.DBDataService;

@Component
@PropertySource("classpath:application.properties")
public class PTZUdpListener implements Runnable {
	
	/*@Value("${ptz_udp_port}")
	int ptz_udp_port;*/
	
	int ptAngleRec;
	
	DBDataService dbserv;
	DatagramSocket serverSocket1;
	byte[] receiveData;
	byte[] sendData;
	
	Logger logger = LoggerFactory.getLogger(PTZUdpListener.class);

	public PTZUdpListener(DBDataService dbserv) throws SocketException {
		//serverSocket1 = new DatagramSocket(140);
	    receiveData = new byte[36];
	    this.dbserv=dbserv;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			serverSocket1 = new DatagramSocket(1000);
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while(true)
        {
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            try {
            	
            	//System.out.println("PTZUdpListener: Port: " + serverSocket1.getLocalPort());
				serverSocket1.receive(receivePacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            String sentence = new String( receivePacket.getData());
            //System.out.println("PacketListener: RECEIVED: " + sentence);
            //logger.info("UDP PacketListener: RECEIVED: " + sentence);

            String command = String.format("%02X ", receiveData[1]).trim()+String.format("%02X ", receiveData[0]).trim();
			Long cmdId = Long.parseLong(command, 16);
			
			//logger.debug("UDP PacketListener: RECEIVED: " + sentence +"command : "+cmdId);
			
	        if(cmdId == 41) {
	        	
	        	command = String.format("%02X ", receiveData[6]).trim();
				Long enable = Long.parseLong(command, 16);
				
	        	command = String.format("%02X ", receiveData[7]).trim();
				Long valid = Long.parseLong(command, 16);
				
				/*if(valid==1) {
	        	
		        	String value = String.format("%02X ", receiveData[11]).trim()+String.format("%02X ", receiveData[10]).trim()+String.format("%02X ", receiveData[9]).trim()+String.format("%02X ", receiveData[8]).trim();
					Long i = Long.parseLong(value, 16);
			        float pan = Float.intBitsToFloat(i.intValue());
			        System.out.println("PTZPacketSender: Pan : "+ pan);
			        
			        value = String.format("%02X ", receiveData[27]).trim()+String.format("%02X ", receiveData[26]).trim()+String.format("%02X ", receiveData[25]).trim()+String.format("%02X ", receiveData[24]).trim();
					i = Long.parseLong(value, 16);
					float northOffset = Float.intBitsToFloat(i.intValue());
			        System.out.println("PTZPacketSender: Yaw : "+ northOffset);
			        
			        dbserv.updateOffset(pan, northOffset);
			        
				}*/
			    
				if(enable == 1) {
		        	String value = String.format("%02X ", receiveData[31]).trim()+String.format("%02X ", receiveData[30]).trim()+String.format("%02X ", receiveData[29]).trim()+String.format("%02X ", receiveData[28]).trim();
					Long i = Long.parseLong(value, 16);
					float lat = Float.intBitsToFloat(i.intValue());
			        System.out.println("PTZUdpListener: Lat : "+ lat);
			        
			        value = String.format("%02X ", receiveData[35]).trim()+String.format("%02X ", receiveData[34]).trim()+String.format("%02X ", receiveData[33]).trim()+String.format("%02X ", receiveData[32]).trim();
					i = Long.parseLong(value, 16);
					float lon = Float.intBitsToFloat(i.intValue());
			        System.out.println("PTZUdpListener: Lon : "+ lon);
			        
			        String latitude = String.valueOf(lat);
			        String longitude = String.valueOf(lon);
			        
			        //to check for correct latitude and longitude
					if(latitude.charAt(0)!='0' || longitude.charAt(0)!='0') {
				        dbserv.updateGPSData(receivePacket.getAddress().getHostAddress(), latitude, longitude, DBDataService.gpsAccuracy);
					}
				}
				
	        }
	        else if(cmdId == 34) {
	        	
	        	String angleInHex = String.format("%02X ", receiveData[5]).trim()+String.format("%02X ", receiveData[4]).trim()+String.format("%02X ", receiveData[3]).trim()+String.format("%02X ", receiveData[2]).trim();
				Long i = Long.parseLong(angleInHex, 16);
		        Float angle = Float.intBitsToFloat(i.intValue());
		        //System.out.println("Angle rec : "+ angle);
		        
		        String angleTiltInHex = String.format("%02X ", receiveData[9]).trim()+String.format("%02X ", receiveData[8]).trim()+String.format("%02X ", receiveData[7]).trim()+String.format("%02X ", receiveData[6]).trim();
				Long j = Long.parseLong(angleTiltInHex, 16);
		        Float angleTilt = Float.intBitsToFloat(j.intValue());
		        //System.out.println("Tilt rec : "+ angleTilt);
		        
		        String epochTime1 = String.format("%02X ", receiveData[17]).trim()+String.format("%02X ", receiveData[16]).trim()+String.format("%02X ", receiveData[15]).trim()+String.format("%02X ", receiveData[14]).trim();
				Long epoch_Time = Long.parseLong(epochTime1, 16);
				//System.out.println("EPOCH TIME RECEIVED: " + epoch_Time);
				
				Date date = new Date(epoch_Time * 1000);
		        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		        format.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		        String formatted_pt_time = format.format(date);
		        String system_time = format.format(new Date(System.currentTimeMillis()));
		        
				ptAngleRec = Math.round(angle);
				if(ptAngleRec <0)
				{
					ptAngleRec = 0;
				}
				//System.out.println("PTZUdpListener: Angle received from PT: "+ptAngleRec);
				logger.debug("PTZData - "+"\tTime : "+epoch_Time+"\tAngle : "+ptAngleRec);
				
				//PTZPacketSender.current_angle = ptAngleRec;
				try {
					PtzDataThread.queue.put("PTZAngle="+ptAngleRec);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//PTZPacketSender.setCurrentAngle(ptAngleRec);
				
				dbserv.recordPTZPositionData(receivePacket.getAddress().getHostAddress(), ptAngleRec, Math.round(angleTilt), formatted_pt_time, system_time);
	        }
			
        }

	}

}
