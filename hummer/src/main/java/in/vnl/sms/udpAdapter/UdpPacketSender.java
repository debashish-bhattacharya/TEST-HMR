package in.vnl.sms.udpAdapter;

import java.io.IOException;
import java.math.BigInteger;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.vnl.sms.model.JMDeviceData;
import in.vnl.sms.service.DBDataService;

public class UdpPacketSender
{
	DBDataService dbserv;
	SimpleDateFormat sdf = new SimpleDateFormat("HHmmss.S");
	private static UdpPacketSender single_instance = null;
	
	public UdpPacketSender()//(DBDataService dbserv) 
	{
		super();
		//this.dbserv=dbserv;
	}
	
	public static UdpPacketSender getInstance()
    {
        if (single_instance == null)
            single_instance = new UdpPacketSender();

        return single_instance;
    }
	
	public String send(String ip,int port,String msg,int numberOfRetries) 
	{
		String response = null;
		for(int i=0;i<numberOfRetries;i++) 
		{
			response = send(ip,port,msg);
			
			if(response != null) 
			{
				
				if(response.equalsIgnoreCase("config")) 
				{
					try 
					{
						response = "";//send(ip,port,(new JSONObject(aa.getJSONObject(0).getString("config"))).getString("controller_conf"));
						
						if(response != null && response.equalsIgnoreCase("success")) 
						{
							send(ip,port,msg,numberOfRetries);
						}
						
					}
					catch(Exception e) 
					{
						System.out.println(e.getMessage());
					}
					
				} 				
				break;
			}
		}
		return response;
	}
	
	public String send(String ip, int port, String msg) 
	{
		String response = null;
		DatagramSocket clientSocket = null;
		try 
	    {
			
			clientSocket = new DatagramSocket(8870);
			
			/*String[] ipStr=ip.split("\\.");
			byte[] bb = new byte[4]; 
			bb[0]=(byte)Integer.parseInt(ipStr[0]);
			bb[1]=(byte)Integer.parseInt(ipStr[1]);
			bb[2]=(byte)Integer.parseInt(ipStr[2]);
			bb[3]=(byte)Integer.parseInt(ipStr[3]);*/
			  
			  
			InetAddress IPAddress = InetAddress.getByName(ip);
			
			byte[] receiveData = new byte[1024];
						  
			byte[] sendData = new BigInteger(msg, 2).toByteArray();
			
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
			
			clientSocket.send(sendPacket);
			clientSocket.setSoTimeout(15000);
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			  
			clientSocket.receive(receivePacket);
			
			StringBuilder sb = new StringBuilder();
			  
			System.out.println("@udp response recived : "+receivePacket.getData()[0]+"-"+receivePacket.getData()[1]);
			  
			response = ((byte)1 == receivePacket.getData()[1])?"success":"fail";
			  
			if(response.equalsIgnoreCase("fail")) 
			{
				response = ((byte)2 == receivePacket.getData()[1])?"config":"fail";
			}
			
		} 
	    catch (SocketException e) 
	    {
	    	e.printStackTrace();
	    	 //response = "fail";
	    } 
	    catch (UnknownHostException e) 
		{
			e.printStackTrace();
			//response = "fail";
	    } 
	    catch (IOException e) 
	    {
	    	e.printStackTrace();
	    	 //response = "fail";
	    }
		catch(Exception e) 
		{
			e.getMessage();
			e.printStackTrace();
		}
		finally 
		{
			clientSocket.close();
		}
		
		return response;
	}
	
	public JMDeviceData sendToJM(String ip, int port, int deviceId, int retry, int jmTimeout) 
	{
		String response = null;
		JMDeviceData data = null;
		DatagramSocket clientSocket = null;
		int retryCount = 0;
		while(retryCount<=retry) {
			try 
		    {
				clientSocket = new DatagramSocket();
				InetAddress IPAddress = InetAddress.getByName(ip);
				
				Date now = new Date();
				String date = sdf.format(now);//.replace(":", "");
				
				String msg = "$VNL,1,"+deviceId+","+date+"*";
				int checkSum = checksum(msg);
				msg = msg+checkSum+"#";
				
				byte[] receiveData = new byte[1024];
				
				byte[] sendData = msg.getBytes();
				
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
				  
				clientSocket.send(sendPacket);
				clientSocket.setSoTimeout(jmTimeout);
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				 
				clientSocket.receive(receivePacket);
				response = new String(receivePacket.getData());
				System.out.println(response);
				String[] arr = response.split(",");
				System.out.println(arr[6]);
				
				String c = arr[6].substring(arr[6].indexOf("*")+1, arr[6].indexOf("#")).trim();
				int checksum = Integer.parseInt(c);//arr[6].indexOf("#")));
				
				if(checksum(response)==checksum) {
				
					data = new JMDeviceData();
					
					data.setDeviceid(deviceId);
					data.setTime(now);//arr[6].substring(0,arr[6].indexOf("*")));
					data.setPan(String.format("%.2f", Double.parseDouble(arr[3])));
					data.setTilt(String.format("%.2f", Double.parseDouble(arr[4])));
					data.setRoll(String.format("%.2f", Double.parseDouble(arr[5])));
				}
				retryCount = retry + 1;
				
				System.out.println("@udp response recived : "+response);
				//dbserv.setJMData(response);
			}
		    catch(SocketException e) 
		    {
		    	e.printStackTrace();
		    	//response = "fail";
		    }
		    catch(UnknownHostException e) 
		    {
		    	e.printStackTrace();
				//response = "fail";
		    } 
		    catch(IOException e) 
		    {
		    	e.printStackTrace();
		    	 //response = "fail";
		    }
			catch(Exception e) 
			{ 
				e.getMessage();
				e.printStackTrace();
			}
			finally 
			{
				retryCount++;
				clientSocket.close();
			}
		}
		return data;
	}
	
	//==============-------BMS DATA ------------=========
	
	public String sendToBMS(String ip,int port,String cmd_btn) 
	{
		String response = null;
		DatagramSocket clientSocket = null;
		
		try 
	    {
			clientSocket = new DatagramSocket();
			
			
			InetAddress IPAddress = InetAddress.getByName(ip);
						
			byte[] receiveData = new byte[1024];
			
			//byte[] sendData = new BigInteger(cmd_btn, 2).toByteArray();
			byte[] sendData = cmd_btn.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress,port);
			 System.out.println("UDP DATA FROM BMS "+sendData);
			 clientSocket.send(sendPacket);
			 clientSocket.setSoTimeout(1000);
			
			byte[] buf = new byte[1024];
			
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			clientSocket.receive(packet);
			response = new String(packet.getData(), 0, packet.getLength());
			
			
			
			//clientSocket.setSoTimeout(15000);
			
			/*DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			 
			clientSocket.receive(receivePacket);
			response = new String(receivePacket.getData());*/
			  
			System.out.println("@udp response recived : "+response);
			//dbserv.setJMData(response);
		}
	    catch(SocketException e) 
	    {
	    	e.printStackTrace();
	    	//response = "fail";
	    }
	    catch(UnknownHostException e) 
	    {
	    	e.printStackTrace();
			//response = "fail";
	    } 
	    catch(IOException e) 
	    {
	    	e.printStackTrace();
	    	 //response = "fail";
	    }
		catch(Exception e) 
		{ 
			e.getMessage();
			e.printStackTrace();
		}
		finally 
		{
			clientSocket.close();
		}
		return response;
	}
	
	public int checksum(String s){
		int j=0;
		try{
			String s1 = s.substring(0,s.lastIndexOf("*"));
			for(int i=0;i<s1.length();i++){
				j = j+(int)s1.charAt(i);
			}
			return j;
		}catch(Exception e){
			return 0;
		}
	}
	
}
