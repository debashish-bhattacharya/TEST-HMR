package in.vnl.sms.udpAdapter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.service.DBDataService;

public class PtzDataThread implements Runnable{

	DBDataService dbserv;
	static BlockingQueue<String> queue;
	Logger logger = LoggerFactory.getLogger(PtzDataThread.class);
	int co=0;
	String destinationIP = "";
	
	public PtzDataThread()
	{
		
	}
	
	public PtzDataThread(DBDataService dbserv)
	{
		queue = new LinkedBlockingQueue<String>();
		this.dbserv = dbserv;
	}
	
	@Override
	public void run() {
		
		
		try {
			String path;
			String sentence;
			String value;
			TreeMap<Double, String[]> maxPeakTree = new TreeMap<Double, String[]>(new Comparator<Double>() {
                @Override
                public int compare(Double e1, Double e2) {
                    return e2.compareTo(e1);
                }
            });
			
			//PTZPacketSender ptzPacketSender = PTZPacketSender.getInstance();
			
			while(true)
			{
				List<PeakDetectionInfo> peakDataList = new ArrayList<PeakDetectionInfo>();
				
				TreeMap<Double, Integer> peakTree = new TreeMap<Double, Integer>(new Comparator<Double>() {
	                @Override
	                public int compare(Double e1, Double e2) {
	                    return e2.compareTo(e1);
	                }
	            });
				
				if((path = queue.take()) != null )
				{
					ArrayList<String> multipleEntries = new ArrayList<String>();
					multipleEntries.add(path);
					
					//if(queue.size() >10)
					//{
						queue.drainTo(multipleEntries);
					//}
					for(int i=0; i < multipleEntries.size(); i++)
					{
						
						String temp[] =  multipleEntries.get(i).split("=");
						sentence = temp[0].trim();
						value = temp[1].trim();
						
						if(sentence.contains("PTZAngle")) {
							
							int newAngle = Integer.parseInt(value);
							
							int previousAngle = PTZPacketSender.current_angle;//ptzPacketSender.getLastAngle();
							
							if(newAngle!=previousAngle) {
								if(maxPeakTree.size()>0)
								{
									LocalDate localDate = LocalDate.now();
									String[] peakInfo = maxPeakTree.firstEntry().getValue();
									dbserv.recordPtzPeakInfo(destinationIP, peakInfo[0], peakInfo[1], localDate+" "+peakInfo[2], peakInfo[3], null, true);
									maxPeakTree.clear();
									peakTree.clear();
								}
							}
							PTZPacketSender.setCurrentAngle(newAngle);
						}
						else {
							
							try {
								PeakDetectionInfo peakData = null;
								boolean isPTZAntennaData = false;
								String[] sentenceNEW = sentence.split("#");
								String[] peakPackets = sentenceNEW[0].split(";");
								LocalDate localDate = LocalDate.now();
								destinationIP = value;
								
								//TreeMap<Double, Integer> peakTree = new TreeMap<Double, Integer>();
								
								for(int index=1; index <= peakPackets.length-1; index++)
								{
									String[] peakInfoValues = peakPackets[index].split(",");
									if(!DBDataService.rotatePTZ && DBDataService.systemType.equals("standalone"))//peakInfoValues.length < 4) and changed here !DBDataService.systemType.equals("integrated")
									{											
										peakData = dbserv.recordPtzPeakInfo(destinationIP, peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3], null, false);//100+"",null);
										isPTZAntennaData=false;
										logger.debug("PeakDataThread - "+"\tTime : "+peakInfoValues[2]+"\tFreq : "+peakInfoValues[1]+"\tPower : "+peakInfoValues[0]);
										if(peakData!=null)
											peakDataList.add(peakData);
										
									}
									else if(DBDataService.rotatePTZ)
									{
										Double freq = Double.parseDouble(peakInfoValues[1]);
										if(DBDataService.currentEventName!=null && (freq>=DBDataService.startFreqForPTZEvent && freq<=DBDataService.stopFreqForPTZEvent)) {
											 
											dbserv.recordPtzPeakInfo(destinationIP, peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3], null, false);
											peakTree.put(Double.parseDouble(peakInfoValues[0]), index);
										}
										isPTZAntennaData=true;
										//logger.debug("PeakDataThread - "+"\tTime : "+peakInfoValues[2]+"\tFreq : "+peakInfoValues[1]+"\tPower : "+peakInfoValues[0]);
									}
									
									peakData=null;
								}
								
								if(isPTZAntennaData && peakTree.size()>0)// && changeAngle)
								{
									int index = peakTree.firstEntry().getValue();
									String[] peakInfo = peakPackets[index].split(",");
									//peakData = dbserv.recordPtzPeakInfo(destinationIP, peakInfo[0], peakInfo[1], localDate+" "+peakInfo[2], peakInfo[3], null);
									maxPeakTree.put(Double.parseDouble(peakInfo[0]), peakInfo);
									peakTree.clear();
									isPTZAntennaData = false;
									
								}else {
									if(peakDataList.size()>0) {				
										logger.debug("peakPTZ info"+peakDataList);
										dbserv.sendPeakDataToSocket(peakDataList);
										peakDataList.clear();
										co=0;
									}
								}
								
								//peakTree = null;
							}catch (Exception e) {
								logger.error(e.toString());
								e.printStackTrace();
								// TODO: handle exception
							}
						}
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			e.printStackTrace();
		}
	}
}
