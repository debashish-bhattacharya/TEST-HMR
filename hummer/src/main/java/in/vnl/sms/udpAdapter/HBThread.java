package in.vnl.sms.udpAdapter;

import java.net.DatagramSocket;
import java.net.ServerSocket;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import in.vnl.sms.service.DBDataService;



@PropertySource("classpath:application.properties")
public class HBThread implements Runnable {
	
	int heartbeat_interval;
	String destinationIP;
	String hbStartTime;
	DBDataService dbserv1;

	public HBThread() {
		// TODO Auto-generated constructor stub
	}
	
	/*public HBThread(String destinationIP, DatagramSocket serverSocket, DBDataService dbserv, String hbStartTime, int heartbeat_interval) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		dbserv1= dbserv;
		this.hbStartTime = hbStartTime;
		this.heartbeat_interval=heartbeat_interval;
	}
	
	public HBThread(String destinationIP, DatagramSocket serverSocket, DBDataService dbserv, int heartbeat_interval) {
		this.destinationIP = destinationIP;
		this.dsocket = serverSocket;
		dbserv1= dbserv;
		this.heartbeat_interval=heartbeat_interval;
	}*/
	
	public HBThread(String destinationIP, DBDataService dbserv, String hbStartTime, int heartbeat_interval) {
		this.destinationIP = destinationIP;
		dbserv1= dbserv;
		this.hbStartTime = hbStartTime;
		this.heartbeat_interval=heartbeat_interval;
	}
	
	public HBThread(String destinationIP, DBDataService dbserv, int heartbeat_interval) {
		this.destinationIP = destinationIP;
		dbserv1= dbserv;
		this.heartbeat_interval=heartbeat_interval;
	}
	
	public void setStartTime(String hbStartTime)
	{
		this.hbStartTime = hbStartTime;
	}
	  /*private static ThreadLocal<Boolean> serverActive= new ThreadLocal<Boolean>() {
		  @Override
		protected Boolean initialValue() {
			// TODO Auto-generated method stub
			return Boolean.FALSE;
		}
	  };	
	  
	  public static void set(boolean value)
	  {
		  serverActive.set(value);
	  }
	  
	  public static void get()
	  {
		  serverActive.get();
	  }*/
	  
	 // public boolean serverActive=false;

	  /*public void updateStatus()
	  {
		  serverActive=true;
	  }
	  */
	


	@Override
	public void run() {
		
		//Send SMS Trigger if previous state was false
		boolean previousActiveState=false;
		PacketListener.existingState.put(Thread.currentThread().getName(), true);
		
		System.out.println("HBThread: Start Thread, HB received for 1st time");
		//inform UI as status is active when the First HB message is received.
		if(hbStartTime==null)
		{
			dbserv1.updateActiveWithoutTime("true", destinationIP,"DOWN");
		}else {
			dbserv1.updateActiveStatusWithState("true", destinationIP,hbStartTime,"DOWN");
		}
		boolean serverActive=true;
		boolean isFirstHB = true;
		
		  // TODO Auto-generated method stub
		  while(true)
		  {
			  try {
				  
				  if(isFirstHB)
				  {
					  System.out.println("HBThread: FIRST HB");
					  if(hbStartTime==null)
						{
							dbserv1.updateActiveWithoutTime("true", destinationIP,"DOWN");
						}else {
							dbserv1.updateActiveStatusWithState("true", destinationIP,hbStartTime,"DOWN");
						}
					  PacketListener.existingState.put(Thread.currentThread().getName(), true);
					  isFirstHB=false;
					  Thread.sleep(heartbeat_interval);
					  serverActive=false;
					  PacketListener.existingState.put(Thread.currentThread().getName(), false);
					  if(hbStartTime==null)
						{
							dbserv1.updateActiveWithoutTime("true", destinationIP,"DOWN");
						}else {
							dbserv1.updateActiveStatusWithState("true", destinationIP,hbStartTime,"DOWN");
						}
				  }
				  
				  if(serverActive)
				  {
					  //inform UI as status is active
					  
					  Thread.sleep(heartbeat_interval);
					  //dbserv1.updateActiveStatusOnly("true",destinationIP,hbStartTime);
					  serverActive=false;
					  //System.out.println("HBThread: HB interval "+heartbeat_interval );
					  //System.out.println("HBThread: HeartBeat Received.");
					  //Thread.sleep(heartbeat_interval);
					  System.out.println("HEARTBEAT NOT RECEIVED");
					  PacketListener.existingState.put(Thread.currentThread().getName(), false);
					  if(hbStartTime==null)
						{
							dbserv1.updateActiveWithoutTime("true", destinationIP,"DOWN");
						}else {
							dbserv1.updateActiveStatusWithState("true", destinationIP,hbStartTime,"DOWN");
						}
				  }else {
					  Thread.sleep(heartbeat_interval);
					  System.out.println("HBThread: Heartbeat not received");
					  PacketListener.existingState.put(Thread.currentThread().getName(), false);
					  if(hbStartTime==null)
						{
							dbserv1.updateActiveWithoutTime("false", destinationIP,"DOWN");
						}else {
							dbserv1.updateActiveStatusWithState("false", destinationIP,hbStartTime,"DOWN");
						}
					  //inform UI as status is down.
					  Thread.sleep(60000); // Random wait if Status gets down.
					  
					  //inform Central CMS about connectivity on basis of flag in property file to find whether it's a Central CMS or Local CMS

				  }
			  } catch (InterruptedException e) {
				  // TODO Auto-generated catch block	
				  //HB received. reset the timer. Wake the thread from sleep and restart the sleep counter
				  if(!serverActive)
				  {
					  System.out.println("HBThread: HeartBeat Received.");
					  dbserv1.updateActiveStatusOnly("true",destinationIP,hbStartTime);
					  serverActive=true;
				  }
				  System.out.println("HBThread: Interrupt: HeartBeat received");
				  PacketListener.existingState.put(Thread.currentThread().getName(), true);
				  // e.printStackTrace();
			  }

		  }
	  }

}
