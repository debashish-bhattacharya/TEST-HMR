package in.vnl.sms.udpAdapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.vnl.sms.service.DBDataService;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

@Component
public class JMCompass implements Runnable{

	public DBDataService dbserv;
	String timer, serialPort;
	String port = "5142";
	ArrayList<String> comPort;
	int flag = 0;
	//static JDialog jd;
	static SerialPort serialPortObj;
	static BlockingQueue<String> queue;
	
	public JMCompass(DBDataService dbserv) {
		queue = new LinkedBlockingQueue<String>();
		this.dbserv=dbserv;
		//jd = new JDialog();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub

		boolean portAvailable = false;
		while(!portAvailable)
		{
			try {
				Thread.sleep(60000);
				System.out.println("Retry to connect to JM after 1 min ");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			portAvailable = connect();
			//Wait for JM Device to connect
		}
			startCapture();
	
	}
	
	
	public boolean connect()
	{
		String j = null;
		if(getComPort()){
			for(String val: comPort)
			{
				if(val.contains("Sili")) {
					j=val;
				}
			}
    		/*String j = (String)JOptionPane.showInputDialog(jd, "Select Serial ComPort :","Select Port", 
    			JOptionPane.PLAIN_MESSAGE, null,comPort.toArray(),comPort.get(0));*/
    		if(flag == 1 && j!=null)
    			serialPort = j.substring(j.indexOf("(")+1, j.indexOf(")"));
    		
    		return true;
    	}
    	else{
    		System.out.println("JMCompass : No connection to Serial COM Port ");
    		return false;
    		//JOptionPane.showMessageDialog(jd, "Sorry no Serial ComPort available to connect", "Error", JOptionPane.ERROR_MESSAGE);
    	}
	}
	
	
	public boolean getComPort(){
		String[] portNames = SerialPortList.getPortNames();
		comPort = new ArrayList<String>();
		boolean temp = false;
		flag = 0;
		if(portNames.length != 0){
			try{
				Process p = Runtime.getRuntime().exec("wmic path Win32_SerialPort get name");
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
				String line = "";
				while ((line = reader.readLine())!= null) {
					if(line.contains("COM")){
						comPort.add(line);
						temp = true;
						if(line.contains("Sili"))
							flag = 1;
					}
				}
				reader.close();
			}catch(Exception e){
				//System.out.println(e);
				//log.error(e);
			}
			return temp;
		}
		else{
			return false;
		}
		
	}
	
	public void startCapture()
	{
		if(serialPort!=null) {
			serialPortObj = new SerialPort(serialPort);
			
			System.out.println("JMCompass : Start JM Capture");
			
			try {
				serialPortObj.openPort();
				serialPortObj.setParams(921600, 8, 1, 0);
				serialPortObj.addEventListener(new SerialPortReader());
		        Thread.sleep(10);
	        }
			catch (InterruptedException ex) {
				ex.printStackTrace();
	        	/*JOptionPane.showMessageDialog(StartScreen.jd, "InterruptedException", "Error", JOptionPane.ERROR_MESSAGE);
	        	return false;*/
	        }
	        catch (SerialPortException e) {
	        	e.printStackTrace();
	        	/*JOptionPane.showMessageDialog(StartScreen.jd, "Please check if device is connected or not", "Error", JOptionPane.ERROR_MESSAGE);
	        	return false;*/
	        }
			
			try {

				String path,angleVal;
				String[] packetVal;
				double angleNum, timeNum;
				List<String> temp;
				Iterator<String> iter;
				boolean DR_Flag = false;
				
				while(true){
					if((path = queue.take()) != null )
					{
						temp = Arrays.asList(path.split("(?=\\$)"));
						iter = temp.iterator();

						while(iter.hasNext()){
							path = iter.next().trim();

							/*if(path.contains("HD")&& path.contains(",F*") && checksum(path)){
								packetVal = path.split(",");
								angleVal = path.split(",")[4];
								
//								DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
								LocalDate localDate = LocalDate.now();
								
								String dateTime = localDate + " " +packetVal[3].substring(0,2)+":"+packetVal[3].substring(2,4)+":"+packetVal[3].substring(4,6);
								//timeNum = Double.parseDouble(timeVal);
								angleNum = Double.parseDouble(angleVal);
								
								System.out.println("JMCompass : Local Time "+ dateTime +" with received JM Angle : " + angleNum);
								dbserv.recordJMData(Inet4Address.getLocalHost().getHostAddress(), dateTime, Double.toString(angleNum));
							}
							
							else*/ if(path.contains("GNRMC")){
								if(path.contains("V")){
									DR_Flag = true;
								}
								else if(checksumNmea(path)){
									packetVal = path.split(",");
									double lat = latLon(packetVal[3], packetVal[4]);
									double lon = latLon(packetVal[5], packetVal[6]);
									
									LocalDateTime now = LocalDateTime.now();
									DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
									String formatDateTime = now.format(format);
									 
									System.out.println("JMCompass : Local Time" + formatDateTime +" witn NMEA Lat value "+ lat +" and lon value : " + lon);
									DR_Flag = false;
									
									dbserv.recordJMGPSData(Inet4Address.getLocalHost().getHostAddress(), formatDateTime, lat, lon);
								}
							}
							
							else if(DR_Flag && path.contains("DR")){
								if(path.contains(",F*") && checksum(path)){
									packetVal = path.split(",");
									double lat = Double.parseDouble(packetVal[4]);
									double lon = Double.parseDouble(packetVal[5]);
									
									LocalDate localDate = LocalDate.now();
									String dateTime = localDate + " " +packetVal[3].substring(0,2)+":"+packetVal[3].substring(2,4)+":"+packetVal[3].substring(4,6);
									System.out.println("JMCompass : Local Time "+ dateTime +" with JM Lat value "+ lat +" and lon value : " + lon);
									dbserv.recordJMGPSData(Inet4Address.getLocalHost().getHostAddress(), dateTime, lat, lon);
								}
							}
							/*else if(path.contains("TI") && path.contains(",F*") && checksum(path)){
								packetVal = path.split(",");
								String jmPacketDateTime = "20"+packetVal[5].substring(0,2)+"-"+packetVal[5].substring(2,4)+"-"+packetVal[5].substring(4,6) + " " + packetVal[3].substring(0,2)+":"+packetVal[3].substring(2,4)+":"+packetVal[3].substring(4,6);
								
								LocalDateTime now = LocalDateTime.now();
								DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
								String formatDateTime = now.format(format);
								
								dbserv.recordJMDateData(Inet4Address.getLocalHost().getHostAddress(), formatDateTime, jmPacketDateTime);
							}*/

						}
					}
				}

			}catch (Exception e) {
				System.out.println(e);
				//log.error(e);
			}finally {

				try{
				    serialPortObj.closePort();
					queue.clear();
				}catch(Exception e){
					System.out.println(e);
					//log.error(e);
				}
				
			}
			
		}
	}
	
	
	
	static class SerialPortReader implements SerialPortEventListener {
        public void serialEvent(SerialPortEvent event) {
            try {
                String buffer = serialPortObj.readString(event.getEventValue());
                //if(!buffer.contains("[H")){
                	queue.put(buffer.trim());
                	Thread.sleep(100);
                //}
            }
            catch (SerialPortException ex) {
                System.out.println(ex);
                //log.error(ex);
            } catch (InterruptedException e) {
				System.out.println(e);
				//log.error(e);
			}
        }
    }
	
	
	
	public boolean checksum(String s){
		int j=0;
		try{
			String s1 = s.substring(0,s.lastIndexOf(","));
			for(int i=0;i<s1.length();i++){
				j = j+(int)s1.charAt(i);
			}
			s = s.substring(s.lastIndexOf("*")+1);
			if(Integer.parseInt(s)==j)
				return true;
			else
				return false;
		}catch(Exception e){
			//log.error(e);
			return false;
		}
	}
	
	public boolean checksumNmea(String s){
		try{
			char check = 0;
			String s1 = s.substring(1,s.lastIndexOf("*"));
		    for (int c = 0; c < s1.length(); c++) {
		    	check = (char)(check ^ s1.charAt(c));
		    }
			s = s.substring(s.lastIndexOf("*")+1);
			if(s.equals(Integer.toHexString(check)))
				return true;
			else
				return false;
		}catch(Exception e){
			//log.error(e);
			return false;
		}
	}
	
	private double latLon(String val, String dir){
		int te = val.indexOf(".")-2;
		double lat = Double.parseDouble(val.substring(0,te))+Double.parseDouble(val.substring(te,val.length()))/60;
		if(dir.equals("S") || dir.equals("W"))
			lat = -1*lat;
		return ((int)(lat*1000000))/1000000.0;
	}

}
