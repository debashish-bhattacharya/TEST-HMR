package in.vnl.sms.controller;

import java.awt.PageAttributes.MediaType;

import javax.ws.rs.core.HttpHeaders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import in.vnl.sms.annotations.LoginRequired;
import in.vnl.sms.service.DBDataService;
import in.vnl.sms.udpAdapter.PacketListener;

@Controller
@PropertySource("classpath:application.properties")
public class ViewController {

	@Autowired
	private DBDataService dataServiceObj;
	
	@Value("${mapServerIp}")
	String mapServiceIp;
	@Value("${ptz_roll_offset}")
	int ptz_roll_offset;

	@Value("${blacklistAlarm}")
	boolean blacklistAlarm;
	@Value("${displayPtzData}")
	boolean ptzDisplayData;
	
	@RequestMapping(value="/login")
	
	public String getLoginPage() 
	{
		
		return "login";
	}
	
	@RequestMapping(value="/dashboard")
	@LoginRequired
	public String getdashboard(Model model) 
	{
		model.addAttribute("mapServerIp",mapServiceIp);
		model.addAttribute("ptzRollOffset", DBDataService.ptz_roll_offset);
		model.addAttribute("ptzOffset", DBDataService.ptzOffset);
		model.addAttribute("startTime", PacketListener.startTime);
		model.addAttribute("type", DBDataService.systemType);
		//model.addAttribute("opr_mode", DBDataService.Opr_mode);
		//model.addAttribute("blacklistAlarm", blacklistAlarm);
		//model.addAttribute("ptzDisplayData", ptzDisplayData);
		return "dashboard";
	}
	
	@RequestMapping(value="/operation")
	@LoginRequired
	public String getoperation(Model model) 
	{
		model.addAttribute("user",getPrincipal());
		model.addAttribute("startTime", PacketListener.startTime);
		model.addAttribute("type", DBDataService.systemType);
		return "operations";
	}
	
	
	@RequestMapping(value="/header")
	public String getheader(Model model) 
	{
		model.addAttribute("user",getPrincipal());
		return "header";
	}
	
	@RequestMapping(value="/sideadmintree")
	public String getsideadmintree(Model model) 
	{
		//model.addAttribute("mapServerIp",mapServiceIp);
		return "sideadmintree";
	}
	
	@RequestMapping(value="/logout")
	public String getlogout(Model model) 
	{
		dataServiceObj.createAuditlog("System Access : Logout\t", "User Id: "+getPrincipal());
		return "login";
	}
	
	@RequestMapping(value="/admin")
	@LoginRequired
	public String getadmin(Model model) 
	{
		model.addAttribute("user",getPrincipal());
		return "admin";
	}
	
	@RequestMapping(value="/fault")
	@LoginRequired
	public String getFault(Model model) 
	{
		return "alarm";
	}
	
	@RequestMapping(value="/faq")
	@LoginRequired
	public String getfaq(Model model) 
	{
		model.addAttribute("user",getPrincipal());
		return "faq";
	}
	
	private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}
