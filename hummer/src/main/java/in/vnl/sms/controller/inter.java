package in.vnl.sms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/view")
public class inter {
	@GetMapping("/view")
	public String index() {
		return "home";
	}

}
