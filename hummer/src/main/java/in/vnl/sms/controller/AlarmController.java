package in.vnl.sms.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import in.vnl.sms.service.DBDataService;

@RestController
@RequestMapping("/fault")
public class AlarmController {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@Autowired
	private DBDataService dbDataService;
    
	Logger logger = LoggerFactory.getLogger(AlarmController.class);
	
	@RequestMapping("/alarm")
	//@Produces(MediaType.APPLICATION_JSON)
	public Response getRequestForCurrentAlarmData() {
		
		String ip = env.getProperty("application.ip");
		String port = env.getProperty("application.port");
		System.out.println(ip+"......................."+port);
		StringBuilder data = new StringBuilder();
		try {
			URL url = new URL("http://"+ip+":"+port+"/fault/api/current");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				data.append(output);
			}
			System.out.println(data.toString()+"\n");
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();

		}
		System.out.println(".............."+data.toString()+".........................................");
		LinkedHashMap<String, String> rs = new LinkedHashMap<String, String>();
		rs.put("result", "success");
		rs.put("message", "ok workinng");
		rs.put("data", data.toString());
		return Response.status(201).entity(data.toString()).build();
	}
	
	@RequestMapping("/setAckTrue")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setAckTrue(@RequestParam("id") int id, @RequestParam("acknowledgement") boolean acknowledgement ) {

		String ip = env.getProperty("application.ip");
		String port = env.getProperty("application.port");
		String url = "http://"+ip+":"+port+"/fault/api/acknowledgement";
		//String ack = "{\"id\":"+id+",\"acknowledgement\":"+acknowledgement+"}";
		JSONObject ack =new JSONObject();
        
		try {
	        ack.put("id", id);
	        ack.put("acknowledgement", acknowledgement);
		}
		catch(JSONException e) {
			logger.error(e.toString());
		}
		
		Response rs = null;
		dbDataService.sendPostRequestToUrl(url, ack);//new ApiCommon().sendRequestToUrl(url, new LinkedHashMap<String,String>(), ack);
		
		return Response.status(201).entity(rs.toString()).build();

	}
	
	@RequestMapping("/clearCurrentAlarmById")
	@Produces(MediaType.APPLICATION_JSON)
	public String clearCurrentAlarmById(@RequestParam("id") int id, @RequestParam("severity") int severity) {
		String ip = env.getProperty("application.ip");
		String port = env.getProperty("application.port");
		String url = "http://"+ip+":"+port+"/fault/api/clear/"+id;
		Response rs = null; 
		dbDataService.sendPostRequestToUrl(url, null);

			String respStr = "{\"id\":\""+id+"\",\"severity\":"+severity+",\"status\":true}";
			messagingTemplate.convertAndSend("/topic/alarm", respStr);
			return "{\"msg\":\"success\"}";
		
		
	}
	
	
	@PostMapping("/newAlarm")
	public void displayCurrentAlarmData(HttpServletRequest request, @RequestBody String data) {
		System.out.println("................."+data);
		messagingTemplate.convertAndSend("/topic/alarm", data);
	}
		
}
