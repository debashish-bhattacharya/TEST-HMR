package in.vnl.sms.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.Commands;
import in.vnl.sms.repository.CommandsRepository;

@Service
public class CommandsService {

	
	@Autowired
	private CommandsRepository cr;
	
	public CommandsService() {
		super();
	}
	
	public Commands getCommand(Long id) 
	{
		return cr.findById(id).orElse(null);
	}
	
}
