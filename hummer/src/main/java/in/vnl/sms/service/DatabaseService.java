package in.vnl.sms.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DatabaseService {
	
	static String database_url;
	static String database_username;
	static String database_password;
	static String faultDatabase_url;
	@Value("${spring.datasource.url}")
	public void setDatabaseUrl(String url) {
		database_url = url;
    }
	@Value("${FaultDatabaseUrl}")
	public void setFaultDatabaseUrl(String url) {
		faultDatabase_url = url;
    }
	@Value("${spring.datasource.username}")
	public void setDatabaseUsername(String username) {
		database_username = username;
    }
	
	@Value("${spring.datasource.password}")
	public void setDatabasePassword(String password) {
		database_password = password;
    }
	
	private static Connection connection = null;
	private static Connection faultConnection = null;
	
    public static void dataBaseConnection() throws ClassNotFoundException, SQLException {
       
    	if(connection==null) {
    		synchronized (DatabaseService.class) 
    	      { 
    	        if(connection==null) 
    	        { 
    	        	Class.forName("org.postgresql.Driver"); 
    	            Connection con=DriverManager.getConnection(database_url, database_username, database_password); 
    	            
    	            connection = con;

    	        } 
    	        
    	      } 
        }
        //Statement statement = con.createStatement();
		
		//return statement;   
    }
    public static void falutdataBaseConnection() throws ClassNotFoundException, SQLException {
        
    	if(faultConnection==null) {
    		synchronized (DatabaseService.class) 
    	      { 
    	        if(faultConnection==null) 
    	        { 
    	        	Class.forName("org.postgresql.Driver"); 
    	            Connection con=DriverManager.getConnection(faultDatabase_url, database_username, database_password); 
    	            
    	            faultConnection = con;

    	        } 
    	        
    	      } 
        }
        //Statement statement = con.createStatement();
		
		//return statement;   
    }
   
    public void executeBatchInsertion(List<String> st) throws ClassNotFoundException, SQLException{
       
    	if(connection==null) {
    		dataBaseConnection();
    	}
			
	    Statement stm = connection.createStatement();
	    for(String query:st) {
	        stm.addBatch(query);
	    }
	    try {
	        stm.executeBatch();
	    }catch(SQLException e) {
	        throw e;
	    }finally {
	        stm.close();
	    }  
    }
    public JSONArray getDbData(String qurey) throws ClassNotFoundException, SQLException{
        
    	if(connection==null) {
    		dataBaseConnection();
    	}
    	Statement insertStmt = null;
        Statement selectStmt = null;	
	    Statement stm = connection.createStatement();
	    selectStmt = connection.createStatement();
        ResultSet rs = selectStmt.executeQuery(qurey);
        JSONArray arr = new JSONArray();
        while(rs.next())
        {
            //First Column
           // return  rs.getString("ref_level")+rs.getString("status");
            arr.put(rs.getString("value"));
        
        }
        
	    try {
	        stm.executeBatch();
	    }catch(SQLException e) {
	        throw e;
	    }finally {
	        stm.close();
	    }
	    return arr;
    }
 public JSONArray getBandDataList(String qurey) throws ClassNotFoundException, SQLException, JSONException{
        
    	if(connection==null) {
    		dataBaseConnection();
    	}
    	Statement insertStmt = null;
        Statement selectStmt = null;	
	    Statement stm = connection.createStatement();
	    selectStmt = connection.createStatement();
	    ResultSet rs = selectStmt.executeQuery(qurey);
        ResultSetMetaData rm = rs.getMetaData();
		int totalColumns = rm.getColumnCount();
		ArrayList<String> columns = new ArrayList<String>();
		
		for(int i=1;i<=totalColumns;i++ )
		{
			columns.add(rm.getColumnName(i));
		}
        JSONArray arr = new JSONArray();
        while(rs.next())
		{				
			
			
			
			JSONObject jb = new JSONObject();
			for(String cname:columns)
			{
				if(rs.getString(cname) == null)
				{
					jb.put(cname, "");
				}
				else
				{
					jb.put(cname, rs.getString(cname));
				}
				
			}
			arr.put(jb);
		}	
	    try {
	        stm.executeBatch();
	    }catch(SQLException e) {
	        throw e;
	    }finally {
	        stm.close();
	    }
	    return arr;
    }
 public JSONArray faultGetDbData(String qurey) throws ClassNotFoundException, SQLException, JSONException{
        
    	if(faultConnection==null) {
    		falutdataBaseConnection();
    	}
    	Statement insertStmt = null;
        Statement selectStmt = null;	
	    Statement stm = faultConnection.createStatement();
	    selectStmt = faultConnection.createStatement();
        ResultSet rs = selectStmt.executeQuery(qurey);
        ResultSetMetaData rm = rs.getMetaData();
		int totalColumns = rm.getColumnCount();
		ArrayList<String> columns = new ArrayList<String>();
		
		for(int i=1;i<=totalColumns;i++ )
		{
			columns.add(rm.getColumnName(i));
		}
        JSONArray arr = new JSONArray();
        while(rs.next())
		{				
			
			
			
			JSONObject jb = new JSONObject();
			for(String cname:columns)
			{
				if(rs.getString(cname) == null)
				{
					jb.put(cname, "");
				}
				else
				{
					jb.put(cname, rs.getString(cname));
				}
				
			}
			arr.put(jb);
		}	
	    try {
	        stm.executeBatch();
	    }catch(SQLException e) {
	        throw e;
	    }finally {
	        stm.close();
	    }
	    return arr;
    }
   
     public void executeBatchDeletion(List<String> st) throws ClassNotFoundException, SQLException{
   
    	 Statement stm = connection.createStatement();
		  for(String query:st) {
			  stm.addBatch(query);
		  }
		  try {
			  stm.executeBatch();
		  }catch(SQLException e) {
			  throw e;
			  
		  }finally {
			  stm.close();
		   
		  }  
	}
        
}
