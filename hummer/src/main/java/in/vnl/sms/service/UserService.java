package in.vnl.sms.service;

import java.util.List;

import in.vnl.sms.exceptions.validation.user.UsernameNotUniqueException;
import in.vnl.sms.model.UserPojo;
import in.vnl.sms.model.UserUpdatePojo;



public interface UserService  {
	public UserPojo create(UserPojo userPojo) throws UsernameNotUniqueException;
	public UserUpdatePojo update(UserUpdatePojo userPojo) throws UsernameNotUniqueException;
	public UserPojo delete(UserPojo userPojo);
	public List<UserPojo> displayUsers();
	public UserPojo getUserById(long id);
	
}
