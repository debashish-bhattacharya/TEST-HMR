package in.vnl.sms.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.Alarm;
import in.vnl.sms.model.BmsStatus;
import in.vnl.sms.repository.AlarmsRepository;



@Service
public class Bms {


	@Autowired
	private AlarmsRepository ar;
	
	@Autowired
	private BmsStatusService bmss;
	
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	public Bms() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public void handlePacket(String ip,String packet) 
	{
		String[] data = packet.replace("*", "").split(",");
		switch(Integer.parseInt(data[1])) 
		{
			case 1:
				
			break;
			
			case 2:
			break;
			
			case 3:
			break;
			
			case 4:
			break;
			
			case 5:
				saveStatus(ip,data);
				messagingTemplate.convertAndSend("/topic/bmsstatus", ip+","+packet.replace("*", ""));
			break;
			case 6:
				saveAlarm(ip,data);
				messagingTemplate.convertAndSend("/topic/alarm", ip+","+packet.replace("*", ""));
			break;
		}
	}
	
	public void saveAlarm(String ip,String[] data) 
	{
		Alarm am = new Alarm();
		//Date dd = new Date(Long.parseLong(data[10].replace("*", ""))*1000);
		am.setIp(ip);
		am.setStatus(1);
		am.setComponentType(data[3]);
		am.setComponentId(data[2]);
		am.setEventDesctiption(data[9]);
		am.setEventId(data[6]);
		am.setEventType(Integer.parseInt(data[7]));
		am.setGenerationTime(Long.parseLong(data[10].replace("*", ""))*1000);
		am.setManagedobjectType(Integer.parseInt(data[5]));
		am.setMangedojectId(data[4]);
		am.setSeverity(Integer.parseInt(data[8]));
		am.setLogtime(new Date());
		ar.save(am);
	}
	
	
	public void saveStatus(String ip,String[] data) 
	{
		BmsStatus bs = new BmsStatus();
		
		//Date dd = new Date(Long.parseLong(data[10]));
		
		bs.setIp(ip);
		bs.setLogtime(new Date());
		bs.setGenerationtime(Long.parseLong(data[2])*1000);
		bs.setBcv1(Integer.parseInt(data[3]));
		bs.setBcv2(Integer.parseInt(data[4]));
		bs.setBcv3(Integer.parseInt(data[5]));
		bs.setBcv4(Integer.parseInt(data[6]));
		bs.setBcv5(Integer.parseInt(data[7]));
		bs.setBcv6(Integer.parseInt(data[8]));
		bs.setBcv7(Integer.parseInt(data[9]));
		bs.setBcv8(Integer.parseInt(data[10]));
		bs.setBcv9(Integer.parseInt(data[11]));
		bs.setBcv10(Integer.parseInt(data[12]));
		bs.setBcv11(Integer.parseInt(data[13]));
		bs.setBcv12(Integer.parseInt(data[14]));
		bs.setBcv13(Integer.parseInt(data[15]));
		bs.setBcv14(Integer.parseInt(data[16]));
		bs.setBtv(Integer.parseInt(data[17]));
		bs.setTbc(Integer.parseInt(data[18]));
		bs.setSoc(Integer.parseInt(data[19]));
		bs.setBtemp(Integer.parseInt(data[20]));
		bs.setAlarmword(Integer.parseInt(data[21]));
		
		bmss.save(bs);
	}

}
