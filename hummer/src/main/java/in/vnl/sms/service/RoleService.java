package in.vnl.sms.service;

import java.util.List;

import in.vnl.sms.exceptions.role.RoleNameDoesNotExistException;
import in.vnl.sms.exceptions.validation.role.RoleNameNotUniqueException;
import in.vnl.sms.model.RolePojo;



public interface RoleService {

	public RolePojo create(RolePojo rolePojo) throws RoleNameNotUniqueException;
	public RolePojo delete(RolePojo rolePojo);
	public RolePojo update(RolePojo rolePojo);
	public List<RolePojo> displayAll();
	public RolePojo getRole(long id);
	public List<RolePojo> getAllRoles();
	public List<RolePojo> saveAllRoles(List<RolePojo> roles);
	public void checkRoleName(String roleName) throws RoleNameNotUniqueException;
	public RolePojo getByRoleName(String roleName) throws RoleNameDoesNotExistException;
	
}
