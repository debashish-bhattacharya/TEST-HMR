package in.vnl.sms.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.BmsConfig;
import in.vnl.sms.repository.BmsConfigRepository;



@Service
public class BmsConfigService 
{
	@Autowired
	private BmsConfigRepository br;
	
	
	public void save(String ip,String name,String tag,String value) 
	{
		br.save(new BmsConfig(null,ip,name,tag,value));
	}
	
	
	public HashMap<String,String> findBMSProperties(String ip)
	{
		List<BmsConfig>  prop = br.getBMSProperties(ip);
		
		HashMap<String,String> hs = new HashMap<String,String>();
		for(BmsConfig bc : prop) 
		{
			hs.put(bc.getName(), bc.getValue());
		}
		return hs;
	}
	
}
