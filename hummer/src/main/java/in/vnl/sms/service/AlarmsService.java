package in.vnl.sms.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.Alarm;
import in.vnl.sms.repository.AlarmsRepository;
import in.vnl.sms.repository.AlarmsDataRepository;
import in.vnl.sms.model.AlarmsData;


@Service
public class AlarmsService 
{
	@Autowired
	private AlarmsRepository ar;
	
	@Autowired
	private AlarmsDataRepository adr;
	
	public void saveAlarm(Alarm am) 
	{
		ar.save(am);
	}
	
	public int acknowledgeAlarm(Long alarmId) 
	{
		return ar.acknowledge(alarmId);
	}
	
	public int clearAlarm(Long alarmId) 
	{
		return ar.clear(alarmId);
	}
	
	
	public List<AlarmsData> getFaultData() 
	{
		return adr.findAll();
	}
}
