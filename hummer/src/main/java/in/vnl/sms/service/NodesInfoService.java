package in.vnl.sms.service;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.NodesInfo;
import in.vnl.sms.repository.NodesInfoRepository;



@Service
public class NodesInfoService 
{
	
	@Autowired
	private NodesInfoRepository nir;
	
	
	public ArrayList<NodesInfo> getNodesInfo()
	{
		ArrayList<NodesInfo> devices = new ArrayList<NodesInfo>();
		
		//Iterable<NodesInfo> itr = nir.findAll();
		
		Iterator<NodesInfo> it = nir.getNodes().iterator();//itr.iterator();
		while(it.hasNext()) 
		{
			devices.add(it.next());
		}
		
		return devices;
	}
	
	
	public  ArrayList<NodesInfo> findNode(String type)
	{
		return nir.findNodesByUseType(type);
	}
}
