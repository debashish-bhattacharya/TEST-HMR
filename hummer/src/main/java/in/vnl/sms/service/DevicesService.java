package in.vnl.sms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.Devices;
import in.vnl.sms.repository.DevicesRepository;

@Service
public class DevicesService {

	
	@Autowired
	private DevicesRepository dr;
	
	public DevicesService() {
		super();
	}
	
	
	public List<Devices> getDevices()
	{
		List<Devices> devices = dr.getDataByStatus(true);//new ArrayList<Devices>(); 
		
		/*Iterable<Devices> itr = dr.findAll();
		
		Iterator<Devices> it = itr.iterator();
		while(it.hasNext()) 
		{
			devices.add(it.next());
		}*/
		
		return devices;
	}
	
}
