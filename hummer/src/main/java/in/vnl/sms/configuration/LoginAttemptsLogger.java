package in.vnl.sms.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.listener.AuditApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import in.vnl.sms.service.DBDataService;

@Component
public class LoginAttemptsLogger {
 
	@Autowired
	private DBDataService dataServiceObj;
	
    @EventListener
    public void auditEventHappened(
      AuditApplicationEvent auditApplicationEvent) {
         
        AuditEvent auditEvent = auditApplicationEvent.getAuditEvent();
        System.out.println("Principal " + auditEvent.getPrincipal() 
          + " - " + auditEvent.getType());
        
        WebAuthenticationDetails details = 
          (WebAuthenticationDetails) auditEvent.getData().get("details");
        System.out.println("Remote IP address: "
          + details.getRemoteAddress());
        System.out.println("  Session Id: " + details.getSessionId());
        
        String ip = details.getRemoteAddress().contains("0:")?"127.0.0.1":details.getRemoteAddress();
        
        dataServiceObj.createAuditlog("System Access", "Access: Login\t"+"User Id: "+auditEvent.getPrincipal()
        								+"\tType: "+auditEvent.getType()+"\tClient IP: "+ip);
    }
}
