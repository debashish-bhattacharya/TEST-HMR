package in.vnl.sms.validation;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.vnl.sms.exceptions.validation.user.UsernameNotUniqueException;
import in.vnl.sms.model.BasePojo;
import in.vnl.sms.model.UserEntity;
import in.vnl.sms.model.UserPojo;
import in.vnl.sms.model.UserUpdatePojo;
import in.vnl.sms.repository.UserRepository;


@Component
public class UserValidation  {
	
	@Autowired
	private UserRepository userRepository;
	
	public void create(BasePojo basePojo) throws UsernameNotUniqueException {
		try {
			UserPojo userPojo=(UserPojo)basePojo;
			this.checkUsername(userPojo.getUsername());
		}
		catch (UsernameNotUniqueException exception) {
			throw exception;
		}
		
	}

	
	public void update(UserUpdatePojo baseEntity) {
		// TODO Auto-generated method stub
		
	}

	
	public void delete(BasePojo baseEntity) {
		// TODO Auto-generated method stub
		
	}
	
	private void checkUsername(String username) throws UsernameNotUniqueException {
		try {
			Optional<UserEntity> user=userRepository.findByUsername(username);
			if(user.isPresent()) {
				throw new UsernameNotUniqueException(username);
			}
		}
		catch(UsernameNotUniqueException exception) {
			throw exception;
		}
		catch (Exception exception) {
			// TODO: handle exception
		}
	}

	

}
