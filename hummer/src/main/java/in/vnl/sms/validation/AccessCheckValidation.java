package in.vnl.sms.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import in.vnl.sms.annotations.LoginRequired;




public class AccessCheckValidation implements ConstraintValidator<LoginRequired, String[]> {

	@Override
	public boolean isValid(String[] roles, ConstraintValidatorContext arg1)  {
		for(String role:roles) {
			if(role.equals("Admin")) {
				return true;
			}
		}
		return false;
		
	}

}
