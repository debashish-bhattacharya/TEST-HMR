package in.vnl.sms.authorization;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import in.vnl.sms.exceptions.auth.AuthenticationException;
import in.vnl.sms.exceptions.auth.AuthorizationException;
import in.vnl.sms.model.UserEntity;



@Component
public class CheckAuthorizationImpl implements CheckAuthorization {

	@Override
	public boolean checkAuthorization(String[] allowedRoles) throws AuthorizationException {
		try {
			UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Set<String> allowedRolesSet = new HashSet<>(Arrays.asList(allowedRoles));
			Set<String> roles = user.getRolesArray();
			Set<String> intersection = new HashSet<String>(allowedRolesSet); // use the copy constructor
			intersection.retainAll(roles);
			if (intersection.size() == 0) {
				throw new AuthorizationException();
			}

		} catch (AuthorizationException exception) {
			throw exception;
		} catch (Exception exception) {

		}
		return true;
	}

	@Override
	public void checkAuthentication() throws AuthenticationException {
		
		try {
			if(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) {
			
				throw new AuthenticationException(); 
			}
			
		
		}

		catch (NullPointerException exception) {
			throw new AuthenticationException();
		}

	}

}
