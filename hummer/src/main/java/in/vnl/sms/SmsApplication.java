package in.vnl.sms;

import java.io.IOException;

import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import in.vnl.sms.udpAdapter.PacketListener;


@SpringBootApplication
public class SmsApplication {

	public static void main(String[] args) {
		
		ConfigurableApplicationContext context = SpringApplication.run(SmsApplication.class, args);

        try {
        	
        	Runnable runnable = new Runnable(){
                public void run(){
                	try {
                		context.getBean(PacketListener.class).service();
                	}catch(IOException e) {
                		System.out.println(e);
                	}
                }
            };
            
            Thread mainThread = new Thread(runnable);
            mainThread.start();
            //context.getBean(PacketListener.class).service();
		
        } catch (BeansException e) {// | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
}
