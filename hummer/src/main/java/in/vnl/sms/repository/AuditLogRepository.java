package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import in.vnl.sms.model.AuditLog;

public interface AuditLogRepository extends CrudRepository<AuditLog,Long>
{
	
	//@Query(value = "SELECT * FROM EVENTS WHERE event_date >= ?1 and event_date <= ?1 order by event_date asc", nativeQuery = true)
	@Query("select al from AuditLog al ORDER BY al.id ASC")
	List<AuditLog> getEvents();
	
//	@Query(value = "select al from AuditLog al where al.logtime=:logtime order by al.id asc")
//	List<AuditLog> findEventsOnGivenDate(@Param("logtime") LocalDateTime logtime);
	
	@Query("select al from AuditLog al where al.createdDateTime between :startTime and :endTime ORDER BY al.id ASC")
    List<AuditLog> getAuditBetweenTime(@Param("startTime") String startTime,@Param("endTime") String endTime);
	
	@Query("select al from AuditLog al where al.logType=:logType and al.createdDateTime between :startTime and :endTime ORDER BY al.id DESC")
    List<AuditLog> getAuditBetweenTimeForType(@Param("startTime") String startTime,@Param("endTime") String endTime, @Param("logType") String logType);
	
	@Query("select al from AuditLog al where al.logType=:cueType  and al.createdDateTime between :startTime and :endTime ORDER BY al.id ASC")
    List<AuditLog> getReplayRecords(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("cueType") String cueType);

	@Modifying
    @Transactional
    @Query("delete from AuditLog al where al.createdDateTime between :startTime and :endTime")
    void deleteBetweenTime(@Param("startTime") String startTime, @Param("endTime") String endTime);
}
