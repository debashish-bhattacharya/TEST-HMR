package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.FreqPowMinThresData;

public interface FreqPowMinThresRepository extends CrudRepository<FreqPowMinThresData, Long>{

	@Query("select fpmt from FreqPowMinThresData fpmt ORDER BY fpmt.id ASC")
	List<FreqPowMinThresData> getMinThresholdData();

}