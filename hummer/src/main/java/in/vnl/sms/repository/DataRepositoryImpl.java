package in.vnl.sms.repository;

import java.util.Optional;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import in.vnl.sms.model.TriggerData;



@Repository
public class DataRepositoryImpl {

	@Autowired
	@Lazy
	private org.hibernate.SessionFactory sessionFactory;
	/*
	@Override
	public <S extends TriggerData> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends TriggerData> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<TriggerData> findById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<TriggerData> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<TriggerData> findAllById(Iterable<String> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(TriggerData entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll(Iterable<? extends TriggerData> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub

	}
*/
/*	@Override
	public TriggerData insertConfigData(String ipAdd, String reachabilityStatus) {
		// TODO Auto-generated method stub
		return null;
	}*/



	public TriggerData getTriggerData(String ipAdd) {
		try {
			Session session = sessionFactory.getCurrentSession();

			Query query = session.createQuery("from SMSTrigger where ipAdd=:ipAdd");

			query.setParameter("ipAdd", ipAdd);
			TriggerData trdata = (TriggerData) query.getSingleResult();
			return trdata;
		}

		catch (Exception exception) {
			throw exception;
		}

}

}
