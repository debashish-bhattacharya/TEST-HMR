package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


import in.vnl.sms.model.BMSData;



public interface BMSRepository extends CrudRepository<BMSData, Integer> 
{
	@Query("select bms from BMSData bms ORDER BY bms.id ASC")
	List<BMSData> getBMSDataList();
}