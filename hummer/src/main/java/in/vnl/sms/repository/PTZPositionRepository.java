package in.vnl.sms.repository;

import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.PTZPositionData;

public interface PTZPositionRepository extends CrudRepository<PTZPositionData, Long> {

}
