package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.Devices;

public interface DevicesRepository extends CrudRepository<Devices, Long>
{
	@Query("select de from Devices de where de.status=:status")
	List<Devices> getDataByStatus(@Param("status") boolean status);
}
