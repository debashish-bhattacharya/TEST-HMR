package in.vnl.sms.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.LEDData;



public interface LEDRepository extends CrudRepository<LEDData, Long> {

	@Query("select l from LEDData l where l.device_id=:device_id")
	LEDData getLEDStatus(@Param("device_id") DeviceInfo device_id);
	
	@Query("select l from LEDData l where l.ip_add=:ip_add")
	LEDData getLEDStatusByIP(@Param("ip_add") String ip_add);
	
	/*@Modifying
	@Query(value = "insert into led_on (ip_add,led_on) VALUES (:ip_add,:led_on)", nativeQuery = true)
	void recordLEDInfo(@Param("ip_add") String ip_add, @Param("led_on") String led_on);*/
}
