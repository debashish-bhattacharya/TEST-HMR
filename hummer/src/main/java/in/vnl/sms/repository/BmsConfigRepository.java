package in.vnl.sms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.vnl.sms.model.BmsConfig;



@Repository
public interface BmsConfigRepository extends CrudRepository<BmsConfig,Long>
{
	@Query(value = "SELECT * FROM bmsconfig WHERE ip = ?1", nativeQuery = true)
	List<BmsConfig> getBMSProperties(String config);
}
