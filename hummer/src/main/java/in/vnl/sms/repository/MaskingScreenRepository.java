package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.MaskingScreenData;

public interface MaskingScreenRepository extends CrudRepository<MaskingScreenData, Integer> 
{
	@Query("select d from MaskingScreenData d where d.profile=:profile ORDER BY d.id ASC")
	List<MaskingScreenData> getMaskingDataList(@Param("profile") String profile);
	
	@Modifying
	@Query("delete from MaskingScreenData d where d.profile=:profile")
	void deleteMaskingDataListByProfile(@Param("profile") String profile);
}
