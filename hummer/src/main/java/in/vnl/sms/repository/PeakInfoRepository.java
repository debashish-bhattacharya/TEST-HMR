package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.PeakDetectionInfo;



public interface PeakInfoRepository extends CrudRepository<PeakDetectionInfo, Long> {

	/*@Modifying
	@Query(value = "insert into peak_info (ip_add,power,frequency,time) VALUES (:ip_add,:power,:frequency,:time)", nativeQuery = true)
	void recordPeakInfo(@Param("ip_add") String ip_add, @Param("power") String power,@Param("frequency") String frequency,@Param("time") String time);
	*/
	@Query("select pk from PeakDetectionInfo pk where pk.ip_add=:ip_add")
	PeakDetectionInfo getPeakInfoRecordByIP(@Param("ip_add") String ip_add);
	
	@Query("select pk from PeakDetectionInfo pk where pk.device_id=:device_id")
	List<PeakDetectionInfo> getPeakInfoRecord(@Param("device_id") DeviceInfo device_id);
	
	@Query(value = "select pk from PeakDetectionInfo pk where pk.time between :startTime and :endTime ORDER BY pk.id DESC")
	List<PeakDetectionInfo> getPeakInfoRecordByTime(@Param("startTime") String startTime, @Param("endTime") String endTime, Pageable pageable);
	
	
	@Query(value = "select pk from PeakDetectionInfo pk where pk.sector != 'STRU' ORDER BY pk.id DESC")
	List<PeakDetectionInfo> getPeakInfoRecordNotStru(@Param("startTime") String startTime, @Param("endTime") String endTime,  Pageable pageable);
	
	@Query(value = "select pk from PeakDetectionInfo pk where pk.sector = 'STRU' ORDER BY pk.id DESC")
	List<PeakDetectionInfo> getPeakInfoRecordStru(@Param("startTime") String startTime, @Param("endTime") String endTime, Pageable pageable);
	
	@Query("select pk from PeakDetectionInfo pk where device_id=:device_id and  pk.time between :startTime and :endTime order by pk.id")
    List<PeakDetectionInfo> getPeakInfoRecordBetweenTime(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("device_id") DeviceInfo deviceInfo);
	
	@Query("select pk from PeakDetectionInfo pk where device_id=:device_id and pk.alarm=:alarm and pk.time between :startTime and :endTime order by pk.id DESC")
    List<PeakDetectionInfo> getPeakInfoRecordBetweenTimeForAlarm(@Param("startTime") String startTime,@Param("endTime") String endTime,@Param("device_id") DeviceInfo deviceInfo
    		,@Param("alarm") boolean alarm, Pageable pageable);
	
	@Modifying
    @Transactional
    @Query("delete from PeakDetectionInfo pk where pk.time between :startTime and :endTime")
    void deleteBetweenTime(@Param("startTime") String startTime, @Param("endTime") String endTime);
	
}
