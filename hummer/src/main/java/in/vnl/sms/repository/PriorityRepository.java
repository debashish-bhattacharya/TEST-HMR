package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.Priority;

public interface PriorityRepository extends CrudRepository<Priority, Integer> 
{
	@Query("select d from Priority d ORDER BY d.priority ASC")
	List<Priority> getPriorityList();
	
	@Modifying
	@Query(value="update Priority d SET d.priority=:priority where d.name=:name")
	void updatePriority(@Param("priority") Integer priority, @Param("name") String name);
}
