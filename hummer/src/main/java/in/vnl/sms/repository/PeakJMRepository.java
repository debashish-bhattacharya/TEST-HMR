package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.PeakJMData;



public interface PeakJMRepository extends CrudRepository<PeakJMData, Long> {

	@Query("select pk from PeakJMData pk where pk.ip_add=:ip_add")
	List<PeakJMData> getPeakInfoRecordByIP(@Param("ip_add") String ip_add);
}