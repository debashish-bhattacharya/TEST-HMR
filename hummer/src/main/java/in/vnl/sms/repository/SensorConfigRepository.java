package in.vnl.sms.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.SensorConfig;

public interface SensorConfigRepository extends CrudRepository<SensorConfig, String> {
	
	@Query(value="select * from sensor_config order by sensornum", nativeQuery = true)
	List<SensorConfig> getSensorConfig();
	
	@Query(value="select count(distinct sector) from sensor_config where usestatus='Yes' and sector!='STRU'", nativeQuery = true)
	String getNumberOfSensors();
	
	@Query(value="select * from sensor_config where sensorenabled = 'Enabled'", nativeQuery = true)
	List<SensorConfig> getUserSensorConfig();
	
	
	
	@Query(value="select  sector, angle, sensornum, sensorport from sensor_config", nativeQuery = true)
	List<Object[]> getPreviousData();
	
	@Query(value="select  sensornum, sensorport from sensor_config", nativeQuery = true)
	List<Object[]> getPreviousSensorNumPort();
	
	@Query(value="select sensornum, sensorport, sector from sensor_config where usestatus = 'Yes'", nativeQuery = true)
	List<Object[]> getSectorMapping();

}

