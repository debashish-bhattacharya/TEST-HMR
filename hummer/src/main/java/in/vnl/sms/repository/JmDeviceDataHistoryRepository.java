package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.JMDeviceDataHistory;

public interface JmDeviceDataHistoryRepository extends CrudRepository<JMDeviceDataHistory, Integer> 
{
	@Query("select d from JMDeviceDataHistory d where d.id in (select max(f.id) from JMDeviceDataHistory f group by f.deviceid)")
	List<JMDeviceDataHistory> getJmDeviceDataHistory();
}
