package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.JMNodeMapping;

public interface JmNodeMappingRepository extends CrudRepository<JMNodeMapping, Integer> 
{
	@Query("select d from JMNodeMapping d ORDER BY d.id ASC")
	List<JMNodeMapping> getNodeMappingList();
}