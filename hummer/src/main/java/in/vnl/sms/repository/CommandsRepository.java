package in.vnl.sms.repository;

import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.Commands;

public interface CommandsRepository extends CrudRepository<Commands,Long>
{
	
}
