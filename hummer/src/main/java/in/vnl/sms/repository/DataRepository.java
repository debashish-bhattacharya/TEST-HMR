package in.vnl.sms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.TriggerData;



public interface DataRepository extends CrudRepository<TriggerData, Long> {

/*	@Query("insert into ConfigData values (ipAdd =:ipAdd, reachabilityStatus =:reachabilityStatus)")
	TriggerData insertConfigData(@Param("ipAdd") String ipAdd,@Param("reachabilityStatus") String reachabilityStatus);
*/
	@Query("select a from TriggerData a where a.ip_add=:ip_add")
	TriggerData getTriggerData(@Param("ip_add") String ip_add);
	
	//public TriggerData getTriggerData(String ipAdd);
}
