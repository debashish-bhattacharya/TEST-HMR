package in.vnl.sms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.Devices;
import in.vnl.sms.model.GpsData;


public interface GpsDataRepository extends CrudRepository<GpsData, Long>{

	@Query("select gp from GpsData gp where gp.inserttime between :startTime and :endTime order by gp.id ASC")
    List<GpsData> getGpsDataBetweenTime(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
	
}
