package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.ConfigData;
import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.SensorConfig;



public interface ConfigRepository extends CrudRepository<ConfigData, Long> {

	@Query("select a from ConfigData a where a.configRepositoryComposite.ip_add=:ip_add and a.configRepositoryComposite.room!=:room")
	List<ConfigData> getConfigData(@Param("ip_add") String ip_add, @Param("room") String room);
	
	@Query("select a from ConfigData a where a.configRepositoryComposite.ip_add=:ip_add")
	List<ConfigData> getConfigDatalist(@Param("ip_add") String ip_add);
	
	@Query("select c from ConfigData c where c.device_id=:device_id")
	ConfigData getConfigDataByID(@Param("device_id") DeviceInfo device_id);
	
	@Query(value="select ref_level from config_data order by room", nativeQuery = true)
	List<ConfigData> getRefl();
	
	@Query("select c from ConfigData c where c.configRepositoryComposite.room=:room")
	ConfigData getConfigDataByRoom(@Param("room") String room);
	
	@Query("select c from ConfigData c where c.device_id=:device_id  and c.configRepositoryComposite.room=:room")
	ConfigData getConfigDataByID(@Param("device_id") DeviceInfo device_id, @Param("room") String room);
	
	@Query(value="select preamp_type from config_data order by room", nativeQuery = true)
	List<String> getPreAmpValues();
}
