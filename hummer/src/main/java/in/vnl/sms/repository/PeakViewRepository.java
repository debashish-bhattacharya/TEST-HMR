package in.vnl.sms.repository;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.PeakView;



public interface PeakViewRepository extends CrudRepository<PeakView, Integer> 
{
	/*@Query(value="select * from NodesInfo where name=?1",nativeQuery=true)
	ArrayList<PeakView> findNodesByUseType(String name);*/
	
	@Query(value="select p from PeakView p where p.device_id=:device_id and p.inserttime between :startTime and :endTime order by p.id")
	ArrayList<PeakView> getPeaksBetweenTime(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("device_id") DeviceInfo deviceInfo);
	
}
