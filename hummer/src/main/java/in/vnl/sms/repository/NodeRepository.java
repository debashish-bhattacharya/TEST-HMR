package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.NodeData;

public interface NodeRepository extends CrudRepository<NodeData, Long> {

	@Query("select nd from NodeData nd")
	List<NodeData> getNodeInfo();
	
	@Query("select nd from NodeData nd where nd.ip_add=:ip_add")
	NodeData getNodeByIP(@Param("ip_add") String ip_add);
	
	@Query("select nd from NodeData nd where nd.type=:type")
	NodeData getNodeByType(@Param("type") String type);
	
	@Modifying
	@Query(value = "update NodeData nd SET nd.status=:status where nd.ip_add=:ip_add")
	void updateNodeStatus(@Param("status") String status, @Param("ip_add") String ip_add);
	
}
