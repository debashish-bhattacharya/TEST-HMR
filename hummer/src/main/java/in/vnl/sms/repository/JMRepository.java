package in.vnl.sms.repository;

import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.JMData;



public interface JMRepository extends CrudRepository<JMData, Long> {

}
