package in.vnl.sms.repository;

import java.io.Serializable;

import javax.persistence.Column;

public class SensorConfigRepositoryComposite implements Serializable {
	
	@Column(name="sensornum")
	private Integer sensornum;
	
	@Column(name="sensorport")
	private Integer sensorport;
	
	public SensorConfigRepositoryComposite() {
		
	}
	
	public SensorConfigRepositoryComposite(Integer sensornum, Integer sensorport) {
		this.sensornum = sensornum;
		this.sensorport = sensorport;
	}
	
	
	public Integer getSensornum() {
		return sensornum;
	}

	public void setSensornum(Integer sensornum) {
		this.sensornum = sensornum;
	}

	public Integer getSensorport() {
		return sensorport;
	}

	public void setSensorport(Integer sensorport) {
		this.sensorport = sensorport;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorConfigRepositoryComposite that = (SensorConfigRepositoryComposite) o;

        if (!sensornum.equals(that.sensornum)) return false;
        return sensorport.equals(that.sensorport);
    }

    @Override
    public int hashCode() {
        int result = sensornum.hashCode();
        result = 31 * result + sensornum.hashCode();
        return result;
    }


}
