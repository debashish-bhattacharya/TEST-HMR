package in.vnl.sms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.vnl.sms.model.Alarm;



@Repository
public interface AlarmsRepository extends CrudRepository<Alarm,Long>
{
	
	 @Override
	 List<Alarm> findAll();
	
	 @Modifying
	 @Transactional
	 @Query(value= "update alarms set status = 2  where id = ?1", nativeQuery = true)
	 int acknowledge(@Param("id") long alarmId);
	 
	 
	 @Modifying
	 @Transactional
	 @Query(value= "update alarms set severity = 0  where id = ?1", nativeQuery = true)
	 int clear(@Param("id") long alarmId);
	
}
