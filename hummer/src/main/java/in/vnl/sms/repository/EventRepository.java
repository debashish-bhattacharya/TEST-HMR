package in.vnl.sms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.Event;

public interface EventRepository extends CrudRepository<Event,Long>
{
	/*
	//@Query(value = "SELECT * FROM EVENTS WHERE event_date >= ?1 and event_date <= ?1 order by event_date asc", nativeQuery = true)
	@Query("select e from Event e ORDER BY e.id ASC")
	List<Event> findEventsOnGivenDate(Date eventDate);
	
	@Query(value = "select e from Event e where e.event_type=:'7' and e.event_date=:event_date order by e.id asc")
	List<Event> findAlarmsOnGivenDate(@Param("event_date") Date eventDate);*/
	
}
