package in.vnl.sms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.TaskPriority;

public interface TaskPriorityRepository extends CrudRepository<TaskPriority, Long> {

	@Query("select tp from TaskPriority tp where tp.device_name=:device_name")
	TaskPriority getDevicePriority(@Param("device_name") String device_name);
}
