package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.JMDeviceData;

public interface JmDeviceDataRepository extends CrudRepository<JMDeviceData, Integer> 
{
	@Query("select d from JMDeviceData d ORDER BY d.id ASC")
	List<JMDeviceData> getJmDeviceData();
}
