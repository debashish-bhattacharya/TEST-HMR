package in.vnl.sms.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.ConfigProfileData;

public interface ConfigProfileRepository extends CrudRepository<ConfigProfileData, Long> {
	
	@Query("select c from ConfigProfileData c where c.profile=:profile")
	ConfigProfileData getConfigProfileDataByProfile(@Param("profile") String profile);
	
	@Modifying
	@Query("delete from ConfigProfileData d where d.profile=:profile")
	void deleteByProfile(@Param("profile") String profile);

}


