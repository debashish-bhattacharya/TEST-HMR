package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import in.vnl.sms.model.AlarmInfo;

public interface AlarmRepository extends CrudRepository<AlarmInfo, Long>{

	@Query("select ai from AlarmInfo ai where ai.type=:type and ai.time between :startTime and :endTime ORDER BY ai.id DESC")
	List<AlarmInfo> getAlarmInfoRecordByTime(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("type") String type);

	@Modifying
    @Transactional
    @Query("delete from AlarmInfo ai where ai.time between :startTime and :endTime")
    void deleteBetweenTime(@Param("startTime") String startTime, @Param("endTime") String endTime);
}
