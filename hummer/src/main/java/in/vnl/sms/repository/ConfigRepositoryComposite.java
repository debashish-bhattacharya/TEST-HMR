package in.vnl.sms.repository;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Embeddable
public class ConfigRepositoryComposite implements Serializable {
	
	@Column(name="ip_add")
    private String ip_add;

	@Column(name="room")
    private String room;

    public ConfigRepositoryComposite() {

    }

    public ConfigRepositoryComposite(String ip_add, String room) {
        this.ip_add = ip_add;
        this.room = room;
    }

    public String getIp_add() {
		return ip_add;
	}
	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}
	
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfigRepositoryComposite that = (ConfigRepositoryComposite) o;

        if (!ip_add.equals(that.ip_add)) return false;
        return room.equals(that.room);
    }

    @Override
    public int hashCode() {
        int result = ip_add.hashCode();
        result = 31 * result + ip_add.hashCode();
        return result;
    }
}
