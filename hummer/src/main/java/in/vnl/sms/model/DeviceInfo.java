package in.vnl.sms.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="device_info")
public class DeviceInfo {

	public DeviceInfo() {
		// TODO Auto-generated constructor stub
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="ip_add")
	private String ip_add;
	@Column(name="name")
	private String name;
	@Column(name="color")
	private String  color;
	@Column(name="lat")
	private String lat;
	@Column(name="lon")
	private String lon;
	@Column(name="is_active")
	private String is_active;
	@Column(name="state")
	private String state;
	@Column(name="starttime")
	private String starttime;
	
	@JsonIgnore
	@OneToMany(mappedBy = "device_id", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	private List<ConfigData> config_data = new ArrayList<>();
	
	@JsonIgnore
	@OneToOne(mappedBy="device_id", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	private LEDData led_data;
	
	@JsonIgnore
	@OneToMany(mappedBy="device_id", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	private List<PeakDetectionInfo> peak_data = new ArrayList<>();
	
	@JsonIgnore
	@OneToOne(mappedBy="device_id", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	private TriggerData trigger_data;
	
	
	public List<ConfigData> getConfig_data() {
		return config_data;
	}
	public void setConfig_data(List<ConfigData> config_data) {
		this.config_data = config_data;
	}
	public LEDData getLed_data() {
		return led_data;
	}
	public void setLed_data(LEDData led_data) {
		this.led_data = led_data;
	}
	public List<PeakDetectionInfo> getPeak_data() {
		return peak_data;
	}
	public void setPeak_data(List<PeakDetectionInfo> peak_data) {
		this.peak_data = peak_data;
	}
	public TriggerData getTrigger_data() {
		return trigger_data;
	}
	public void setTrigger_data(TriggerData trigger_data) {
		this.trigger_data = trigger_data;
	}
	public String getIs_active() {
		return is_active;
	}
	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIp_add() {
		return ip_add;
	}
	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	
}
