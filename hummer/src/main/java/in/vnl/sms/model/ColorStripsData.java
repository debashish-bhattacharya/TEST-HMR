package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="color_code")
public class ColorStripsData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="startfreq")	
	private double startfreq;
	
	@Column(name="stopfreq")
	private double stopfreq;
	
	@Column(name="depl")
	private String depl;
	
	@Column(name="epr")
	private String epr;
	
	@Column(name="modulation")
	private String modulation;
	
	@Column(name="networktype")
	private String networktype;
	
	@Column(name="color")
	private String color;

	@Column(name="visible")
	private boolean visible;
	
	@Column(name="profile")
	private String profile;
	
	public ColorStripsData createCopy(ColorStripsData cd, String profile) {
		ColorStripsData cd1 = new ColorStripsData();
		cd1.setColor(cd.getColor());
		cd1.setDepl(cd.getDepl());
		cd1.setEpr(cd.getEpr());
		cd1.setModulation(cd.getModulation());
		cd1.setNetworktype(cd.getNetworktype());
		cd1.setStartfreq(cd.getStartfreq());
		cd1.setStopfreq(cd.getStopfreq());
		cd1.setVisible(cd.getVisible());
		cd1.setProfile(profile);
		return cd1;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getStartfreq() {
		return startfreq;
	}

	public void setStartfreq(double startfreq) {
		this.startfreq = startfreq;
	}


	public double getStopfreq() {
		return stopfreq;
	}

	public void setStopfreq(double stopfreq) {
		this.stopfreq = stopfreq;
	}

	public String getDepl() {
		return depl;
	}

	public void setDepl(String depl) {
		this.depl = depl;
	}
	
	public String getEpr() {
		return epr;
	}

	public void setEpr(String epr) {
		this.epr = epr;
	}
	
	public String getModulation() {
		return modulation;
	}

	public void setModulation(String modulation) {
		this.modulation = modulation;
	}
	public String getNetworktype() {
		return networktype;
	}

	public void setNetworktype(String networktype) {
		this.networktype = networktype;
	}


	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	public boolean getVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	public String toString() {
		return "ColorStripsData [startfreq=" + startfreq +",stopfreq=" +stopfreq+",networktype=" +networktype+" ,color=" +color+"]";
	}
	
		
	
}
