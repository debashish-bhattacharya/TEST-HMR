package in.vnl.sms.model;

public class PeakInfoPojo {

	private Long id;
	private String ip_add;
	private String power;
	private String  frequency;
	private String time;
	private String deviceName;
	private String ptzangle;
	private int currentconfigid;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIp_add() {
		return ip_add;
	}
	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getPtzangle() {
		return ptzangle;
	}
	public void setPtzangle(String ptzangle) {
		this.ptzangle = ptzangle;
	}
	public int getCurrentconfigid() {
		return currentconfigid;
	}
	public void setCurrentconfigid(int currentconfigid) {
		this.currentconfigid = currentconfigid;
	}

	
}
