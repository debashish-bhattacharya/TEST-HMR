package in.vnl.sms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="bms_info")
public class BMSData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="on_btn")	
	private String on_btn;
	
   	@Column(name="reset_btn")	
	private String reset_btn;
	
	@Column(name="pr_btn")	
	private String pr_btn;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getOn_btn() {
		return on_btn;
	}

	public void setOn_btn(String on_btn) {
		this.on_btn = on_btn;
	}

	public String getReset_btn() {
		return reset_btn;
	}

	public void setReset_btn(String reset_btn) {
		this.reset_btn = reset_btn;
	}

	public String getPr_btn() {
		return pr_btn;
	}

	public void setPr_btn(String pr_btn) {
		this.pr_btn = pr_btn;
	}

}
