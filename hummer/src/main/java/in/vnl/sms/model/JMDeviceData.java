package in.vnl.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jmdevice_data")
public class JMDeviceData {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="deviceid")
	private int deviceid;
	
	@Column(name="time")
	private Date time;
	
	@Column(name="roll")
	private String roll;
	
	@Column(name="tilt")
	private String tilt;
	
	@Column(name="pan")
	private String pan;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public int getDeviceid() {
		return deviceid;
	}
	
	public void setDeviceid(int deviceid) {
		this.deviceid = deviceid;
	}
	
	public Date getTime() {
		return time;
	}
	
	public void setTime(Date time) {
		this.time = time;
	}
	
	public String getRoll() {
		return roll;
	}
	
	public void setRoll(String roll) {
		this.roll = roll;
	}
	
	public String getTilt() {
		return tilt;
	}
	
	public void setTilt(String tilt) {
		this.tilt = tilt;
	}
	
	public String getPan() {
		return pan;
	}
	
	public void setPan(String pan) {
		this.pan = pan;
	}
}