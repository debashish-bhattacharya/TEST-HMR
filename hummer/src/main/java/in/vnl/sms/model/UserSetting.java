package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_setting")
public class UserSetting 
{
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="type")
	private String type;
	
	@Column(name="mode")
	private String mode;
	
	@Column(name="gps")
	private String gps;
	
	@Column(name="gps_accuracy")
	private double gpsAccuracy;
	
	/*
	 * @Column(name="opr_mode") private String opr_mode;
	 * 
	 * public String getOpr_mode() { return opr_mode; }
	 * 
	 * public void setOpr_mode(String opr_mode) { this.opr_mode = opr_mode; }
	 */

	public long getId() 
	{
		return id;
	}
	
	public void setId(long id) 
	{
		this.id = id;
	}
	
	public String getType() 
	{
		return type;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	
	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getGps() {
		return gps;
	}

	public void setGps(String gps) {
		this.gps = gps;
	}
	
	public double getGpsAccuracy() {
		return gpsAccuracy;
	}

	public void setGpsAccuracy(double gpsAccuracy) {
		this.gpsAccuracy = gpsAccuracy;
	}
	
}
