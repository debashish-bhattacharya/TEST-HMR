package in.vnl.sms.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="jmdevicedata_history")
public class JMDeviceDataHistory {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="deviceid")	
	private int deviceid;
	
	@Column(name="time")
	private Date time;
	
	@Column(name="roll")
	private String roll;
	
	@Column(name="tilt")
	private String tilt;
	
	@Column(name="pan")
	private String pan;
	
	@JsonIgnore
	@OneToMany(mappedBy="jmdevice_id", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
	private List<PeakDetectionInfo> peak_data = new ArrayList<>();
	
	public List<PeakDetectionInfo> getPeak_data() {
		return peak_data;
	}
	public void setPeak_data(List<PeakDetectionInfo> peak_data) {
		this.peak_data = peak_data;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public int getDeviceid() {
		return deviceid;
	}
	
	public void setDeviceid(int deviceid) {
		this.deviceid = deviceid;
	}
	
	public Date getTime() {
		return time;
	}
	
	public void setTime(Date time) {
		this.time = time;
	}
	
	public String getRoll() {
		return roll;
	}
	
	public void setRoll(String roll) {
		this.roll = roll;
	}
	
	public String getTilt() {
		return tilt;
	}
	
	public void setTilt(String tilt) {
		this.tilt = tilt;
	}
	
	public String getPan() {
		return pan;
	}
	
	public void setPan(String pan) {
		this.pan = pan;
	}
}