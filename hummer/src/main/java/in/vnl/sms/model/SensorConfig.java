package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import in.vnl.sms.repository.ConfigRepositoryComposite;
import in.vnl.sms.repository.SensorConfigRepositoryComposite;

@Entity
@Table(name="sensor_config")

public class SensorConfig {
	
	@EmbeddedId
    private SensorConfigRepositoryComposite sensorConfigRepositoryComposite = new SensorConfigRepositoryComposite();
	
		
	@Column(name="sensorenabled")
	private String sensorenabled;
	
	@Column(name="sensorip")
	private String sensorip;
	
		
	@Column(name="rotatingsensor")
	private String rotatingsensor;
	
	@Column(name="omcip")
	private String omcip;
	
	@Column(name="rfswitchip")
	private String rfswitchip;
	
	@Column(name="angle")
	private Double angle;
	
	@Column(name="beamwidth")
	private Double beamwidth;
	
	@Column(name="gsmserverip")
	private String gsmserverip;

	@Column(name="usestatus")
	private String usestatus;
	
	@Column(name="sector")
	private String sector;

	public Integer getSensornum() {
		return sensorConfigRepositoryComposite.getSensornum();
	}

	public void setSensornum(Integer sensornum) {
		sensorConfigRepositoryComposite.setSensornum(sensornum);
	}

	public Integer getSensorport() {
		return sensorConfigRepositoryComposite.getSensorport();
	}

	public void setSensorport(Integer sensorport) {
		sensorConfigRepositoryComposite.setSensorport(sensorport);
	}

	

	public String getSensorenabled() {
		return sensorenabled;
	}

	public void setSensorenabled(String sensorenabled) {
		this.sensorenabled = sensorenabled;
	}

	public String getSensorip() {
		return sensorip;
	}

	public void setSensorip(String sensorip) {
		this.sensorip = sensorip;
	}

	
	public String getRotatingsensor() {
		return rotatingsensor;
	}

	public void setRotatingsensor(String rotatingsensor) {
		this.rotatingsensor = rotatingsensor;
	}

	public String getOmcip() {
		return omcip;
	}

	public void setOmcip(String omcip) {
		this.omcip = omcip;
	}

	public String getRfswitchip() {
		return rfswitchip;
	}

	public void setRfswitchip(String rfswitchip) {
		this.rfswitchip = rfswitchip;
	}

	
	public Double getAngle() {
		return angle;
	}

	public void setAngle(Double angle) {
		this.angle = angle;
	}

	public Double getBeamwidth() {
		return beamwidth;
	}

	public void setBeamwidth(Double beamwidth) {
		this.beamwidth = beamwidth;
	}

	public String getGsmserverip() {
		return gsmserverip;
	}

	public void setGsmserverip(String gsmserverip) {
		this.gsmserverip = gsmserverip;
	}

	public String getUsestatus() {
		return usestatus;
	}

	public void setUsestatus(String usestatus) {
		this.usestatus = usestatus;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	
	
	

}
