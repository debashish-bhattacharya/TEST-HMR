package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="devices")
public class Devices 
{
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@Column(name="status")
	private boolean status;
	
	@Column(name="node_name")
	private String node_name;
	


	public Devices() 
	{
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Devices(long id, boolean status,String node_name) 
	{
		super();
		this.id = id;
		this.status = status;
		this.node_name = node_name;
	}
	
	public long getId() 
	{
		return id;
	}
	
	public void setId(long id) 
	{
		this.id = id;
	}
	
	public boolean getStatus() 
	{
		return status;
	}
	
	public void setStatus(boolean status) 
	{
		this.status = status;
	}
	
	public String getNode_name() {
		return node_name;
	}

	public void setNode_name(String node_name) {
		this.node_name = node_name;
	}
	
}
