package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="led_status")
public class LEDData {

	/*@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;*/
	
	
	@OneToOne
	@JoinColumn(name="device_id", nullable=false)
	private DeviceInfo device_id;
	
	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ip_add")
	private String ip_add;
	
	@Column(name="led_on")
	private int led_on;




	/*public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}*/




	public DeviceInfo getDevice_id() {
		return device_id;
	}

	public void setDevice_id(DeviceInfo device_id) {
		this.device_id = device_id;
	}

	public String getIp_add() {
		return ip_add;
	}

	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}

	public int getLed_on() {
		return led_on;
	}

	public void setLed_on(int led_on) {
		this.led_on = led_on;
	}
	
	
}
