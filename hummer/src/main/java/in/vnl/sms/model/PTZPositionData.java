package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ptz_position_data")
public class PTZPositionData {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private Long id;
	
	@Column(name="ptz_ip")
	private String ptz_ip;
	
	@Column(name="pan_position")
	private float pan_position;
	
	@Column(name="tilt_position")
	private float tilt_position;
	
	@Column(name="date_time")
	private String date_time;

	@Column(name="system_time")
	private String system_time;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPtz_ip() {
		return ptz_ip;
	}

	public void setPtz_ip(String ptz_ip) {
		this.ptz_ip = ptz_ip;
	}

	public float getPan_position() {
		return pan_position;
	}

	public void setPan_position(float pan_position) {
		this.pan_position = pan_position;
	}

	public float getTilt_position() {
		return tilt_position;
	}

	public void setTilt_position(float tilt_position) {
		this.tilt_position = tilt_position;
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public String getSystem_time() {
		return system_time;
	}

	public void setSystem_time(String system_time) {
		this.system_time = system_time;
	}
	
	
}
