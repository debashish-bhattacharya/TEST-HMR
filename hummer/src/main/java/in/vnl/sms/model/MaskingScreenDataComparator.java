package in.vnl.sms.model;

import java.util.Comparator;

public class MaskingScreenDataComparator implements Comparator<MaskingScreenData>{
	
	public int compare(MaskingScreenData data1, MaskingScreenData data2) 
    { 
        Double firstFrequency = data1.getFrequency();
        Double secondFrequency = data2.getFrequency();
        return firstFrequency.compareTo(secondFrequency); 
    } 
	
}
