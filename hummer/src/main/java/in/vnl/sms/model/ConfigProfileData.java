package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="config_profile")
public class ConfigProfileData {
	
	@ManyToOne
	@JoinColumn(name="device_id", nullable=false)
	@JsonIgnore
	private DeviceInfo device_id;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name="ip_add")
	private String ip_add;
	@Column(name="floor")
	private String floor;
	@Column(name="room")
	private String  room;
	@Column(name="calibration")
	private String calibration;
	@Column(name="start_freq")
	private String start_freq;
	@Column(name="stop_freq")
	private String stop_freq;
	@Column(name="threshold")
	private String threshold;
	@Column(name="mask_offset")
	private String mask_offset;
	@Column(name="use_mask")
	private String use_mask;
	@Column(name="start_time")
	private String start_time;
	@Column(name="stop_time")
	private String stop_time;
	@Column(name="cable_length")
	private String cable_length;
	@Column(name="preamp_type")
	private String preamp_type;
	@Column(name="gsm_dl")
	private String gsm_dl;
	@Column(name="wcdma_dl")
	private String wcdma_dl;
	@Column(name="wifi_band")
	private String wifi_band;
	@Column(name="lte_dl")
	private String lte_dl;
	@Column(name="band_start1")
	private String band_start1;
	@Column(name="band_stop1")
	private String band_stop1;
	@Column(name="band_en1")
	private String band_en1;
	@Column(name="band_start2")
	private String band_start2;
	@Column(name="band_stop2")
	private String band_stop2;
	@Column(name="band_en2")
	private String band_en2;
	@Column(name="band_start3")
	private String band_start3;
	@Column(name="band_stop3")
	private String band_stop3;
	@Column(name="band_en3")
	private String band_en3;
	@Column(name="band_start4")
	private String band_start4;
	@Column(name="band_stop4")
	private String band_stop4;
	@Column(name="band_en4")
	private String band_en4;
	@Column(name="band_start5")
	private String band_start5;
	@Column(name="band_stop5")
	private String band_stop5;
	@Column(name="band_en5")
	private String band_en5;
	@Column(name="country_code")
	private String country_code;
	@Column(name="profile")
	private String profile;
	@Column(name="ref_level")
	private String ref_level;
	@Column(name="band_specific")
	private String band_specific;
	@Column(name="band_filter")
	private String band_filter;
	public String getBand_filter() {
		return band_filter;
	}

	public void setBand_filter(String band_filter) {
		this.band_filter = band_filter;
	}

	public String getBand_specific() {
		return band_specific;
	}

	public void setBand_specific(String band_specific) {
		this.band_specific = band_specific;
	}

	public String getRef_level() {
		return ref_level;
	}

	public void setRef_level(String ref_level) {
		this.ref_level = ref_level;
	}
	
	
public static ConfigData copy(ConfigProfileData c) {
		
		ConfigData cData = new ConfigData();
		cData.setDevice_id(c.getDevice_id());
		cData.setIp_add(c.getIp_add());
		cData.setFloor("1");
		cData.setRoom(c.getRoom());
		cData.setCalibration(c.getCalibration());
		cData.setStart_freq(c.getStart_freq());
		cData.setStop_freq(c.getStop_freq());
		cData.setThreshold(c.getThreshold());
		cData.setMask_offset(c.getMask_offset());
		cData.setUse_mask(c.getUse_mask());
		cData.setStart_time(c.getStart_time());
		cData.setStop_time(c.getStop_time());
		cData.setCable_length(c.getCable_length());
		cData.setPreamp_type(c.getPreamp_type());
		cData.setGsm_dl(c.getGsm_dl());
		cData.setWcdma_dl(c.getWcdma_dl());
		cData.setWifi_band(c.getWifi_band());
		cData.setLte_dl(c.getLte_dl());
		cData.setBand_start1(c.getBand_start1());
		cData.setBand_stop1(c.getBand_stop1());
		cData.setBand_en1(c.getBand_en1());
		cData.setBand_start2(c.getBand_start2());
		cData.setBand_stop2(c.getBand_stop2());
		cData.setBand_en2(c.getBand_en2());
		cData.setBand_start3(c.getBand_start3());
		cData.setBand_stop3(c.getBand_stop3());
		cData.setBand_en3(c.getBand_en3());
		cData.setBand_start4(c.getBand_start4());
		cData.setBand_stop4(c.getBand_stop4());
		cData.setBand_en4(c.getBand_en4());
		cData.setBand_start5(c.getBand_start5());
		cData.setBand_stop5(c.getBand_stop5());
		cData.setBand_en5(c.getBand_en5());
		cData.setCountry_code(c.getCountry_code());
		cData.setRef_level(c.getRef_level());
		cData.setBand_specific(c.getBand_specific());
		cData.setBand_filter(c.getBand_filter());
		return cData;
	}

	public static ConfigProfileData copy(ConfigData c, String profile) {
		
		ConfigProfileData cData = new ConfigProfileData();
		cData.setDevice_id(c.getDevice_id());
		cData.setIp_add(c.getIp_add());
		cData.setFloor("1");
		cData.setRoom(c.getRoom());
		cData.setCalibration(c.getCalibration());
		cData.setStart_freq(c.getStart_freq());
		cData.setStop_freq(c.getStop_freq());
		cData.setThreshold(c.getThreshold());
		cData.setMask_offset(c.getMask_offset());
		cData.setUse_mask(c.getUse_mask());
		cData.setStart_time(c.getStart_time());
		cData.setStop_time(c.getStop_time());
		cData.setCable_length(c.getCable_length());
		cData.setPreamp_type(c.getPreamp_type());
		cData.setGsm_dl(c.getGsm_dl());
		cData.setWcdma_dl(c.getWcdma_dl());
		cData.setWifi_band(c.getWifi_band());
		cData.setLte_dl(c.getLte_dl());
		cData.setBand_start1(c.getBand_start1());
		cData.setBand_stop1(c.getBand_stop1());
		cData.setBand_en1(c.getBand_en1());
		cData.setBand_start2(c.getBand_start2());
		cData.setBand_stop2(c.getBand_stop2());
		cData.setBand_en2(c.getBand_en2());
		cData.setBand_start3(c.getBand_start3());
		cData.setBand_stop3(c.getBand_stop3());
		cData.setBand_en3(c.getBand_en3());
		cData.setBand_start4(c.getBand_start4());
		cData.setBand_stop4(c.getBand_stop4());
		cData.setBand_en4(c.getBand_en4());
		cData.setBand_start5(c.getBand_start5());
		cData.setBand_stop5(c.getBand_stop5());
		cData.setBand_en5(c.getBand_en5());
		cData.setCountry_code(c.getCountry_code());
		cData.setProfile(profile);
		cData.setRef_level(c.getRef_level());
		cData.setBand_specific(c.getBand_specific());
		cData.setBand_filter(c.getBand_filter());
		return cData;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getIp_add() {
		return ip_add;
	}
	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getCalibration() {
		return calibration;
	}
	public void setCalibration(String calibration) {
		this.calibration = calibration;
	}

/*	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}*/



	public DeviceInfo getDevice_id() {
		return device_id;
	}
	public void setDevice_id(DeviceInfo device_id) {
		this.device_id = device_id;
	}
	public String getStart_freq() {
		return start_freq;
	}
	public void setStart_freq(String start_freq) {
		this.start_freq = start_freq;
	}
	public String getStop_freq() {
		return stop_freq;
	}
	public void setStop_freq(String stop_freq) {
		this.stop_freq = stop_freq;
	}
	public String getThreshold() {
		return threshold;
	}
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}
	public String getMask_offset() {
		return mask_offset;
	}
	public void setMask_offset(String mask_offset) {
		this.mask_offset = mask_offset;
	}
	public String getUse_mask() {
		return use_mask;
	}
	public void setUse_mask(String use_mask) {
		this.use_mask = use_mask;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getStop_time() {
		return stop_time;
	}
	public void setStop_time(String stop_time) {
		this.stop_time = stop_time;
	}
	public String getCable_length() {
		return cable_length;
	}
	public void setCable_length(String cable_length) {
		this.cable_length = cable_length;
	}
	public String getPreamp_type() {
		return preamp_type;
	}
	public void setPreamp_type(String preamp_type) {
		this.preamp_type = preamp_type;
	}
	public String getGsm_dl() {
		return gsm_dl;
	}
	public void setGsm_dl(String gsm_dl) {
		this.gsm_dl = gsm_dl;
	}
	public String getWcdma_dl() {
		return wcdma_dl;
	}
	public void setWcdma_dl(String wcdma_dl) {
		this.wcdma_dl = wcdma_dl;
	}
	public String getWifi_band() {
		return wifi_band;
	}
	public void setWifi_band(String wifi_band) {
		this.wifi_band = wifi_band;
	}
	public String getLte_dl() {
		return lte_dl;
	}
	public void setLte_dl(String lte_dl) {
		this.lte_dl = lte_dl;
	}
	public String getBand_start1() {
		return band_start1;
	}
	public void setBand_start1(String band_start1) {
		this.band_start1 = band_start1;
	}
	public String getBand_stop1() {
		return band_stop1;
	}
	public void setBand_stop1(String band_stop1) {
		this.band_stop1 = band_stop1;
	}
	public String getBand_en1() {
		return band_en1;
	}
	public void setBand_en1(String band_en1) {
		this.band_en1 = band_en1;
	}
	public String getBand_start2() {
		return band_start2;
	}
	public void setBand_start2(String band_start2) {
		this.band_start2 = band_start2;
	}
	public String getBand_stop2() {
		return band_stop2;
	}
	public void setBand_stop2(String band_stop2) {
		this.band_stop2 = band_stop2;
	}
	public String getBand_en2() {
		return band_en2;
	}
	public void setBand_en2(String band_en2) {
		this.band_en2 = band_en2;
	}
	public String getBand_start3() {
		return band_start3;
	}
	public void setBand_start3(String band_start3) {
		this.band_start3 = band_start3;
	}
	public String getBand_stop3() {
		return band_stop3;
	}
	public void setBand_stop3(String band_stop3) {
		this.band_stop3 = band_stop3;
	}
	public String getBand_en3() {
		return band_en3;
	}
	public void setBand_en3(String band_en3) {
		this.band_en3 = band_en3;
	}
	public String getBand_start4() {
		return band_start4;
	}
	public void setBand_start4(String band_start4) {
		this.band_start4 = band_start4;
	}
	public String getBand_stop4() {
		return band_stop4;
	}
	public void setBand_stop4(String band_stop4) {
		this.band_stop4 = band_stop4;
	}
	public String getBand_en4() {
		return band_en4;
	}
	public void setBand_en4(String band_en4) {
		this.band_en4 = band_en4;
	}
	public String getBand_start5() {
		return band_start5;
	}
	public void setBand_start5(String band_start5) {
		this.band_start5 = band_start5;
	}
	public String getBand_stop5() {
		return band_stop5;
	}
	public void setBand_stop5(String band_stop5) {
		this.band_stop5 = band_stop5;
	}
	public String getBand_en5() {
		return band_en5;
	}
	public void setBand_en5(String band_en5) {
		this.band_en5 = band_en5;
	}
	public String getCountry_code() {
		return country_code;
	}
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
}
