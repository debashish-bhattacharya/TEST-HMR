package in.vnl.sms.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name="jmdata")
public class JMData {

	
	/*@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;*/
	
	@Column(name="ip_add")	
	private String ip_add;
	
	@Id
	@Column(name="time")
	private String time;
	
	@Column(name="angle")
	private String  angle;
	
	@Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime created_at;
	
	/*@Column(name="localtime")
	private String localtime;*/

	@Column(name="lat")
	private double lat;
	
	@Column(name="lon")
	private double lon;
	
	@Column(name="jmpacketdatetime")
	private String jmpacketdatetime;

	
	
	
	/*public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}*/
	
	
	public String getIp_add() {
		return ip_add;
	}

	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAngle() {
		return angle;
	}

	public void setAngle(String angle) {
		this.angle = angle;
	}

	/*public String getLocaltime() {
		return localtime;
	}

	public void setLocaltime(String localtime) {
		this.localtime = localtime;
	}*/

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public String getJmpacketdatetime() {
		return jmpacketdatetime;
	}

	public void setJmpacketdatetime(String jmpacketdatetime) {
		this.jmpacketdatetime = jmpacketdatetime;
	}

	public LocalDateTime getCreated_at() {
		return created_at;
	}

	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}

	
}
