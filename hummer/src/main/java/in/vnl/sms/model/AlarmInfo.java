package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alarm_info")
public class AlarmInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="transid")	
	private String transid;
	
	@Column(name="cueid")	
	private String cueid;
	
	@Column(name="ip_addr")	
	private String ip_addr;
	
	@Column(name="type")	
	private String type;
	
	@Column(name="trigger")	
	private String trigger;
	
	@Column(name="power")
	private String power;
	
	@Column(name="frequency")
	private String  frequency;
	
	@Column(name="time")
	private String time;
	
	//NEW UPDATE: USE PTZ instead of JM for angle.
	@Column(name="angle")
	private String angle;
	
	@Column(name="range")
	private double range;
	
	@Column(name="duration")
	private int duration;
	
	@Column(name="sample")
	private int sample;
	
	@Column(name="lat")
	private double latitude;
	
	@Column(name="lon")
	private double longitude;
	
	@Column(name="alarm")
	private boolean alarm;
	
	@Column(name="emitterpower")
	private double emitterpower;
	
	@Column(name="bandwidth")
	private double bandwidth;
	
	//UPDATE END
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTransid() {
		return transid;
	}
	
	public void setTransid(String transid) {
		this.transid = transid;
	}
	
	public String getCueid() {
		return cueid;
	}
	
	public void setCueid(String cueid) {
		this.cueid = cueid;
	}
	
	public String getIp_addr() {
		return ip_addr;
	}
	
	public void setIp_addr(String ip_addr) {
		this.ip_addr = ip_addr;
	}

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getTrigger() {
		return trigger;
	}
	
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	
	public String getPower() {
		return power;
	}
	
	public void setPower(String power) {
		this.power = power;
	}
	
	public String getFrequency() {
		return frequency;
	}
	
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	public String getAngle() {
		return angle;
	}
	
	public void setAngle(String angle) {
		this.angle = angle;
	}
	
	public Double getRange() {
		return range;
	}
	
	public void setRange(double range) {
		this.range = range;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	public int getSample() {
		return sample;
	}
	
	public void setSample(int sample) {
		this.sample = sample;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public boolean getAlarm() {
		return alarm;
	}
	
	public void setAlarm(boolean alarm) {
		this.alarm = alarm;
	}
	
	public double getEmitterpower() {
		return emitterpower;
	}
	
	public void setEmitterpower(double emitterpower) {
		this.emitterpower = emitterpower;
	}

	public double getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(double bandwidth) {
		this.bandwidth = bandwidth;
	}
	
	
}

