package in.vnl.sms.model;

public class FreqAutoInfo {

	private String eventName;
	private double frequency;
	private int startAngle;
	private int stopAngle;
	
	public FreqAutoInfo(String eventName, double frequency, int startAngle, int stopAngle) {
		this.eventName = eventName;
		this.frequency = frequency;
		this.startAngle = startAngle;
		this.stopAngle = stopAngle;
	}
	
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	public double getFrequency() {
		return frequency;
	}
	
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}
	
	public int getStartAngle() {
		return startAngle;
	}
	
	public void setStartAngle(int startAngle) {
		this.startAngle = startAngle;
	}
	
	public int getStopAngle() {
		return stopAngle;
	}
	
	public void setStopAngle(int stopAngle) {
		this.stopAngle = stopAngle;
	}
}
