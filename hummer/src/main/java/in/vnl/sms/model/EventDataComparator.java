package in.vnl.sms.model;

import java.util.Comparator;

public class EventDataComparator implements Comparator<EventData>{ 
    
    // Overriding compare()method of Comparator  
                // for ascending order of type 
    public int compare(EventData d1, EventData d2) { 
//        if (d1.getType() > d2.getType()) 
//            return 1; 
//        else if (d1.getType() < d2.getType()) 
//            return -1;
//        else
//            return 0; 
        if(d1.getType()!=d2.getType()) {
        	return d1.getType()-d2.getType();
        }
        else {
        	return d1.getDate().compareTo(d2.getDate());
        }
    } 
    
}