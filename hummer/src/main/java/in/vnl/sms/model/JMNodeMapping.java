package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jmnode_mapping")
public class JMNodeMapping {

	/*@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;*/
	
	@Id
	@Column(name="deviceid")
	private int deviceid;
	
	@Column(name="antennaid")
	private int antennaid;
	
	@Column(name="sector")
	private int sector;
	
	/*public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}*/

	public int getDeviceid() {
		return deviceid;
	}
	
	public void setDeviceid(int deviceid) {
		this.deviceid = deviceid;
	}
	
	public int getAntennaid() {
		return antennaid;
	}
	
	public void setAntennaid(int antennaid) {
		this.antennaid = antennaid;
	}
	
	public int getSector() {
		return sector;
	}
	
	public void setSector(int sector) {
		this.sector = sector;
	}
}