package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="trigger_data")
public class TriggerData {

	/*@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;*/
	
	@OneToOne
	@JoinColumn(name="device_id", nullable=false)
	private DeviceInfo device_id;
	
	@Id
	@Column(name="ip_add")
	private String ip_add;
	
	@Column(name="unit_no")
	private String unit_no;
	

	@Column(name="product_name")
	private String  product_name;
	@Column(name="model")
	private String model;
	@Column(name="mfg_date")
	private String mfg_date;
	@Column(name="client")
	private String client;
	@Column(name="location")
	private String location;
	@Column(name="country")
	private String country;
	
	
/*		
	public TriggerData(String ip_add, String unit_no, String product_name, String model, String mfg_date, String client, String location, String country)
	{
		super();
		this.ip_add = ip_add;
		this.unit_no = unit_no;
		this.product_name = product_name;
		this.model = model;
		this.mfg_date = mfg_date;
		this.client = client;
		this.location = location;
		this.country = country;
		
	}*/
	

	public String getIp_add() {
		return ip_add;
	}
	/*public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}*/


	public DeviceInfo getDevice_id() {
		return device_id;
	}
	public void setDevice_id(DeviceInfo device_id) {
		this.device_id = device_id;
	}
	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}
	
	
	

	public String getUnit_no() {
		return unit_no;
	}
	public void setUnit_no(String unit_no) {
		this.unit_no = unit_no;
	}
	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMfg_date() {
		return mfg_date;
	}

	public void setMfg_date(String mfg_date) {
		this.mfg_date = mfg_date;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
