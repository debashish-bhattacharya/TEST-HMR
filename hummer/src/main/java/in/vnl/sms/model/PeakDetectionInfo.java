package in.vnl.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="peak_info")
public class PeakDetectionInfo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="device_id", nullable=true)
	@JsonIgnore
	private DeviceInfo device_id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="jmdevice_id", nullable=true)
	@JsonIgnore
	private JMDeviceDataHistory jmdevice_id;
	
	@Column(name="ip_add")	
	private String ip_add;
	
	@Column(name="power")
	private String power;
	
	@Column(name="frequency")
	private String  frequency;
	
	@Column(name="time")
	private String time;
	
	//NEW UPDATE: USE PTZ instead of JM for angle.
	@Column(name="angle")
	private String angle;
	
	@Column(name="antennaid")
	private String antennaid;
	
	@Column(name="sector")
	private String sector;
	
	@Column(name="currentconfigid")
	private int currentconfigid;
	
	@Column(name="alarm")
	private boolean alarm;
	
	@Column(name="type")
	private String type;
	//UPDATE END
	
	@Column(name="inserttime")
	private Date inserttime;

	@Column(name="technology")
	private String technology;
	
	@Column(name="lat")
	private double lat;

	@Column(name="lon")
	private double lon;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
	public DeviceInfo getDevice_id() {
		return device_id;
	}
	public void setDevice_id(DeviceInfo device_id) {
		this.device_id = device_id;
	}
	
	public JMDeviceDataHistory getJmdevice_id() {
		return jmdevice_id;
	}
	public void setJmdevice_id(JMDeviceDataHistory jmdevice_id) {
		this.jmdevice_id = jmdevice_id;
	}
	
	public String getIp_add() {
		return ip_add;
	}
	public void setIp_add(String ip_add) {
		this.ip_add = ip_add;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	//NEW UPDATE: USE PTZ instead of JM for angle.
	public String getAntennaid() {
		return antennaid;
	}
	
	public void setAntennaid(String antennaid) {
		this.antennaid = antennaid;
	}
	
	public String getSector() {
		return sector;
	}
	
	public void setSector(String sector) {
		this.sector = sector;
	}
	
	public String getAngle() {
		return angle;
	}
	
	public void setAngle(String angle) {
		this.angle = angle;
	}
	
	public int getCurrentconfigid() {
		return currentconfigid;
	}
	
	public void setCurrentconfigid(int currentconfigid) {
		this.currentconfigid = currentconfigid;
	}
	
	public boolean getAlarm() {
		return alarm;
	}
	
	public void setAlarm(boolean alarm) {
		this.alarm = alarm;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Date getInserttime() {
		return inserttime;
	}
	
	public void setInserttime(Date inserttime) {
		this.inserttime = inserttime;
	}
	
	public String getTechnology() {
		return technology;
	}
	
	public void setTechnology(String technology) {
		this.technology = technology;
	}
	
	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLon() {
		return lon;
	}
	
	public void setLon(double lon) {
		this.lon = lon;
	}
	
}
