package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="frequencyconfiguration_data")
public class MaskingScreenData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="frequency")	
	private double frequency;
	
	@Column(name="bandwidth")	
	private double bandwidth;
	
	@Column(name="freqtype")
	private String freqtype;
	
	@Column(name="profile")
	private String profile;

	public MaskingScreenData createCopy(MaskingScreenData md, String profile) {
		MaskingScreenData md1 = new MaskingScreenData();
		md1.setFreqtype(md.getFreqtype());
		md1.setBandwidth(md.getBandwidth());
		md1.setFrequency(md.getFrequency());
		md1.setProfile(profile);
		return md1;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	public double getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(double bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getFreqtype() {
		return freqtype;
	}

	public void setFreqtype(String freqtype) {
		this.freqtype = freqtype;
	}
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	public String toString() {
		return "frequency :"+frequency+" bandwidth :"+bandwidth+" freqtype :"+freqtype;
	}
	
		
	
}
