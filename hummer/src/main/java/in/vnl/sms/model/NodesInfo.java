package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;
import javax.persistence.Table;

@Entity
@Immutable
@Table(name="nodes_info")
public class NodesInfo 
{
	
	
	@Id
	@Column(name="ip_add")
	private String ipAdd;
	private String name;
	
	
	public NodesInfo() 
	{
		
	}


	public NodesInfo(String ipAdd, String name) {
		super();
		this.ipAdd = ipAdd;
		this.name = name;
	}


	public String getIpAdd() {
		return ipAdd;
	}


	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
}
