package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ptz_info")
public class PTZData {

	
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	/*@Column(name="id")
	private Long id;*/
	
	@Id
	@Column(name="device_ip")	
	private String device_ip;
	
	@Column(name="ptz_ip")
	private String ptz_ip;
	
	@Column(name="ptz_offset")
	private String ptz_offset;
	
	@Column(name="sector_offset")
	private String sector_offset;
	
	@Column(name="name")
	private String name;

	public String getDevice_ip() {
		return device_ip;
	}

	public void setDevice_ip(String device_ip) {
		this.device_ip = device_ip;
	}

	public String getPtz_ip() {
		return ptz_ip;
	}

	public void setPtz_ip(String ptz_ip) {
		this.ptz_ip = ptz_ip;
	}


	public String getPtz_offset() {
		return ptz_offset;
	}

	public void setPtz_offset(String ptz_offset) {
		this.ptz_offset = ptz_offset;
	}
	
	public String getSector_offset() {
		return sector_offset;
	}

	public void setSector_offset(String sector_offset) {
		this.sector_offset = sector_offset;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
	
}
