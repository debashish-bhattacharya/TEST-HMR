package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="min_threshold")
public class FreqPowMinThresData {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="startfreq")	
	private int startfreq;
	
	@Column(name="stopfreq")	
	private int stopfreq;
	
	@Column(name="power")
	private int power;
	
	@Column(name="attenuation")
	private int  attenuation;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public int getStartfreq() {
		return startfreq;
	}
	
	public void setStartFreq(int startfreq) {
		this.startfreq = startfreq;
	}
	
	public int getStopfreq() {
		return stopfreq;
	}
	
	public void setStopFreq(int stopfreq) {
		this.stopfreq = stopfreq;
	}
	
	public int getPower() {
		return power;
	}
	
	public void setPower(int power) {
		this.power = power;
	}
	
	public int getAttenuation() {
		return attenuation;
	}
	
	public void setAttenuation(int attenuation) {
		this.attenuation = attenuation;
	}
}

