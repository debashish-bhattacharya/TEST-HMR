package in.vnl.sms.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Table;

@Entity(name="alarms")
public class Alarm 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long 	id;
	private Long 	generationTime;
	private int 	severity;
	private int 	managedobjectType;
	private int 	eventType;	
	private int 	status;	
	private String 	ip;
	private String 	componentId;	
	private String 	mangedojectId;	
	private String 	eventId;
	private String 	eventDesctiption;
	private String 	componentType;
	private Date 	logtime;
	public Alarm() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Alarm(Long id, Long generationTime, int severity, int managedobjectType, int eventType, int status,
			String ip, String componentId, String mangedojectId, String eventId, String eventDesctiption,
			String componentType, Date logtime) {
		super();
		this.id = id;
		this.generationTime = generationTime;
		this.severity = severity;
		this.managedobjectType = managedobjectType;
		this.eventType = eventType;
		this.status = status;
		this.ip = ip;
		this.componentId = componentId;
		this.mangedojectId = mangedojectId;
		this.eventId = eventId;
		this.eventDesctiption = eventDesctiption;
		this.componentType = componentType;
		this.logtime = logtime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getGenerationTime() {
		return generationTime;
	}
	public void setGenerationTime(Long generationTime) {
		this.generationTime = generationTime;
	}
	public int getSeverity() {
		return severity;
	}
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	public int getManagedobjectType() {
		return managedobjectType;
	}
	public void setManagedobjectType(int managedobjectType) {
		this.managedobjectType = managedobjectType;
	}
	public int getEventType() {
		return eventType;
	}
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	public String getMangedojectId() {
		return mangedojectId;
	}
	public void setMangedojectId(String mangedojectId) {
		this.mangedojectId = mangedojectId;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getEventDesctiption() {
		return eventDesctiption;
	}
	public void setEventDesctiption(String eventDesctiption) {
		this.eventDesctiption = eventDesctiption;
	}
	public String getComponentType() {
		return componentType;
	}
	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}
	public Date getLogtime() {
		return logtime;
	}
	public void setLogtime(Date logtime) {
		this.logtime = logtime;
	}
	
	
	
}
