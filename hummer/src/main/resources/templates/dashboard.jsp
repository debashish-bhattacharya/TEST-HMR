<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/lib/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="css/lib/fontawesome-all.min.css">
<link rel="stylesheet" href="css/lib/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="css/lib/leaflet.css">
<link rel="stylesheet" href="css/lib/leaflet.fullscreen.css">
<link href="css/dashboard.css" rel="stylesheet" />
<title></title>
<script>
	var remoteAddr = '<%= request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() %>';
</script>
</head>
<body>
<div class="container">

<div class="row">
	<div class="col-md-5" >
		<div class="row" >
			<div class="col-md-12">
				
			    <div class="card">
					  <div class="card-header">
					    Device Info <a id="add_node" data-toggle="modal" data-target="#myModal"><span><i class="fa fa-plus" aria-hidden="true"></i> Add another device</span></a>
					  </div>
					  <div class="card-body">
					    <!-- Network configuration -->
						<div class="row">
							<div class="col-md-8">
								<ul id="nodes_box" class="node_list_box">
									<!--  <li><span class="color-symbol"></span><span><input type="checkbox"></span><span>Node One</span></li>-->
								</ul>
							</div>
							<div class=col-md-4>
								<div id="node_status" class="not_reachable"></div>
								<!--  <div id="def_container" class="def_container">
									<button class="btn btn-info"  data-toggle="modal" data-target="#def_modal">DEF</button>
								</div>-->
								<!--<div id="led_status" class="lead_status_class"></div>-->
							</div>
						</div>
					  </div>
					</div>
			        
				
										
			</div>
		</div>
		<div class="row" id="status_container">
			<div class="col-md-12">
				
				<div class="card">
					  <div class="card-header">
					    Statistics
					  </div>
					  <div class="card-body">
						<div class="row">
							<div class="col-md-12 ">
								
							<ul class="nav nav-tabs" id="myTab" role="tablist">
							  <li class="nav-item">
							    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#detected_emitters" role="tab" aria-controls="home" aria-selected="true">Detected</a>
							  </li>
							  <li class="nav-item">
							    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#node_config" role="tab" aria-controls="profile" aria-selected="false">Config</a>
							  </li>
							</ul>
							<div class="tab-content" id="myTabContent">
							  <div class="tab-pane fade show active" id="detected_emitters" role="tabpanel" aria-labelledby="home-tab">
							  	<table class="table status_table_head"><thead><th>Node</th><th>Power(dBm)</th><th>Freq(MHz)</th><th>Time(yyyy-mm-dd HH:MM:SS)</th></thead></table>
									<div id="status_tab_container">
									<table class="table status_table">
										<!--  <thead>
											<th colspan=4></th>							
										</thead>-->
										<tbody>
											
										</tbody>
									</table>
								</div>	
							  </div>
							  <div class="tab-pane fade" id="node_config" role="tabpanel" aria-labelledby="profile-tab">
							  	<table class="table">
									<!--  <thead>
										<th colspan=3></th>
									</thead>-->
									<tbody>
										
										<tr><td>Start Frequency(MHz)</td><td><input type="hidden" id="current_node"><input type="text" value="0" id="start_freq" /></td><td>[0,3000]</td></tr>
										<tr><td>Stop Frequency(MHz)</td><td><input type="text" value="0" id="stop_freq" /></td><td>[0,3000]</td></tr>
										<tr><td>Threshold Level(dBm)</td><td><input type="text" value="0" id="threshold" /></td><td>[-100,-10]</td></tr>
										<tr><td>Mask Offset(dB)</td><td><input type="text" value="0" id="mask_offset" /></td><td>[-10,+20]</td></tr>
										<tr><td>Cable Length(M)</td><td><input type="text" value="0" id="cable_length" /></td><td>[0,100]</td></tr>
										<tr><td>Preamp Type</td><td><input type="text" value="0" id="preamp_type" /></td><td>[0,9]</td></tr>
										
										<tr><td>Start Time</td><td><input type="text" value="0" id="config_start_time" /></td><td></td></tr>
										<tr><td>Stop Time</td><td><input type="text" value="0" id="config_stop_time" /></td><td></td></tr>
										<tr><td>Country Code</td><td><input type="text" value="0" id="country_code" /></td><td></td></tr>
										
										<tr><td><input type="checkbox" value="" id="gsm_dl" />&nbspGSM DL</td><td><input type="checkbox" value="" id="wcdma_dl" />&nbspWCDMA DL</td><td><input type="checkbox" value="" id="wifi" />&nbspWi-Fi</td></tr>
										<tr><td><input type="checkbox" value="" id="use_mask" />&nbspUse Mask</td><td><input type="checkbox" value="" id="lte" />&nbspLTE</td><td><input type="checkbox" value="" id="calibrate" />&nbspCalibrate</td></tr>
										<tr><td colspan=3 style="text-align:center;">Fillter</td></tr>
										<tr><td>Start Frequency</td><td>Stop Frequency</td><td>Status</td></tr>
										<tr><td><input type="text" value="0" id="start_1" /></td><td><input type="text" value="0" id="stop_1" /></td><td><input type="checkbox" id="start_stop_check_1" />Apply</td></tr>
										<tr><td><input type="text" value="0" id="start_2" /></td><td><input type="text" value="0" id="stop_2" /></td><td><input type="checkbox" id="start_stop_check_2" />Apply</td></tr>
										<tr><td><input type="text" value="0" id="start_3" /></td><td><input type="text" value="0" id="stop_3" /></td><td><input type="checkbox" id="start_stop_check_3" />Apply</td></tr>
										<tr><td><input type="text" value="0" id="start_4" /></td><td><input type="text" value="0" id="stop_4" /></td><td><input type="checkbox" id="start_stop_check_4" />Apply</td></tr>
										<tr><td><input type="text" value="0" id="start_5" /></td><td><input type="text" value="0" id="stop_5" /></td><td><input type="checkbox" id="start_stop_check_5" />Apply</td></tr>
										<tr><td><input type="text" value="0" id="start_6" /></td><td><input type="text" value="0" id="stop_6" /></td><td><input type="checkbox" id="start_stop_check_6" />Apply</td></tr>
										<tr><td colspan=3 id="button_row" class="btn-row"><button class="btn btn-info"  onclick="updateConfig()">Apply</button></td></tr>
										
									</tbody>
								</table>
							  </div>
									 
							</div>
								
							
							</div>
						</div>
					  </div>
					</div>
				
										
			</div>
		</div>
				<!--  <div class="row card" >
			<div class="col-md-12">
				
				<div class="row">
					<div class="col-md-12 card-head" >
						<h2>System Configuration</h2>
					</div>
				</div>
				<div class="row">
					<div class=col-md-12>
						
						<table class="table system_table">
							<thead>
								<th colspan=4></th>
							</thead>
							<tbody>
								<tr><td>Maximize Signal at freq</td>
									<td>
										<select>
											<option value=30>30 MHz</option>
											<option value=50>40 MHz</option>
											<option value=40>50 MHz</option>
										</select>
									</td></tr>
								<tr><td>Steps of rotation</td><td><input type="text" id="degree_of_rotation" value=""/></td></tr>
								<tr><td>Rotation Time Intervel</td><td><input type="text" id="degree_of_rotation" value=""/>(sec)</td></tr>
							</tbody>
						</table>
					</div>
				</div>						
			</div>
		</div>-->
	</div>
	<div class=col-md-7>
			<div class="card"	>
			  <div class="card-body">
		<div class="row">
			<div class=col-md-12>
				<div id="map_leaf"></div>				
			</div>
		</div>
		<div class="row">
			<div class=col-md-12>
				<!-- Graph area -->
				<canvas id="canvas"></canvas>
			</div>
		</div>
		</div>
		</div>	
	</div>

</div>
	<div class="row status-row">
	<div class="col-md-12">
		<table class="table table-bordered status_bar">
			<thead>
					<th>Start Freq<span>(MHz)</span></th>
					<th>Stop freq<span>(MHz)</span></th>
					<th>Power Threshold<span>(dBm)</span></th>
					<th>GSM DL</th>
					<th>WCDMA DL</th>
					<th>LTE</th>
					<th>WiFi</th>
					<!-- <th>Sweep Time</th> -->
					<th>Mask Offset</th>
					<th>Start Time</th>
				
			</thead>
			<tbody>
			
				<!--  <tr>
					<td>Start Freq&nbsp;:&nbsp;<span id="start_freq_status" class="status_bar">1000</span><span>(MHz)</span></td>
					<td>Stop freq&nbsp;:&nbsp;<span id="stop_freq_status" class="status_bar">1000</span><span>(MHz)</span></td>
					<td>Power Threshold&nbsp;:&nbsp;<span id="threshold_status" class="status_bar">1000</span><span>(dBm)</span></td>
					<td>GSM Downlink&nbsp;:&nbsp;<span id="gsm_status" class="status_bar">OFF</span></td>
					<td>WCDMA Downlink&nbsp;:&nbsp;<span id="wcdma_status" class="status_bar">OFF</span></td>
					<td>LTE&nbsp;:&nbsp;<span id="lte_status" class="status_bar">OFF</span></td>
					<td>WiFi&nbsp;:&nbsp;<span id="wifi_status" class="status_bar">OFF</span></td>
					 <td>Sweep Time&nbsp;:&nbsp;<span id="sweep_status" class="status_bar">0.45637</span></td>
					<td>Mask Offset&nbsp;:&nbsp;<span id="mask_status" class="status_bar">OFF</span></td>
					<td>Start Time&nbsp;:&nbsp;<span id="start_status" class="status_bar">27-3-5 45:45:45</span></td>
				</tr>-->
				
				<tr>
					<td>Start Freq<span>(MHz)</span></td>
					<td>Stop freq<span>(MHz)</span></td>
					<td>Power Threshold<span>(dBm)</span></td>
					<td>GSM Downlink</td>
					<td>WCDMA Downlink</td>
					<td>LTE</td>
					<td>WiFi</td>
					<!--  <td>Sweep Time</td>-->
					<td>Mask Offset</td>
					<td>Start Time</td>
				</tr>
				
				<tr>
					<td><span id="start_freq_status" class="status_bar"></span></td>
					<td><span id="stop_freq_status" class="status_bar"></span></td>
					<td><span id="threshold_status" class="status_bar"></span></td>
					<td><span id="gsm_status" class="status_bar"></span></td>
					<td><span id="wcdma_status" class="status_bar"></span></td>
					<td><span id="lte_status" class="status_bar"></span></td>
					<td><span id="wifi_status" class="status_bar"></span></td>
					<!-- <td><span id="sweep_status" class="status_bar"></span></td>-->
					<td><span id="mask_status" class="status_bar"></span></td>
					<td><span id="start_status" class="status_bar"></span></td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>

<div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Device</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <table class="table" id="add_node_form">
          	<thead>
          		<th colspan=2></th>
          	</thead>
          	<tbody>
          		<tr>
          			<td>Name</td>
          			<td><input type="text" id="node_name" /></td>
          		</tr>
          		<tr>
          			<td>IP</td>
          			<td><input type="text" id="node_ip" /></td>
          		</tr>
          		<tr>
          			<td>Color</td>
          			<td><input type="text" id="node_color" /><span class="input-group-addon"><i></i></span></td>
          		</tr>
          		<tr>
          			<td>Lat</td>
          			<td><input type="text" id="node_lat" /></td>
          		</tr>
          		<tr>
          			<td>Lon</td>
          			<td><input type="text" id="node_lon" /></td>
          		</tr>
          		<tr>
          			<td colspan=2 class="btn-row"><button class="btn btn-info" onclick="addNode()">Save</button></td>
          		</tr>
          	</tbody>
          </table>
        </div>
        
      </div>
    </div>
  </div>
</div>


<div class="modal" id="def_modal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">DEF</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <table class="table" id="def_form">
          	<thead>
          		<th colspan=2></th>
          	</thead>
          	<tbody>
          		<tr>
          			<td>Antena Rotation </td>
          			<!--  <td>From&nbsp;:&nbsp;<input type="text" id="start_angle" />&nbsp; to &nbsp;<input type="text" id="end_angle"/></td>-->
          			<td><input type="numeric" id="start_angle" placeholder="Start Angle"/>&nbsp; to &nbsp;<input type="numeric" id="end_angle" placeholder="End Angle"/></td>
          		</tr>
          		<tr>
          			<td>Frequencey</td>
          			<td><input type="numeric" id="def_freq" /></td>
          		</tr>
          		<tr>
          			<td colspan=2 class="btn-row"><button class="btn btn-info" onclick="startDef()">Start</button></td>
          		</tr>
          	</tbody>
          </table>
        </div>
        
      </div>
    </div>
  </div>
</div>



<!--  SCRIPT ATTACHMENT SECTION -->
<script src="js/lib/jquery-3.3.1.min.js" ></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzeZS7SkEdznPknWj0AgRRbd9dJYKTtz4"></script>
<script src="js/lib/sockjs.min.js" ></script>
<script src="js/lib/stomp.min.js" ></script>
<script src="js/lib/bootstrap.min.js" ></script>
<script src="js/lib/Chart.bundle.min.js" ></script>
<script src="js/lib/bootstrap-colorpicker.min.js" ></script>

<script src="js/lib/leaflet.js" ></script>
<script src="js/lib/Leaflet.GoogleMutant.js" ></script>
<script src="js/lib/leaflet.geometryutil.js" ></script>

<script src="js/lib/Leaflet.fullscreen.min.js" ></script>

<script src="js/lib/turf.min.js" ></script>
<script src="js/urls.js"></script>
<script src="js/dashboard.js"></script>
<!--  SCRIPT ATTACHMENT SECTION -->
</body>
</html>