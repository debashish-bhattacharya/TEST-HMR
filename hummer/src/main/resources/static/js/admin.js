var currGroup="";
var globalGrandParentNode="";
var globalParentNode="";
var globalCurrentNode="";
var globalReportTypeSelected="";
var scannedServerData = [];
var cdrServerData = [];
var widsServerData = [];
var utcStartTime="";
var utcEndTime="";
var profile=[];
var user;
var sensorConfigDeploy=[];

$(document).ready(function(){
	user = $("#user").val();
	$('#target_scan').hide();
	$(".dateSelect").datetimepicker({
		dateFormat: "yy-mm-dd",
		timeFormat: 'HH:mm'
	}); 
	loadJSTree();

	$("#downloadReport").click(function(){
		getReport();
	});
	
	$("#purgeData").click(function(){
		purgeData();
	});
	
	$("#downloadDb").click(function(){
		getDbBackup();
	});
	
	$("#downloadSysLog").click(function(){
		getSysLog();
	});
	
	$("#resetAll").click(function(){
		resetAll();
	});
	
	$("#factoryReset").click(function(){
		factoryConfigurationReset();
	});
	
	$("#changePasswordButton").click(function(){
		updatePassword($('#userDataSelect').val());
	});
	
	$("#saveProfile").click(function(){
		var pro = $('#saveProfileName').val();
		if(pro=="")
		{
			swal("Error","Profile name cannot be blank","error");
			return;
		}
		for(var i=0;i<profile.length;i++){
			if(profile[i]==pro){
				swal("Error","Profile name already exist","error");
				return;
			}
		}
		profileOperation('save', $("#saveProfileName").val());
	});
	
	$("#showProfile").click(function(){
		var profile = $('#profileSelect').val();
		if(profile==null)
		{
			swal("Error","Profile name cannot be blank","error");
			return;
		}
		showProfile($('#profileSelect').val());
	});
	
	$("#applyProfile").click(function(){
		var profile = $('#profileSelect').val();
		if(profile==null)
		{
			swal("Error","Profile name cannot be blank","error");
			return;
		}
		profileOperation('apply', $('#profileSelect').val());
	});
	
	$("#deleteProfile").click(function(){
		var profile = $('#profileSelect').val();
		if(profile==null)
		{
			swal("Error","Profile name cannot be blank","error");
			return;
		}
		profileOperation('delete',$('#profileSelect').val());
	});
	
	getAllSystemUsers();
	getAllProfiles();
	getBackupHistory();
	createDeploymentGrid("load");
	connectSocket();
});


var connectSocket = function()
{

    var stompClient = null;

	xData = [];
    var socket = new SockJS(endPoint.socket);

    stompClient = Stomp.over(socket);
	stompClient.debug = null
    stompClient.connect({}, function (frame)
            {

            stompClient.subscribe(topics.configdeploy, function (data) {
            	createDeploymentGrid("socket");			
            });
			
            stompClient.subscribe(topics.DbStatus, function (data) 
        			{
        				getBackupHistory();
        				swal({
        	        	     title: "Success",
        	        	     text: data.body,
        	        	     type: "success",
        	        	     timer: 2000,
        	        	     buttons:false,
        	        	     className: "right-bottom"
        	        	     });
        				
        			});
            
            });

}



function validateIpAddress(value, colname) {
	var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	var result = value.match(ipformat);
	
	if (result == null) 
		   return [false,"IP " + value + " Is Not Valid"];
		else 
		   return [true,""];
		}


var createDeploymentGrid = function(callType)
{
	
	$.ajax({
		url: serviceUrls.sensorConfigDeploy,
        dataType:"json",
        success:function(data)
        {	           
        	sensorConfigDeploy=data;
        	if(callType=="load"){
        	deployGrid(sensorConfigDeploy);
        	}else{
        		$("#configDeployDiv").jqGrid("clearGridData", true)
        		jQuery("#configDeployDiv").jqGrid('setGridParam', {  datatype: 'local',  data:sensorConfigDeploy  }).trigger("reloadGrid");
        	}
        },
        error:function(data){
        	swal("Error","Sensor Configuration Cannot Be Loaded","error");
        }

     });
}
	
	
	var deployGrid = function(deployData) {
        $("#configDeployDiv").jqGrid({
                data : deployData,
                datatype : "local",
                type : 'POST',
                colNames : ['Sensor', 'Status', 'IP', 'Port','Rotating','OMC IP','RF Switch IP','Angle', 'Sector', 'Beamwidth','GSM Server IP', 'Active Status'],          				
				colModel : [ {
                        name : 'sensornum',
                        index : 'sensornum',
                        edittype : 'select',
                        editoptions:{value:{1:'1', 2:'2' , 3:'3', 4:'4'}},
						width : 60,
						editable: true
                },{
                        name : 'sensorenabled',
                        index : 'sensorenabled',
                        edittype : 'select',
                        editoptions:{value:{Enabled:'Enabled', Disabled:'Disabled'}},
                        width : 90,
                        editable: true,
                                                
                },{
                    name : 'sensorip',
                    index : 'sensorip',
                    width : 100,
                    editrules:{custom: true, custom_func: validateIpAddress},
                    editable: true
                    
                },{
                        name : 'sensorport',
                        index : 'sensorport',
                        edittype : 'select',
                        editoptions:{value:{1:'1', 2:'2', 3:'3'}},
						width : 50,
						editable: true
						
                },{
                    name : 'rotatingsensor',
                    index : 'rotatingsensor',
                    edittype : 'select',
                    editoptions:{value:{No:'No', Yes:'Yes'}},
					width : 65,
					editable: true
			
                },{
                    name : 'omcip',
                    index : 'omcip',
					width : 100,
					editrules:{custom: true, custom_func: validateIpAddress},
					editable: true
					
                },{
                    name : 'rfswitchip',
                    index : 'rfswitchip',
					width : 100,
					editrules:{custom: true, custom_func: validateIpAddress},
					editable: true
					
                },{
                    name : 'angle',
                    index : 'angle',
					width : 65,
					editable: false
					
                },{
                    name : 'sector',
                    index : 'sector',
					width : 65,
					edittype : 'select',
                    editoptions:{value:{1:'1', 2:'2', 3:'3' , STRU:'STRU'}},
					editable: true
                },{
                    name : 'beamwidth',
                    index : 'beamwidth',
                    width : 90,
                    resizable: false,
                    edittype : 'select',
                    editoptions:{value:{30:'30', 60:'60'}},
					editable: true
					
                },{
                        name : 'gsmserverip',
                        index :'gsmserverip',
                        width:	110,
                        editrules:{custom: true, custom_func: validateIpAddress},
                        editable: true
                },{
                    name : 'usestatus',
                    index :'usestatus',
                    edittype : 'select',
                    editoptions:{value:{Yes:'Yes', No:'No'}},
                    width:	100,
                    editable: true
            }],
                width: 'auto',
                loadOnce : true,
                toppager: false,
                viewrecords: true,
                rowNum : 4,
                pager : '#configDeployDivPager',
                editurl: serviceUrls.editSensorConfig,
                gridview : true,
                caption : 'Sensor Configuration Deployment'
                
        });
        
        
        
        $('#configDeployDiv').navGrid('#configDeployDivPager',
                // the buttons to appear on the toolbar of the grid
                { edit: true, add: true, del: false, search: false, refresh: false, view: false, position: "left", cloneToTop: false },
                    
                /*Options For Editing Records*/
                {
                    height: 'auto',
                    width: 'auto',
                    editCaption: "Change Sensor Configuration",
                    recreateForm: true,
                    closeAfterEdit: true,
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                
                },
                
                /*Options For Adding Records*/
                {
                	 height: 'auto',
                     width: 'auto',
                     addCaption: "Add Sensor Configuration",
                     recreateForm: true,
                     closeAfterAdd: true,
                     errorTextFormat: function (data) {
                         return 'Error: ' + data.responseText
                     }
                
                });
        
}


	
var updatePassword =function(type)
	{
	var changedPassword =  $("#newAdminPassword").val();
	var confirmPassword = $("#confirmAdminPassword").val();
	var match = false;
	var oldPassword = "not applicable";
	var userId = $('#userDataSelect').val();
    if(changedPassword=="" || confirmPassword=="")
    {
       swal("Error","Password cannot be blank","error");
    }
	else if(changedPassword == confirmPassword)
	{
		match = true;
	}
	else
	{
		swal("Error","Confirm New Password mismatch","error");
	}
	if(match==true)
	{
		$.ajax
		({
			url:serviceUrls.updatePassword,
			async:true,
			type: "post",
			data:
			{
				"type":type,
				"userId":userId,
				"oldPassword":oldPassword,
				"changedPassword":changedPassword,
				"confirmPassword":confirmPassword
			},
			success:function(data)
			{  
				if(data.result=="success")
				{
					swal("Success","Password Changed Successfully","success");
					window.location.reload();
					
				}else{
					alert(data.message);
				}
			},
			error:function()
			{
				alert("Change password failed", function() {	});
			}
		});
	}
}

var getBackupHistory = function(){
	$.ajax
	({
		url:serviceUrls.getBackupHistory,
		type: "post",
		success:function(data)
		{ 
			$("#backupHistoryTableTbody").html("");
			var rows="";
			//var fileArray=data.fileNames;
			for(var i=0;i<data.length;i++){
				rows+='<tr><td>'+data[i]+'</td>'+
					  '<td><input type="button" class="backupButton" value="Download" id="downloadBackupHistory" onclick="downloadBackupHistory(\''+data[i]+'\')" /></td>'+
					  '<td><input type="button" class="backupButton" value="Delete" id="deleteBackupHistory" onclick="deleteBackupHistory(\''+data[i]+'\')" /></td></tr>';
			}
			$("#backupHistoryTableTbody").html(rows);
		},
		error:function()
		{
			//alert("Error in getting backup history", function() {	});
			console.log("Error in getting backup history");
		}
	});

}

var downloadBackupHistory = function(fileName){

	//window.location.href='../../resources/'+dirName+'/'+fileName;
	$('#loadingBox').show();
	$('#backupHistoryDiv').hide();
	$.ajax({
		url: serviceUrls.downloadBackupReport,
		type:"post",
		data:{"fileName":fileName},
		xhrFields: {
			responseType: 'blob'
		},
		beforeSend:function()
		{
		},
		success: function (data, status, request) {
			filename = request.getResponseHeader('filename');
			$('#loadingBox').hide();
			$('#backupHistoryDiv').show();
			var a = document.createElement('a');
			var url = window.URL.createObjectURL(data);
			a.href = url;
			a.download = filename;
			a.click();
			window.URL.revokeObjectURL(url);
		},
		complete:function()
		{
		}
	});

}

var deleteBackupHistory = function(fileName){
	$.ajax
	({
		url:serviceUrls.deleteBackup,
		type:'post',
		data:{"fileName":fileName},
		success:function(data)
		{
			if(data.result=="success"){
				swal("Success","Backup Deleted Successfully","success");
				//window.location.reload();
				getBackupHistory();
			}else{
				swal("Error","Problem in deleting Backup","error");
			}
		},
		error:function(data){
			alert("Problem in deleting Backup");
		}
	});
}

function loadJSTree(){
//var sufiData=getSufiData();
	var child;
	if(user == "superadmin"){
		child = [
			{"data":"User Management","attr":{"rel":"2ndLevel","id":"changePasswordId"}},
			{"data":"Data Backup/Reset","attr":{"rel":"2ndLevel","id":"dataBackupId"}},
			{"data":"Backup History","attr":{"rel":"2ndLevel","id":"backupHistoryId"}},
			{"data":"Configuration Save/Apply","attr":{"rel":"2ndLevel","id":"profileId"}},
			{"data":"Configuration Deployment","attr":{"rel":"2ndLevel","id":"configDeploy"}}
			]
	}
	
	else{
		child = [
			{"data":"User Management","attr":{"rel":"2ndLevel","id":"changePasswordId"}},
			{"data":"Data Backup/Reset","attr":{"rel":"2ndLevel","id":"dataBackupId"}},
			{"data":"Backup History","attr":{"rel":"2ndLevel","id":"backupHistoryId"}},
			{"data":"Configuration Save/Apply","attr":{"rel":"2ndLevel","id":"profileId"}}
			]
	}
	
 
	$("#leftReportTree").jstree({
                    "json_data" :{
					"data":[{
					"data":"Admin",
					"attr":{"rel":"1stLevel","id":"adminId"},
					"children": child
					}
					]
					},
         "types": {
             'types': {
                 '1stLevel': {
                     'icon': {
                         'image': '../../'+"images/RootContainer.gif"
                     }
                 },
                 "2ndLevel": {
                     "icon": {
                         "image": '../../'+"images/job.gif"
                     }
                 },
                 "3rdLevel": {
                     'icon': {
                         'image': '../../'+"resources/images/ne.gif"
                     }
                 },
				 "4thLevel": {
                     "icon": {
                         "image": '../../'+"resources/images/ViewNode.gif"
                     }
                 }
             }
         },
                    "plugins" : ["themes", "json_data", "ui", "contextmenu", "crrm", "types","sort","state"]
                }).bind("select_node.jstree", function(e, data)
				{
				console.log("select node jstree");
				//var cuid=$('#leftTree').jstree('get_selected').attr('id');
				//var cuattr=$('#leftTree').jstree('get_selected').attr('rel');
				    var leftTreeSelectedNode = data.rslt.obj;
                	var list = data.rslt.obj.parents('ul');
					var parentNode = ($(list[0]).prev('a')).text();
					parentNode=parentNode.trim();
					var grandParentNode = ($(list[1]).prev('a')).text();
					grandParentNode=grandParentNode.trim();
					//var currentNode=$("#leftTree").jstree("get_selected").text();
					var currentNode=$('#leftReportTree .jstree-clicked').text();
					currentNode=currentNode.trim();
					console.log('curr is :'+currentNode);
					globalGrandParentNode=grandParentNode;
					globalParentNode=parentNode;
					globalCurrentNode=currentNode;
					var currentNodeLevel=$('#leftReportTree').jstree('get_selected').attr('rel');
					
					if(currentNode=='Admin'){
						$('#changePasswordDiv').show();
						$('#backupPurgeDiv').hide();
						$('#backupHistoryDiv').hide();
						$('#profileConfigure').hide();
						$('#configDeployment').hide();
					}else if(parentNode=='Admin' && currentNode=='User Management'){
						$('#changePasswordDiv').show();
						$('#backupPurgeDiv').hide();
						$('#backupHistoryDiv').hide();
						$('#profileConfigure').hide();
						$('#configDeployment').hide();
						
					}else if(parentNode=='Admin' && currentNode=='Data Backup/Reset'){
						showBackupPurgeView();
						$('#configDeployment').hide();
					}else if(parentNode=='Admin' && currentNode=='Configuration Save/Apply'){
						$('#changePasswordDiv').hide();
						$('#backupPurgeDiv').hide();
						$('#backupHistoryDiv').hide();
						$('#profileConfigure').show();
						$('#configDeployment').hide();
					}else if(parentNode=='Admin' && currentNode=='Configuration Deployment'){
						$('#changePasswordDiv').hide();
						$('#backupPurgeDiv').hide();
						$('#backupHistoryDiv').hide();
						$('#profileConfigure').hide();
						$('#configDeployment').show();

					}else{
						$('#changePasswordDiv').hide();
						$('#backupPurgeDiv').hide();
						$('#backupHistoryDiv').show();
						$('#profileConfigure').hide();
						$('#configDeployment').hide();
					}
				});
				
	    $('#leftReportTree').bind("loaded.jstree", function (event, data) {
		
		$(this).jstree("open_all");
    });
	
	$("#leftReportTree").delegate("a", "dblclick", function (e) {
		$("#leftReportTree").jstree("toggle_node", this);
	});
	$('#leftReportTree li ul').css("color","red");
	$('#leftReportTree li a').css("background-color","green");
}

var showBackupPurgeView = function(){
	$('#changePasswordDiv').hide();
	$('#backupPurgeDiv').show();
	$('#backupHistoryDiv').hide();
	$('#profileConfigure').hide();
	$('#operationType').text('Data Backup/Reset');
}

function getDbBackup(){
	
	$('#loadingBox').show();
	$('#backupPurgeDiv').hide();
	$.ajax({
		url: serviceUrls.dbBackup,
		type:"post",
		data:{},
		success:function(data)
		{
			if(data.result=="success"){
				$('#loadingBox').hide();
				$('#backupPurgeDiv').show();
				swal("Success","Full Backup scheduled successfully. It will be available under backup history when completed","success");
			}else{
				swal("Error","Problem in creating backup Successfully","error");
			}						
		},
		error:function(data){
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			data = JSON.parse(data.responseText);
			responseAlert(data,false);
		}
	});
}

/*xhrFields: {
			responseType: 'blob'
		},
		beforeSend:function()
		{
		},
		success: function (data) {
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			var a = document.createElement('a');
			var url = window.URL.createObjectURL(data);
			a.href = url;
			a.download = 'dbbackup.zip';
			a.click();
			window.URL.revokeObjectURL(url);
		},
		complete:function()
		{
		}*/

function getSysLog(){
	
	$('#loadingBox').show();
	$('#backupPurgeDiv').hide();
	$.ajax({
		url: serviceUrls.sysLog,
		type:"post",
		data:{},
		xhrFields: {
			responseType: 'blob'
		},
		beforeSend:function()
		{
		},
		success: function (data, status, request) {
			filename = request.getResponseHeader('filename');
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			var a = document.createElement('a');
			var url = window.URL.createObjectURL(data);
			a.href = url;
			a.download = filename;
			a.click();
			window.URL.revokeObjectURL(url);
		},
		complete:function()
		{
		}
	});
}

function getReport(){
	if($("#startTime").val() == null || $("#startTime").val() == "")
	{
	swal("Error","Please Fill the Start Date","error");
	return;
	}
    if($("#endTime").val() == null || $("#endTime").val() == "")
	{
	swal("Error","Please Fill the End Date","error");
	return;
	}
	//utcStartTime=toUtcTime($("#startTime").val());
	//utcEndTime=toUtcTime($("#endTime").val());
	
	var hours = Math.abs(Date.parse($("#startTime").val()) - Date.parse($("#endTime").val())) / 36e5;
	/*if(hours>1){
		alert("Please download report for less than 1 hours");
		return;
	}*/
	
	$('#loadingBox').show();
	$('#backupPurgeDiv').hide();
	$.ajax({
		url: serviceUrls.fetchReport,
		type:"post",
		data:{"START_TIME":$("#startTime").val(),"STOP_TIME":$("#endTime").val(),"REPORT_TYPE":"1"},
		xhrFields: {
			responseType: 'blob'
		},
		beforeSend:function()
		{
		},
		success: function (data, status, request) {
			filename = request.getResponseHeader('filename');
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			var a = document.createElement('a');
			var url = window.URL.createObjectURL(data);
			a.href = url;
			a.download = filename;
			a.click();
			window.URL.revokeObjectURL(url);
		},
		complete:function()
		{
		},
		error:function(data)
		{
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			data = JSON.parse(data.responseText);
			responseAlert(data,false);
		}
	});
}

function purgeData(){
	if($("#startTime").val() == null || $("#startTime").val() == "")
	{
	swal("Error","Please Fill the Start Date","error");
	return;
	}
	if($("#endTime").val() == null || $("#endTime").val() == "")
	{
	swal("Error","Please Fill the End Date","error");
	return;
	}
	
	$('#loadingBox').show();
	$('#backupPurgeDiv').hide();
	$.ajax({
		url:serviceUrls.dataPurge,
		data:{"startTime":$("#startTime").val(),"endTime":$("#endTime").val(),"type":""},
		type:'get',
		success:function(data)
		{
			if(data.result=="success"){
				$('#loadingBox').hide();
				$('#backupPurgeDiv').show();
				swal("Success","Data purged Successfully","success");
			}else{
				swal("Error","Problem in purging Successfully","error");
			}						
		},
		error:function(data){
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			data = JSON.parse(data.responseText);
			responseAlert(data,false);
		}
	});
}	

/*function makeSelectSensorDropDown(){
	var numberOfSensors = $('#sensorCount').val();
	//$("#sensorNum").empty();
	$("#sensorNum").html('');
	for (var i=1; i<=numberOfSensors; i++){
		$("#sensorNum").append('<option value='+i+'>'+i+'</option>');
	}
}

function makeSelectSensorAngleDropDown(){
	var rotatingSensor=$('#rotatingSensor').val();
	if(rotatingSensor=="true"){
		$("#sensorAngle").html('');
		$("#sensorAngle").val("0");
		$("#sensorAngle").attr("disabled",true);
	}
	else{
		$("#sensorAngle").val("");
		$("#sensorAngle").attr("disabled",false);
	}
	
	
}
*/


function resetAll(){
	
	var eventDataType=$('#eventDataType').val();
	if(eventDataType=="select"){
		swal("Error","Please select valid option","error");
		return;
	}
	var resetAllConfirmStatus=confirm("Are you sure to reset data?");
	if(resetAllConfirmStatus){
		$('#loadingBox').show();
		$('#backupPurgeDiv').hide();
		$.ajax({
			url:serviceUrls.dataPurge,
			data:{"startTime":"","endTime":"","type":eventDataType},
			type:'get',
			success:function(data)
			{
				if(data.result=="success"){
					$('#loadingBox').hide();
					$('#backupPurgeDiv').show();
					swal("Success","Data reset Successfully","success");
				}else{
					swal("Error","Problem in resetting data","error");
				}						
			},
			error:function(data){
				$('#loadingBox').hide();
				$('#backupPurgeDiv').show();
				data = JSON.parse(data.responseText);
				responseAlert(data,false);
			}
		});
	}
}

function factoryConfigurationReset(){
	
	$('#loadingBox').show();
	$('#backupPurgeDiv').hide();
	$.ajax({
		url:serviceUrls.factoryReset,
		data:{},
		type:'get',
		success:function(data)
		{
			if(data.result=="success"){
				$('#loadingBox').hide();
				$('#backupPurgeDiv').show();
				swal("Success","Factory Configuration Reset Successfully","success");
			}else{
				swal("Error","Problem in factory resetting data","error");
			}						
		},
		error:function(data){
			$('#loadingBox').hide();
			$('#backupPurgeDiv').show();
			data = JSON.parse(data.responseText);
			responseAlert(data,false);
		}
	});
}			

var responseAlert = function(data,reload)
{
	
    if(data.result == "success")
    {
       
        if(reload)
            location.reload();
    }
    else
    {
        swal("Something Went Wrong","Fail : "+data.message,"error");
        if(reload)
            location.reload();
    }

}

function getAllSystemUsers(){
	$.ajax({
	url:serviceUrls.getUsers,
	type:"post",
	success:function(data)
	{
		$('#userDataSelect').html('');
		var options='';
		data = data.split(",");
		for(var i=0;i<data.length;i++){
			if(user=="superadmin" && data[i] == "superadmin")
				options+='<option value="'+data[i]+'">'+data[i]+'</option>';
			if((user=="superadmin" || user=="admin") && data[i] == "admin")
				options+='<option value="'+data[i]+'">'+data[i]+'</option>';
			if(data[i] == "user")
				options+='<option value="'+data[i]+'">'+data[i]+'</option>';
		}
		$('#userDataSelect').html(options);
	}
	});
}

function getAllProfiles(){
	$.ajax({
	url:serviceUrls.getProfiles,
	type:"post",
	success:function(data)
	{
		$('#profileSelect').html('');
		profile=[];
		profile = data;
		var options='';
		//data = data.split(",");
		for(var i=0;i<data.length;i++){
			options+='<option value="'+data[i]+'">'+data[i]+'</option>';
		}
		$('#profileSelect').html(options);
	}
	});
}

function profileOperation(operation, profile){
	
	$.ajax({
		url:serviceUrls.profileOpr,
		data:{"operation":operation,"profile":profile},
		type:'get',
		success:function(data)
		{
			if(data.result=="success"){
				getAllProfiles();
				swal("Success","Profile operation performed Successfully","success");
			}else{
				swal("Error","Problem in performing profile operation","error");
			}						
		},
		error:function(data){
			data = JSON.parse(data.responseText);
			responseAlert(data,false);
		}
	});
}

function showProfile(profile){
	
	$("#config_data_table tbody").html("");
	$("#system_data_table tbody").html("");
	$("#masking_data_table tbody").html("");
	$("#emitter_data_table tbody").html("");
	
	$.ajax({
		url:serviceUrls.getConfigProfile+"/"+profile,
		type:"post",
		success:function(data)
		{
			var gsm = data.gsm_dl=='0'?'OFF':'ON';
			var wcdma = data.wcdma_dl=='0'?'OFF':'ON';
			var wifi = data.wifi_band=='0'?'OFF':'ON';
			var lte = data.lte_dl=='0'?'OFF':'ON';
			var row="";
			row+="<tr>";
			row+="<td>"+data.start_freq+"</td>"+
				"<td>"+data.stop_freq+"</td>"+
				"<td>"+data.threshold+"</td>"+
				"<td>"+data.cable_length+"</td>"+
				"<td>"+gsm+"</td>"+
				"<td>"+wcdma+"</td>"+
				"<td>"+wifi+"</td>"+
				"<td>"+lte+"</td></tr>";

			$("#config_data_table tbody").append(row);						
		}
	});
	
	$.ajax({
		url:serviceUrls.getSystemConfiguration+"/"+profile,
		type:"post",
		success:function(data)
		{
			var row="";
			row+="<tr>";
			var manual = data.ugs==0?'Disable':'Enable';
			var tmdas = data.tmdas==0?'Disable':'Enable';
			var auto = data.auto==0?'Disable':'Enable';
			row+="<td>"+manual+"</td>"+
				"<td>"+tmdas+"</td>"+
				"<td>"+auto+"</td>"+
				"<td>"+data.timeout+"</td>"+
				"<td>"+data.validity+"</td>"+
				"<td>"+data.selftimeout+"</td>"+
				"<td>"+data.selfvalidity+"</td></tr>";

			$("#system_data_table tbody").append(row);						
		}
	});
	
	$.ajax({
		url:serviceUrls.getMaskingDataList+"/"+profile,
		dataType:"json",
		success:function(data)
		{
			var row="";
			for(var i=0;i<data.length;i++)
			{	
				row+="<tr>";
				row+="<td>"+data[i].frequency+"</td>"+
					"<td>"+data[i].bandwidth+"</td>"+
					"<td>"+data[i].freqtype+"</td></tr>";
				
			}

			$("#masking_data_table tbody").append(row);						
		}
	});
	
	$.ajax({
		url:serviceUrls.getColorCodeList+"/"+profile,
		dataType:"json",
		success:function(data)
		{
			var row="";
			for(var i=0;i<data.length;i++)
			{	
				row+="<tr>";
				row+="<td>"+data[i].startfreq+"</td>"+
					"<td>"+data[i].stopfreq+"</td>"+
					"<td>"+data[i].networktype+"</td>"+
					"<td>"+data[i].depl+"</td>"+
					"<td>"+data[i].epr+"</td>"+
					"<td>"+data[i].modulation+"</td>"+
					"<td>"+data[i].color+"</td></tr>";
				
			}

			$("#emitter_data_table tbody").append(row);						
		}
	});
	
	$("#profile_data").modal('show');
}

/*var connectSocket = function()
{

    var stompClient = null;

	xData = [];
    var socket = new SockJS(endPoint.socket);

    stompClient = Stomp.over(socket);
	stompClient.debug = null
    stompClient.connect({}, function (frame)
    {
			
			stompClient.subscribe(topics.DbStatus, function (data) 
			{
				getBackupHistory();
				swal({
	        	     title: "Success",
	        	     text: data.body,
	        	     type: "success",
	        	     timer: 2000,
	        	     buttons:false,
	        	     className: "right-bottom"
	        	     });
				
			});

     });

}*/