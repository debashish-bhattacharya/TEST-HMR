var htmlContent='';
var ctxPath="trgl";
var menueContents='';
menueContents += '<div id="navigationMenu" class="row" style="margin-right:0px;">\
			       <div class="col-xs-12 menuWidth"  style="padding-right:0px;">\
				<!--  end of logo part -->\
				<div class="row" style="margin-right:0px;">\
				<div class="col-xs-12 menuWidth" style="padding-right:0px;">\
				<!--start of menue part-->\
				<div class="navbar navbar-default menuWidth" role="navigation">\
				<div class="navbar-collapse collapse menuWidth">\
				<ul class="nav navbar-nav">\
							<li class="dropdown"><img src="../resources/images/logo.png" class="vnlLogo"></li>\
							<li class="dropdown" id="kpi"><a href="#" class="pageLink" data-toggle="dropdown">KPI<span class="caret"></span></a>\
                                <ul class="dropdown-menu">\
									<li><a class="pageLink" href="'+ctxPath+'/kpi/KPITool">KPI Tool</a></li>\
									</ul>\
									</li>\
									<li class="dropdown" id="report_type"><a href="#" data-toggle="dropdown">Reports<span class="caret"></span></a>\
									<ul class="dropdown-menu">\
									<li id="TRAI_reports"><a class="pageLink" href="'+ctxPath+'/kpi/KPI_Reports" >TRAI Report</a></li>\
									<li id="Erlang_reports"><a class="pageLink" href="'+ctxPath+'/kpi/Erlang_Reports" >CCA Report</a></li>\
									<li id="Downtime_reports"><a class="pageLink" href="'+ctxPath+'/kpi/outage_report" >BSNL Outage Details</a></li>\
									<li id="general_reports"><a class="pageLink" href="'+ctxPath+'/kpi/general_reports" >General Report</a></li>\
									</ul>\
									</li>\
									<li id="Site_Outage_panel" class="dropdown"><a class="pageLink" id="Site_Outage_panel_link" href="'+ctxPath+'/kpi/outageUpdate" >Site Outage</a></li>\
                            <li class="dropdown" id="Administration"><a href="#" data-toggle="dropdown">Administration<span class="caret"></span></a>\
					   <ul class="dropdown-menu">\
                              <li><a class="pageLink" href="'+ctxPath+'/kpi/Administration">User Administration</a></li>\
                           </ul>\
					   </li>\
					   <li class="dropdown"><a href="#" data-toggle="dropdown">Help<span class="caret"></span></a>\
							<ul class="dropdown-menu">\
                                    <li><a class="pageLink" href="#">Help</a></li>\
                                    <li><a onclick = "showAbout()">About</a></li>\
                                </ul>\
							</li>\
                        </ul>\
						<div class="loginDetails">\
						<a href="/KPI-Tool/main/homePage" style="float: left;margin-right: 12px;margin-top: 5px;\
							"><img src="../resources/images/home.png" style="width: 28px;height: 23px;"></a>\
						<div class="btn-group navbar-right ">\
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">\
                                <img style="width:16px;height:16px;" src="../resources/images/user.png" alt="" title="User">\
                                <span><strong>sajal</strong> (Login since )</span>\
                            </button>\
                            <ul class="dropdown-menu" role="menu">\
                                <li><a class="pageLink" data-toggle="modal" data-backdrop="static" data-target="#change-pass" href="#">Change Password</a></li>\
                                <li class="divider"></li>\
                                <li><a class="pageLink" href="'+ctxPath+'/logout">Logout</a></li>\
                            </ul>\
                        </div>\
						</div>\
						<div class="modal fade" id="change-pass" tabindex="-1" role="dialog" aria-labelledby="modal-title-change-pass" aria-hidden="true">\
									<div id="ChangePassForUser" class="modal-dialog">\
										<div class="modal-content">\
											<div class="modal-header modal-header-color">\
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
												<h4 class="modal-title" id="modal-title-change-pass">Change Password</h4>\
											</div>\
											<div class="modal-body">\
												<div class="row">\
													<div class="col-xs-12">\
														<div class="row">\
															<div class="col-lg-6">\
																<label style="color:black;">Old Password</label>\
															</div>\
															<div class="col-xs-4">\
																<input id="old-pass" name="old-pass" type="password" class="form-control" placeholder="Password">\
															</div>\
														</div>\
														<div class="row">\
															<div class="col-lg-6">\
																<label style="color:black;">New Password:</label>\
															</div>\
															<div class="col-xs-4">\
																<input id="changed-pass" name="changed-pass" type="password" class="form-control" placeholder="Password">\
															</div>\
														</div>\
														<div class="row">\
															<div class="col-lg-6">\
																<label style="color:black;">Confirm New Password:</label>\
															</div>\
															<div class="col-xs-4">\
																<input id="confirm-pass" name="confirm-pass" type="password" class="form-control" placeholder="Password">\
															</div>\
														</div>\
 													</div>\
												</div>\
											</div>\
											<div class="modal-footer">\
												<button class="btn btn-primary" id="change-pass-save" alt="Save" onClick="savePasswordDetails()" >Save</button>\
												<button class="btn btn-primary" id="change-pass-close" alt="Close" data-dismiss="modal">Close</button>\
											</div>\
										</div>\
									</div>\
						</div>	\
					</div>\
				</div>\
			</div>\
			<!--  end of menue part -->\
			</div>\
			</div>\
		</div>';
htmlContent +=menueContents;
document.write(htmlContent);

	
	

	
var savePasswordDetails =function()
{
	var oldPassword = $("#old-pass").val();
	var changedPassword =  $("#changed-pass").val();
	var confirmPassword = $("#confirm-pass").val();
	var match = false;
    if(oldPassword=="" || changedPassword=="" || confirmPassword=="")
    {
       alert("Password cannot be blank");
    }
    else if(oldPassword == changedPassword)
	{
		alert("New password is same as Old password!!!");
	}
	else if(changedPassword == confirmPassword)
	{
		match = true;
	}
	else
	{
		alert("Confirm New Password mismatch");
	}
	if(match==true)
	{
		$.ajax
				({
					url:($("#ctxPath").val()) + "/ChangePassword",
					async:true,
					type: "post",
					data:
					{
						"oldPassword":oldPassword,
						"changedPassword":changedPassword,
						"confirmPassword":confirmPassword
					},
					success:function(data)
					{  
                                                if(data=="Incorrect oldPassword")
                                                {
                                                alert(data);
                                                return;
                                                } 
						var result=manageResponseMenuBar(data);
						if(result!="Exception")
						{
						alert("Password Changed", function() {	});
						window.location.href = "/KPI-Tool/logout";
						}
						else
						{
							return;
						}
					},
					error:function()
						{
							alert("Change password failed", function() {	});
						}
				});
	}
}
function manageResponseMenuBar( result)
		{
                 if(result.status=="error")
                    {
						var exp = "Exception";
                         bootbox.alert(result.msg);
                         return exp;
                    }
                    else if(result.status=="success")
                    {
						console.log("success");
                            return result.data;
					}
		}
		
function checkisFailureResponse( result)
{
		if(result.status=="error")
		{
			//bootbox.alert(result.msg);
			bootbox.alert(jQuery.i18n.prop(result.msg));
			return;
		}
		else if(result.status=="success")
		{
			return result.data;
		}
}

var showAbout = function()
{
	
	var html = "";
	var box = bootbox.dialog({
		  title: "<div id='title_about'>About KPI Consolidation Tool</div>",
		  message: '<div style="height: 49px;font-size:30px;font-weight:200;"><div style="text-align:center;">Version 1.0.4</div></div>'
		 
		},{
			 backdrop:false
			 });
		//box.find('.modal-content').css("width","330px");
		box.find('.modal-header').css("background-color","#428bca");
		box.find('.bootbox-body').css("color","black");
		console.log(box.find('.modal-header').parent().parent());
		box.find('.modal-header').parent().parent().css("margin-top","25%");
		console.log(box);
		
		
		
		
		
}

