
var graphDataForCurrentSelectedNode=[];
var xData = [];
var dashborad = {};
var myLineChart = "";
var towerMarkerLayer = null;
var antennaId=21;
polylines = [];
polyLines={};
getRealTimeData=true;
currentNodeLat='';
currentNodeLon=''
nodesInfo={};
maximizeFrequency=false;
var maskingScreenData=[];
var colorStripsData=[];
frequencyAngleCount={};
frequencyFilter=0;

latLonData={};
ptzAngleOffset=0;
frequencySelected=false;
ptzOperationStarted=false;

/***************************************************Custom Chart********************/

Chart.defaults.derivedline = Chart.defaults.scatter;

//controller.scatter.js

var custom = Chart.controllers.scatter.extend({
    draw: function(ease) {
        // Call super method first
        Chart.controllers.scatter.prototype.draw.call(this, ease);

        // Now we can do some custom drawing for this dataset. Here we'll draw a red box around the first point in each dataset
        var meta = this.getMeta();
//       console.log(meta);
        /*var ctx1 = this.chart.chart.ctx;
        ctx1.beginPath();
ctx1.moveTo(parseInt($("#start_freq").val()),parseInt($("#threshold").val()));
ctx1.lineTo(pt0._xScale.right,pt0._view.x,parseInt($("#threshold").val()));

        ctx1.strokeStyle="blue";
        ctx1.stroke();*/


        var points = meta.data;
        
        for(var i in points)
        {
            var pt0 = points[i];
            var radius = pt0._view.radius;
            //console.log(pt0);
            var ctx = this.chart.chart.ctx;
            //console.log(ctx);
            ctx.save();
            //ctx.strokeStyle = 'red';
            ctx.lineWidth = 1;
           //console.log(pt0);
            // ctx.strokeRect(pt0._view.x - radius, pt0._view.y - radius, 2 * radius, 2 * radius);
            ctx.beginPath();
            ctx.moveTo(pt0._view.x,pt0._view.y);
            ctx.lineTo(pt0._view.x,pt0._yScale.bottom);

            //ctx.strokeStyle="red";
            ctx.stroke();



        }

    }
});

Chart.pluginService.register({
	beforeDraw: function (chart, easing) {
		if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
			var ctx = chart.chart.ctx;
			var chartArea = chart.chartArea;

			ctx.save();
			ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
			ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
			ctx.restore();
			
		}

	}
});


Chart.controllers.derivedline = custom;






var config1 = {
        type: 'derivedline',
        data: {
            //labels: ['0', '500', '1000', '1500', '2000', '2500', '3000'],
            datasets: [{
                label: 'Freq(MHz) vs Power(dBm)',
                backgroundColor: '#ff6384',
                borderColor: '#ff6384',
                //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
                data: [],
                fill: false
                //,showLine:true
            },
            {
				label: '',
				borderColor: colorToHex(96,94,94),
				backgroundColor: colorToHex(96,94,94),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(160,155,155),
				backgroundColor: colorToHex(160,155,155),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(204,201,201),
				backgroundColor: colorToHex(204,201,201),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(249,237,237),
				backgroundColor: colorToHex(249,237,237),
				data: [],
				fill:false
			}]
        },
        options: {
            responsive: true,
            onClick: updateChartFrequency,
			layout: {
	            padding: {
	                left: 0,
	                right: 10,
	                top: -40,
	                bottom: 0
	            }
	        },
            title: {
                display: true,
                //text: 'Freq(MHz) vs Power(dBm)'
                text: ''
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Freq(MHz)'
                    },
                ticks: {
                    min: 0,
                    max: 3000,
                    stepSize: 500
                }
                }],
                yAxes: [{
                    display: true,
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Power(dBm)'
                    },
                ticks: {
                    min: -120,
                    max: 0,
                    stepSize: 20
                }
                }]
            }
        }
    };
	
	
var config2 = {
        data: {
			datasets: [{
				label: '',
				borderColor: colorToHex(255,0,0),
				backgroundColor: colorToHex(255,0,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(255,128,0),
				backgroundColor: colorToHex(255,128,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(255,165,0),
				backgroundColor: colorToHex(255,165,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(255,206,0),
				backgroundColor: colorToHex(255,206,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(255,255,0),
				backgroundColor: colorToHex(255,255,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(184,255,0),
				backgroundColor: colorToHex(184,255,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(0,255,0),
				backgroundColor: colorToHex(0,255,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(0,208,0),
				backgroundColor: colorToHex(0,208,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(0,196,196),
				backgroundColor: colorToHex(0,196,196),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(0,148,255),
				backgroundColor: colorToHex(0,148,255),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(80,80,255),
				backgroundColor: colorToHex(80,80,255),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(0,38,255),
				backgroundColor: colorToHex(0,38,255),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(142,63,255),
				backgroundColor: colorToHex(142,63,255),
				data: [],
				fill:false
			}]
		},
        options: {
	        legend: {
	            display: false,
	        },
	        layout: {
	            padding: {
	                left: 30,
	                right: 10,
	                top: -30,
	                bottom: 0
	            }
	        },
        	animation: false,
			responsive: true,
			title: {
				display: true,
				text: ''
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: false,
						labelString: 'Freq(MHz)'
					},
				ticks: {
					display:false,
					min: 0,
					max: 3000,
					stepSize: 300
				},
				gridLines:{
					drawOnChartArea: false,
					color: "#101010",
					lineWidth:-1
				}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Time'
					},
				ticks: {
					display: false,
					min: -50,
					max: 0.1,
					stepSize: 5
				},
				gridLines:{
					drawOnChartArea: false,
					color: "#101010",
					lineWidth:0
				}
				}]
			},
			chartArea: {
				backgroundColor: 'rgba(10, 10, 9, 1)'
			}
		}
    };


function updateChartFrequency(evt){
	
	 var activePoint = myLineChart.getElementAtEvent(evt);
	 const clickedElementIndex = activePoint[0]._index;
	   const label = myLineChart.data.labels[clickedElementIndex];
	   
	   // get value by index
	   const value = myLineChart.data.datasets[0].data[clickedElementIndex];
	   //console.log(clickedElementIndex, label, value)
	   var idx = activePoint[0]['_index'];
	   maximizeAngle(value.x);
	   
}

function maximizeAngle(frequency){
	 maxAngle=0;
	 maxAngleValue=0;
	 $("#def_freq").val(frequency);
	  for(angle in frequencyAngleCount){
		   if(frequencyAngleCount[angle]>maxAngleValue){
			  maxAngle=angle;
			  maxAngleValue=frequencyAngleCount[angle];
		   }
	   }
	    if($("#ptz_angle_offset").val()!=""){
	   		ptzAngleOffset=parseFloat($("#ptz_angle_offset").val());
	   }
	   
	   if(maxAngle!=0){
	   	 $("#start_angle").val(maxAngle-30);
	   	 $("#end_angle").val(parseInt(maxAngle)+30);
	   	 $("#current_frequency_df").val(frequency);
	   	 $("#current_angle_df").val(parseInt(maxAngle));
	   //maximizeFrequency=true;
	   	 frequencyFilter=frequency
	   
	   }
	   removePolylines();
	   createLineForTheTower(currentNodeLat,currentNodeLon,2000,0+ptzAngleOffset,$("#current_node_ip").val(),'black');
	     createLineForTheTower(currentNodeLat,currentNodeLon,2000,180+ptzAngleOffset,$("#current_node_ip").val(),'black');
	     createSemiCircle();
	     if(maxAngle!=0){
		   createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt(maxAngle)-30+ptzAngleOffset,$("#current_node_ip").val(),'black');
		   //createLineForTheTower(currentNodeLat,currentNodeLon,2000,90,$("#current_node_ip").val(),'black');
		   
		   
		   createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat(maxAngle)+30+ptzAngleOffset,$("#current_node_ip").val(),'black');
		   frequencySelected=true;
		}
	 

}




function updateFrequencyCone(){
	/* maxAngle=0;
	   maxAngleValue=0;
	   for(angle in frequencyAngleCount){
		   if(frequencyAngleCount[angle]>maxAngleValue){
			   
			  maxAngle=angle;
			  maxAngleValue=frequencyAngleCount[angle];
		   }
	   }
	    if($("#ptz_angle_offset").val()!=""){
	   		ptzAngleOffset=parseFloat($("#ptz_angle_offset").val());
	   }
	    removePolylines();
	    createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt(maxAngle)-30+ptzAngleOffset,$("#current_node_ip").val(),'black');
	    createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat(maxAngle)+30+ptzAngleOffset,$("#current_node_ip").val(),'black');
	*/
}
/***************************************************Custom Chart********************/

var config = {
        type: 'scatter',
        data: {
            //labels: ['0', '500', '1000', '1500', '2000', '2500', '3000'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: '#ff6384',
                borderColor: '#ff6384',
                //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
                data: [],
                fill: false,
                showLine:true
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Freq(MHz) vs Power(dBm)'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Freq(MHz)'
                    },
                ticks: {
                    min: 0,
                    max: 3000,
                    stepSize: 500
                }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Power(dBm)'
                    },
                ticks: {
                    min: -120,
                    max: 0,
                    stepSize: 20
                }
                }]
            }
        }
    };




//Not In use
/*window.onload = function() {
    var ctx = document.getElementById('canvas').getContext('2d');
    //alert("11");
    myLineChart = new Chart(ctx, config1);
    //setTimeout(function(){ updateGraphRealTime({x:2800,y:-20}) }, 3000);
};*/

/*var updateGraphRealTime = function(data)
{
    var oldData = myLineChart.data.datasets[0].data;
    var newData = oldData.shift();
    console.log(oldData);
    oldData.push(data);
    console.log(oldData);
    myLineChart.data.datasets[0].data = oldData;
    myLineChart.update();
}*/


var events = function()
{
	
    $("#nodes_box").on("change","input",function()
    {
    	
        if($(this).is(":checked"))
        {
            clearData();
            getNodeConfigurationData($(this).val());
           
//            getPeakDataOfNode($(this).val());
            getLeadStatus($(this).val());
            currentNodeLat=nodesInfo[$(this).val()]['lat'];
            currentNodeLon=nodesInfo[$(this).val()]['lon'];
            
            getDeviceInfo($(this).val());

            $(".selectednode").each(function(){
                $(this).removeClass("selectednode");
            });
            $(this).parent().parent().addClass("selectednode");

        }
    });

    $("#node_color").change(function(){
        $("#node_color").css("background",$("#node_color").val());
        $("#node_color").css("color",$("#node_color").val());
    });

}


function getCordinatesForAngle(angle){
	lat="";
	lon="";
	if(angle==0){
		lat=28.5492602;
		lon=79.1790986;
	}
	
	else if(angle==60){
		
	}
}

var createNodesList = function(data)
{
    var html = "";
    for(var i in data)
    {
        html +='<li data-lat="'+data[i].lat+'" data-lon="'+data[i].lon+'" data-color="'+data[i].color+'" data-time="'+data[i].starttime+'" ><span class="color-symbol" style="background:'+data[i].color+' !important;"></span><span><input type="radio" name="nodes" value="'+data[i].id+'" data-id="'+data[i].id+'" data-ip="'+data[i].ip_add+'"></span>&nbsp;<span>'+data[i].name+'</span>&nbsp;-&nbsp;'+data[i].ip_add+'&nbsp;&nbsp;&nbsp;&nbsp;<span style="float:right"><a href="#"  onclick=deleteNode('+data[i].id+')><i class="fa fa-times"></i></a></span></li>'
        nodeInfoData={"lat":data[i].lat,"lon":data[i].lon};
        nodesInfo[data[i].id]=nodeInfoData;
    }
    
    //console.log(nodesInfo);
    $("#nodes_box").html("");
    $("#nodes_box").append(html);
}

function deleteNode(nodeId){
	
	 swal({
	      title: "Are you sure?",
	      text: "Are You Sure You Want To Delete Node!!",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	    }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  $.ajax({
			        url:serviceUrls.deleteNode+"/"+nodeId,
			        
			        dataType:"json",
			        type:"post",
			        success:function(data)
			        {
			        	  swal("Success","Node Data Has Been Deleted","success");	
			        	  responseAlert(data,true);
			        },
			 		error:function(data){
			 			  responseAlert(data,false);
			 		}
			    });
	      } else {
	        swal("Cancelled", "Node Data Has Not Been Deleted", "error");
	      }
	    })
	
//	swal({
//		  title: "Are you sure?",
//		  text: "Node Data will be deleted",
//		  type: "warning",
//		  showCancelButton: true,
//		  confirmButtonClass: "btn-danger",
//		  confirmButtonText: "Yes, delete it!",
//		  cancelButtonText: "No, cancel plx!",
//		  closeOnConfirm: false,
//		  closeOnCancel: false
//		},
//		function(isConfirm) {
//		  if (isConfirm) {
//			  $.ajax({
//			        url:serviceUrls.deleteNode+"/"+nodeId,
//			//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeConfig+"/"+nodeId,
//			        dataType:"json",
//			        type:"post",
//			        success:function(data)
//			        {
//			        	  swal("Success","Node Data Has Been Deleted","success");	
//			        	  responseAlert(data,true);
//			        },
//			 		error:function(data){
//			 			  responseAlert(data,false);
//			 		}
//			    });
//		  } else {
//		    swal("Cancelled", "Delete Node Operation Cancelled :)", "error");
//		  }
//		});
//	
	
}
var getNodesData = function()
{
    $.ajax({
        url:serviceUrls.getNodesData,
//url:"http://10.100.207.117:9000/security_server/api/getDevicesList",
        dataType:"json",
        success:function(data)
        {
            
            createNodesList(data);
            currentNodeLat=data[0].lat
            currentNodeLon=data[0].lon
            $($("#nodes_box li input")[0]).trigger("click");
        }
    })
}

var getNodeConfigurationData = function(nodeId)
{
	
	
    $.ajax({
        url:serviceUrls.getNodeConfig+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeConfig+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
            $("#current_node").val(nodeId);
            
//            console.log(data);
            
            setupConfigurationInForm(data);
        }
    });
}

var getPeakDataOfNode = function(nodeId)
{
    $.ajax({
        url:serviceUrls.peakData+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.peakData+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
           
            updatePeekTable(data);
            createOrUpdateGraph(data);

        }
    });
}

var getLeadStatus= function(nodeId)
{
    $.ajax({
        url:serviceUrls.ledStatus+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.ledStatus+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
           
           
           
        }
    });
}

var updateLedStatus = function()
{
    if(data.status == 0)
    {
        $("#led_status").css("background-position","-4490px -512px")
    }
    else
    {
        $("#led_status").css("background-position","-4544px -512px;")
    }
}

var setupConfigurationInForm = function(data)
{
	
    $("#start_freq").val(data.start_freq);
    $("#stop_freq").val(data.stop_freq);
    $("#threshold").val(data.threshold);
    $("#mask_offset").val(data.mask_offset);
    $("#cable_length").val(data.cable_length);
    $("#preamp_type").val(data.preamp_type);

    $("#config_start_time").val(data.start_time);
    $("#config_stop_time").val(data.stop_time);
    $("#country_code").val(data.country_code);
    $("#current_node_ip").val(data.ip_add);
    
    data.gsm_dl == 0?$("#gsm_dl").prop("checked",false):$("#gsm_dl").prop("checked",true);
    data.wcdma_dl == 0?$("#wcdma_dl").prop("checked",false):$("#wcdma_dl").prop("checked",true);
    data.wifi_band == 0?$("#wifi_band").prop("checked",false):$("#wifi").prop("checked",true);
    data.use_mask == 0?$("#use_mask").prop("checked",false):$("#use_mask").prop("checked",true);
    data.lte_dl == 0?$("#lte_dl").prop("checked",false):$("#lte_dl").prop("checked",true);
    data.calibration == 0?$("#calibrate").prop("checked",false):$("#calibration").prop("checked",true);

    $("#start_1").val(data.band_start1);
    $("#start_2").val(data.band_start2);
    $("#start_3").val(data.band_start3);
    $("#start_4").val(data.band_start4);
    $("#start_5").val(data.band_start5);

    $("#stop_1").val(data.band_stop1);
    $("#stop_2").val(data.band_stop2);
    $("#stop_3").val(data.band_stop3);
    $("#stop_4").val(data.band_stop4);
    $("#stop_5").val(data.band_stop5);

    data.band_en1 == 0?$("#start_stop_check_1").prop("checked",false):$("#start_stop_check_1").prop("checked",true);
    data.band_en2 == 0?$("#start_stop_check_2").prop("checked",false):$("#start_stop_check_2").prop("checked",true);
    data.band_en3 == 0?$("#start_stop_check_3").prop("checked",false):$("#start_stop_check_3").prop("checked",true);
    data.band_en4 == 0?$("#start_stop_check_4").prop("checked",false):$("#start_stop_check_4").prop("checked",true);
    data.band_en5 == 0?$("#start_stop_check_5").prop("checked",false):$("#start_stop_check_5").prop("checked",true);


    setupStatusData(data);
    graphXAxis();
    getPtzInfo();
    

}





var updateConfig = function()
{
	
    var configData =
    {
              "floor": "1",
              "room": "1",
              "calibration": $("#calibrate").is(":checked")?"1":"0",
              "start_freq": $("#start_freq").val(),
              "stop_freq": $("#stop_freq").val(),
              "threshold": $("#threshold").val(),
              "mask_offset": $("#mask_offset").val(),
              "use_mask": $("#use_mask").is(":checked")?"1":"0",


              "start_time":  $("#config_start_time").val(),
              "stop_time": $("#config_stop_time").val(),
              "country_code":$("#country_code").val(),

              "cable_length": $("#cable_length").val(),
              "preamp_type": $("#preamp_type").val(),



              "gsm_dl": $("#gsm_dl").is(":checked")?"1":"0",
              "wcdma_dl": $("#wcdma_dl").is(":checked")?"1":"0",
              "wifi_band": $("#wifi").is(":checked")?"1":"0",
              "lte_dl": $("#lte").is(":checked")?"1":"0",
              "band_start1": $("#start_1").val(),
              "band_stop1": $("#stop_1").val(),
              "band_en1": $("#start_stop_check_1").is(":checked")?"1":"0",
              "band_start2": $("#start_2").val(),
              "band_stop2": $("#stop_2").val(),
              "band_en2": $("#start_stop_check_2").is(":checked")?"1":"0",
              "band_start3": $("#start_3").val(),
              "band_stop3": $("#stop_3").val(),
              "band_en3": $("#start_stop_check_3").is(":checked")?"1":"0",
              "band_start4": $("#start_4").val(),
              "band_stop4": $("#stop_4").val(),
              "band_en4": $("#start_stop_check_4").is(":checked")?"1":"0",
              "band_start5": $("#start_5").val(),
              "band_stop5": $("#stop_5").val(),
              "band_en5": $("#start_stop_check_5").is(":checked")?"1":"0"
            }

    $.ajax({
        url:serviceUrls.updateConfig+"/"+$("#current_node").val(),
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.updateConfig+"/"+$("#current_node").val(),
        dataType:"json",
        type:"post",
        data:configData,
        success:function(data)
        {
            swal("Success","Configuration Data Has Been Saved","success");
            responseAlert(data,false);
            //setupConfigurationInForm(data);
            setupStatusData(configData);
            graphXAxis();
        },
        error:function(data){
        	responseAlert(data,false)
        }
    });
}


var setupStatusData = function(data)
{
	
	//console.log(data)
    $(".status_bar tr td").removeClass();
    /*$(".status_bar tr td").each(function(){
            $(this).removeClass("ON");
            $(this).removeClass("OFF");
    })*/

    $("#start_freq_status").html(data.start_freq);
    $("#stop_freq_status").html(data.stop_freq);
    $("#threshold_status").html(data.threshold);

    $("#gsm_status").html(data.gsm_dl     == 0?"OFF":"ON");
    $("#gsm_status").parent().addClass(data.gsm_dl     == 0?"OFF":"ON");

    $("#lte_status").html(data.lte_dl     == 0?"OFF":"ON");
    $("#lte_status").parent().addClass(data.lte_dl     == 0?"OFF":"ON");

    $("#wifi_status").html(data.wifi_band     == 0?"OFF":"ON");
    $("#wifi_status").parent().addClass(data.wifi_band     == 0?"OFF":"ON");

    $("#wcdma_status").html(data.wcdma_dl == 0?"OFF":"ON");

    $("#wcdma_status").parent().addClass(data.wcdma_dl     == 0?"OFF":"ON");

    //$("#sweep_status").html("Unknown");
    $("#mask_status").html(data.use_mask     == 0?"OFF":"ON");
    $("#mask_status").parent().addClass(data.use_mask == 0?"OFF":"ON");
	$("#start_status").parent().addClass("START_TIME");
    $("#start_status").html($(".selectednode").data("time"));

    getNodeStartTimeAndStatus()


}

var getNodeStartTimeAndStatus = function (nodeId)
{
    $.ajax({
url:serviceUrls.getNodeDetails+"/"+$("#current_node").val(),
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeDetails+"/"+$("#current_node").val(),
        success:function(data)
        {
            setStatusOfNode(data.state,data.is_active);
            setNodePositionOverMap(data);
        }
    });

    //$("#node_status").html("<p>Not reachable</p>");
}

var checkNodeRechablity = function(isActive,state)
{
    if(isActive == "true")
    {
        if(state == "NA")
            state = "reachable";
    }
    else
    {
        state = "not reachable";
    }
    return state;
}

var setStatusOfNode  = function(text,isActive)
{

    if(isActive != null)
    {
        text = checkNodeRechablity(isActive,text);
    }

    switch(text.toLowerCase())
    {
        case "not reachable":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("not_reachable");
        break;
        case "reachable":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("reachable");
        break;
        case "running":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("reachable");
        break;
        case "calibrating":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("calibration");
        break;
        case "monitoring":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("monitor");
        break;

    }

}

var setNodePositionOverMap = function(data)
{
	
	  
      addTower([currentNodeLat,currentNodeLon],'images/tower.png');
//    createLineForTheTower(data.lat,data.lon,100,0,data.ip_add);
}

/****************web socket section*************/


var connectSocket = function()
{

    var stompClient = null;

	xData = [];
    var socket = new SockJS(endPoint.socket);

    stompClient = Stomp.over(socket);
	stompClient.debug = null
    stompClient.connect({}, function (frame)
            {
//                console.log('Connected: ' + frame);
                stompClient.subscribe(topics.status, function (data) {
                    var statusData = jQuery.parseJSON(data.body);
//                    console.log(statusData);
updatedeviceStatus(statusData.id,statusData.ip_add,statusData.state,statusData.is_active)

                });
            stompClient.subscribe(topics.ptzoperation, function (data) {

            	maximizeFrequency=true;
            	$("#start_def_button").attr('disabled',true);

            	swal("Success","Maximize frequency has been set","success");
            	$("#def_modal").modal('toggle'); 
                            });
                stompClient.subscribe(topics.peak, function (data) {

                                var statusData = jQuery.parseJSON(data.body);
//                                console.log(statusData);
								
                                if(getRealTimeData==true){
                                    updatePeekTable(statusData);
									createOrUpdateGraph(statusData);
                                }



                            });
				
				stompClient.subscribe(topics.freqAuto, function (data) {

                                var freqData = jQuery.parseJSON(data.body);
//                                console.log(statusData);
								
                                if(getRealTimeData==true){
                                    updateMaximizeFrquency(freqData);
                                }



                            });

            stompClient.subscribe(topics.led, function (data) {

                //callback for socket
//                console.log(data.body);


            });
            stompClient.subscribe(topics.status, function (data) {
                var statusData = jQuery.parseJSON(data.body);
//                console.log(statusData);
updatedeviceStatus(statusData.id,statusData.ip_add,statusData.state,statusData.is_active)

            });    

        stompClient.subscribe(topics.nodeAngle, function (data) {

                        //callback for socket
        	
            var angleData = jQuery.parseJSON(data.body);
            updateDirectionOfNode(angleData);


                    });


            });


 // Open the connection
    /*socket.onopen = function() {
      console.log('open');
      //stompClient =


    };

    // On connection close
    socket.onclose = function() {
      console.log('close');
    };*/



}







/****************web socket section*************/


var updatedeviceStatus = function(id,ip,state,isActive)
{
    if($('input[name=nodes]:checked').data("id") == id)
    {
        setStatusOfNode(state,isActive);
    }
}

var addNode = function()
{

    if(validateAddNodeForm())
    {
    	
        var nodeData =
        {
                "name":$("#node_name").val(),
                "ip":$("#node_ip").val(),
                "color":$("#node_color").val(),
                "lat":$("#node_lat").val(),
                "lon":$("#node_lon").val(),
                "id":$("#node_id").val()
        }

        $.ajax({
            url:serviceUrls.addNode,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
            type:"post",
            data:nodeData,
            success:function(data)
            {
                responseAlert(data,true);
               
            }

        });
    }



}



function updatePriority()
{
	var prioNames="";
	var l =document.getElementById('sortable');
	$('#sortable').find('li').each(function()
	{
		prioNames+=$(this).text()+",";
	});
	prioNames=prioNames.substring(0,prioNames.length-1);
    $.ajax({
        url:serviceUrls.updatePriority,
        type:"post",
        data:{"prioNames":prioNames},
        success:function(data)
        {	           
        	swal("Success","System Configuration data has been saved","success");
        }

    });
}

function addSystemConfiguration()
{
	var ugs=$('input[type=radio][name=radiobtn]:checked').val();
	var tmdas=$('input[type=radio][name=radio]:checked').val();
	var auto = $('input[type=radio][name=radiobtn1]:checked').val();
	var timeout=$("#timeout").val();
	var validity=$("#validity").val();
	var selftimeout=$("#selftimeout").val();
	if((ugs==undefined)||(tmdas==undefined)||(auto==undefined))
	{
        swal("Error","Please select radiobutton","error");
		return;		
	}
	if((timeout=="select")||(validity=="select")||(selftimeout=="select"))
	{
        swal("Error","Please select values from the dropdown","error");
		return;
	}
    var systemConfigurationData =
    {
         "ugs":ugs,
         "tmdas":tmdas,
		 "auto":auto,
         "timeout":timeout,
         "validity":validity,
         "selftimeout":selftimeout   
    }

    $.ajax({
        url:serviceUrls.addSystemConfiguration,
        type:"post",
        data:systemConfigurationData,
        success:function(data)
        {	           
        	swal("Success","System Configuration data has been saved","success");
            //responseAlert(data,true); 
        	$("#system_config").modal('hide');
        }

    });
}

function defineColorStrips()
{
	var startfreq=$("#startfrequency").val();
	var stopfreq=$("#stopfrequency").val();
	var networktype=$("#networktype").val();
	var color=$("#color").val();
	console.log($("#startfreq").val());
	if(stopfreq=="0")
	{
        swal("Error","Stop frequency can't be 0!!!","error");
		return;		
	}
	if(networktype=="select")
	{
        swal("Error","Please select networktype first!!!","error");
		return;
	}
        var colorData =
        {
             "startfreq":startfreq,
             "stopfreq":stopfreq,
             "networktype":networktype,
             "color":color
        }

        $.ajax({
            url:serviceUrls.defineColorStrips,
            type:"post",
            data:colorData,
            success:function(data)
            {	           
            	swal("Success","Data has been saved","success");
                responseAlert(data,true);   
            }

        });
}

function addMaskingData()
{
	var frequency=$("#frequency").val();
	var bandwidth=$("#bandwidth").val();
	var freq_type=$("#freqtype").val();
	if(frequency=="0")
	{
        swal("Error","Frequency can't be 0!!!","error");
		return;		
	}
	if(bandwidth=="0")
	{
        swal("Error","Bandwidth can't be 0!!!","error");
		return;		
	}
	if(freq_type=="select")
	{
        swal("Error","Please select frequency type first!!!","error");
		return;
	}
        var maskingData =
        {
             "frequency":frequency,
             "bandwidth":bandwidth,
             "freqtype":freq_type
        }

        $.ajax({
            url:serviceUrls.addMaskingData,
            type:"post",
            data:maskingData,
            success:function(data)
            {	           
            	swal("Success","Data has been saved","success");
                responseAlert(data,true);   
            }

        });
}

var getMaskingDataList = function()
{
    $.ajax({
        url:serviceUrls.getMaskingDataList,
        dataType:"json",
        success:function(data)
        {
           addDataToMaskingGridTable(data);
        }
    });
}


var getColorCodeList = function()
{
    $.ajax({
        url:serviceUrls.getColorCodeList,
        dataType:"json",
        success:function(data)
        {
           colorStripsData = data;
        }
    });
}

var startDef = function()
{

	 removePolylines();
    if(validateDefForm())
    {
        var defData =
        {
                "startAngle":$("#start_angle").val(),
                "stopAngle":$("#end_angle").val(),
                "tilt_angle":$("#tilt_angle").val(),
                "ip_add":$("#current_node_ip").val()
        }
        
        /*if(parseInt($("#start_angle").val())+parseInt(ptzAngleOffset)<0){
        	alert("PTZ Operation Not Allowed Check Offset");
        	return;
        } */
        if( parseInt($("#start_angle").val())>parseInt($("#end_angle").val())){
        	swal("Success","Start Angle Should be Less Than End Angle","error");
        	return;
        }
        
         createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt($("#start_angle").val())+ptzAngleOffset,$("#current_node_ip").val(),'black');
         createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat($("#end_angle").val())+ptzAngleOffset,$("#current_node_ip").val(),'black');
		
        $.ajax({
            url:serviceUrls.startDef,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
            type:"post",
            data:defData,
            success:function(data)
            {
            
            	
            	maximizeFrequency=true;
            	ptzOperationStarted=true;
            	$("#def_modal").modal().hide();
                responseAlert(data,false);
                $("#start_def_button").attr('disabled',true);
                
                //createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt($("#start_angle").val())-30+ptzAngleOffset,$("#current_node_ip").val(),'black');
         	   //createLineForTheTower(currentNodeLat,currentNodeLon,2000,90,$("#current_node_ip").val(),'black');
         	   
         	   
         	  // createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat($("#end_angle").val())+30+ptzAngleOffset,$("#current_node_ip").val(),'black');
                //fetchLatLonSignals();
            }

        });
    }
}

var updateMaximizeFrquency = function(freqData)
{

	removePolylines();
	var event_name
	var frequency;
	var start_angle;
	var end_angle;
	
	for(var i in freqData)
    {
		event_name = freqData[i].eventName;
		frequency = parseFloat(freqData[i].frequency);
		start_angle = parseFloat(freqData[i].startAngle);
		end_angle = parseFloat(freqData[i].stopAngle); 
	}
	createLineForTheTower(currentNodeLat,currentNodeLon,2000,start_angle+ptzAngleOffset,$("#current_node_ip").val(),'black');
	createLineForTheTower(currentNodeLat,currentNodeLon,2000,end_angle+ptzAngleOffset,$("#current_node_ip").val(),'black');
		
	frequencyFilter = frequency;
	maximizeFrequency=true;
	ptzOperationStarted=true;
	$("#def_modal").modal().hide();
	$("#start_def_button").attr('disabled',true);
		
}

function stopDef(){
	
	var defData =
    {
			"ip_add":$("#current_node_ip").val()
    }
	 $.ajax({
         url:serviceUrls.stopDef,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
         type:"post",
         data:defData,
         success:function(data)
         {
         	
        	 maximizeFrequency=false;
             //responseAlert(data,false);
             $("#start_def_button").attr('disabled',false);
//             
         }

     });
	
}

function graphView(){
	
	var value = $("#show_graph").text();
	var graph = document.getElementById("spectrogram");
	if(value == "Show"){
		graph.style.display = "block";
		$("#show_graph").text("Hide");
	}
	else{
		graph.style.display = "none";
		$("#show_graph").text("Show");
	}
	
}

var responseAlert = function(data,reload)
{
	
    if(data.result == "success")
    {
       
        if(reload)
            location.reload();
    }
    else
    {
        swal("Something Went Wrong","Fail : "+data.message,"error");
        if(reload)
            location.reload();
    }

}

var createOrUpdateGraph = function(data)
{

    for(var i in data)
    {
        if($('input[name=nodes]:checked').data("id") == data[i].device_id.id)
        {
            if(i == 0 && data[i].time != $(".status_table tbody tr:first td:last").text())
            {
                graphDataForCurrentSelectedNode = [];
            }
		    if(data[i].antennaid!=antennaId)
				graphDataForCurrentSelectedNode[data[i].frequency]=data[i].power;
        }
    }
    //myLineChart.data.datasets[0].data = convertToGraphData(graphDataForCurrentSelectedNode);
    myLineChart.data.datasets[0].borderColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    myLineChart.data.datasets[0].backgroundColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    //myScatterChart.data.datasets[0].data = convertToData(graphDataForCurrentSelectedNode);
    convertToData(graphDataForCurrentSelectedNode);
	myScatterChart.update();
    myLineChart.update();
}

var createOrUpdateOfflineGraph = function(data){
    graphDataForCurrentSelectedNode = [];
    for(var i in data)
    {

            if(i == 0 && data[i].time != $(".status_table tbody tr:first td:last").text())
            {

                graphDataForCurrentSelectedNode = [];
                //graphDataForTime = [];
            }
graphDataForCurrentSelectedNode[data[i].frequency]=data[i].power;

    }
    //myLineChart.data.datasets[0].data = convertToGraphData(graphDataForCurrentSelectedNode);
    myLineChart.data.datasets[0].borderColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    myLineChart.data.datasets[0].backgroundColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    //myScatterChart.data.datasets[0].data = convertToData(graphDataForCurrentSelectedNode);
    convertToData(graphDataForCurrentSelectedNode);
	myScatterChart.update();
    myLineChart.update();
}

var graphXAxis =  function( )
{



    var maxVal  = parseInt(($("#stop_freq").val() == 0)?3000:($("#stop_freq").val()));
    var minVal  = parseInt($("#start_freq").val());
    
    var threshold  = parseInt($("#threshold").val());

    myLineChart.options = {
        responsive: true,
		layout: {
			padding: {
				left: 0,
				right: 10,
				top: -40,
				bottom: 0
			}
		},
        onClick: updateChartFrequency,
        title: {
            display: true,
            text: ''
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Freq(MHz)'
                },
            ticks: {
                min: minVal,
                max: maxVal,
                stepSize: getStepSize(maxVal),
                xPadding: 6
            }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Power(dBm)'
                },
            ticks: {
                min: -120,
                max: 0,
                stepSize: 20
            }
            }]
        },
        annotation: {
            annotations: colorOutputData()
        }
    };
	
	myScatterChart.options={
        legend: {
            display: false,
        },
        layout: {
            padding: {
                left: 30,
                right: 10,
                top: -30,
                bottom: 0
            }
        },
    	animation: false,
		responsive: true,
		title: {
			display: true,
			text: ''
		},
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: false,
					labelString: 'Freq(MHz)'
				},
			ticks: {
				display:false,
				min: minVal,
				max: maxVal,
				stepSize: getStepSize(maxVal)
			},
			gridLines:{
				drawOnChartArea: false,
				color: "#101010",
				lineWidth:-1
			}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Time'
				},
			ticks: {
				display: false,
				min: -50,
				max: 0.1,
				stepSize: 5
			},
			gridLines:{
				drawOnChartArea: false,
				color: "#101010",
				lineWidth:0
			}
			}]
		},
		chartArea: {
			backgroundColor: 'rgba(10, 10, 9, 1)'
		}
	};
    myLineChart.update();
	myScatterChart.update();

}

var colorOutputData =function()
{
	var len = colorStripsData.length;
	var output = [];
	if(len>5)
		len=5;
	for(var i=0;i<len;i++){
		output.push({
            type: 'box',
            drawTime: 'beforeDatasetsDraw',
            xScaleID: 'x-axis-1',
            xMin: colorStripsData[i].startfreq,
            xMax: colorStripsData[i].stopfreq,
            backgroundColor: colorStripsData[i].color

        })
	}
	return output;
	
}

var addDataToMaskingGridTable = function (data) 
{
	maskingScreenData = data;
    $("#freq_grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['Id','Frequency','Bandwidth', 'Type'],
        colModel: [{
            name : 'id',
            index : 'id',
            width : 10
        },{
            name : 'frequency',
            index : 'frequency',
            width : 15,
            editable : true
        },{
            name : 'bandwidth',
            index : 'bandwidth',
            width : 15,
            editable : true
        },{
			name : 'freqtype',
			index : 'freqtype',
			width: 15,
            editable : true
        }],
        pager : '#freq_pager',
        rowNum : 5,
        width : 520,
		height: 'auto',
		loadonce : true,
        sortname : 'id',
        sortorder : 'asc',
        viewrecords : true,
        gridview : true,
		caption: "Frequency Configuration"
    });
    jQuery("#freq_grid").jqGrid('navGrid', '#freq_pager', {
        edit: true,
        add: true,
        del: true,
        search: true
    });
}


var addDataToGridTable = function (data) 
{
	colorStripsData = data;
    $("#grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['Id','Start Frequency','Stop Frequency', 'Network Type','Color'],
        colModel: [{
            name : 'id',
            index : 'id',
            width : 90
        },{
            name : 'startfreq',
            index : 'startfreq',
            width : 100,
            editable : true
        },{
            name : 'stopfreq',
            index : 'stopfreq',
            width : 90,
            editable : true
        },{
			name : 'networktype',
			index : 'networktype',
			width: 90,
            editable : true
        },{
            name : 'color',
            index :'color',
            width : 100,
            editable : true
        }],
        pager : '#pager',
        rowNum : 5,
        width : 770,
		height: 'auto',
		loadonce : true,
        sortname : 'id',
        sortorder : 'asc',
        viewrecords : true,
        gridview : true,
		caption: "Color Bands"
    });
    jQuery("#grid").jqGrid('navGrid', '#pager', {
        edit: true,
        add: true,
        del: true,
        search: true
    });
}

/*var lastSelection;

function editRow(id) 
{
    if (id && id !== lastSelection) 
    {
        var grid = $("#grid");
        grid.jqGrid('restoreRow',lastSelection);
        grid.jqGrid('editRow',id, {keys: true} );
        lastSelection = id;
    }
}*/

var getStepSize = function(maxValue)
{
    return (maxValue/1000)*100
}


var updateDirectionOfNode = function(data)
{
    line = polylines[data.ip_add];
    if(line != undefined)
    {
        var path =  line.getLatLngs();
        lineEndPoint = calulateLatLongAtGivenAngleAndDistance(path[0].lat,path[0].lng,data.angle,100);
        path[1] = lineEndPoint;
        line.setLatLngs(path);
        $("#angle_map").html("");
        $("#angle_map").html(data.angle+"&deg;");
    }
}

var updatePeekTable = function(data)
{
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	$("#peek_table_date_time").text(date);
	dbm=-1000;
	frequencyValue=0;
	currentAngle=0;
	if(data.length>0)
		frequencyAngleCount={};
    for(var i in data)
    {
    	currentAngle=data[i].angle;
        if(data[i].device_id.id == $('input[name=nodes]:checked').data("id"))
        {
            /*if($(".status_table tbody tr").length >=200)
            {
                $(".status_table tbody tr:last").remove();
            }*/
            if(i == 0)
            {
                $(".status_table tbody").html("");
            }
           
            if(maximizeFrequency==true){
            	
            	frequencyRange=parseFloat($("#frequency_offset").val());
            	console.log("Frequency range is "+frequencyRange);
				//alert(frequencyFilter+" "+frequencyRange+" " +data[i].frequency);
            	if( data[i].antennaid==antennaId && ((parseFloat(data[i].frequency)>=parseFloat(frequencyFilter)-parseFloat(frequencyRange))&&(parseFloat(data[i].frequency)<=parseFloat(frequencyFilter)+parseFloat(frequencyRange)))){
					
					//alert(frequencyFilter+" "+frequencyRange+" " +data[i].frequency+" in");
					if(dbm<data[i].power){
						dbm=data[i].power;
						
						frequencyValue=parseFloat(data[i].frequency);
						console.log("Inside maximizeFrequency frequency is"+frequencyValue);
					}
            	
            		/*$("#current_angle_df").val(parseInt(data[i].angle)+parseInt(ptzAngleOffset));
            		colorCode=colorToHex('255','255','0');
            		 //createLineForTheTower(currentNodeLat,currentNodeLon,100,data[i].angle-30,0,data[i].ip_add,colorCode);
            		
            		powerValue=Math.pow(10,parseInt(data[i].power)/20);
            		distance=powerValue-maximizeFrequency-Math.pow(10,parseInt(27.55/20));
            		
            		exp = (27.55 - (20 * Math.log10(frequencyFilter)) + (30-data[i].power)) / 20.0;
            		distance=Math.pow(10.0, exp);
            		console.log("distance is "+distance+" and angle is "+data[i].angle+"frequency is "+frequencyFilter+" and power is "+ data[i].power);
            		color=getColorCode(data[i].power)
            		
            		if(data[i].angle!=$("#start_angle").val() && data[i].angle!=$("#end_angle").val()){
            			createLineForTheTower(currentNodeLat,currentNodeLon,distance,parseInt(parseInt(data[i].angle)+parseInt(ptzAngleOffset)),$("#current_node_ip").val(),color);
            		}*/
            		
            	}
				if(data[i].antennaid!=antennaId){
            	 //$("#current_angle_df").val(data[i].angle);
            	 $(".status_table tbody").prepend("<tr><td>"+data[i].device_id.name+"</td><td>"+data[i].power+"</td><td>"+data[i].frequency+"</td><td>"+data[i].time.split(" ")[1]+"</td><td>"+data[i].angle+"</td><td>"+data[i].antennaid+"</td><td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
				}
				else{
					//console.log("HELLO    "+data[i].angle);
					$("#current_angle_df").val(data[i].angle);
				}
            }
            else{
				if(data[i].antennaid!=antennaId)
					$(".status_table tbody").prepend("<tr><td>"+data[i].device_id.name+"</td><td>"+data[i].power+"</td><td>"+data[i].frequency+"</td><td>"+data[i].time.split(" ")[1]+"</td><td>"+data[i].angle+"</td><td>"+data[i].antennaid+"</td><td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
//            	 createLineForTheTower(currentNodeLat,currentNodeLon,100,data[i].angle-30,0,data[i].ip_add);
//            	 createLineForTheTower(currentNodeLat,currentNodeLon,100,data[i].angle+30,0,data[i].ip_add);
            }
            
            
           
            if(data[i].angle in frequencyAngleCount){
            	frequencyAngleCount[data[i].angle]=frequencyAngleCount[data[i].angle]+1;
            }
            else{
            	frequencyAngleCount[data[i].angle]=1;
            }
        }

    }
	
	if(maximizeFrequency==true && dbm!=-1000){
		console.log("Maximum power is found"+dbm);
		$("#current_angle_df").val(parseFloat(currentAngle)+parseFloat(ptzAngleOffset));
		exp = (27.55 - (20 * Math.log10(frequencyValue)) + (30-dbm)) / 20.0;
        distance=Math.pow(10.0, exp);
		console.log("distance is "+distance+" and angle is "+currentAngle+"frequency is "+frequencyValue+" and power is "+ dbm);
        color=getColorCode(dbm);
		//removeSelectedPolylines();
		if(currentAngle!=$("#start_angle").val() && currentAngle!=$("#end_angle").val()){
            			createLineForTheTower(currentNodeLat,currentNodeLon,distance,parseFloat(parseFloat(currentAngle)+parseFloat(ptzAngleOffset)),$("#current_node_ip").val(),color);
            		}
		
	}
    
       maxAngle=0;
	   maxAngleValue=0;
	   console.log(frequencyAngleCount);
	   for(angle in frequencyAngleCount){
		   if(frequencyAngleCount[angle]>maxAngleValue){
			   
			  maxAngle=angle;
			  maxAngleValue=frequencyAngleCount[angle];
		   }
	   }
	   
	   
	   //$("#current_angle_df").val(maxAngle);
    
   
    /*if(frequencySelected==true && maximizeFrequency==false){
    	updateFrequencyCone();
    }*/
}

var updateOfflinePeekTable = function(data)
{
	frequencyAngleCount={};
	
    for(var i in data)
    {

            /*if($(".status_table tbody tr").length >=200)
            {
                $(".status_table tbody tr:last").remove();
            }*/
            if(i == 0)
            {
                $(".status_table tbody").html("");
            }
            
            colorCode=colorToHex('0','255','0');
//            createLineForTheTower(currentNodeLat,currentNodeLon,100,90-data[i].ptzangle,data[i].ip_add,colorCode);
            date=data[i].time.split(" ");
            //powerValue=(Math.pow(10,parseInt(data[i].power)+27.55)/20);
    		//distance=powerValue/900;
    		
    		exp = (27.55 - (20 * Math.log10(frequencyFilter)) + (Math.abs(data[i].power)+33)) / 20.0;
            		distance=Math.pow(10.0, exp);
    		
            $(".status_table tbody").prepend("<tr><td>"+data[i].deviceName+"</td><td>"+data[i].power+"</td><td>"+data[i].frequency+"</td><td>"+date[1]+"</td><td>"+data[i].ptzangle+"</td><td>"+data[i].antennaId+"</td><td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
           
            $("#peek_table_date_time").text(date[0]);
            if(data[i].ptzangle in frequencyAngleCount){
            	frequencyAngleCount[data[i].ptzangle]=frequencyAngleCount[data[i].ptzangle]+1;
            }
            else{
            	frequencyAngleCount[data[i].ptzangle]=1;
            }

    }
    console.log(frequencyAngleCount);
    	
}

var convertToGraphData = function(data)
{
    var graphData =[];
    var i=0;
    for(var j in data)
    {
        var obj = {};

        obj.x = j;
        obj.y = data[j];

        graphData[i]=obj

        i++;
    }
    return graphData;
}

var convertToData = function(data)
{
    var temp = [];
    for(var t =0;t<13;t++){
		if(t<5)
			myLineChart.data.datasets[t].data = [];
    	myScatterChart.data.datasets[t].data = [];
	}
	

    for(var j in data)
    {
        var obj1 = {};

        obj1.x = j;
        obj1.y = data[j];
        //xData.unshift(data[j]);
		//temp.unshift(j);
		temp.unshift(obj1);
        //graphData[i]=obj

        //i++;
    }
    xData.unshift(temp);
    //return graphData;
	
	//graphData = [];
	//xData.unshift(data);
	var len = xData.length;
	if(len>50){
		xData.pop();
	}
	var index = 0;
	var count =0;
	for(var x in xData){
		for(var y in xData[x]){
			var obj = {};
			var object = xData[x][y];
			obj.x = object.x;
			obj.y = index;
			//graphData.push(obj);
			if(count<5){
				myLineChart.data.datasets[count].data.push(object);
			}
			else{}
			if(object.y>-10){
				myScatterChart.data.datasets[0].data.push(obj);
			}else if (object.y<=-10 && object.y>-20){
				myScatterChart.data.datasets[1].data.push(obj);
			}else if (object.y<=-20 && object.y>-30){
				myScatterChart.data.datasets[2].data.push(obj);
			}else if (object.y<=-30 && object.y>-40){
				myScatterChart.data.datasets[3].data.push(obj);
			}else if (object.y<=-40 && object.y>-50){
				myScatterChart.data.datasets[4].data.push(obj);
			}else if (object.y<=-50 && object.y>-60){
				myScatterChart.data.datasets[5].data.push(obj);
			}else if (object.y<=-60 && object.y>-70){
				myScatterChart.data.datasets[6].data.push(obj);
			}else if (object.y<=-70 && object.y>-80){
				myScatterChart.data.datasets[7].data.push(obj);
			}else if (object.y<=-80 && object.y>-90){
				myScatterChart.data.datasets[8].data.push(obj);
			}else if (object.y<=-90 && object.y>-100){
				myScatterChart.data.datasets[9].data.push(obj);
			}else if (object.y<=-100 && object.y>-110){
				myScatterChart.data.datasets[10].data.push(obj);
			}else if (object.y<=-110 && object.y>-120){
				myScatterChart.data.datasets[11].data.push(obj);
			}else if (object.y<=-120){
				myScatterChart.data.datasets[12].data.push(obj);
			}
		}
		count++;
		index--;
	}
	//console.log(graphData);
	/*for(var i =-len+1;i<=0;i++){
		var obj = {};
		obj.x = xData[-i];
		obj.y = i;
		graphData.push(obj);
	}*/
	//return graphData;
}


var createLineForTheTower = function(lat,lon,lineLength,angle,ip,color)
{
	/*if(angle >=0 && angle<=90){
		angle=90-angle
	}
	else if(angle > 90 && angle<=180){
		angle=(180-angle)+270
	}
	else if(angle >180 && angle<=270){
		angle=(270-angle)+180;
	}
	else{
		angle=(360-angle)+90;
	}*/
	
	//console.log("hello value is    "+angle);
	if(angle<0){
		
		angle=360+parseFloat(angle);
	}
	lineEndPoint=calulateLatLongAtGivenAngleAndDistance(parseFloat(lat.trim()),parseFloat(lon.trim()),angle,lineLength,color);
    //alert(lineEndPoint);
     var line = addPolyLine([lat,lon],lineEndPoint,color);
     polylines.push(line);
     
     circle=new L.Circle(lineEndPoint, 5).addTo(map);
     circle.on('mouseover', function(e) {
    	 var popup = L.popup()
    	   .setLatLng(e.latlng) 
    	   .setContent("Distance From Tower is "+parseFloat(lineLength/1000)+" Km.")
    	   .openOn(map);
     });
     circle.on('mouseout', function (e) {
         this.closePopup();
     });
     
     //circle.bindPopup();
     polylines.push(circle); 
    //line.id = ip
//    polylines[ip] = line;
    
//    console.log(polylines[ip]);
	
}


function createSemiCircle(){
	
	 L.semiCircle([currentNodeLat,currentNodeLon], {
         radius: 2000,
         fill: true,
         fillColor:'#a1ba03',
         fillOpacity: 0.5,
         color: '#a1ba03',
         opacity: 0.5,
         startAngle: 20,
         stopAngle: 200
     }).addTo(map);
}
var clearData = function()
{
	console.log("Inside clear Data");
    graphDataForCurrentSelectedNode = [];
    $(".status_table tbody").html("");
   
    /*for(var i in polylines)
    {
    	polylines[i].remove();
    }
    
    
    
    map.removeLayer(polyLines);
    polylines = [];
	*/
}

var removePolylines=function(){
	
	console.log("Inside removePolyLines");
	//console.log(polylines);
	 for(var i in polylines)
	    {
			//console.log(polyLines[i]);
	    	polylines[i].remove();
	    }
	    
	 
	    //map.removeLayer(polyLines);
	    //polylines = [];
}

var removeSelectedPolylines=function(){
	
	console.log("Inside removeSelectedPolyLines");
	var count = 0;
	 for(var i in polylines)
	    {
			//console.log(i);
	    	//polylines[i].remove();
			if(count>3)
				polyLines[i].remove();
			count++;
	    }
	    
	   
	    map.removeLayer(polyLines);
	    //polylines = [];
}



/*************************** MAP Section **********************************/
function loadLeafMap()
{
    map = new L.Map('map_leaf', {fullscreenControl: true,center: new L.LatLng(28.7041, 77.1025),  zoom: 4, minZoom: 0,maxZoom: 18});

    map.zoomControl.setPosition('bottomright');

    var googleMaplayer = L.gridLayer.googleMutant({
            type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
        }).addTo(map);
	mapServerIp=$("#map_server_ip").val();

    //var openstreatMapLayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      var openstreatMapLayer = L.tileLayer('http://'+mapServerIp+'/hot/{z}/{x}/{y}.png', {
        attribution: 'Imagery Â© <a href="http://mapbox.com">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map);


      towerMarkerLayer = L.layerGroup().addTo(map);

    var toggleOfflineOnlineMap =  L.Control.extend({

          options: {
            position: 'bottomright'
          },

          onAdd: function (map)
          {
            var container = L.DomUtil.create('img');
            container.id="map_type";
            container.title="Offline";
            container.src="images/offline.png"
            container.style.backgroundColor = 'white';
            $(container).addClass("offline");
            container.style.width = '33px';
            container.style.height = '33px';

            container.onclick = function(){
              //console.log($(container));
              if($(container).hasClass('offline')){
                 map.removeLayer(openstreatMapLayer);
                 container.title="Online";
                 $(container).removeClass("offline");
                 $(container).addClass("online");
                 container.src="images/online.png"
            }
            else {
                //map.removeLayer(googleMapLayer);
                map.addLayer(openstreatMapLayer);
                container.title="Offline";
                $(container).addClass("offline");
                 $(container).removeClass("online");
                 container.src="images/offline.png"
            }

            }
            return container;
          }
        });


    var compasImg =  L.Control.extend({

          options: {
            position: 'bottomright'
          },

          onAdd: function (map)
          {
            var container = L.DomUtil.create('img');
            container.id="map_compass";
            container.title="Up(North)-Down(South)";
            container.src="images/compass.png";
            //container.style.backgroundColor = 'white';
            //$(container).addClass("offline");
            container.style.width = '33px';
            container.style.height = '33px';
            container.style.transform = "rotate(-42deg)";

            return container;
          }
        });

    var currentAngle =  L.Control.extend({

          options: {
            position: 'bottomleft'
          },

          onAdd: function (map)
          {
            var container = L.DomUtil.create('div');
            container.id="angle_map";
            container.title="Device current angle";
            container.style.backgroundColor = 'white';
            //$(container).addClass("offline");
            container.style.width = '45px';
            container.style.height = '35px';
            container.style["font-size"] = '13px';
            container.style["font-weight"] = '900';
            container.style.padding = '7PX';

            return container;
          }
        });

	var logo = L.control({position: 'bottomleft'});
    logo.onAdd = function(map){
        var div = L.DomUtil.create('div', 'myclass');
        div.innerHTML= "<img src='images/received_strength_analysis.png'/>";
        return div;
    }
     logo.addTo(map); 
     map.addControl(new toggleOfflineOnlineMap());
     map.addControl(new compasImg());
     map.addControl(new currentAngle());

}


var addTower = function(position,url)
{
    try
    {

        var myIcon = L.icon
        ({
                iconUrl: url,
                //iconSize: [15, 15],
                iconSize: [30, 30],
                iconAnchor: [15, 28]
                //iconAnchor: [7, 7]
        })
        towerMarkerLayer.clearLayers();

        towerMarker = L.marker(position,{icon:myIcon}).addTo(towerMarkerLayer);
        map.setZoom(14);
        try
        {
            map.panTo(position);
        }
        catch(err)
        {

        }

    }
    catch(err)
    {

    }

    return towerMarker;

}

var addPolyLine = function(start,end,lineColor)
{

    // create a red polyline from an array of LatLng points
    var latlngs = [
        start,
        end
    ];
    var polyline = L.polyline(latlngs, {color: lineColor,opacity:0.2}).addTo(map);
    // zoom the map to the polyline
    map.fitBounds(polyline.getBounds());
   
    return polyline;
}


var calulateLatLongAtGivenAngleAndDistance = function(lat1,lon1,angle,dist,color)
{
	//console.log("Distance is "+distance);
	//alert("Angle in final calculation"+angle);
    return latLng= L.GeometryUtil.destination(L.latLng(lat1,lon1),angle,dist);
    //radius=6378.14;
    //brng = angle * (3.14/180);
    //latitude2 = Math.asin(Math.sin(lat)*Math.cos(distance/radius) + Math.cos(lat)*Math.sin(lat/radius)*Math.cos(brng));
    //longitude2 = lng + Math.atan2(Math.sin(brng)*Math.sin(distance/radius)*Math.cos(lat),Math.cos(distance/radius)-Math.sin(lat)*Math.sin(latitude2));
	
	//var start = L.marker([28.5492602, 77.1790986]).addTo(map);
	
	var start = L.marker([lat1, lon1]).addTo(map);
	var length = 5
	//var angle = 120
	
	
	if(angle >=0 && angle<=90){
		angle=90-angle
	}
	else if(angle > 90 && angle<=180){
		angle=(180-angle)+270
	}
	else if(angle >180 && angle<=270){
		angle=(270-angle)+180;
	}
	else{
		angle=(360-angle)+90;
	}
	
	
	
	
	var end_x = start.getLatLng().lng + length * Math.cos(angle * Math.PI / 180)
	var end_y = start.getLatLng().lat + length * Math.sin(angle * Math.PI / 180)
	var end = L.marker([end_y, end_x]).addTo(map)
		
	var line = L.polyline([start.getLatLng(), end.getLatLng()],{color: color,opacity:0.6}).addTo(map)
	polylines.push(line);
	
	
	//var lat = lat1 + dist * Math.cos(angle * Math.PI / 180)
	//var lng = lon1 + dist * Math.sin(angle * Math.PI / 180)

	/*var a = 6378137,
     b = 6356752.3142,
     f = 1 / 298.257223563, // WGS-84 ellipsiod
     s = dist,
     alpha1 = brng;
     sinAlpha1 = Math.sin(alpha1),
     cosAlpha1 = Math.cos(alpha1),
     tanU1 = (1 - f) * Math.tan(toRad(lat1)),
     cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1,
     sigma1 = Math.atan2(tanU1, cosAlpha1),
     sinAlpha = cosU1 * sinAlpha1,
     cosSqAlpha = 1 - sinAlpha * sinAlpha,
     uSq = cosSqAlpha * (a * a - b * b) / (b * b),
     A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq))),
     B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq))),
     sigma = s / (b * A),
     sigmaP = 2 * Math.PI;
 while (Math.abs(sigma - sigmaP) > 1e-12) {
  var cos2SigmaM = Math.cos(2 * sigma1 + sigma),
      sinSigma = Math.sin(sigma),
      cosSigma = Math.cos(sigma),
      deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
  sigmaP = sigma;
  sigma = s / (b * A) + deltaSigma;
 };
 var tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1,
     lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1, (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp)),
     lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1),
     C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha)),
     L = lambda - (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM))),
     revAz = Math.atan2(sinAlpha, -tmp); // final bearing
 	console.log(toDeg(lat2))
 	console.log(lon1 + toDeg(L));
 	
 	lat = Number(Number(toDeg(lat2)));	
	lng = Number(Number(lon1 + toDeg(L)));*/
	latLng={"lat":lat1,"lng":lon1};
 	return latLng;
	
	
	//return destVincenty(lat,lng,angle,distance);
	//console.log(destVincenty(lat,lng,angle,distance));
	//console.log(latitude2+" "+longitude2);
	//return latLng;
}

function destVincenty(lat1, lon1, brng, dist) {
 var a = 6378137,
     b = 6356752.3142,
     f = 1 / 298.257223563, // WGS-84 ellipsiod
     s = dist,
     alpha1 = brng;
     sinAlpha1 = Math.sin(alpha1),
     cosAlpha1 = Math.cos(alpha1),
     tanU1 = (1 - f) * Math.tan(toRad(lat1)),
     cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1,
     sigma1 = Math.atan2(tanU1, cosAlpha1),
     sinAlpha = cosU1 * sinAlpha1,
     cosSqAlpha = 1 - sinAlpha * sinAlpha,
     uSq = cosSqAlpha * (a * a - b * b) / (b * b),
     A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq))),
     B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq))),
     sigma = s / (b * A),
     sigmaP = 2 * Math.PI;
 while (Math.abs(sigma - sigmaP) > 1e-12) {
  var cos2SigmaM = Math.cos(2 * sigma1 + sigma),
      sinSigma = Math.sin(sigma),
      cosSigma = Math.cos(sigma),
      deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
  sigmaP = sigma;
  sigma = s / (b * A) + deltaSigma;
 };
 var tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1,
     lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1, (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp)),
     lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1),
     C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha)),
     L = lambda - (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM))),
     revAz = Math.atan2(sinAlpha, -tmp); // final bearing
 	console.log(toDeg(lat2))
 	console.log(lon1 + toDeg(L));
 	return L.latLng(toDeg(lat2),lon1 + toDeg(L));
 //return new LatLon(toDeg(lat2), lon1 + toDeg(L));
};

function toDeg(n) {
 return n * 180 / Math.PI;
};

function toRad(n) {
 return n * Math.PI / 180;
};
var createArcFromGivenAngle = function(center,angle)
{
    var truf_center = turf.point(center);//([-75, 40]);

    var radius = 100;

    //var bearing1 = angle/2;

    var bearing1 = (angle - 30)<0?360+(angle-30):(angle-30);

    var bearing2 = (angle + 30)>360?(angle + 30)-360:(angle + 30);

    var arc = turf.lineArc(truf_center, radius, bearing1, bearing2);


    var latlngs = [];
    latlngs.push(center);
    var len = arc.geometry.coordinates.length
    for(i=0;i<arc.geometry.coordinates.length;i++)
    {
        //latlngs.push(arc.geometry.coordinates[i]);
    }
latlngs.push(arc.geometry.coordinates[0],arc.geometry.coordinates[len-1]);


    var polygon = L.polygon(latlngs, {color: 'red'}).addTo(map);
    //map.fitBounds(polygon.getBounds());
    return polygon;
}





/*************************** MAP Section **********************************/
/*****************************validation***********************************/

var validateAddNodeForm = function()
{
    var emptyCheck = false;
    $("#add_node_form input").each(function(){
        if(!checkIfTextBoxIsEmpty($(this)))
        {

            emptyCheck = true;
            //break;
        }
    });
    if(emptyCheck)
    {
        swal("Error","Please Fill All the details","error");
        return false;
    }

    if(!ValidateIPaddress($("#node_ip").val()))
    {
                return false;
    }

    return true;
}


var validateDefForm = function()
{
    var emptyCheck = false;
    $("#def_form input").each(function(){
        if(!checkIfTextBoxIsEmpty($(this)))
        {

            emptyCheck = true;
            //break;
        }
    });
    if(emptyCheck)
    {
        swal("Error","Please Fill All the details","error");
        return false;
    }
    return true;
}

var checkIfTextBoxIsEmpty = function(element)
{
    if(element.val().trim().length <= 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ValidateIPaddress(ipaddress)
{
 if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))
  {
    return (true)
  }
swal("Error","You have entered an invalid IP address!","error")
return (false)
}
/*****************************validation***********************************/

var initData = function()
{
    loadLeafMap();

    var ctx = document.getElementById('canvas').getContext('2d');
    
    myLineChart = new Chart(ctx, config1);
	
	var ctx1 = document.getElementById('canvas1').getContext('2d');
  
	myScatterChart = Chart.Scatter(ctx1, config2);
	
    Chart.defaults.global.animation = false;
    

    $('#node_color').colorpicker();
    $('#color').colorpicker();
    getNodesData();
    connectSocket();
   
}

function getDeviceInfo(deviceId){
	 $.ajax({
	        url:serviceUrls.deviceinfo+"/"+deviceId,
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",

	        success:function(data)
	        {
	        	
	        	$("#node_name").val(data.name);
	        	$("#node_ip").val(data.ip_add);
	        	$("#node_color").val(data.color);
	        	$("#node_lat").val(data.lat);
	        	$("#node_lon").val(data.lon);
	        
	        	$("#node_id").val(data.id);
	        	if(data.id!=0){
	        		$('#node_ip').attr('readonly', true);
	        	} 
	        	$("#node_color").css("background",$("#node_color").val());
	             $("#node_color").css("color",$("#node_color").val());
	        	
	        },
	 error:function(){
		 swal("Error","Something Went Wrong","error");
	 }

	    });
}

var testInit  = function()
{
    var polylines = {};
    addTower([28,73],"images/tower.png");

    lineEndPoint = calulateLatLongAtGivenAngleAndDistance(28,73,0,100);
    var line = ([28,73],lineEndPoint,'red');
    line.id = 1
    polylines[1] =line ;

    var updateLine  = function(id,angle)
    {
        line = polylines[id];
        var path =  line.getLatLngs();
        lineEndPoint = calulateLatLongAtGivenAngleAndDistance(path[0].lat,path[0].lng,angle,100);
        path[1] = lineEndPoint;
        line.setLatLngs(path);
    }
    var count = 0;

}

function getPtzInfo(){
	
	 $.ajax({
	        url:serviceUrls.ptzinfo+"/"+$("#current_node_ip").val(),
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",

	        success:function(data)
	        {
	        	
	        	$("#ptz_ip").val(data.ptz_ip);
	        	$("#ptz_angle_offset").val(data.ptz_offset);
	        	
	        }

	    });
}

function addPtzNode(){
	
	
	 
	var ptzData =
    {
            "device_ip":$("#current_node_ip").val(),
            "ptz_ip":$("#ptz_ip").val(),
            "ptz_offset":$("#ptz_angle_offset").val()
            
    }
	$.ajax({
	        url:serviceUrls.addPtz+"/"+$("#current_node").val(),
	        data:ptzData,
	        type:"post",
	        
	        success:function(data)
	        {
	            swal("Success","Ptz Data Has Been Saved","success");
	            $("#addPtzModal").modal('hide');
	           
	           
	            
	        }

	    });
}

$(document).ready(function()
{
	//document.getElementById("manual").disabled = true;
	//document.getElementById("auto").disabled = true;
	getColorCodeList();
    events();
    initData();
});

function loadPriorityList()
{
        $.ajax({
            url:serviceUrls.getPriorityList,
            type:"post",
            success:function(data)
            {	
            	$('#sortable').html('');
            	var priorityList='';
            	priorityList+='<li class="unsortable"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Manual</li>';
            	for(var i=2;i<=4;i++)
            	{
            	  priorityList+='<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+data[i-1].name+'</li>';
            	}
            	priorityList+='<li class="unsortable"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Auto</li>';
            	$('#sortable').html(priorityList);
            }

        });
}

function loadSystemConfiguration()
{
        $.ajax({
            url:serviceUrls.getSystemConfiguration,
            type:"post",
            success:function(data)
            {	
            	$('input[name=radiobtn][value="'+data.ugs+'"]').prop('checked',true);
            	$('input[name=radio][value="'+data.tmdas+'"]').prop('checked',true);
				$('input[name=radiobtn1][value="'+data.auto+'"]').prop('checked',true);
            	$("#timeout").val(data.timeout);
            	$("#validity").val(data.validity);
            	$("#selftimeout").val(data.selftimeout);
            }

        });
}

function getOfflineData(reportType){
	

    startDate=$("#search-from-date").val().split("/").join("-")
    endDate=$("#search-to-date").val().split("/").join("-");
    
        $.ajax({
        url:serviceUrls.offlineData+"/"+startDate+"/"+endDate+"/"+$("#current_node").val(),
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
        type:"post",

        success:function(data)
        {
            //generateCsvData(data);
        	if(reportType=='csv'){
        		 createCsvFile(data);
        	}
        	else if(reportType=='tsv'){
        		createTsvFile(data);
        	}
        	else{
        		 plotOfflineData(data);
        	}
           
           
            
        }

    });
}



 async function plotOfflineData(data){
	xData = [];
    for( var i in data) {

        let delayres = await delay(1000);
        updateOfflinePeekTable(data[i]);
        createOrUpdateOfflineGraph(data[i]);
    }
}
 async function delay(delayInms) {
    return new Promise(resolve  => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }

 $(document).ready(function () {
        $("input[type='radio']").on('change', function () {
             var selectedValue = $("input[name='dataAccessRadio']:checked").val();
             if (selectedValue=="Offline") {
                 getRealTimeData=false;
                 $("#dateFilter").show();
               }
             else{
                 getRealTimeData=true;
                 $("#dateFilter").hide();
             }
         });

        $("#search-from-date").datetimepicker();
        $("#search-to-date").datetimepicker();
//        $('#search-from-date, #search-to-date').datetimepicker();
    });
 
 
 function createCsvFile (data) {

	 const rows = [["Node", "Power", "Frequency","Timestamp" ]];
	 let csvContent = "data:text/csv;charset=utf-8,";
//	 rows.forEach(function(rowArray){
//	    let row = rowArray.join(",");
//	    csvContent += row + "\r\n";
//	 }); 
	 csvContent += "Node,Power,Frequency,Timestamp"+ "\r\n";
	 for(var i in data)
	    {
		 		let row
		 		records=data[i];
		 		for (r in records){
		 			
//		 			console.log(records[r].ptzangle);
		 			row=records[r].deviceName+","+records[r].power+","+records[r].frequency+","+records[r].time;
		 		}
		 		
		 		csvContent += row + "\r\n";
	   }
	 var encodedUri = encodeURI(csvContent);
	 var link = document.createElement("a");
	 link.setAttribute("href", encodedUri);
	 link.setAttribute("download", "my_data.csv");
	 link.innerHTML= "Click Here to download";
	 document.body.appendChild(link); // Required for FF

	 link.click(); 
	}
 
 function createTsvFile(data){
	 const rows = [["Node", "Power", "Frequency","Timestamp" ]];
	 let csvContent = "data:text/csv;charset=utf-8,";
//	 rows.forEach(function(rowArray){
//	    let row = rowArray.join(",");
//	    csvContent += row + "\r\n";
//	 }); 
	 csvContent += "Node,Power,Frequency,Timestamp"+ "\r\n";
	 for(var i in data)
	    {
		 		let row
		 		records=data[i];
		 		for (r in records){
//		 			console.log(records[r].deviceName);
		 			row=records[r].deviceName+"\t"+records[r].power+"\t"+records[r].frequency+"\t"+records[r].time;
		 		}
		 		
		 		csvContent += row + "\t\n";
	   }
	 var encodedUri = encodeURI(csvContent);
	 var link = document.createElement("a");
	 link.setAttribute("href", encodedUri);
	 link.setAttribute("download", "my_data.tsv");
	 link.innerHTML= "Click Here to download";
	 document.body.appendChild(link); // Required for FF

	 link.click(); 
	}
 
 function getColorCode(signal){
 
 	
	 if(signal<=0 && signal>-10){
		 return colorToHex(255,0,0);
	 }
	 else if(signal <=10 && signal>-20){
		 return colorToHex(255,128,0);
	 }
	 else if(signal<=-20 && signal>-30){
		 return colorToHex(255,165,0);
	 }
	 
	 else if(signal<=-30 && signal>-40){
		 return colorToHex(255,206,0);
	 }
	 
	 else if(signal<=-40 && signal>-50){
		 return colorToHex(255,255,0);
	 }
	 
	 else if(signal<=-50 && signal>-60){
		 return colorToHex(184,255,0);
	 }
	 
	 else if(signal<=-60 && signal>-70){
		 return colorToHex(0,255,0);
	 }
	 else if(signal<=-70 && signal>-80){
		 return colorToHex(0,208,0);
	 }
	 else if(signal<=-80 && signal>-90){
		 return colorToHex(0,196,196);
	 }
	 else if(signal<=-90 && signal>-100){
		 return colorToHex(0,148,255);
	 }
	 else if(signal<=-100 && signal>-110){
		 return colorToHex(80,80,255);
	 }
	 else if(signal<=-110 && signal>-120){
		 return colorToHex(0,38,255);
	 }
	 else if(signal<=-120 && signal>-130){
		 return colorToHex(142,63,255);
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
 }
 function colorToHex(red, green, blue) {

		green = parseInt(green);
		blue = parseInt(blue);
		red = parseInt(red);
		return "#" + hex(red) + hex(green) + hex(blue);

	}
 
 function hex(x) {
		var hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f");
		return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	}

	 


