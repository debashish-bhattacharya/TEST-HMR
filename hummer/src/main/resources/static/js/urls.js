var apiPrefix = "api";
var faultPrefix = "fault";
/*var serviceUrls=
{
		"getNodesData":apiPrefix+"/getDevicesList",
		"getNodeConfig":apiPrefix+"/getConfigDataByID",
		"addNode":apiPrefix+"/addDevice",
		"getNodeDetails":apiPrefix+"/getDeviceInfo",
		"updateConfig":apiPrefix+"/recordConfigData",
		"peakData":apiPrefix+"/getPeakInfoData",
		"ledStatus":apiPrefix+"/getLedOnStatus",
		"startDef":apiPrefix+"/startDef"
			
}*/



var serviceUrls=
{
		"getNodesData":apiPrefix+"/getDevicesList",
		"addBandFilterFrq":apiPrefix+"/addBandFilterFrq",
		"UpdateBandFilterFrq":apiPrefix+"/UpdateBandFilterFrq",
		"getNodeConfig":apiPrefix+"/getConfigDataByID",
		"getNodeConfigByRoom":apiPrefix+"/getConfigDataByRoom",
		"addNode":apiPrefix+"/addDevice",
		"getPtzDataNowVar":apiPrefix+"/getPtzData",
		"getNodeDetails":apiPrefix+"/getDeviceInfo",
		"updateConfig":apiPrefix+"/recordConfigData",
		"peakData":apiPrefix+"/getPeakInfoData",
		"oldPeakData":apiPrefix+"/getOldPeakInfoData",
		"alarmData":apiPrefix+"/getAlarmInfoData",
		"getColorStripData":apiPrefix+"/getColorStripData",
		"maximizeData":apiPrefix+"/getMaximizeData",
		"eventCue":apiPrefix+"/getEventCue",
		"ledStatus":apiPrefix+"/getLedOnStatus",
		"startDef":apiPrefix+"/rotateDevice",
		"offlineData":apiPrefix+"/peakinfo/range/",
		"ptzinfo":apiPrefix+"/getPTZInfo/",
		"addPtz":apiPrefix+"/addPTZ/",
		"stopDef":apiPrefix+"/stopRotateDevice",
		"getStrengthAnalysis":apiPrefix+"/strength-analysis",
		"deviceinfo":apiPrefix+"/getDeviceInfo/",
		"deleteNode":apiPrefix+"/deleteDevice",
		"deleteNodeData":apiPrefix+"/deleteNode",
		"addMaskingData":apiPrefix+"/addMaskingData",
		"defineColorStrips":apiPrefix+"/defineColorStrips",
		"getColorCodeList":apiPrefix+"/getColorCodeList",
		"addSystemConfiguration":apiPrefix+"/addSystemConfiguration",
		"updatePriority":apiPrefix+"/updatePriority",
		"getMaskingDataList":apiPrefix+"/getMaskingDataList",
		"getPriorityList":apiPrefix+"/getPriorityList",
		"getSystemConfiguration":apiPrefix+"/getSystemConfiguration",
		"checkNodeStatus":apiPrefix+"/checkNodeStatus",
		"addJmNode":apiPrefix+"/addJmDevice",
		"getJmData":apiPrefix+"/getJmData",
		"ptzOperation":apiPrefix+"/ptzOperation",
		"bmsData":apiPrefix+"/bmsData",
		"getBandDataList":apiPrefix+"/getBandDataList",
		"devices":apiPrefix+"/devices",
		"prop":apiPrefix+"/bms/prop",
		"fault":apiPrefix+"/fault",
		"ackFault":apiPrefix+"/ackFault",
		"dataPurge":apiPrefix+"/dataPurge",
		"getUsers":apiPrefix+"/getUsers",
		"updatePassword":apiPrefix+"/updatePassword",
		"fetchReport":apiPrefix+"/fetchReport",
		"getCurrentEvent":apiPrefix+"/getCurrentEvent",
		"deleteMaskingData":apiPrefix+"/removeMaskingData",
		"deleteFrqBandData":apiPrefix+"/deleteFrqBandData",
		"deleteColorStrips":apiPrefix+"/removeColorStrips",
		"downloadBackupReport":apiPrefix+"/downloadBackupReport",
		"factoryReset":apiPrefix+"/factoryReset",
		"getBackupHistory":apiPrefix+"/getBackup",
		"deleteBackup":apiPrefix+"/deleteBackup",
		"clearFault":apiPrefix+"/clearFault",
		"ptzUpdate":apiPrefix+"/ptzUpdate",
		"spsUpdate":apiPrefix+"/spsUpdate",
		"getJMList":apiPrefix+"/getJMList",
		"dbBackup":apiPrefix+"/dbBackup",
		"sysLog":apiPrefix+"/sysLog",
		"getProfiles":apiPrefix+"/getProfiles",
		"profileOpr":apiPrefix+"/profileOpr",
		"getConfigProfile":apiPrefix+"/getConfigProfile",
		"getSession":apiPrefix+"/getSession",
		"getRefLevel":apiPrefix+"/getRefLevel",
		"setSession":apiPrefix+"/setSession",
		"sensorConfigDeploy":apiPrefix+"/sensorConfigDeploy",
		"userSensorConfigDeploy":apiPrefix+"/userSensorConfigDeploy",
		"editSensorConfig":apiPrefix+"/editSensorConfig",
		"updateSensorConfig":apiPrefix+"/updateSensorConfig",
		"getSensorConfiguration":apiPrefix+"/getSensorConfiguration",
		
		"getUserSetting":apiPrefix+"/getUserSetting",
		"changeUserSetting":apiPrefix+"/changeUserSetting",
		"getGpsData":apiPrefix+"/getGpsData",
		"getPtzOffset":apiPrefix+"/getPtzOffset",
		"getThresholdMap":apiPrefix+"/getThresholdMap",
		"getAlarm":faultPrefix+"/alarm",
		"setAckTrue":faultPrefix+"/setAckTrue",
		"clearCurrentAlarmById":faultPrefix+"/clearCurrentAlarmById"
}

var endPoint = 
{
		"socket":remoteAddr+"/gs-guide-websocket",
}

var topics = 
{
		"status":"/topic/activeStatus",
		"peak":"/topic/PeakInfo",
		"alarm":"/topic/AlarmInfo",
		"eventcue":"/topic/EventCueInfo",
		"freqAuto":"/topic/FreqAutoEvent",
		"nodeStatus":"/topic/NodeStatus",
		"led":"/topic/LEDInfo",
		"nodeAngle":"/topic/nodeAngle",
		"ptzoperation":"/topic/ptzStarted",
		"jmData":"/topic/JmDeviceData",
		"bmsstatus":"/topic/bmsstatus",
		"fault":"/topic/alarm",
		"configdeploy":"/topic/configdeploy",
		"latlon":"/topic/LatLongValue",
		"currentevent":"/topic/curevent",
		"currentmode":"/topic/currentmode",
		"DbStatus":"/topic/DbStatus"
}

