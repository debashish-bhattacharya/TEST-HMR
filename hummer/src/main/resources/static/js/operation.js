
var graphDataForCurrentSelectedNode=[];
var xData = [];
var freqPowMinThres = [];
var dashborad = {};
var myLineChart = "";
var towerMarkerLayer = null;
var antennaId=21;
polylines = [];
polyLines={};
getRealTimeData=true;
currentNodeLat='';
currentNodeLon=''
nodesInfo={};
maximizeFrequency=false;
var maskingScreenData=[];
var colorStripsData=[];
frequencyAngleCount={};
var current_ip_addr;
var current_device_id;
frequencyFilter=0;
var user;
var reflevel=[];
var cp_calibration=0;
var calibration=0;
var opr_mode='';
var userSensorConfigDeploy = [];
var defaultProfile = 'default'
var systemType='';
var systemGps='';
var user_min_threshold=-78;
var system_default_threshold=-85;
var system_default_threshold1=-95;
var system_default_threshold2=-110;
latLonData={};
ptzAngleOffset=0;
frequencySelected=false;
ptzOperationStarted=false;
 OperartionMode=[];
var createNodesList = function(data)
{
    var html = "";
    for(var i in data)
    {
        html +='<li data-lat="'+data[i].lat+'" data-lon="'+data[i].lon+'" data-color="'+data[i].color+'" data-time="'+data[i].starttime+'" ><span class="color-symbol" style="background:'+data[i].color+' !important;"></span><span><input type="radio" name="nodes" value="'+data[i].id+'" data-id="'+data[i].id+'" data-ip="'+data[i].ip_add+'"></span>&nbsp;<span>'+data[i].name+'</span>&nbsp;-&nbsp;'+data[i].ip_add+'&nbsp;&nbsp;&nbsp;&nbsp;<span style="float:right"><a href="#"  onclick=deleteNode('+data[i].id+')><i class="fa fa-times"></i></a></span></li>'
        nodeInfoData={"lat":data[i].lat,"lon":data[i].lon};
        nodesInfo[data[i].id]=nodeInfoData;
    }
    
    //console.log(nodesInfo);
    $("#nodes_box").html("");
    $("#nodes_box").append(html);
}

function deleteNode(nodeId){
	
	 swal({
	      title: "Are you sure?",
	      text: "Are You Sure You Want To Delete Node!!",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	    }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  $.ajax({
			        url:serviceUrls.deleteNode+"/"+nodeId,
			        
			        dataType:"json",
			        type:"post",
			        success:function(data)
			        {
			        	  swal("Success","Node Data Has Been Deleted","success");	
			        	  responseAlert(data,true);
			        },
			 		error:function(data){
			 			  responseAlert(data,false);
			 		}
			    });
	      } else {
	        swal("Cancelled", "Node Data Has Not Been Deleted", "error");
	      }
	    })
	
}
var getNodesData = function()
{
    $.ajax({
        url:serviceUrls.getNodesData,
        dataType:"json",
        success:function(data)
        {
			current_ip_addr = data[0].ip_add;
			current_device_id = data[0].id;
			currentNodeLat = data[0].lat;
			currentNodeLon = data[0].lon;
			$("#location").html(data[0].lat+", "+data[0].lon);
			getPtzInfo();
        }
    })
}

var getNodeConfigurationData = function(nodeId)
{
	
	
    $.ajax({
        url:serviceUrls.getNodeConfigByRoom,//serviceUrls.getNodeConfig+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeConfig+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
            //$("#current_node").val(nodeId);
            
//            console.log(data);
            
            setupConfigurationInForm(data);
        },
        error:function(data){
        	data = JSON.parse(data.responseText);
        	responseAlert(data,false);
        	calibration=0;
        }
    });
}

var getLeadStatus= function(nodeId)
{
    $.ajax({
        url:serviceUrls.ledStatus+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.ledStatus+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
           
           
           
        }
    });
}

var updateLedStatus = function()
{
    if(data.status == 0)
    {
        $("#led_status").css("background-position","-4490px -512px")
    }
    else
    {
        $("#led_status").css("background-position","-4544px -512px;")
    }
}

var setupConfigurationInForm = function(data)
{
 
	$("#calibtimer").val(data.calib_timer);
    $("#start_freq").val(data.start_freq);
    $("#stop_freq").val(data.stop_freq);
    if(data.threshold==system_default_threshold){
     data.threshold != system_default_threshold?$("#sdth").prop("checked",false):$("#sdth").prop("checked",true);
   //    $("#threshold").disabled=1;
       	document.getElementById("threshold").disabled=true;
     $("#threshold").val(user_min_threshold);
     }
     
    else if(data.threshold==system_default_threshold1){
    data.threshold != system_default_threshold1?$("#sdth").prop("checked",false):$("#sdth").prop("checked",true);
    document.getElementById("threshold").disabled=true;
     $("#threshold").val(user_min_threshold);
   }
   else if(data.threshold==system_default_threshold2){
     data.threshold != system_default_threshold2?$("#sdth").prop("checked",false):$("#sdth").prop("checked",true);
     document.getElementById("threshold").disabled=true;
     $("#threshold").val(user_min_threshold);
   }
   else{
    $("#threshold").val(data.threshold);
   }
    $("#reflevel").val(data.ref_level);
    $("#mask_offset").val(data.mask_offset);
    $("#cable_length").val(data.cable_length);
    $("#preamp_type").val(data.preamp_type);
    $("#distance").val(2000);
    $("#tx_power").val(30);
    $("#fade_margin").val(0);
    $("#calib_timer").val(0);

    $("#config_start_time").val(data.start_time);
    $("#config_stop_time").val(data.stop_time);
    $("#country_code").val(data.country_code);
    $("#current_node_ip").val(data.ip_add);
    
    data.gsm_dl == 0?$("#gsm_dl").prop("checked",false):$("#gsm_dl").prop("checked",true);
    data.wcdma_dl == 0?$("#wcdma_dl").prop("checked",false):$("#wcdma_dl").prop("checked",true);
    data.wifi_band == 0?$("#wifi").prop("checked",false):$("#wifi").prop("checked",true);
    data.band_specific == 0?$("#band_specific").prop("checked",false):$("#band_specific").prop("checked",true);
    data.use_mask == 0?$("#use_mask").prop("checked",false):$("#use_mask").prop("checked",true);
    data.lte_dl == 0?$("#lte").prop("checked",false):$("#lte").prop("checked",true);
    data.calibration == 0?$("#calibrate").prop("checked",false):$("#calibration").prop("checked",true);

    $("#start_1").val(data.band_start1);
    $("#start_2").val(data.band_start2);
    $("#start_3").val(data.band_start3);
    $("#start_4").val(data.band_start4);
    $("#start_5").val(data.band_start5);

    $("#stop_1").val(data.band_stop1);
    $("#stop_2").val(data.band_stop2);
    $("#stop_3").val(data.band_stop3);
    $("#stop_4").val(data.band_stop4);
    $("#stop_5").val(data.band_stop5);

    data.band_en1 == 0?$("#start_stop_check_1").prop("checked",false):$("#start_stop_check_1").prop("checked",true);
    data.band_en2 == 0?$("#start_stop_check_2").prop("checked",false):$("#start_stop_check_2").prop("checked",true);
    data.band_en3 == 0?$("#start_stop_check_3").prop("checked",false):$("#start_stop_check_3").prop("checked",true);
    data.band_en4 == 0?$("#start_stop_check_4").prop("checked",false):$("#start_stop_check_4").prop("checked",true);
    data.band_en5 == 0?$("#start_stop_check_5").prop("checked",false):$("#start_stop_check_5").prop("checked",true);
	
	document.getElementById("start_1").disabled = !$("#start_stop_check_1").checked;
	document.getElementById("stop_1").disabled = !$("#start_stop_check_1").checked;
	document.getElementById("start_2").disabled = !$("#start_stop_check_2").checked;
	document.getElementById("stop_2").disabled = !$("#start_stop_check_2").checked;
	document.getElementById("start_3").disabled = !$("#start_stop_check_3").checked;
	document.getElementById("stop_3").disabled = !$("#start_stop_check_3").checked;
	document.getElementById("start_4").disabled = !$("#start_stop_check_4").checked;
	document.getElementById("stop_4").disabled = !$("#start_stop_check_4").checked;
	document.getElementById("start_5").disabled = !$("#start_stop_check_5").checked;
	document.getElementById("stop_5").disabled = !$("#start_stop_check_5").checked;


    setupStatusData(data);
    getPtzInfo();
    calculateRbwBandwidth();

}





var updateConfig = function(calb)
{    

	
   if(calibration==1 && calb!=2){
   swal("Error","Scanning is in progress to proceed please stop Scanning","error");
   return;
   }
	cp_calibration=0;
	if( parseInt($("#start_freq").val())<9 || parseInt($("#start_freq").val())>6000 || $("#start_freq").val()==''){
        swal("Error","Start Frequency should be between given range","error");
        return;
    }
	
	if( parseInt($("#stop_freq").val())<9 || parseInt($("#stop_freq").val())>6000 || $("#stop_freq").val()==''){
        swal("Error","Stop Frequency should be between given range","error");
        return;
    }
	
	if( parseInt($("#start_freq").val())>=parseInt($("#stop_freq").val())){
        swal("Error","Start Freq Should be Less Than Stop Freq","error");
        return;
    }
	
	if( parseInt($("#threshold").val())<-100 || parseInt($("#threshold").val())>-10 || $("#threshold").val()==''){
        swal("Error","Threshold should be between given range","error");
        return;
    }
	
	if( parseInt($("#mask_offset").val())<-10 || parseInt($("#mask_offset").val())>20 || $("#mask_offset").val()==''){
        swal("Error","Mask Offset should be between given range","error");
        return;
    }
	
	if( parseInt($("#cable_length").val())<0 || parseInt($("#cable_length").val())>100 || $("#cable_length").val()==''){
        swal("Error","Cable Length should be between given range","error");
        return;
    }
	
	if( parseInt($("#distance").val())<0 || parseInt($("#distance").val())>100000 || $("#distance").val()==''){
        swal("Error","Distance should be between given range","error");
        return;
    }
	
	if( parseInt($("#tx_power").val())<-100 || parseInt($("#tx_power").val())>100 || $("#tx_power").val()==''){
        swal("Error","Tx Power should be between given range","error");
        return;
    }
	
	if( parseInt($("#fade_margin").val())<0 || parseInt($("#fade_margin").val())>20 || $("#fade_margin").val()==''){
        swal("Error","Fade Margin should be between given range","error");
        return;
    }
	
	if( parseInt($("#calibtimer").val())<0 || parseInt($("#calibtimer").val())>20 || $("#calibtimer").val()==''){
        swal("Error","Calibration timer should be between given range","error");
        return;
    }
	/*if( parseInt($("#preamp_type").val())<0 || parseInt($("#preamp_type").val())>9 || $("#preamp_type").val()==''){
        swal("Error","Preamp Type should be between given range","error");
        return;
    }*/
	
	if( $("#country_code").val()==''){
        swal("Error","Country code cannot be blank","error");
        return;
    }
	
	if( (parseInt($("#start_1").val())>=parseInt($("#stop_1").val()) && $("#start_stop_check_1").is(":checked"))|| (parseInt($("#start_2").val())>=parseInt($("#stop_2").val()) && $("#start_stop_check_2").is(":checked"))
		|| (parseInt($("#start_3").val())>=parseInt($("#stop_3").val()) && $("#start_stop_check_3").is(":checked")) || (parseInt($("#start_4").val())>=parseInt($("#stop_4").val()) && $("#start_stop_check_4").is(":checked"))
		|| (parseInt($("#start_5").val())>=parseInt($("#stop_5").val()) && $("#start_stop_check_5").is(":checked"))){
        swal("Error","Start Frequency Should be Less Than Stop Frequency","error");
        return;
    }
	
	if( ((parseInt($("#start_1").val())<parseInt($("#start_freq").val()) || parseInt($("#stop_1").val())>parseInt($("#stop_freq").val())) && $("#start_stop_check_1").is(":checked"))
		|| ((parseInt($("#start_2").val())<parseInt($("#start_freq").val()) || parseInt($("#stop_2").val())>parseInt($("#stop_freq").val())) && $("#start_stop_check_2").is(":checked"))
		|| ((parseInt($("#start_3").val())<parseInt($("#start_freq").val()) || parseInt($("#stop_3").val())>parseInt($("#stop_freq").val())) && $("#start_stop_check_3").is(":checked")) 
		|| ((parseInt($("#start_4").val())<parseInt($("#start_freq").val()) || parseInt($("#stop_4").val())>parseInt($("#stop_freq").val())) && $("#start_stop_check_4").is(":checked"))
		|| ((parseInt($("#start_5").val())<parseInt($("#start_freq").val()) || parseInt($("#stop_5").val())>parseInt($("#stop_freq").val())) && $("#start_stop_check_5").is(":checked"))){
        swal("Error","Start Frequency and Stop Frequency should be between main frequency interval","error");
        return;
    }

	/*Feature: No Two Intervals Can Have Same Frequency Band*/
	/*Taking The CheckBox Number In An Array statusOfCheckBoxChild Which Are Enabled On Clicking Apply Button*/
	var statusOfCheckBox = [ $("#start_stop_check_1").is(":checked"), $("#start_stop_check_2").is(":checked"), $("#start_stop_check_3").is(":checked"), $("#start_stop_check_4").is(":checked"), $("#start_stop_check_5").is(":checked")];
	var statusOfCheckBoxChild = [];
	var locationOfCheckBox = 0;
	for(i=0; i<statusOfCheckBox.length; i++){
		if(statusOfCheckBox[i]){
			statusOfCheckBoxChild[locationOfCheckBox++]=i+1;
		}
	}
	
	for(i=0; i<statusOfCheckBoxChild.length; i++){
		for(j=0; j<statusOfCheckBoxChild.length && j!=i; j++){
			if( (parseInt($("#start_"+statusOfCheckBoxChild[i]).val())>= parseInt($("#start_"+statusOfCheckBoxChild[j]).val())  && parseInt($("#start_"+statusOfCheckBoxChild[i]).val())<=parseInt($("#stop_"+statusOfCheckBoxChild[j]).val()) ) || ( parseInt($("#stop_"+statusOfCheckBoxChild[i]).val())>= parseInt($("#start_"+statusOfCheckBoxChild[j]).val()) && parseInt($("#stop_"+statusOfCheckBoxChild[i]).val())<= parseInt($("#stop_"+statusOfCheckBoxChild[j]).val())) )
				{
				swal("Error","No two intervals can have same frequency bands","error");
		        return;
				}				
		}
	}
	

/*	if( (parseInt($("#start_1").val())>=parseInt($("#stop_1").val()) && $("#start_stop_check_1").is(":checked"))|| (parseInt($("#start_2").val())>=parseInt($("#stop_2").val()) && $("#start_stop_check_2").is(":checked"))
		|| (parseInt($("#start_3").val())>=parseInt($("#stop_3").val()) && $("#start_stop_check_3").is(":checked")) || (parseInt($("#start_4").val())>=parseInt($("#stop_4").val()) && $("#start_stop_check_4").is(":checked"))
		|| (parseInt($("#start_5").val())>=parseInt($("#stop_5").val()) && $("#start_stop_check_5").is(":checked"))){
        swal("Error","No two intervals can have same frequency bands","error");
        return;
    }
*/	
	var antennanum = $("#antennanum").val()
	var preamptypepath = $("#preamptypepath").val()
	var userthreshold=$("#threshold").val()
	if($("#sdth").is(":checked")) {
	if($("#reflevel").val()=='l1'){
	userthreshold=system_default_threshold;
	}
	else if($("#reflevel").val()=='l2'){
	userthreshold=system_default_threshold1;
	}
	else if($("#reflevel").val()=='l3'){
	userthreshold=system_default_threshold2;
	}
	}
	var clb=0;
	if(calb==1){
	 	 clb=$("#calibtimer").val();
	 	 calibration=1;
	}
	else{
		calibration=0;
	}
	var startfrq='';
    var stopfrq='';
	var status='';
    var allRowsInGrid = $('#band_freq_grid').jqGrid('getRowData');
 
	for (i = 0; i < allRowsInGrid.length; i++) {
	
 		startfrq= allRowsInGrid[i].start_frq;
		stopfrq=allRowsInGrid[i].stop_frq;
		status = allRowsInGrid[i].status;
		var frqData =
        {
             "startfrequency":startfrq,
             "stopfrequency":stopfrq,
             "status":status
        }

		
  
  $.ajax({
            url:serviceUrls.UpdateBandFilterFrq,
            type:"post",
            data:frqData,
            success:function(data)
            {
			}
	});
		
	}
    var configData =
    {
              "floor": "1",
              "room": "1",
              "calibration": calb,
              "start_freq": $("#start_freq").val(),
              "stop_freq": $("#stop_freq").val(),
              "threshold": userthreshold,
              "mask_offset": $("#mask_offset").val(),
              "use_mask": $("#use_mask").is(":checked")?"1":"0",
 				"band_specific": $("#band_specific").is(":checked")?"1":"0",
              "ref_level":  $("#reflevel").val(),
              "start_time":  $("#config_start_time").val(),
              "stop_time": $("#config_stop_time").val(),
              "country_code":$("#country_code").val(),

              "cable_length": $("#cable_length").val(),
              "preamp_type": $("#preamp_type").val(),
              "distance":$("#distance").val(),
              "tx_power":$("#tx_power").val(),
              "fade_margin":$("#fade_margin").val(),
             // "calib_timer":$("#calib_timer").val(),
              "calib_timer":clb,
              "gsm_dl": $("#gsm_dl").is(":checked")?"1":"0",
              "wcdma_dl": $("#wcdma_dl").is(":checked")?"1":"0",
              "wifi_band": $("#wifi").is(":checked")?"1":"0",
              "lte_dl": $("#lte").is(":checked")?"1":"0",
              "band_start1": $("#start_1").val(),
              "band_stop1": $("#stop_1").val(),
              "band_en1": $("#start_stop_check_1").is(":checked")?"1":"0",
              "band_start2": $("#start_2").val(),
              "band_stop2": $("#stop_2").val(),
              "band_en2": $("#start_stop_check_2").is(":checked")?"1":"0",
              "band_start3": $("#start_3").val(),
              "band_stop3": $("#stop_3").val(),
              "band_en3": $("#start_stop_check_3").is(":checked")?"1":"0",
              "band_start4": $("#start_4").val(),
              "band_stop4": $("#stop_4").val(),
              "band_en4": $("#start_stop_check_4").is(":checked")?"1":"0",
              "band_start5": $("#start_5").val(),
              "band_stop5": $("#stop_5").val(),
              "band_en5": $("#start_stop_check_5").is(":checked")?"1":"0"
              
            }

    $.ajax({
        url:serviceUrls.updateConfig+"/"+current_device_id+"/"+antennanum+"/"+preamptypepath,
        //url:"http://10.100.207.117:9000/security_server/"+serviceUrls.updateConfig+"/"+$("#current_node").val(),
        dataType:"json",
        type:"post",
        data:configData,
        success:function(data)
        {
            //swal("Success","Configuration Data Has Been Saved","success");
            if(calb==1){
             calibration=1;
             msg="Calibration Has Been Started";
              }
            else
          	  msg="Configuration Data Has Been Saved";
          	  calibration=0;
        	swal({
          	     title: "Success",
          	     text: msg,
          	     type: "success",
          	     timer: 2000,
          	     buttons:false,
          	     className: "right-bottom"
          	     });
            responseAlert(data,false);
            setupConfigurationInForm(configData);
            setupStatusData(configData);
      	   
        },
        error:function(data){
        	data = JSON.parse(data.responseText);
        	responseAlert(data,false);
        	calibration=0;
        }
    });
}


var setupStatusData = function(data)
{
	
	//console.log(data)
    $(".status_bar tr td").removeClass();
    /*$(".status_bar tr td").each(function(){
            $(this).removeClass("ON");
            $(this).removeClass("OFF");
    })*/
	$("#Opr_mode").parent().addClass("Operation Mode");
	if(data.ref_level=='l1'){
    $("#Opr_mode").html("Default");
    }if(data.ref_level=='l2'){
    $("#Opr_mode").html("Normal");
    }if(data.ref_level=='l3'){
    $("#Opr_mode").html("High Sensitivity");
    }
	$("#start_freq_status").parent().addClass("START_TIME");
    $("#start_freq_status").html(data.start_freq);
	$("#stop_freq_status").parent().addClass("START_TIME");
    $("#stop_freq_status").html(data.stop_freq);
	 $("#threshold_status").parent().addClass("START_TIME");
    if (data.threshold==system_default_threshold)
	  $("#threshold_status").html("System Default");
	   else if (data.threshold==system_default_threshold1)
	  $("#threshold_status").html("System Default");
	  else if (data.threshold==system_default_threshold2)
	  $("#threshold_status").html("System Default");
	else  
		$("#threshold_status").html(data.threshold);
	
  
    $("#gsm_status").html(data.gsm_dl     == 0?"OFF":"ON");
    $("#gsm_status").parent().addClass(data.gsm_dl     == 0?"OFF":"ON");

    $("#lte_status").html(data.lte_dl     == 0?"OFF":"ON");
    $("#lte_status").parent().addClass(data.lte_dl     == 0?"OFF":"ON");

    $("#wifi_status").html(data.wifi_band     == 0?"OFF":"ON");
    $("#wifi_status").parent().addClass(data.wifi_band     == 0?"OFF":"ON");

    $("#wcdma_status").html(data.wcdma_dl == 0?"OFF":"ON");

    $("#wcdma_status").parent().addClass(data.wcdma_dl     == 0?"OFF":"ON");

    //$("#sweep_status").html("Unknown");
    //$("#mask_status").html(data.use_mask     == 0?"OFF":"ON");
    //$("#mask_status").parent().addClass(data.use_mask == 0?"OFF":"ON");
	$("#start_status").parent().addClass("START_TIME");
	$("#location").parent().addClass("START_TIME");
    //$("#start_status").html($(".selectednode").data("time"));

    //getNodeStartTimeAndStatus()


}

var getNodeStartTimeAndStatus = function (nodeId)
{
    $.ajax({
url:serviceUrls.getNodeDetails+"/"+current_device_id,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeDetails+"/"+$("#current_node").val(),
        success:function(data)
        {
            setStatusOfNode(data.state,data.is_active);
            setNodePositionOverMap(data);
        }
    });

    //$("#node_status").html("<p>Not reachable</p>");
}

var checkNodeRechablity = function(isActive,state)
{
    if(isActive == "true")
    {
        if(state == "DOWN")
            state = "reachable";
    }
    else
    {
        state = "not reachable";
    }
    return state;
}

var setStatusOfNode  = function(text,isActive)
{

    if(isActive != null)
    {
        text = checkNodeRechablity(isActive,text);
    }

    switch(text.toLowerCase())
    {
        case "not reachable":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("not_reachable");
        break;
        case "reachable":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("reachable");
        break;
        case "running":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("reachable");
        break;
        case "Scanning":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("Scanning");
        break;
        case "monitoring":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("monitor");
        break;

    }

}


var connectSocket = function()
{

    var stompClient = null;

	xData = [];
    var socket = new SockJS(endPoint.socket);

    stompClient = Stomp.over(socket);
	stompClient.debug = null
    stompClient.connect({}, function (frame)
            {
//          console.log('Connected: ' + frame);
			stompClient.subscribe(topics.status, function (data) {
				var statusData = jQuery.parseJSON(data.body);
//                    console.log(statusData);
				updatedeviceStatus(statusData.id,statusData.ip_add,statusData.state,statusData.is_active)

			});
			
				
			stompClient.subscribe(topics.configdeploy, function (data) {
				createUserSettingsGrid("socket");			
            });
			
            stompClient.subscribe(topics.led, function (data) {

                //callback for socket
//                console.log(data.body);

            });
			
            stompClient.subscribe(topics.nodeStatus, function (data) {
                var nodeData = jQuery.parseJSON(data.body);
//              console.log(statusData);
				createDeviceStatusRow(nodeData);

            });    

            
            stompClient.subscribe(topics.latlon, function (data) {
                var latLon = jQuery.parseJSON(data.body);
                createLatLon(latLon);
				

            });
         
            stompClient.subscribe(topics.currentmode, function (data) 
			{
				var status = data.body;
				console.log(status);
				currentMode(status);
			});
            
            
			stompClient.subscribe(topics.nodeAngle, function (data) {

				//callback for socket
				var angleData = jQuery.parseJSON(data.body);
				updateDirectionOfNode(angleData);
			});
			
			stompClient.subscribe(topics.alarm, function (data) {

				//callback for socket
				var alarmData = jQuery.parseJSON(data.body);
				console.log(alarmData);
			});
			
			stompClient.subscribe(topics.bmsstatus, function (data) 
			{
				var bmsstatus = jQuery.parseJSON(data.body);
				console.log(bmsstatus);
				updateStatusBMS(bmsstatus);
			});
			
			stompClient.subscribe(topics.DbStatus, function (data) 
			{
				swal({
	        	     title: "Success",
	        	     text: data.body,
	        	     type: "success",
	        	     timer: 2000,
	        	     buttons:false,
	        	     className: "right-bottom"
	        	     });
				
			});

            });

}

var openAddDeviceModal = function(){
/*if($('#useType option:selected').attr('data-type')=="modal"){
	$('#myaddDeviceModalLabel').text($('#'+$('#useType option:selected').attr('data-modal_id')).attr('data-value'));
	$('#'+$('#useType option:selected').attr('data-modal_id')).modal('show');
}else{*/
	//var useTypeSelected=$('#useType option:selected').text();
	var useTypeSelected=$('#useType option:selected').val()
	
	/*if(useTypeSelected.indexOf("controller")!=-1 || useTypeSelected.indexOf("ptz")!=-1){
		$('#antennaProfileTd').css('display','none');
	}else{
		$('#antennaProfileTd').css('display','table-cell');	
	}*/	
	
	if(useTypeSelected == 2)
	{
		$("#tr_ptz_offset").css("display","table-cell");
		$("#tr_sector_offset").css("display","table-cell");
		$("#tr_sms_lat").css("display","none");
		$("#tr_sms_lon").css("display","none");
		$("#tr_sms_unit").css("display","none");
	}
	else if(useTypeSelected == 3)
	{
		$("#tr_ptz_offset").css("display","none");
		$("#tr_sector_offset").css("display","none");
		$("#tr_sms_lat").css("display","table-cell");
		$("#tr_sms_lon").css("display","table-cell");
		$("#tr_sms_unit").css("display","table-cell");
	}
	else
	{
		$("#tr_ptz_offset").css("display","none");
		$("#tr_sector_offset").css("display","none");
		$("#tr_sms_lat").css("display","none");
		$("#tr_sms_lon").css("display","none");
		$("#tr_sms_unit").css("display","none");
	}
//}
}
var currentMode = function(data){	
	
	$("#Opr_mode").html(data);
	
}
var validateIp = function(value) {
    var split = value.split('.');
    if (split.length != 4) 
        return false;
            
    for (var i=0; i<split.length; i++) {
        var s = split[i];
        if (s.length==0 || isNaN(s) || s<0 || s>255)
            return false;
    }
    return true;
}

var addDevice = function(){
	
	var useTypeId=$('#useType').val();
	if(useTypeId=="select"){
		alert('Please select Use Type');
		return;
	}
	
	var deviceIp=$('#deviceIp').val().trim();
	if(!validateIp(deviceIp)){
		alert('Please provide valid IP');
		return;
	}
	
	var useTypeSelected=$('#useType option:selected').text();

	/*for(var ips in globalBtsDevices)
	{
		if(globalBtsDevices[ips] == deviceIp)
		{
			alert("Device already present kindly remove first.");
			return false;
		}
	}*/
	var offset;
	var sector_offset;
	var lat;
	var lon;
	var unit;
	
	if($('#useType').val() == 2)
	{
		offset = $("#ptz_offset").val();
		sector_offset = $("#sector_offset").val();
		if(offset==""){
			alert('Please enter North Offset');
			return;
		}
		if(sector_offset==""){
			alert('Please enter Sector Offset');
			return;
		}
		
		offset = parseFloat(offset);
		
		if(offset>360)
		{
			swal("Error","North offset should be less than 360","error");
			return;
		}
		if(offset>180)
			offset = offset-360;
		
		lat = "";
		lon = "";
		unit="";
	}
	else if($('#useType').val() == 3)
	{
		offset = "";
		sector_offset = "";
		lat = $("#sms_lat").val();
		lon = $("#sms_lon").val();
		unit = $("#sms_unit").val();
		if(lat=="" || lon=="" || unit==""){
			alert('Please enter detail in all fields');
			return;
		}
	}
	else
	{
		offset = "";
		sector_offset = "";
		lat = "";
		lon = "";
		unit = "";
	}



	deviceData={"ip":deviceIp,"name":useTypeSelected,"north_offset":offset,"sector_offset":sector_offset,"lat":lat,"lon":lon,"unit":unit};
	
	$.ajax
	({
			url:serviceUrls.addNode,
			data:deviceData,
			type:'post',
			beforeSend:function()
			{
				//$('#addDeviceButton').css("display","none");
				//$('#addDeviceLoadingBox').css("display","block");
			},
			success:function(data)
			{
				swal("Success","Node has been successfully added","success");
				responseAlert(data,false);
				getNodesData();
				getNodeConfigurationData();
				//swal("Success","Node has been successfully added","success");
				/*if(data.result=="success"){
					alert('Device Added Successfully');
					//updateNodesStatus(deviceIp);
					location.reload();
				
				}else{
					alert('Problem in Adding Device');
					$('#addDeviceButton').css("display","block");
					$('#addDeviceLoadingBox').css("display","none");
				}*/
			},
			error:function(data){	
				data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}


var getDevices = function()
{
	$("#useType").html("");
	
	
	$.ajax({
		url:serviceUrls.devices,
		dataType:"JSON",
		success:function(data)
		{
			console.log(data);
			var html = ""
			for(var i in data)
			{
				html +="<option value='"+data[i].id+"'>"+data[i].node_name+"</option>"
			}
			
			$("#useType").append(html);
		}
	})
}

var updatedeviceStatus = function(id,ip,state,isActive)
{
    if($('input[name=nodes]:checked').data("id") == id)
    {
        setStatusOfNode(state,isActive);
    }
}

var addNode = function()
{

    if(validateAddNodeForm())
    {
    	
        var nodeData =
        {
                "name":$("#node_name").val(),
                "ip":$("#node_ip").val(),
                "color":$("#node_color").val(),
                "lat":$("#node_lat").val(),
                "lon":$("#node_lon").val(),
                "id":$("#node_id").val()
        }

        $.ajax({
            url:serviceUrls.addNode,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
            type:"post",
            data:nodeData,
            success:function(data)
            {
                responseAlert(data,true);
               
            }

        });
    }



}



function updatePriority()
{
	var prioNames="";
	var l =document.getElementById('sortable');
	$('#sortable').find('li').each(function()
	{
		prioNames+=$(this).text()+",";
	});
	prioNames=prioNames.substring(0,prioNames.length-1);
    $.ajax({
        url:serviceUrls.updatePriority,
        type:"post",
        data:{"prioNames":prioNames},
        success:function(data)
        {	           
        	//swal("Success","System Configuration data has been saved","success");
        	swal({
        	     title: "Success",
        	     text: "Priority data has been saved",
        	     type: "success",
        	     timer: 2000,
        	     buttons:false,
        	     className: "right-bottom"
        	     });
        }

    });
}

function addSystemConfiguration()
{
	var ugs=$('input[type=radio][name=radiobtn]:checked').val();
	var tmdas=$('input[type=radio][name=radio]:checked').val();
	var auto = $('input[type=radio][name=radiobtn1]:checked').val();
	var timeout=$("#timeout").val();
	var validity=$("#validity").val();
	var selftimeout=$("#selftimeout").val();
	var selfvalidity=$("#selfvalidity").val();
	var range=$("#range").val();
	if((ugs==undefined)||(tmdas==undefined)||(auto==undefined))
	{
        swal("Error","Please select radiobutton","error");
		return;		
	}
	if((timeout=="select")||(validity=="select")||(selftimeout=="select")||(selfvalidity=="select"))
	{
        swal("Error","Please select values from the dropdown","error");
		return;
	}
	if((range<0)||(range>5000))
	{
        swal("Error","Please select values in Range 0 to 5000","error");
		return;
	}
    var systemConfigurationData =
    {
         "ugs":ugs,
         "tmdas":tmdas,
		 "auto":auto,
         "timeout":timeout,
         "validity":validity,
         "selftimeout":selftimeout,   
		 "selfvalidity":selfvalidity,
		 "range":range
    }

    $.ajax({
        url:serviceUrls.addSystemConfiguration,
        type:"post",
        data:systemConfigurationData,
        success:function(data)
        {	           
        	//swal("Success","System Configuration data has been saved","success");
        	swal({
         	     title: "Success",
         	     text: "System Configuration data has been saved",
         	     type: "success",
         	     timer: 2000,
         	     buttons:false,
         	     className: "right-bottom"
         	     });
            //responseAlert(data,true); 
        	$("#system_config").modal('hide');
        },
        error:function(data){	
			data = JSON.parse(data.responseText);
		 	responseAlert(data,false);
		}

    });
}

function defineColorStrips()
{
	var startfreq=$("#emtstartfrequency").val();
	var stopfreq=$("#emtstopfrequency").val();
	var depl=$("#depl").val();
	var epr=$("#epr").val();
	var modulation=$("#modulation").val();
	var networktype=$("#networktype").val().toUpperCase();;
	var color=$("#color").val();
	var visible = "true";
	
	if(stopfreq=="0")
	{
        swal("Error","Stop frequency can't be 0!!!","error");
		return;		
	}
	if(startfreq=="" || stopfreq=="" || epr=="" || depl=="" || modulation=="")
	{
        swal("Error","No field can be blank!!!","error");
		return;		
	}
	if(parseFloat(stopfreq)<=parseFloat(startfreq))
	{
        swal("Error","Stop frequency can't be less than Start frequency!!!","error");
		return;		
	}
	if(networktype=="")
	{
        swal("Error","Please add technology first!!!","error");
		return;
	}
	$.ajax({
			async: false,
            url:serviceUrls.getColorStripData+"/"+defaultProfile,
             dataType:"json",
            success:function(data)
            {	
              colorStripsData =data;

					}
    });
	
	
	
	              for(var i in colorStripsData){
				if(parseFloat(startfreq)>=parseFloat(colorStripsData[i].startfreq) && parseFloat(startfreq)<=parseFloat(colorStripsData[i].stopfreq)){
					swal("Error","Start frequency already present!!!","error");
					return;
				}
				else if(parseFloat(stopfreq)>=parseFloat(colorStripsData[i].startfreq) && parseFloat(startfreq)<=parseFloat(colorStripsData[i].stopfreq)){
					swal("Error","Stop frequency already present!!!","error");
					return;
				}
			}
	
	
	if(color=="")
	{
		color="#ffffff";
		visible="false";
	}
        var colorData =
        {
             "startfreq":startfreq,
             "stopfreq":stopfreq,
			 "depl":depl,
			 "epr":epr,
			 "modulation":modulation,
             "networktype":networktype,
             "color":color,
			 "visible":visible
        }

        $.ajax({
            url:serviceUrls.defineColorStrips,
            type:"post",
            data:colorData,
            success:function(data)
            {	      
				$.ajax({
					url:serviceUrls.getColorCodeList+"/"+defaultProfile,
					dataType:"json",
					success:function(colorData)
					{
						jQuery('#grid').jqGrid('clearGridData');
						for ( var i = 0; i < colorData.length; i++) {
							jQuery("#grid ").jqGrid('addRowData', colorData[i].id, colorData[i]);
						}
						jQuery("#grid").jqGrid().trigger("reloadGrid");
						colorStripsData = colorData;
					}
				});
            	swal("Success","Data has been saved","success");
                //responseAlert(data,true);   
            }

        });
}

function addMaskingData()
{
	var frequency=$("#frequency").val();
	var bandwidth=$("#bandwidth").val();
	var freq_type=$("#freqtype").val();
	if(frequency=="0")
	{
        swal("Error","Frequency can't be 0!!!","error");
		return;		
	}
	if((bandwidth<0 || bandwidth>20) && freq_type=="blacklist")
	{
        swal("Error","Bandwidth can't be less than 0 and greater than 20Mhz !!!","error");
		return;		
	}
	if(freq_type=="select")
	{
        swal("Error","Please select frequency type first!!!","error");
		return;
	}
	if(bandwidth=="0" && freq_type=="whitelist")
	{
        swal("Error","Bandwidth can't be equal to 0 for whitelist Frequencies !!!","error");
		return;		
	}
        var maskingData =
        {
             "frequency":frequency,
             "bandwidth":bandwidth,
             "freqtype":freq_type
        }

        $.ajax({
            url:serviceUrls.addMaskingData,
            type:"post",
            data:maskingData,
            success:function(data)
            {	
				$.ajax({
					url:serviceUrls.getMaskingDataList+"/"+defaultProfile,
					dataType:"json",
					success:function(maskData)
					{
						jQuery('#freq_grid').jqGrid('clearGridData');
						for ( var i = 0; i < maskData.length; i++) {
							jQuery("#freq_grid ").jqGrid('addRowData', maskData[i].id, maskData[i]);
						}
						jQuery("#freq_grid").jqGrid().trigger("reloadGrid");
						maskingScreenData = maskData;
					}
				});
				
            	//swal("Success","Data has been saved","success");
				swal({
	         	     title: "Success",
	         	     text: "Data has been saved",
	         	     type: "success",
	         	     timer: 2000,
	         	     buttons:false,
	         	     className: "right-bottom"
	         	     });
                //responseAlert(data,true);   
            },
			error:function(data){
				data = JSON.parse(data.responseText);
				responseAlert(data,false);
			}
        });
}
function applyBand()
{
	var stratfrequency=$("#startfrequency").val();
	var stopfrequency=$("#stopfrequency").val();
	if( parseInt($("#startfrequency").val())<9 || parseInt($("#startfrequency").val())>6000 || $("#startfrequency").val()==''){
        swal("Error","Start Frequency should be between given range","error");
        return;
    }
	
	if( parseInt($("#stopfrequency").val())<9 || parseInt($("#stopfrequency").val())>6000 || $("#stopfrequency").val()==''){
        swal("Error","Stop Frequency should be between given range","error");
        return;
    }
	
	if( parseInt($("#startfrequency").val())>=parseInt($("#stopfrequency").val())){
        swal("Error","Start Freq Should be Less Than Stop Freq","error");
        return;
    }
	
        var frqData =
        {
             "startfrequency":stratfrequency,
             "stopfrequency":stopfrequency
             
        }

        $.ajax({
            url:serviceUrls.addBandFilterFrq,
            type:"post",
            data:frqData,
            success:function(data)
            {	
				$.ajax({
					 url:serviceUrls.getBandDataList,
					dataType:"json",
					success:function(BandData)
					{
						jQuery('#band_freq_grid').jqGrid('clearGridData');
						for ( var i = 0; i < BandData.length; i++) {
							jQuery("#band_freq_grid ").jqGrid('addRowData', BandData[i].id, BandData[i]);
						}
						jQuery("#band_freq_grid").jqGrid().trigger("reloadGrid");
						//maskingScreenData = maskData;
					}
				});
				
            	//swal("Success","Data has been saved","success");
				swal({
	         	     title: "Success",
	         	     text: "Data has been saved",
	         	     type: "success",
	         	     timer: 2000,
	         	     buttons:false,
	         	     className: "right-bottom"
	         	     });
                //responseAlert(data,true);   
            },
			error:function(data){
				data = JSON.parse(data.responseText);
				responseAlert(data,false);
			}
        });
}


var getMaskingDataList = function()
{
    $.ajax({
        url:serviceUrls.getMaskingDataList+"/"+defaultProfile,
        dataType:"json",
        success:function(data)
        {
           addDataToMaskingGridTable(data);
		   
        }
    });
}
var getBandDataList = function()
{
    $.ajax({
            url:serviceUrls.getBandDataList,
             dataType:"json",
            success:function(data)
            {	
              
              addDataToBandFrqTable(data);
            }
    });
}

var getColorCodeList = function()
{
    $.ajax({
        url:serviceUrls.getColorCodeList+"/"+defaultProfile,
        dataType:"json",
        success:function(data)
        {
           addDataToGridTable(data);
        }
    });
}

var getJmList = function()
{
    $.ajax({
        url:serviceUrls.getJMList,
        dataType:"json",
        success:function(data)
        {
           addJmToGridTable(data);
        }
    });
}

var responseAlert = function(data,reload)
{
	
    if(data.result == "success")
    {
       
        if(reload)
            location.reload();
    }
    else
    {
        swal("Something Went Wrong","Fail : "+data.message,"error");
        if(reload)
            location.reload();
    }

}

var colorOutputData =function()
{
	var len = colorStripsData.length;
	var output = [];
	if(len>5)
		len=5;
	for(var i=0;i<len;i++){
		output.push({
            type: 'box',
            drawTime: 'beforeDatasetsDraw',
            xScaleID: 'x-axis-1',
            xMin: colorStripsData[i].startfreq,
            xMax: colorStripsData[i].stopfreq,
            backgroundColor: colorStripsData[i].color

        })
	}
	return output;
	
}

var addDataToMaskingGridTable = function (data) 
{
	maskingScreenData = data;
	//jQuery("#myGrid").setGridParam({ 'data':  maskingScreenData }).trigger("reloadGrid");
    $("#freq_grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['Frequency','Bandwidth', 'Type'],
        colModel: [{
            name : 'frequency',
            index : 'frequency',
			sortable : false,
            width : 15,
            editable : true
        },{
            name : 'bandwidth',
            index : 'bandwidth',
			sortable : false,
            width : 15,
            editable : true
        },{
			name : 'freqtype',
			index : 'freqtype',
			sortable : false,
			width: 15,
            editable : true
        }],
        pager : '#freq_pager',
        rowNum : 5,
        width : 580,
		height: 'auto',
		loadonce : false,
        viewrecords : true,
        gridview : true,
		caption: "Frequency Configuration"
    });
    jQuery("#freq_grid").jqGrid('navGrid', '#freq_pager', {
		edit: false,
        add: false,
        del: true,
        refresh: false
	},{},{},
	{
		url: serviceUrls.deleteMaskingData,
		caption: "Delete",
		msg: "Delete selected record(s)?",
		bSubmit: "Delete",
		bCancel: "Cancel"
	});
}

var addDataToBandFrqTable = function (data) 
{
//	bandFrqData = data;
	
    $("#band_freq_grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['start Frequency','stop Frequency','Status'],
        colModel: [{
            name : 'start_frq',
            index : 'start_frq',
			sortable : true,
            width : 15,
            editable : true
        },{
            name : 'stop_frq',
            index : 'stop_frq',
			sortable : false,
            width : 15,
            editable : true
        },{
            name : 'status',
            index : 'status',
			sortable : false,
            width : 15,
   	        formatter: "checkbox",
			formatoptions: { disabled: false },
			editable: true   
	}],
        pager : '#band_freq_pager',
        rowNum : 10000,
		multiSort: true,
        width : 580,
		height: 115,
		loadonce : false,
        viewrecords : true,
        gridview : true,
		caption: " Band Frequency Configuration"
    });
    jQuery("#band_freq_grid").jqGrid('navGrid', '#band_freq_pager', {
		edit: false,
        add: false,
        del: true,
        refresh: false
	},{},{},
	{
		url: serviceUrls.deleteFrqBandData,
		caption: "Delete",
		msg: "Delete selected record(s)?",
		bSubmit: "Delete",
		bCancel: "Cancel"
	});
}

 var applyBandWithConfig = function () {
			 ApplyConfig();
		 }
var addDataToGridTable = function (data) 
{
	colorStripsData = data;
    $("#grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['Start Freq','Stop Freq', 'Default Power(dBm)', 'Emitter Range(dBm)', 'Modulation', 'Technology','Color'],
        colModel: [{
            name : 'startfreq',
            index : 'startfreq',
			sortable : false,
            width : 70,
            editable : true
        },{
            name : 'stopfreq',
            index : 'stopfreq',
			sortable : false,
            width : 70,
            editable : true
        },{
            name : 'depl',
            index :'depl',
			sortable : false,
            width : 130,
            editable : true
        },{
            name : 'epr',
            index :'epr',
			sortable : false,
            width : 150,
            editable : true
        },{
            name : 'modulation',
            index :'modulation',
			sortable : false,
            width : 130,
            editable : true
        },{
			name : 'networktype',
			index : 'networktype',
			sortable : false,
			width: 130,
            editable : true
        },{
            name : 'color',
            index :'color',
			sortable : false,
            width : 50,
            editable : true,
			cellattr : function(rowId, val, rawObject, cm, rdata){
				return 'style="background-color:'+rawObject.color+'"';
			},
			formatter:function(val){
				return "";
			}
        }],
        pager : '#pager',
        rowNum : 5,
		shrinkToFit:false,
		height: 'auto',
        width : '580',
		loadonce : true,
        viewrecords : true,
        gridview : true,
		caption: "Emitter Info"
    });
    jQuery("#grid").jqGrid('navGrid', '#pager', {
        edit: false,
        add: false,
        del: true,
        search: false
    },{},{},
	{
		url: serviceUrls.deleteColorStrips,
		caption: "Delete",
		msg: "Delete selected record(s)?",
		bSubmit: "Delete",
		bCancel: "Cancel"
	});
	
}

var addJmToGridTable = function (data) 
{
	//jmData = data;
    $("#jm_grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['JM Id','Antenna Id', 'Sector'],
        colModel: [{
            name : 'deviceid',
            index : 'deviceid',
			sortable : false,
            editable : true
        },{
            name : 'antennaid',
            index : 'antennaid',
			sortable : false,
            editable : true
        },{
            name : 'sector',
            index :'sector',
			sortable : false,
            editable : true
        }],
		autowidth:true,
        pager : '#jm_pager',
        rowNum : 5,
		shrinkToFit:false,
		height: 'auto',
        width : '450',
		loadonce : true,
        viewrecords : true,
        gridview : true,
		caption: "JM Info"
    });
    jQuery("#jm_grid").jqGrid('navGrid', '#jm_pager', {
        edit: false,
        add: false,
        del: false,
        search: false
    },{},{},{}
	);
	
}

var checkBoxListener = function(checkBox){
	
	var no = checkBox.id.charAt(checkBox.id.length-1);
	console.log(no);
	document.getElementById("start_"+no).disabled = !checkBox.checked;
	document.getElementById("stop_"+no).disabled = !checkBox.checked;
	
}


var createUserSettingsGrid = function(callType)
{
	
	$.ajax({
		url: serviceUrls.userSensorConfigDeploy,
        dataType:"json",
        success:function(data)
        {	           
        	userSensorConfigDeploy=data;
        	if(callType=="load"){
        		deployUserSettingsGrid(userSensorConfigDeploy);
        	}else{
        		$("#userSettingsDiv").jqGrid("clearGridData", true)
        		jQuery("#userSettingsDiv").jqGrid('setGridParam', {  datatype: 'local',  data:userSensorConfigDeploy  }).trigger("reloadGrid");
        	}
        },
        error:function(data){
        	swal("Error","Sensor Configuration Cannot Be Loaded","error");
        }

     });
}


var deployUserSettingsGrid = function(deployUserSettingsData) {
    $("#userSettingsDiv").jqGrid({
            data : deployUserSettingsData,
            datatype : "local",
            type : 'POST',
            colNames : ['Sensor', 'Status', 'IP', 'Port','Rotating','OMC IP','RF Switch IP','Angle', 'Sector', 'Beamwidth','GSM Server IP', 'Active Status'],
			colModel : [ {
                    name : 'sensornum',
                    index : 'sensornum',
					width : 100,
					editable: true,
					editoptions: { readonly: "readonly" }
            },{
                name : 'sensorenabled',
                index : 'sensorenabled',
                hidden: true,
				width : 70,
				editable: true
            },{
                name : 'sensorip',
                index : 'sensorip',
				width : 70,
				hidden: true,
				editable: true
            },{
                name : 'sensorport',
                index : 'sensorport',
				width : 70,
				editoptions: { readonly: "readonly" },
				/*edittype : 'select',
                editoptions:{value:{1:'1', 2:'2', 3:'3'}},*/
				editable: true
            },{
                name : 'rotatingsensor',
                index : 'rotatingsensor',
				width : 90,
				
				editoptions: { readonly: "readonly" },
				editable: true
            },{
                name : 'omcip',
                index : 'omcip',
				width : 140,
				hidden: true,
				
				editable: true
				
            },{
                name : 'rfswitchip',
                index : 'rfswitchip',
				width : 140,
				hidden: true,
				/*editrules:{custom: true, custom_func: validateIpAddress},*/
				editable: true
				
            },{
                name : 'angle',
                index : 'angle',
				width : 100,
				editable: true,
				editoptions: { readonly: "readonly" }
				
            },{
                name : 'sector',
                index : 'sector',
				width : 100,
				editable: true,
				editoptions: { readonly: "readonly" }
				
            },{
                name : 'beamwidth',
                index : 'beamwidth',
                hidden: true,
				width : 110,
				editable: true
				
            },{
                    name : 'gsmserverip',
                    index :'gsmserverip',
                    width:	140,
                    hidden: true,
                    editable: true
            },{
                    name : 'usestatus',
                    index : 'usestatus',
                    edittype : 'select',
                    editoptions:{value:{Yes:'Yes', No:'No'}},
                    width : 140,
                    editable: true
                                            
            }],
            
            loadOnce : true,
            /*toppager: true,*/
            viewrecords: true,
            rowNum : 4,
            pager : '#userSettingsDivPager',
            editurl: serviceUrls.updateSensorConfig,
            gridview : true,
            caption : 'Antenna Profile'
    });
    
    
    
    $('#userSettingsDiv').navGrid('#userSettingsDivPager',
            // the buttons to appear on the toolbar of the grid
            { edit: true, add: false, del: false, search: false, refresh: false, view: false, position: "left", cloneToTop: false },

            
            
            // options for the Edit Dialog
            {
                height: 'auto',
                width: 550,
                editCaption: "Change Antenna Profile",
                recreateForm: true,
                closeAfterEdit: true,
                errorTextFormat: function (data) {
                    return 'Error: ' + data.responseText
                }
            
            });
    
}



var checkNodeStatus = function(){
	$.ajax({
        url:serviceUrls.checkNodeStatus,
        dataType:"json",
        success:function(data)
        {
           createDeviceStatusRow(data);
        }
    });
}


var createSensorList = function(data){
		for(var i =0; i<data.length; ++i){
			$("#ussensornum").append('<option value='+data[i]+'>'+data[i]+'</option>');			
		}
		
	
}



var createDeviceStatusRow =function(data)
{
	
	$("#list_table_body").html("");
	var total = data.length;
	var up = 0;
	var test=1;
	var down = 0;
	var adminstate=["DOWN","LOCK","UNLOCK"];
	var groupNodeCount=0;
	
	var row="";
	var addedRow="";
	//globalBtsDevices=[];
	//globalBtsDevicesStatus["1.1.1.1"]="NOT REACHABLE"
	//globalBtsDevicesStatus["2.2.2.2"]="NOT REACHABLE"
	for(var i=0;i<data.length;i++)
	{	
		//globalBtsDevices.push(data[i].ip);
		
		//globalBtsDevicesStatus[data[i].ip]=data[i].status
		
		var colorStaus={};
		colorStaus = colorAndStatusForStatusCodeRest(data[i].status);
		
		if(data[i].type=="STRU"){
			row+="<tr>";
			row+="<td>"+data[i].type+"</td>"+
				"<td>"+data[i].ip_add+"</td>"+
				"<td style='background:"+colorStaus.color.backgroundColor+";color:"+colorStaus.color.fontColor+"'>"+data[i].status+"</td>";
			row+="<td colspan=\"2\">";
			row+="<i style='cursor: pointer;' title='Configuration' class=\"ti-settings\" onclick=\"getPTZConfigurationPage('"+data[i].ip_add+"','"+data[i].status+"')\"></i>";
			if(user=="superadmin")
				row+="&nbsp;|&nbsp;<i style='cursor: pointer;color: red;' title='Delete' class=\"ti-trash\"  onclick=\"deleteDevice('"+data[i].ip_add+"', '"+data[i].type+"')\"></i>";
			row+="</td>";
			row+="</tr>";
		}
		else if (data[i].type=="BMS"){
			row+="<tr>";
			row+="<td>"+data[i].type+"</td>"+
				"<td>"+data[i].ip_add+"</td>"+
				"<td style='background:"+colorStaus.color.backgroundColor+";color:"+colorStaus.color.fontColor+"'>"+data[i].status+"</td>";
			row+="<td colspan=\"2\">";
			row+="<i style='cursor: pointer;' title='Configuration' class=\"ti-settings\" onclick=\"getBMSConfigurationPage('"+data[i].ip_add+"',this)\"></i>";
			if(user=="superadmin")
				row+="&nbsp;|&nbsp;<i style='cursor: pointer;color: red;' title='Delete' class=\"ti-trash\"  onclick=\"deleteDevice('"+data[i].ip_add+"', '"+data[i].type+"')\"></i>";
			row+="</td>";
			row+="</tr>";
		}
		else if (data[i].type=="SPS"){
			row+="<tr>";
			row+="<td>"+data[i].type+"</td>"+
				"<td>"+data[i].ip_add+"</td>"+
				"<td style='background:"+colorStaus.color.backgroundColor+";color:"+colorStaus.color.fontColor+"'>"+data[i].status+"</td>";
			row+="<td colspan=\"2\">";
			row+="<i style='cursor: pointer;' title='Configuration' class=\"ti-settings\" onclick=\"getLatLonPage('"+data[i].ip_add+"')\"></i>";
			if(user=="superadmin")
				row+="&nbsp;|&nbsp;<i style='cursor: pointer;color: red;' title='Delete' class=\"ti-trash\"  onclick=\"deleteDevice('"+data[i].ip_add+"', '"+data[i].type+"')\"></i>";
			row+="</td>";
			row+="</tr>";
			if(data[i].status=="Scanning"){
			var change = document.getElementById("StartCalibrate");
			 change.innerHTML = "Abort";
				calibration=0;
			}else{
			var change = document.getElementById("StartCalibrate");
			 change.innerHTML = "Start Environment Scan";
			
			}
		}
		else{
			if(systemType!="integrated" && (data[i].type=="FINLEY" || data[i].type=="FALCON" || data[i].type=="OXFAM")){
				
			}
			else{
				row+="<tr>";
				row+="<td>"+data[i].type+"</td>"+
					"<td>"+data[i].ip_add+"</td>"+
					"<td style='background:"+colorStaus.color.backgroundColor+";color:"+colorStaus.color.fontColor+"'>"+data[i].status+"</td>";
				if(user=="superadmin")
					row+="<td><i style='cursor: pointer;color: red;' title='Delete' class=\"ti-trash\"  onclick=\"deleteDevice('"+data[i].ip_add+"', '"+data[i].type+"')\"></i></td>";
				row+="</tr>";
			}
		}
	}

	$("#list_table_body").append(row);
	
}

function deleteDevice(ip_add, type){
	
	swal({
	      title: "Are you sure?",
	      text: "Are You Sure You Want To Delete Node!!",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	    }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  $.ajax({
			        url:serviceUrls.deleteNode+"/"+ip_add+"/"+type,
			        dataType:"json",
			        type:"post",
			        success:function(data)
			        {
			        	  swal("Success","Node Data Has Been Deleted","success");	
			        	  responseAlert(data,true);
			        },
			 		error:function(data){
						  data = JSON.parse(data.responseText);
			 			  responseAlert(data,false);
			 		}
			    });
	      } else {
	        swal("Cancelled", "Node Data Has Not Been Deleted", "error");
	      }
	    })
		
}

function ptzUpdate(){
	var northOffset = $('#northOffset').val();
	if(northOffset == "")
	{
        swal("Error","Please enter North Offset","error");
		return;
	}
	
	var sectorOffset = $('#sectorOffset').val();
	if(sectorOffset==""){
		swal("Error","Please enter Sector Offset","error");
		return;
	}
	
	var offset = parseFloat(northOffset);
	if(offset>360)
	{
        swal("Error","North offset should be less than 360","error");
		return;
	}
	
	if(offset>180)
		offset = offset-360;
	//ptzOperation={"operation":ptzOpr};
	
	$.ajax
	({
			url:serviceUrls.ptzUpdate+"/"+current_ip_addr+"/"+offset+"/"+sectorOffset,
			dataType:"json",
			type:'post',
			success:function(data)
			{
				swal("Success","Offset successfully updated","success");
				responseAlert(data,false);
				//getNodesData();
				//getNodeConfigurationData();
			},
			error:function(data){	
				data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}

/*function ptzOperation(){
	var ptzOpr = $('#ptz_opr').val();
	if(ptzOpr == "select")
	{
        swal("Error","Please select values from the dropdown","error");
		return;
	}
	
	var target = $('#targetValue').val();
	if(target==""){
		alert('Please enter Target Value');
		return;
	}

	//ptzOperation={"operation":ptzOpr};
	
	$.ajax
	({
			url:serviceUrls.ptzOperation+"/"+current_ip_addr+"/"+ptzOpr+"/"+target,
			dataType:"json",
			type:'post',
			success:function(data)
			{
				swal("Success","Operation successfully performed","success");
				responseAlert(data,false);
				//getNodesData();
				//getNodeConfigurationData();
			},
			error:function(data){	
				data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}*/


/******************Vaibhav Code *******************************/
function ptzOperation(buttonname){
	var ptzOpr = buttonname;
	
	var target=$('#ptz_degree').val();
	
	tiltval=document.getElementById('tiltval').innerHTML;
	rollval=document.getElementById('rollval').innerHTML;
	//EDITED by Vaibhav Chhabra VNL1424
	/*if(rollval == "N/A"&& tiltval=="N/A"){
  		swal("Error","Roll or Tilt Value must not be N/A","error");
  		return;
	}*/
	
	if(target<=0 || target>300){
		swal("Error","Degree value should be between 1-300","error");
		return;
	}
	
	switch(ptzOpr) {
		case "UP":
		  	//target=document.getElementById('tiltval').innerHTML;
		  	//target+=10;
			break;
		case "DOWN":  
			//target=document.getElementById('tiltval').innerHTML;
			//target-=10;	
			break;
		case "LEFT":
			//target=document.getElementById('rollval').innerHTML;
			//target-=10;
			break;
		case "RIGHT":
		  	//target=document.getElementById('rollval').innerHTML;
			//target+=10;
			break;
		case "RESTORE":
		  	//target=document.getElementById('rollval').innerHTML;
			//target+=10;
			break;
	  default:
	    // code block
	}


	//ptzOperation={"operation":ptzOpr};
	
	$.ajax
	({
			url:serviceUrls.ptzOperation+"/"+current_ip_addr+"/"+ptzOpr+"/"+target,
			dataType:"json",
			type:'post',
			success:function(data)
			{
				swal("Success","Operation successfully performed","success");
				responseAlert(data,false);
				
				//getNodesData();
				//getNodeConfigurationData();
				
				
			},
			error:function(data){	
				data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}

/*********************************finish***************************************************/

function getPtzOffset(){
	$.ajax({
        url:serviceUrls.getPtzOffset,      //startDate+"/"+endDate,
        type:"post",
        success:function(data)
        {
			var offset = parseFloat(data);
			if(offset==-999){
				swal("Error","Device not connected. Please wait.","error");
			}
			else if(offset==-1000){
				swal("Error","North Heading stablization in progress. Please wait.","error");
			}
			else{
				if(offset<0)
					$("#northOffset").val(offset+360);
				else
					$("#northOffset").val(offset);
				swal("Success","Offset successfully updated","success");
				//$('#northOffset').val(data);
			}
		}
    });
}

function addJM(){
	var jmDeviceId=$('#jm_deviceid').val();
	if(jmDeviceId==""){
		alert('Please enter JM Device Id');
		return;
	}
	
	var antennaId=$('#antenna_id').val();
	if(antennaId==""){
		alert('Please enter Antenna ID');
		return;
	}
	
	var sector = $("#sector").val();
	if(sector==""){
		alert('Please enter Sector');
		return;
	}

	jmData={"jmdeviceid":jmDeviceId,"antennaid":antennaId,"sector":sector};
	
	$.ajax
	({
			url:serviceUrls.addJmNode,
			data:jmData,
			type:'post',
			success:function(data)
			{
				$.ajax({
					url:serviceUrls.getJMList,
					dataType:"json",
					success:function(JMData)
					{
						jQuery('#jm_grid').jqGrid('clearGridData');
						for ( var i = 0; i < JMData.length; i++) {
							jQuery("#jm_grid ").jqGrid('addRowData', JMData[i].id, JMData[i]);
						}
						jQuery("#jm_grid").jqGrid().trigger("reloadGrid");
						//colorStripsData = colorData;
					}
				});
				swal("Success","JM Device has been successfully added","success");
				responseAlert(data,false);
				//getNodesData();
				//getNodeConfigurationData();
			},
			error:function(data){		  
			 	data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}

function latLonUpdate(){
	var lat = $('#spsLat').val();
	if(lat == "")
	{
        swal("Error","Please enter Latitude value","error");
		return;
	}
	
	var lon = $('#spsLon').val();
	if(lon==""){
		swal("Error","Please enter Longitude value","error");
		return;
	}
	
	$.ajax
	({
			url:serviceUrls.spsUpdate+"/"+current_ip_addr+"/"+lat+"/"+lon,
			dataType:"json",
			type:'post',
			success:function(data)
			{
				swal("Success","SPS Info successfully updated","success");
				responseAlert(data,false);
				getNodesData();
				//getNodeConfigurationData();
			},
			error:function(data){	
				data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}

function getLatLonPage(ip_add){
	$('#spsLat').val(currentNodeLat);
	$('#spsLon').val(currentNodeLon);
	$("#sps_modal").modal('show');
	$("#sps_update").show();
}

function getPTZConfigurationPage(ip_add,status){
	if(systemGps!='dgps'){
		$("#getptzoffset").attr('disabled',true);
	}
	if(status.toLowerCase()=="not reachable"){
		swal("Error","Cannot use Configuration tab when node is not reachable","error")
	}
	else{
		//$("#ptz_modal").modal('show');
		$("#ptz_modal").modal('show');
		$("#ptz_operation").hide();
		$("#jm_config").hide();
	}
}

function getBMSConfigurationPage(ip_add,element){
	$("#bms_modal").modal('show');
	$("#ipBms").val(ip_add);
	getBMSProp(ip_add,element);		
}

	
/*	$(".ab").click(function() {
	    var clicked_button = $(this).data("cmd");
	    alert(clicked_button);
	    alert("Chal pda");
	});*/
	
	
	
	
	

function getDisplay(first, second, third){
	$("#"+first).show();
	$("#"+second).hide();
	$("#"+third).hide();
}

function updateUserSetting(){
	
	var type = $('input[type=radio][name=settingbtn]:checked').val().toLowerCase();
	var mode = $('input[type=radio][name=settingMode]:checked').val().toLowerCase();
	var gps = $('input[type=radio][name=settingGps]:checked').val().toLowerCase();
	var oprmode = $('input[type=radio][name=oprbtn]:checked').val();
	var accuracy = $('#gps_accuracy').val();
	
	//var value = $("#usersetting").val().toLowerCase();
	if(type=='integrated' && mode=='moving'){
		swal("Error","Cannot use Moving mode in Integrated System Type","error");
		return;
	}
	/*if (OperartionMode!=4){
	  	if (oprmode!=4){
		var	cfm=confirm("Environment Scan is must before Changing Operation Mode.\nMode must be selected based on MaxInputSigLevel value as reported during Environment Scan.\nEquipment Operation under high inputs signals may damage the equipment and void warranty.\nIntegrated:\nSafe:MaxInputSigLevel:< -10 dBm  Normal:MaxInputSigLevel:< -20 dBm High Sensitivity MaxInputSigLevel:< -40 dBm \nHummer Only:\nSafe:MaxInputSigLevel:< -10 dBm Normal:MaxInputSigLevel:< -40 dBm High Sensitivity MaxInputSigLevel:< -60 dBm \nStandalone:\n(Single Antenna)Safe:MaxInputSigLevel:< -20 dBm Normal:MaxInputSigLevel:< -40 dBm High Sensitivity:MaxInputSigLevel:< -60 dBm");
 			  if(cfm==false){
			    return;
			}
		}
	
	}*/
	var userData={"type":type,"mode":mode,"gps":gps,"oprmode":oprmode,"accuracy":accuracy};
	
	$.ajax
	({
			url:serviceUrls.changeUserSetting,
			data:userData,
			type:'post',
			success:function(data)
			{
				swal("Success","User Setting successfully updated","success");
				responseAlert(data,true);
				//getNodesData();
				//getNodeConfigurationData();
			},
			error:function(data){		  
			 	data = JSON.parse(data.responseText);
			 	responseAlert(data,false);
			}
	});
}

function getUserSetting()
{
        $.ajax({
            url:serviceUrls.getUserSetting,
            type:"post",
            success:function(data)
            {	
                
			    OperartionMode=data.opr_mode;
				systemType = data.type;
				$("#preamp_type_path").hide();
	
				if(systemType=="standalone"){//systemType!="integrated"){
					$("#system-config-tab").hide();
					$("#priority-tab").hide();
					
					/*Setting Antennna Num To All And PreAmp Type Path to 1 In Case Of StandAlone*/
					/*$("#antennanum").val("all");
					$("#preamptypepath").val("1");
					*/
				
					
				}
				if(systemType=="hummer"){
					var value=0;
					//document.getElementById("Enable").disabled=true;
					 //$("Enable").prop("disabled", true);
					 //$("#radio3").hide();
					 //$("#lable").hide();
					document.getElementById("timeout").disabled=true;
					//$("#radio2").prop("checked", true);
					document.getElementById("radio20").disabled=true;
					document.getElementById("radio3").disabled=true;
					//$('input[name=radio][value="0"]').prop('checked',true);
					document.getElementById("validity").disabled=true;
					$("#priority-tab").hide();
				}
				$('input[name=settingbtn][value="'+data.type+'"]').prop('checked',true);
				$('input[name=settingMode][value="'+data.mode+'"]').prop('checked',true);
				$('input[name=settingGps][value="'+data.gps+'"]').prop('checked',true);
			 //   $('input[name=oprbtn][value="'+data.opr_mode+'"]').prop('checked',true);
				
				$('#gps_accuracy').val(data.gpsAccuracy);
				systemGps = data.gps;
				
            }

        });
}

var colorAndStatusForStatusCodeRest = function(code)
{
	var color = {};
	color.fontColor = "black";
	color.backgroundColor = "white";
	var statusCount={};
	statusCount.run=0;
	statusCount.wait=0;
	statusCount.reachable=0;
	statusCount.down=0;
	
	var status={};
	
	switch(code.toLowerCase())
	{
		case "not reachable":
            color.backgroundColor = COLOR['nor'];
			statusCount.reachable++;
			break;
        case "reachable":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
        case "running":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
        case "scanning":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
        case "monitoring":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
		case "down":
            color.backgroundColor = COLOR['down'];
			statusCount.down++;
			break;
	
	}	
	status.color =color;
	status.count =statusCount;
	return status;
}

var COLOR = {
             	'run':"rgb(198,227,159)",
             	'wait':"rgb(251,201,142)",
             	'nor':"rgb(255,229,130)",
             	'down':"rgb(240,141,144)"
			};

var getStepSize = function(maxValue)
{
    return (maxValue/1000)*100
}

/*****************************validation***********************************/

var validateAddNodeForm = function()
{
    var emptyCheck = false;
    $("#add_node_form input").each(function(){
        if(!checkIfTextBoxIsEmpty($(this)))
        {

            emptyCheck = true;
            //break;
        }
    });
    if(emptyCheck)
    {
        swal("Error","Please Fill All the details","error");
        return false;
    }

    if(!ValidateIPaddress($("#node_ip").val()))
    {
                return false;
    }

    return true;
}


var validateDefForm = function()
{
    var emptyCheck = false;
    $("#def_form input").each(function(){
        if(!checkIfTextBoxIsEmpty($(this)))
        {

            emptyCheck = true;
            //break;
        }
    });
    if(emptyCheck)
    {
        swal("Error","Please Fill All the details","error");
        return false;
    }
    return true;
}

var checkIfTextBoxIsEmpty = function(element)
{
    if(element.val().trim().length <= 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ValidateIPaddress(ipaddress)
{
 if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))
  {
    return (true)
  }
swal("Error","You have entered an invalid IP address!","error")
return (false)
}
/*****************************validation***********************************/

var initData = function()
{   
	startTime = $("#start_time").val();
	user = $("#user").val();
	$("#start_status").html(startTime);
	
    $('#node_color').colorpicker();
    $('#color').colorpicker();
    getNodesData();
    connectSocket();
	getNodeConfigurationData(1);
	getDevices();
	
	$("#bms_modal").on("hidden.bs.modal", function () {
		$("#bms_modal_status_table").hide();
	});
	
   
}

function getDeviceInfo(deviceId){
	 $.ajax({
	        url:serviceUrls.deviceinfo+"/"+deviceId,
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",

	        success:function(data)
	        {
	        	
	        	$("#node_name").val(data.name);
	        	$("#node_ip").val(data.ip_add);
	        	$("#node_color").val(data.color);
	        	$("#node_lat").val(data.lat);
	        	$("#node_lon").val(data.lon);
	        
	        	$("#node_id").val(data.id);
	        	if(data.id!=0){
	        		$('#node_ip').attr('readonly', true);
	        	} 
	        	$("#node_color").css("background",$("#node_color").val());
	             $("#node_color").css("color",$("#node_color").val());
	        	
	        },
	 error:function(){
		 swal("Error","Something Went Wrong","error");
	 }

	    });
}

function getPtzInfo(){
	
	 $.ajax({
	        url:serviceUrls.ptzinfo+"/"+$("#current_node_ip").val(),
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",

	        success:function(data)
	        {
				var offset = parseFloat(data.ptz_offset);
	        	if(offset<0)
					$("#northOffset").val(offset+360);
				else
					$("#northOffset").val(offset);
	        	//$("#northOffset").val(data.ptz_offset);
	        	$("#sectorOffset").val(data.sector_offset);
	        	
	        }

	    });
}


//VAIBHAV BELOW
function getPtzDataNow(){
	
	 $.ajax({
	        url:serviceUrls.getPtzDataNowVar+"/"+$("#current_node_ip").val(),
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",
	    	//EDITED by Vaibhav Chhabra VNL1424
	        success:function(data)
	        {
	        	
	        	updateRollTiltLabels(data);
//				var offset = parseFloat(data.ptz_offset);
//	        	if(offset<0)
//					$("#northOffset").val(offset+360);
//				else
//					$("#northOffset").val(offset);
//	        	//$("#northOffset").val(data.ptz_offset);
//	        	$("#sectorOffset").val(data.sector_offset);
	        	
	        }

	    });
}


function updateRollTiltLabels(data)
{	
	if(data[0]==-10000){
		document.getElementById('rollval').innerHTML = "N/A";
		
		swal("Error","Couldn't fetch data as node in not reachable","error");
		//responseAlert(data,false);
		
	}
	else{
		document.getElementById('rollval').innerHTML = data[0]+"&deg;";

	}
	
	
	if(data[1]==-10000){
		

	}
	else{
		document.getElementById('tiltval').innerHTML = data[1]+"&deg;";

	}
	
}

//GET VAIBHAV above



function addPtzNode(){
	
	
	 
	var ptzData =
    {
            "device_ip":$("#current_node_ip").val(),
            "ptz_ip":$("#ptz_ip").val(),
            "ptz_offset":$("#ptz_angle_offset").val()
            
    }
	$.ajax({
	        url:serviceUrls.addPtz+"/"+$("#current_node").val(),
	        data:ptzData,
	        type:"post",
	        
	        success:function(data)
	        {
	            swal("Success","Ptz Data Has Been Saved","success");
	            $("#addPtzModal").modal('hide');
	           
	           
	            
	        }

	    });
}

$(document).ready(function()
{
	$("#header").load("header");
	getBandDataList();
	getUserSetting();
	
	getFreqPowMinThres();
	getRefLevel();
	getColorCodeList();
	getMaskingDataList();
	getJmList();
    initData();
    loadPriorityList();
    loadSystemConfiguration();
    createUserSettingsGrid("load");
	checkNodeStatus();	
	getPtzDataNow();
});

function loadPriorityList()
{
        $.ajax({
            url:serviceUrls.getPriorityList,
            type:"post",
            success:function(data)
            {	
            	$('#sortable').html('');
            	var priorityList='';
            	priorityList+='<li class="unsortable"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>MANUAL</li>';
            	for(var i=2;i<=4;i++)
            	{
            	  priorityList+='<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+data[i-1].name+'</li>';
            	}
            	priorityList+='<li class="unsortable"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>AUTO</li>';
            	$('#sortable').html(priorityList);
            }

        });
}

function loadSystemConfiguration()
{
        $.ajax({
            url:serviceUrls.getSystemConfiguration+"/"+defaultProfile,
            type:"post",
            success:function(data)
            {	
            	$('input[name=radiobtn][value="'+data.ugs+'"]').prop('checked',true);
            	$('input[name=radio][value="'+data.tmdas+'"]').prop('checked',true);
				$('input[name=radiobtn1][value="'+data.auto+'"]').prop('checked',true);
            	$("#timeout").val(data.timeout);
            	$("#validity").val(data.validity);
            	$("#selftimeout").val(data.selftimeout);
				$("#selfvalidity").val(data.selfvalidity);
				$("#range").val(data.range);
            }

        });
}

function getFreqPowMinThres()
{
        $.ajax({
            url:serviceUrls.getThresholdMap,
            type:"post",
            success:function(data)
            {	
            	freqPowMinThres = data;
            }

        });
}

function getRefLevel()
{
        $.ajax({
            url:serviceUrls.getRefLevel,
            type:"post",
            success:function(data)
            {	
               var ref_value=JSON.parse(data);
               reflevel=ref_value;
            	
			}
        });
}
function confirmRefLevel(){ 

}
function getOfflineData(reportType){
	

    startDate=$("#search-from-date").val().split("/").join("-")
    endDate=$("#search-to-date").val().split("/").join("-");
    
        $.ajax({
        url:serviceUrls.offlineData+"/"+startDate+"/"+endDate+"/"+current_device_id,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
        type:"post",

        success:function(data)
        {
            //generateCsvData(data);
        	if(reportType=='csv'){
        		 createCsvFile(data);
        	}
        	else if(reportType=='tsv'){
        		createTsvFile(data);
        	}
        	else{
        		 plotOfflineData(data);
        	}
           
           
            
        }

    });
}

 async function delay(delayInms) {
    return new Promise(resolve  => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }

 $(document).ready(function () {
        

        $("#search-from-date").datetimepicker();
        $("#search-to-date").datetimepicker();
//        $('#search-from-date, #search-to-date').datetimepicker();
        
        $("#bms_modal_status_table").hide();
    });
 
 
 function createCsvFile (data) {

	 const rows = [["Node", "Power", "Frequency","Timestamp" ]];
	 let csvContent = "data:text/csv;charset=utf-8,";
//	 rows.forEach(function(rowArray){
//	    let row = rowArray.join(",");
//	    csvContent += row + "\r\n";
//	 }); 
	 csvContent += "Node,Power,Frequency,Angle,Timestamp"+ "\r\n";
	 for(var i in data)
	    {
		 		let row
		 		records=data[i];
		 		for (r in records){
		 			
//		 			console.log(records[r].ptzangle);
		 			row=records[r].deviceName+","+records[r].power+","+records[r].frequency+","+records[r].ptzangle+","+records[r].time;
					csvContent += row + "\r\n";
				}
		 		
		 		//csvContent += row + "\r\n";
	   }
	 var encodedUri = encodeURI(csvContent);
	 var link = document.createElement("a");
	 link.setAttribute("href", encodedUri);
	 link.setAttribute("download", "my_data.csv");
	 link.innerHTML= "Click Here to download";
	 document.body.appendChild(link); // Required for FF

	 link.click(); 
	}
 
 function createTsvFile(data){
	 const rows = [["Node", "Power", "Frequency","Timestamp" ]];
	 let csvContent = "data:text/csv;charset=utf-8,";
//	 rows.forEach(function(rowArray){
//	    let row = rowArray.join(",");
//	    csvContent += row + "\r\n";
//	 }); 
	 csvContent += "Node"+"\t"+"Power"+"\t"+"Frequency"+"\t"+"Angle"+"\t"+"Timestamp"+ "\r\n";
	 for(var i in data)
	    {
		 		let row
		 		records=data[i];
		 		for (r in records){
//		 			console.log(records[r].deviceName);
		 			row=records[r].deviceName+"\t"+records[r].power+"\t"+records[r].frequency+"\t"+records[r].ptzangle+"\t"+records[r].time;
		 		}
		 		
		 		csvContent += row + "\t\n";
	   }
	 var encodedUri = encodeURI(csvContent);
	 var link = document.createElement("a");
	 link.setAttribute("href", encodedUri);
	 link.setAttribute("download", "my_data.tsv");
	 link.innerHTML= "Click Here to download";
	 document.body.appendChild(link); // Required for FF

	 link.click(); 
	}
 
 function colorToHex(red, green, blue) {

		green = parseInt(green);
		blue = parseInt(blue);
		red = parseInt(red);
		return "#" + hex(red) + hex(green) + hex(blue);

	}
 
 function hex(x) {
		var hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f");
		return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	}

	 


//@sandeep-------------------------------
 
 var updateStatusBMS = function(data)
 {
 		data = data.split(",");
 		
 		var ip = data[0];
 		var status = data[1];
 		$("#sl_table tr").each(function()
 		{

 		console.log($(this).find("td:eq(1)").text());
 		if($(this).find("td:eq(1)").text() == ip)
 		{
 			($(this).find("td:eq(2)").text(status));
 		}

 		});
 }
 
 var createLatLon = function(data)
 {
	 console.log(data);
	 if(data.ptzoffset<0)
			$("#northOffset").val(data.ptzoffset+360);
		else
			$("#northOffset").val(data.ptzoffset);
	 
	 offset = data.ptzoffset;
	 console.log(offset);
	 
	 currentNodeLat = data.lat;
	 currentNodeLon = data.lon;
	 $("#location").html(data.lat+", "+data.lon);
	 $("#spsLat").val(data.lat);
	 $("#spsLon").val(data.lon);
 } 
 
 
 var getBMSProp =  function(ip,element)
 {
	 $.ajax
	 ({
		  url:serviceUrls.prop,
		  data:{"ip":ip},
		  dataType:"json",
		  success:function(data)
		  {
			  /*if(data.load == "1")
			  {
				  $("#load_on").hide();
				  $("#load_off").show();
				  $("#lod_txt").text("On");
			  }
			  else
			  {
				  $("#load_on").show();
				  $("#load_off").hide();
				  $("#lod_txt").text("Off");
			  }*/
			  
			  $("#Pr_text").val(data.periodicity);
			  $("#bms_sys_time").text(new Date(parseInt(data.systemtime)*1000).toLocaleString());
		  }
	 });
 }
 
 
 function sendBtnData(obj){
		
		
		
		var btnData =
	    {
	            "CMD":  $(obj).data("cmd"),
				"ID" : $(obj).data("id"),
				"ip" : $("#ipBms").val(),
	            "pr_key" :""
	    }
		
		if(($(obj).data("cmd"))=="PR")
			{
			
			btnData.pr_key=$("#Pr_text").val();
			}
		
		
		
		console.log(btnData);
		
		$.ajax({
			url:serviceUrls.bmsData,
		        data:btnData,
		        type:"post",
		        
		        success:function(data)
		        {
		            swal("Success","BMS Data Has Been Saved","success");
		            //$("#bms_modal").modal('hide');
		            //$(obj).trigger("click");
		            afterCommandSend($(obj).data("id"),data.data);
		        }

		    });
	}
	
var afterCommandSend = function(id,data)
{
	switch(id)
	{
		case 1:
			//$("#load_on").hide();
			//$("#load_off").show();
			//$("#lod_txt").text("On");
			$("#bms_modal").modal('hide');
			
		break;
		case 2:
			//$("#load_on").show();
			//$("#load_off").hide();
			//$("#lod_txt").text("Off");
			$("#bms_modal").modal('hide');
		break;
		case 4:
			$("#bms_modal").modal('hide');
		break;
		case 5:
			$("#bms_modal").modal('hide');
		break;
		case 6:
			data = data.split(",");
			var i =2;
			$("#bms_modal_status_table tr td").each(function(){
				
					//$(this).find("td:eq(1)").text(data[i]);
					if(i<18)
						data[i] = (parseFloat(data[i])/1000).toFixed(2);
					$("#stat_"+(i-1)).text(data[i]);
				
					i++;
			});
			
			for(var i in data)
			{
				
			}
			$("#bms_modal_status_table").show();
		break;
		case 10:
			$("#bms_modal").modal('hide');
		break;
		case 11:
			$("#bms_modal").modal('hide');
		break;
	}
}

var calculateRbwBandwidth = function()
{
	var scale = 100;
	var rbwMaxSamples = 4001;
	
	var band = parseInt($("#stop_freq").val())-parseInt($("#start_freq").val());
	
	if(band<=40 && band>4) {
		scale = 10;
	}
	else if(band<=4) {
		scale = 1;
	}
	
	var bandwidth = (Math.ceil(((band)*1000/(rbwMaxSamples-1))/scale))*scale;//changed here *2;
	bandwidth = bandwidth/1000.0;
	
	$('#rbwBandwidth').html(bandwidth);
	
	calculateMaxThreshold(parseInt($("#start_freq").val()), parseInt($("#stop_freq").val()));
} 

var sdth =function(){
      var enable=document.getElementById("sdth");
      if (enable.checked==false){
     document.getElementById("threshold").disabled=false;
      }
      else {
       document.getElementById("threshold").disabled=true;
       }
       
      
}
function StartCalibrt(){
     
        document.getElementById("calibtimer").disabled=false;
   		var change = document.getElementById("StartCalibrate");
                if (change.innerHTML == "Start Environment Scan")
                {
                swal({
	      title: "Are you sure?",
	      text: "This will start the System Environment Scanning process",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	  	  }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  change.innerHTML = "Abort";
                    updateConfig(1);
	  	    } else {
	        //swal("Cancelled", "Setting Has Not Been Applied", "error");
	        return;
	  		    }
	  		  })
                   
      				
                }
       		 	else{
      				  change.innerHTML = "Start Environment Scan";
      				  updateConfig(2);
    				 
        
       			 }
}

/*function StopCalibrt(){
   	 
       updateConfig(2);
       if(calibration==0){
       document.getElementById("calibtimer").disabled=true;
       }
  
}*/
function ApplyConfig(){
   	 
   var test='';   
   var rflevel= document.getElementById("reflevel").value;
 	 var e = document.getElementById("reflevel");
     var rftext=  e.options[e.selectedIndex].text;
 	for(i=0; i<reflevel.length; i++){
   if(reflevel[i]==rflevel){
   test=1;
   
   swal({
	      title: "Are you sure?",
	      text: "ADC Alarm detected on Operation Mode- "+rftext+" during Environment Scanning. Environment Masks is not created for this Operation Mode,Factory Mask shall be apply",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	  	  }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  updateConfig(0);
	  	    } else {
	       // swal("Cancelled", "Setting Has Not Been Applied", "error");
	        return;
	  	    }
	  	  })
				}
			}
			if(test==1){
			    return;
		    }
	     	updateConfig(0);
   
	
       
  
}
var calculateMaxThreshold = function(startFreq, stopFreq){
	var startThreshold = 0;
	var stopThreshold = 0;
	var thres = 0;
	var rbw=0;
	var frequency = stopFreq - startFreq;
	
	//Iterator<FreqPowMinThresData> itr = freqPowMinThres.iterator();
	for(var i in freqPowMinThres){
		var fp = freqPowMinThres[i];
		if(startFreq >= fp.startfreq && startFreq <= fp.stopfreq) {
			startThreshold = fp.power;
		}
		if(stopFreq >= fp.startfreq && stopFreq <= fp.stopfreq) {
			stopThreshold = fp.power;
		}
	}
	
	rbw = frequency>4?10:1;
	thres = startThreshold>stopThreshold?startThreshold:stopThreshold;
	
	var threshold = parseInt(thres + 10*Math.log10(frequency*1000000.0) + 10*Math.log10(rbw/(frequency*1000.0)) + 10);
	threshold=user_min_threshold;
	$('#maxThreshold').html(threshold);
	$("#thresholdrange").html("["+threshold+",-10]");
}
 