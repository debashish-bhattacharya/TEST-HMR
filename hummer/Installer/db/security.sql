--
-- PostgreSQL database dump
--

-- Dumped from database version 10.15
-- Dumped by pg_dump version 10.15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: delete_previous_prop(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_previous_prop() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
delete from bmsconfig where ip = new.ip and name = new.name;
RETURN new;
END;
$$;


ALTER FUNCTION public.delete_previous_prop() OWNER TO postgres;

--
-- Name: insert_config_data(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.insert_config_data() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE
result record;
BEGIN
      select * from config_data where config_data.device_id=new.device_id into result;
      insert into config_data_history(ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code)
values(result.ip_add,result.floor,result.room,result.calibration,result.start_freq,result.stop_freq,result.threshold,result.mask_offset,result.use_mask,result.start_time,
result.stop_time,result.cable_length,result.preamp_type,result.gsm_dl,result.wcdma_dl,result.wifi_band,result.lte_dl,
             result.band_start1, result.band_stop1, result.band_en1, result.band_start2, result.band_stop2,
             result.band_en2, result.band_start3, result.band_stop3, result.band_en3,
             result.band_start4, result.band_stop4, result.band_en4, result.band_start5,
             result.band_stop5, result.band_en5, result.device_id, result.country_code);
      RETURN new;

    END;

$$;


ALTER FUNCTION public.insert_config_data() OWNER TO postgres;

--
-- Name: move_alarm_to_history_table(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.move_alarm_to_history_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

insert into alarms_history select * from alarms where id = new.id;
if new.severity == 0 THEN
delete from alarms where mangedoject_id = new.mangedoject_id and component_id = new.component_id and event_id = new.event_id;
ELSE
delete from alarms where mangedoject_id = new.mangedoject_id and component_id = new.component_id and event_id = new.event_id and id != new.id;
END IF;
RETURN new;
END;
$$;


ALTER FUNCTION public.move_alarm_to_history_table() OWNER TO postgres;

--
-- Name: move_alarm_to_history_table_on_ack(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.move_alarm_to_history_table_on_ack() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
insert into alarms_history select * from alarms where id = new.id;
delete from alarms where ip = new.ip;
RETURN new;
END;
$$;


ALTER FUNCTION public.move_alarm_to_history_table_on_ack() OWNER TO postgres;

--
-- Name: move_alarm_to_history_table_on_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.move_alarm_to_history_table_on_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
insert into alarms_history select * from alarms where id = new.id;
if new.severity == 0 THEN
delete from alarms where id = new.id;
END IF;
RETURN new;
END;
$$;


ALTER FUNCTION public.move_alarm_to_history_table_on_update() OWNER TO postgres;

--
-- Name: move_bms_status_to_history_table(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.move_bms_status_to_history_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
insert into bmsstatus_history select * from bmsstatus where ip = new.ip;
delete from bmsstatus where ip = new.ip;
RETURN new;
END;
$$;


ALTER FUNCTION public.move_bms_status_to_history_table() OWNER TO postgres;

--
-- Name: move_gps(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.move_gps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
 IF NEW.lat <> OLD.lat OR NEW.lon <> OLD.lon THEN
 INSERT INTO gps_data(lat,lon)
 VALUES(OLD.lat::numeric, OLD.lon::numeric);
 END IF;
 
 RETURN NEW;
END;
$$;


ALTER FUNCTION public.move_gps() OWNER TO postgres;

--
-- Name: move_jmdevice_data_to_history_table(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.move_jmdevice_data_to_history_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

insert into jmdevicedata_history select * from jmdevice_data where id = new.id;
delete from jmdevice_data where deviceid = new.deviceid and id!=new.id;
RETURN new;
END;
$$;


ALTER FUNCTION public.move_jmdevice_data_to_history_table() OWNER TO postgres;

--
-- Name: update_peak_info(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.update_peak_info() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

	DECLARE
	result record;
	BEGIN
		  update peak_info set currentconfigid= (SELECT id FROM config_data_history ORDER BY ID DESC LIMIT 1) where currentconfigid=-1;
		  RETURN new;

		END;

	$$;


ALTER FUNCTION public.update_peak_info() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alarm_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alarm_info (
    id integer NOT NULL,
    type character varying,
    trigger character varying,
    power character varying,
    frequency character varying,
    "time" character varying,
    angle character varying,
    range numeric,
    duration bigint,
    sample bigint,
    lat numeric,
    lon numeric,
    ip_addr character varying,
    alarm boolean,
    transid character varying,
    cueid character varying,
    emitterpower numeric,
    bandwidth numeric
);


ALTER TABLE public.alarm_info OWNER TO postgres;

--
-- Name: alarm_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.alarm_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alarm_info_id_seq OWNER TO postgres;

--
-- Name: alarm_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.alarm_info_id_seq OWNED BY public.alarm_info.id;


--
-- Name: alarms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alarms (
    id integer NOT NULL,
    ip character varying,
    component_id character varying,
    component_type numeric,
    mangedoject_id character varying,
    managedobject_type numeric,
    event_id character varying,
    event_type numeric,
    severity numeric,
    event_desctiption text,
    generation_time numeric,
    status numeric DEFAULT 1,
    logtime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.alarms OWNER TO postgres;

--
-- Name: component_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.component_type (
    id integer NOT NULL,
    component character varying,
    component_type int4range,
    status boolean DEFAULT true
);


ALTER TABLE public.component_type OWNER TO postgres;

--
-- Name: event_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.event_type (
    id integer NOT NULL,
    event_node character varying,
    event_type int4range,
    status boolean DEFAULT true
);


ALTER TABLE public.event_type OWNER TO postgres;

--
-- Name: managedobject_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.managedobject_type (
    id integer NOT NULL,
    managed_object character varying,
    managed_object_id numeric,
    status boolean DEFAULT true
);


ALTER TABLE public.managedobject_type OWNER TO postgres;

--
-- Name: severity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.severity (
    id integer NOT NULL,
    severity character varying,
    severity_id numeric,
    status boolean DEFAULT true
);


ALTER TABLE public.severity OWNER TO postgres;

--
-- Name: alarms_data; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.alarms_data AS
 SELECT alarms.id,
    alarms.ip,
    alarms.component_id,
    alarms.component_type,
    alarms.mangedoject_id,
    alarms.managedobject_type,
    alarms.event_id,
    alarms.event_type,
    alarms.severity,
    alarms.event_desctiption,
    alarms.generation_time,
    alarms.status,
    alarms.logtime,
    event_type.event_node,
    component_type.component,
    managedobject_type.managed_object,
    severity.severity AS severity_type
   FROM ((((public.alarms
     LEFT JOIN public.event_type ON (((alarms.event_type)::integer <@ event_type.event_type)))
     LEFT JOIN public.component_type ON (((alarms.component_type)::integer <@ component_type.component_type)))
     LEFT JOIN public.managedobject_type ON ((alarms.managedobject_type = managedobject_type.managed_object_id)))
     LEFT JOIN public.severity ON ((alarms.severity = severity.severity_id)));


ALTER TABLE public.alarms_data OWNER TO postgres;

--
-- Name: alarms_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alarms_history (
    id integer,
    ip character varying,
    component_id numeric,
    componen_type character varying,
    mangedoject_id numeric,
    managedobject_type character varying,
    event_id numeric,
    event_type character varying,
    severity numeric,
    event_desctiption text,
    generation_time timestamp without time zone,
    acknowledged boolean,
    logtime timestamp without time zone
);


ALTER TABLE public.alarms_history OWNER TO postgres;

--
-- Name: alarms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.alarms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alarms_id_seq OWNER TO postgres;

--
-- Name: alarms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.alarms_id_seq OWNED BY public.alarms.id;


--
-- Name: audit_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.audit_log (
    id integer NOT NULL,
    log_type character varying,
    logtime character varying,
    description text
);


ALTER TABLE public.audit_log OWNER TO postgres;

--
-- Name: audit_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.audit_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_log_id_seq OWNER TO postgres;

--
-- Name: audit_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.audit_log_id_seq OWNED BY public.audit_log.id;


--
-- Name: band_filter; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.band_filter (
    id integer NOT NULL,
    start_frq character varying,
    stop_frq character varying,
    status boolean
);


ALTER TABLE public.band_filter OWNER TO postgres;

--
-- Name: band_filter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.band_filter_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.band_filter_id_seq OWNER TO postgres;

--
-- Name: band_filter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.band_filter_id_seq OWNED BY public.band_filter.id;


--
-- Name: bms_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bms_info (
    id integer NOT NULL,
    on_btn character varying,
    reset_btn character varying,
    pr_btn character varying
);


ALTER TABLE public.bms_info OWNER TO postgres;

--
-- Name: bms_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bms_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bms_info_id_seq OWNER TO postgres;

--
-- Name: bms_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bms_info_id_seq OWNED BY public.bms_info.id;


--
-- Name: bmsconfig; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bmsconfig (
    id integer NOT NULL,
    ip character varying,
    name character varying,
    tag character varying,
    value character varying
);


ALTER TABLE public.bmsconfig OWNER TO postgres;

--
-- Name: bmsconfig_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bmsconfig_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bmsconfig_id_seq OWNER TO postgres;

--
-- Name: bmsconfig_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bmsconfig_id_seq OWNED BY public.bmsconfig.id;


--
-- Name: bmsstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bmsstatus (
    id integer NOT NULL,
    ip character varying,
    bcv1 numeric,
    bcv2 numeric,
    bcv3 numeric,
    bcv4 numeric,
    bcv5 numeric,
    bcv6 numeric,
    bcv7 numeric,
    bcv8 numeric,
    bcv9 numeric,
    bcv10 numeric,
    bcv11 numeric,
    bcv12 numeric,
    bcv13 numeric,
    bcv14 numeric,
    btv numeric,
    tbc numeric,
    soc numeric,
    btemp numeric,
    alarmword numeric,
    logtime timestamp without time zone DEFAULT now(),
    generationtime numeric
);


ALTER TABLE public.bmsstatus OWNER TO postgres;

--
-- Name: bmsstatus_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bmsstatus_history (
    id integer,
    ip character varying,
    bcv1 numeric,
    bcv2 numeric,
    bcv3 numeric,
    bcv4 numeric,
    bcv5 numeric,
    bcv6 numeric,
    bcv7 numeric,
    bcv8 numeric,
    bcv9 numeric,
    bcv10 numeric,
    bcv11 numeric,
    bcv12 numeric,
    bcv13 numeric,
    bcv14 numeric,
    btv numeric,
    tbc numeric,
    soc numeric,
    btemp numeric,
    alarmword numeric,
    logtime timestamp without time zone,
    generationtime numeric
);


ALTER TABLE public.bmsstatus_history OWNER TO postgres;

--
-- Name: bmsstatus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bmsstatus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bmsstatus_id_seq OWNER TO postgres;

--
-- Name: bmsstatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bmsstatus_id_seq OWNED BY public.bmsstatus.id;


--
-- Name: calibration; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calibration (
    ip_add character varying,
    ant_id character varying,
    ref_level character varying,
    status character varying
);


ALTER TABLE public.calibration OWNER TO postgres;

--
-- Name: color_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.color_code (
    id integer NOT NULL,
    startfreq numeric,
    stopfreq numeric,
    networktype character varying,
    color character varying,
    depl character varying,
    epr character varying,
    modulation character varying,
    visible boolean,
    profile character varying
);


ALTER TABLE public.color_code OWNER TO postgres;

--
-- Name: color_code_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.color_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.color_code_id_seq OWNER TO postgres;

--
-- Name: color_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.color_code_id_seq OWNED BY public.color_code.id;


--
-- Name: commands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.commands (
    id integer NOT NULL,
    node_id numeric,
    tag character varying,
    cmd character varying,
    status boolean DEFAULT true
);


ALTER TABLE public.commands OWNER TO postgres;

--
-- Name: commands_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.commands_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.commands_id_seq OWNER TO postgres;

--
-- Name: commands_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.commands_id_seq OWNED BY public.commands.id;


--
-- Name: component_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.component_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.component_type_id_seq OWNER TO postgres;

--
-- Name: component_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.component_type_id_seq OWNED BY public.component_type.id;


--
-- Name: config_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.config_data (
    ip_add character varying NOT NULL,
    floor character varying,
    room character varying NOT NULL,
    calibration character varying,
    start_freq character varying,
    stop_freq character varying,
    threshold character varying,
    mask_offset character varying,
    use_mask character varying,
    start_time character varying,
    stop_time character varying,
    cable_length character varying,
    preamp_type character varying,
    gsm_dl character varying,
    wcdma_dl character varying,
    wifi_band character varying,
    lte_dl character varying,
    band_start1 character varying,
    band_stop1 character varying,
    band_en1 character varying,
    band_start2 character varying,
    band_stop2 character varying,
    band_en2 character varying,
    band_start3 character varying,
    band_stop3 character varying,
    band_en3 character varying,
    band_start4 character varying,
    band_stop4 character varying,
    band_en4 character varying,
    band_start5 character varying,
    band_stop5 character varying,
    band_en5 character varying,
    device_id bigint,
    country_code character varying,
    distance character varying,
    tx_power character varying,
    fade_margin character varying,
    calib_timer character varying,
    ref_level character varying,
    band_specific character varying,
    band_filter character varying
);


ALTER TABLE public.config_data OWNER TO postgres;

--
-- Name: config_data_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.config_data_history (
    id integer NOT NULL,
    ip_add character varying NOT NULL,
    floor character varying,
    room character varying,
    calibration character varying,
    start_freq character varying,
    stop_freq character varying,
    threshold character varying,
    mask_offset character varying,
    use_mask character varying,
    start_time character varying,
    stop_time character varying,
    cable_length character varying,
    preamp_type character varying,
    gsm_dl character varying,
    wcdma_dl character varying,
    wifi_band character varying,
    lte_dl character varying,
    band_start1 character varying,
    band_stop1 character varying,
    band_en1 character varying,
    band_start2 character varying,
    band_stop2 character varying,
    band_en2 character varying,
    band_start3 character varying,
    band_stop3 character varying,
    band_en3 character varying,
    band_start4 character varying,
    band_stop4 character varying,
    band_en4 character varying,
    band_start5 character varying,
    band_stop5 character varying,
    band_en5 character varying,
    device_id bigint,
    country_code character varying,
    fade_margin character varying,
    tx_power character varying,
    distance character varying,
    calib_timer character varying,
    ref_level character varying,
    band_specific character varying,
    band_filter character varying
);


ALTER TABLE public.config_data_history OWNER TO postgres;

--
-- Name: config_data_history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.config_data_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.config_data_history_id_seq OWNER TO postgres;

--
-- Name: config_data_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.config_data_history_id_seq OWNED BY public.config_data_history.id;


--
-- Name: config_profile; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.config_profile (
    ip_add character varying NOT NULL,
    floor character varying,
    room character varying NOT NULL,
    calibration character varying,
    start_freq character varying,
    stop_freq character varying,
    threshold character varying,
    mask_offset character varying,
    use_mask character varying,
    start_time character varying,
    stop_time character varying,
    cable_length character varying,
    preamp_type character varying,
    gsm_dl character varying,
    wcdma_dl character varying,
    wifi_band character varying,
    lte_dl character varying,
    band_start1 character varying,
    band_stop1 character varying,
    band_en1 character varying,
    band_start2 character varying,
    band_stop2 character varying,
    band_en2 character varying,
    band_start3 character varying,
    band_stop3 character varying,
    band_en3 character varying,
    band_start4 character varying,
    band_stop4 character varying,
    band_en4 character varying,
    band_start5 character varying,
    band_stop5 character varying,
    band_en5 character varying,
    device_id bigint,
    country_code character varying,
    profile character varying,
    id integer NOT NULL,
    ref_level character varying,
    band_specific character varying,
    band_filter character varying
);


ALTER TABLE public.config_profile OWNER TO postgres;

--
-- Name: config_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.config_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.config_profile_id_seq OWNER TO postgres;

--
-- Name: config_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.config_profile_id_seq OWNED BY public.config_profile.id;


--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO postgres;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO postgres;

--
-- Name: device_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.device_info (
    id integer NOT NULL,
    ip_add character varying,
    name character varying,
    color character varying,
    lat character varying,
    lon character varying,
    is_active character varying,
    state character varying,
    starttime character varying
);


ALTER TABLE public.device_info OWNER TO postgres;

--
-- Name: device_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.device_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.device_info_id_seq OWNER TO postgres;

--
-- Name: device_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.device_info_id_seq OWNED BY public.device_info.id;


--
-- Name: devices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.devices (
    id integer NOT NULL,
    node_name character varying,
    status boolean DEFAULT true
);


ALTER TABLE public.devices OWNER TO postgres;

--
-- Name: devices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.devices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.devices_id_seq OWNER TO postgres;

--
-- Name: devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.devices_id_seq OWNED BY public.devices.id;


--
-- Name: event_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.event_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_type_id_seq OWNER TO postgres;

--
-- Name: event_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.event_type_id_seq OWNED BY public.event_type.id;


--
-- Name: frequencyconfiguration_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.frequencyconfiguration_data (
    id integer NOT NULL,
    frequency numeric,
    bandwidth numeric,
    freqtype character varying,
    profile character varying
);


ALTER TABLE public.frequencyconfiguration_data OWNER TO postgres;

--
-- Name: frequencyconfiguration_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.frequencyconfiguration_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.frequencyconfiguration_data_id_seq OWNER TO postgres;

--
-- Name: frequencyconfiguration_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.frequencyconfiguration_data_id_seq OWNED BY public.frequencyconfiguration_data.id;


--
-- Name: gps_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gps_data (
    id integer NOT NULL,
    lat numeric,
    lon numeric,
    inserttime timestamp without time zone DEFAULT now()
);


ALTER TABLE public.gps_data OWNER TO postgres;

--
-- Name: gps_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gps_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gps_data_id_seq OWNER TO postgres;

--
-- Name: gps_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gps_data_id_seq OWNED BY public.gps_data.id;


--
-- Name: jmdata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jmdata (
    ip_add character varying,
    "time" character varying NOT NULL,
    angle character varying,
    lat double precision,
    lon double precision,
    jmpacketdatetime character varying,
    created_at timestamp without time zone
);


ALTER TABLE public.jmdata OWNER TO postgres;

--
-- Name: jmdevice_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jmdevice_data (
    id integer NOT NULL,
    deviceid numeric NOT NULL,
    "time" timestamp without time zone DEFAULT now(),
    roll character varying,
    tilt character varying,
    pan character varying
);


ALTER TABLE public.jmdevice_data OWNER TO postgres;

--
-- Name: jmdevicedata_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jmdevicedata_history (
    id integer NOT NULL,
    deviceid numeric,
    "time" timestamp without time zone DEFAULT now(),
    roll character varying,
    tilt character varying,
    pan character varying
);


ALTER TABLE public.jmdevicedata_history OWNER TO postgres;

--
-- Name: jmdevice_data_history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jmdevice_data_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jmdevice_data_history_id_seq OWNER TO postgres;

--
-- Name: jmdevice_data_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jmdevice_data_history_id_seq OWNED BY public.jmdevicedata_history.id;


--
-- Name: jmdevice_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jmdevice_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jmdevice_data_id_seq OWNER TO postgres;

--
-- Name: jmdevice_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jmdevice_data_id_seq OWNED BY public.jmdevice_data.id;


--
-- Name: jmnode_mapping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jmnode_mapping (
    deviceid numeric NOT NULL,
    antennaid numeric,
    sector numeric
);


ALTER TABLE public.jmnode_mapping OWNER TO postgres;

--
-- Name: jmnode_mapping_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jmnode_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jmnode_mapping_id_seq OWNER TO postgres;

--
-- Name: led_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.led_status (
    ip_add character varying NOT NULL,
    led_on integer,
    device_id bigint NOT NULL
);


ALTER TABLE public.led_status OWNER TO postgres;

--
-- Name: managedobject_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.managedobject_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.managedobject_type_id_seq OWNER TO postgres;

--
-- Name: managedobject_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.managedobject_type_id_seq OWNED BY public.managedobject_type.id;


--
-- Name: min_threshold; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.min_threshold (
    id integer NOT NULL,
    startfreq numeric,
    stopfreq numeric,
    power numeric,
    attenuation numeric
);


ALTER TABLE public.min_threshold OWNER TO postgres;

--
-- Name: min_threshold_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.min_threshold_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.min_threshold_id_seq OWNER TO postgres;

--
-- Name: min_threshold_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.min_threshold_id_seq OWNED BY public.min_threshold.id;


--
-- Name: node_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.node_info (
    id integer NOT NULL,
    ip_add character varying,
    type character varying,
    status character varying
);


ALTER TABLE public.node_info OWNER TO postgres;

--
-- Name: node_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.node_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.node_info_id_seq OWNER TO postgres;

--
-- Name: node_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.node_info_id_seq OWNED BY public.node_info.id;


--
-- Name: ptz_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ptz_info (
    device_ip character varying NOT NULL,
    ptz_ip character varying,
    ptz_offset character varying,
    name character varying,
    sector_offset character varying
);


ALTER TABLE public.ptz_info OWNER TO postgres;

--
-- Name: nodes_info; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.nodes_info AS
 SELECT device_info.ip_add,
    device_info.name
   FROM public.device_info
UNION
 SELECT node_info.ip_add,
    node_info.type AS name
   FROM public.node_info
UNION
 SELECT ptz_info.ptz_ip AS ip_add,
    ptz_info.name
   FROM public.ptz_info;


ALTER TABLE public.nodes_info OWNER TO postgres;

--
-- Name: peak_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peak_info (
    ip_add character varying,
    power character varying,
    frequency character varying,
    "time" character varying,
    device_id bigint,
    id integer NOT NULL,
    angle character varying,
    currentconfigid bigint,
    antennaid character varying,
    alarm boolean,
    jmdevice_id bigint,
    type character varying,
    sector character varying,
    inserttime timestamp without time zone DEFAULT now(),
    technology character varying,
    lat numeric,
    lon numeric
);


ALTER TABLE public.peak_info OWNER TO postgres;

--
-- Name: peak_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peak_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peak_info_id_seq OWNER TO postgres;

--
-- Name: peak_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peak_info_id_seq OWNED BY public.peak_info.id;


--
-- Name: peak_jm_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.peak_jm_view AS
 SELECT peak_info.id,
    peak_info.ip_add,
    peak_info.power,
    peak_info.frequency,
    peak_info."time",
    peak_info.device_id,
    peak_info.angle,
    peak_info.antennaid,
    peak_info.alarm,
    peak_info.jmdevice_id,
    peak_info.type,
    peak_info.sector,
    peak_info.inserttime,
    peak_info.lat,
    peak_info.lon
   FROM public.peak_info;


ALTER TABLE public.peak_jm_view OWNER TO postgres;

--
-- Name: peak_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.peak_view AS
 SELECT peak_info.ip_add,
    peak_info.power,
    peak_info.frequency,
    peak_info."time",
    peak_info.device_id,
    peak_info.id,
    peak_info.angle,
    peak_info.currentconfigid,
    peak_info.antennaid,
    peak_info.alarm,
    peak_info.jmdevice_id,
    peak_info.type,
    peak_info.sector,
    peak_info.inserttime
   FROM public.peak_info
  ORDER BY peak_info.id;


ALTER TABLE public.peak_view OWNER TO postgres;

--
-- Name: priority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.priority (
    id integer NOT NULL,
    priority integer,
    name character varying
);


ALTER TABLE public.priority OWNER TO postgres;

--
-- Name: priority_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.priority_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.priority_id_seq OWNER TO postgres;

--
-- Name: priority_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.priority_id_seq OWNED BY public.priority.id;


--
-- Name: ptz_position_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ptz_position_data (
    id integer NOT NULL,
    ptz_ip character varying,
    pan_position real,
    tilt_position real,
    date_time character varying,
    system_time character varying
);


ALTER TABLE public.ptz_position_data OWNER TO postgres;

--
-- Name: ptz_position_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ptz_position_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ptz_position_data_id_seq OWNER TO postgres;

--
-- Name: ptz_position_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ptz_position_data_id_seq OWNED BY public.ptz_position_data.id;


--
-- Name: reflevel_master; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reflevel_master (
    id character varying NOT NULL,
    value character varying
);


ALTER TABLE public.reflevel_master OWNER TO postgres;

--
-- Name: reflevel_calb; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.reflevel_calb AS
 SELECT a.ip_add,
    a.ant_id,
    a.ref_level,
    a.status,
    b.value
   FROM public.calibration a,
    public.reflevel_master b
  WHERE ((a.ref_level)::text = (b.id)::text);


ALTER TABLE public.reflevel_calb OWNER TO postgres;

--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id integer NOT NULL,
    role character varying(255),
    active integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: sensor_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sensor_config (
    sensornum integer NOT NULL,
    sensorenabled character varying,
    sensorip character varying,
    sensorport integer NOT NULL,
    rotatingsensor character varying,
    omcip character varying,
    rfswitchip character varying,
    angle double precision,
    beamwidth double precision,
    gsmserverip character varying,
    usestatus character varying,
    sector character varying
);


ALTER TABLE public.sensor_config OWNER TO postgres;

--
-- Name: severity_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.severity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.severity_id_seq OWNER TO postgres;

--
-- Name: severity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.severity_id_seq OWNED BY public.severity.id;


--
-- Name: switch_conf; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.switch_conf (
    id integer NOT NULL,
    sid integer,
    pathid integer,
    switchid integer,
    val character varying,
    seq integer
);


ALTER TABLE public.switch_conf OWNER TO postgres;

--
-- Name: switch_conf_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.switch_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.switch_conf_id_seq OWNER TO postgres;

--
-- Name: switch_conf_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.switch_conf_id_seq OWNED BY public.switch_conf.id;


--
-- Name: systemconfiguration_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.systemconfiguration_data (
    id integer NOT NULL,
    ugs integer,
    tmdas integer,
    timeout integer,
    validity integer,
    selftimeout numeric,
    auto integer,
    selfvalidity integer,
    profile character varying,
    range character varying
);


ALTER TABLE public.systemconfiguration_data OWNER TO postgres;

--
-- Name: systemconfiguration_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.systemconfiguration_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.systemconfiguration_data_id_seq OWNER TO postgres;

--
-- Name: systemconfiguration_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.systemconfiguration_data_id_seq OWNED BY public.systemconfiguration_data.id;


--
-- Name: task_priority; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task_priority (
    ip_add character varying,
    priority character varying NOT NULL,
    device_name character varying
);


ALTER TABLE public.task_priority OWNER TO postgres;

--
-- Name: trigger_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trigger_data (
    ip_add character varying NOT NULL,
    unit_no character varying,
    product_name character varying,
    model character varying,
    mfg_date character varying,
    client character varying,
    location character varying,
    country character varying,
    device_id bigint
);


ALTER TABLE public.trigger_data OWNER TO postgres;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    id integer NOT NULL,
    user_id bigint,
    role_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_role_id_seq OWNER TO postgres;

--
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_role_id_seq OWNED BY public.user_role.id;


--
-- Name: user_setting; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_setting (
    id integer NOT NULL,
    type character varying,
    mode character varying,
    gps character varying,
    gps_accuracy numeric,
    opr_mode character varying
);


ALTER TABLE public.user_setting OWNER TO postgres;

--
-- Name: user_setting_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_setting_id_seq OWNER TO postgres;

--
-- Name: user_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_setting_id_seq OWNED BY public.user_setting.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    active integer,
    email character varying(255),
    last_name character varying(255),
    password character varying(255),
    first_name character varying(255),
    mobile character varying(255),
    username character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: alarm_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alarm_info ALTER COLUMN id SET DEFAULT nextval('public.alarm_info_id_seq'::regclass);


--
-- Name: alarms id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alarms ALTER COLUMN id SET DEFAULT nextval('public.alarms_id_seq'::regclass);


--
-- Name: audit_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.audit_log ALTER COLUMN id SET DEFAULT nextval('public.audit_log_id_seq'::regclass);


--
-- Name: band_filter id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.band_filter ALTER COLUMN id SET DEFAULT nextval('public.band_filter_id_seq'::regclass);


--
-- Name: bms_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bms_info ALTER COLUMN id SET DEFAULT nextval('public.bms_info_id_seq'::regclass);


--
-- Name: bmsconfig id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bmsconfig ALTER COLUMN id SET DEFAULT nextval('public.bmsconfig_id_seq'::regclass);


--
-- Name: bmsstatus id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bmsstatus ALTER COLUMN id SET DEFAULT nextval('public.bmsstatus_id_seq'::regclass);


--
-- Name: color_code id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color_code ALTER COLUMN id SET DEFAULT nextval('public.color_code_id_seq'::regclass);


--
-- Name: commands id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commands ALTER COLUMN id SET DEFAULT nextval('public.commands_id_seq'::regclass);


--
-- Name: component_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component_type ALTER COLUMN id SET DEFAULT nextval('public.component_type_id_seq'::regclass);


--
-- Name: config_data_history id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config_data_history ALTER COLUMN id SET DEFAULT nextval('public.config_data_history_id_seq'::regclass);


--
-- Name: config_profile id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config_profile ALTER COLUMN id SET DEFAULT nextval('public.config_profile_id_seq'::regclass);


--
-- Name: device_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.device_info ALTER COLUMN id SET DEFAULT nextval('public.device_info_id_seq'::regclass);


--
-- Name: devices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.devices ALTER COLUMN id SET DEFAULT nextval('public.devices_id_seq'::regclass);


--
-- Name: event_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_type ALTER COLUMN id SET DEFAULT nextval('public.event_type_id_seq'::regclass);


--
-- Name: frequencyconfiguration_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.frequencyconfiguration_data ALTER COLUMN id SET DEFAULT nextval('public.frequencyconfiguration_data_id_seq'::regclass);


--
-- Name: gps_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gps_data ALTER COLUMN id SET DEFAULT nextval('public.gps_data_id_seq'::regclass);


--
-- Name: jmdevice_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jmdevice_data ALTER COLUMN id SET DEFAULT nextval('public.jmdevice_data_id_seq'::regclass);


--
-- Name: jmdevicedata_history id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jmdevicedata_history ALTER COLUMN id SET DEFAULT nextval('public.jmdevice_data_history_id_seq'::regclass);


--
-- Name: managedobject_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.managedobject_type ALTER COLUMN id SET DEFAULT nextval('public.managedobject_type_id_seq'::regclass);


--
-- Name: min_threshold id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.min_threshold ALTER COLUMN id SET DEFAULT nextval('public.min_threshold_id_seq'::regclass);


--
-- Name: node_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_info ALTER COLUMN id SET DEFAULT nextval('public.node_info_id_seq'::regclass);


--
-- Name: peak_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peak_info ALTER COLUMN id SET DEFAULT nextval('public.peak_info_id_seq'::regclass);


--
-- Name: priority id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.priority ALTER COLUMN id SET DEFAULT nextval('public.priority_id_seq'::regclass);


--
-- Name: ptz_position_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ptz_position_data ALTER COLUMN id SET DEFAULT nextval('public.ptz_position_data_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: severity id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.severity ALTER COLUMN id SET DEFAULT nextval('public.severity_id_seq'::regclass);


--
-- Name: switch_conf id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.switch_conf ALTER COLUMN id SET DEFAULT nextval('public.switch_conf_id_seq'::regclass);


--
-- Name: systemconfiguration_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.systemconfiguration_data ALTER COLUMN id SET DEFAULT nextval('public.systemconfiguration_data_id_seq'::regclass);


--
-- Name: user_role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role ALTER COLUMN id SET DEFAULT nextval('public.user_role_id_seq'::regclass);


--
-- Name: user_setting id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_setting ALTER COLUMN id SET DEFAULT nextval('public.user_setting_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: alarm_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alarm_info (id, type, trigger, power, frequency, "time", angle, range, duration, sample, lat, lon, ip_addr, alarm, transid, cueid, emitterpower, bandwidth) FROM stdin;
\.


--
-- Data for Name: alarms; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alarms (id, ip, component_id, component_type, mangedoject_id, managedobject_type, event_id, event_type, severity, event_desctiption, generation_time, status, logtime) FROM stdin;
\.


--
-- Data for Name: alarms_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alarms_history (id, ip, component_id, componen_type, mangedoject_id, managedobject_type, event_id, event_type, severity, event_desctiption, generation_time, acknowledged, logtime) FROM stdin;
\.


--
-- Data for Name: audit_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.audit_log (id, log_type, logtime, description) FROM stdin;
5732	System Access	2019-11-27 12:35:35	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5733	System Access	2019-11-27 12:40:42	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5734	System Access	2020-06-01 13:40:48	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5735	System Access	2020-06-02 16:15:37	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5736	System Access	2020-06-02 17:02:31	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5737	System Access	2020-06-03 10:00:50	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5738	User System Configuration	2020-06-03 10:16:17	Type : standalone Mode : stationary GPS : dgps
5739	User System Configuration	2020-06-03 10:16:54	Type : hummer Mode : stationary GPS : dgps
5740	User System Configuration	2020-06-03 10:16:56	Type : hummer Mode : stationary GPS : dgps
5741	System Access	2020-06-03 19:40:02	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5742	Target List	2020-06-03 19:41:21	Added\tType:blacklist\tFrequency:700.0\tBandwidth:1.0
5743	System Access	2020-06-03 19:49:41	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5744	System Access	2020-06-03 19:56:59	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5745	Target List	2020-06-03 19:59:16	Added\tType:whitelist\tFrequency:800.0\tBandwidth:1.0
5746	Target List	2020-06-03 20:36:24	Delete\tType:blacklist\tFrequency:900.0\tBandwidth:1.0
5747	Target List	2020-06-03 20:44:08	Delete\tType:blacklist\tFrequency:900.0\tBandwidth:1.0
5748	System Access	2020-06-03 20:44:46	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5749	System Access	2020-06-03 20:44:25	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5750	System Access	2020-06-04 12:25:02	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5751	System Configuration	2020-06-04 12:34:57	Manual Override:true\tFalcon Enabled:true\tAuto Enabled:false\tTimeout:1\tValidity:1\tSelf Timeout:4.0\tSelf Validity:1
5752	System Configuration	2020-06-04 12:36:19	Manual Override:true\tFalcon Enabled:true\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5753	System Configuration	2020-06-04 12:39:49	Manual Override:true\tFalcon Enabled:true\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5754	System Access	2020-06-04 13:37:39	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5755	System Access	2020-06-04 13:46:04	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5756	System Access	2020-06-04 13:48:04	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5757	System Access	2020-06-04 14:10:41	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5758	System Access	2020-06-04 14:45:53	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5759	System Access	2020-06-04 15:26:28	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5760	System Access	2020-06-05 11:15:45	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5761	Profile Action	2020-06-05 11:26:50	Save Profile munish
5762	System Access	2020-06-05 14:12:15	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5763	Reset	2020-06-05 14:18:23	Factory Reset Device
5764	Configuration Changed	2020-06-05 14:18:23	IP192.168.4.3\tRoom:1\tStart Freq:0\tStop Freq:6000\tThreshold:-70\tPreamp Type:11
5765	Configuration Changed	2020-06-05 14:18:23	IP192.168.4.3\tRoom:2\tStart Freq:0\tStop Freq:6000\tThreshold:-70\tPreamp Type:21
5766	Configuration Changed	2020-06-05 14:18:23	IP192.168.4.3\tRoom:3\tStart Freq:0\tStop Freq:6000\tThreshold:-70\tPreamp Type:31
5767	Configuration Changed	2020-06-05 14:18:23	IP192.168.4.3\tRoom:4\tStart Freq:0\tStop Freq:6000\tThreshold:-70\tPreamp Type:41
5768	Profile Action	2020-06-05 14:19:03	Delete Profile munish
5769	User System Configuration	2020-06-05 14:41:56	Type : integrated Mode : stationary GPS : dgps
5770	User System Configuration	2020-06-05 14:41:57	Type : integrated Mode : stationary GPS : dgps
5771	User System Configuration	2020-06-05 14:42:12	Type : standalone Mode : stationary GPS : dgps
5772	User System Configuration	2020-06-05 14:42:13	Type : standalone Mode : stationary GPS : dgps
5773	User System Configuration	2020-06-05 14:44:58	Type : hummer Mode : stationary GPS : dgps
5774	User System Configuration	2020-06-05 14:45:02	Type : hummer Mode : stationary GPS : dgps
5775	User System Configuration	2020-06-05 14:45:18	Type : standalone Mode : stationary GPS : dgps
5776	User System Configuration	2020-06-05 14:45:25	Type : standalone Mode : stationary GPS : dgps
5777	User System Configuration	2020-06-05 14:45:32	Type : integrated Mode : stationary GPS : dgps
5778	User System Configuration	2020-06-05 14:45:34	Type : integrated Mode : stationary GPS : dgps
5779	User System Configuration	2020-06-05 14:45:43	Type : hummer Mode : stationary GPS : dgps
5780	User System Configuration	2020-06-05 14:45:44	Type : hummer Mode : stationary GPS : dgps
5781	User System Configuration	2020-06-05 14:45:59	Type : standalone Mode : stationary GPS : dgps
5782	System Configuration	2020-06-05 14:45:59	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5783	User System Configuration	2020-06-05 14:46:00	Type : standalone Mode : stationary GPS : dgps
5784	User System Configuration	2020-06-05 14:46:01	Type : standalone Mode : stationary GPS : dgps
5785	User System Configuration	2020-06-05 14:46:02	Type : standalone Mode : stationary GPS : dgps
5786	User System Configuration	2020-06-05 14:46:02	Type : standalone Mode : stationary GPS : dgps
5787	User System Configuration	2020-06-05 14:46:02	Type : standalone Mode : stationary GPS : dgps
5788	User System Configuration	2020-06-05 14:46:29	Type : standalone Mode : stationary GPS : dgps
5789	System Configuration	2020-06-05 14:54:38	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5790	System Configuration	2020-06-05 14:55:08	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5791	System Access	2020-06-05 15:27:03	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5792	System Access	2020-06-05 16:20:17	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5793	User System Configuration	2020-06-05 16:34:03	Type : standalone Mode : stationary GPS : dgps
5794	System Access	2020-06-05 17:16:15	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5795	System Access	2020-06-08 10:17:54	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5796	System Access	2020-06-11 16:15:10	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5797	System Access	2020-06-13 17:53:08	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5798	System Access	2020-06-13 18:26:30	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5799	System Access	2020-06-13 18:30:42	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5800	System Access	2020-06-13 18:33:54	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5801	System Access	2020-06-13 18:39:22	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5802	System Access	2020-06-13 18:43:20	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5803	System Access	2020-06-13 18:47:25	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5804	System Access	2020-06-13 19:02:38	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5805	System Access	2020-06-13 19:04:56	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5806	System Access	2020-06-13 19:11:49	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5807	System Access	2020-06-13 20:06:43	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5808	System Access	2020-06-13 20:06:43	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5809	System Access	2020-06-13 20:32:56	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5810	System Access	2020-06-14 11:02:40	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5811	System Access	2020-06-14 18:56:24	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5812	System Access	2020-06-14 19:01:18	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5813	System Access	2020-06-14 19:25:18	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5814	System Access	2020-06-14 20:48:55	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5815	User System Configuration	2020-06-14 20:56:55	Type : integrated Mode : stationary GPS : dgps
5816	User System Configuration	2020-06-14 20:56:56	Type : integrated Mode : stationary GPS : dgps
5817	System Access	2020-06-15 00:06:06	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5818	System Access	2020-06-15 00:13:42	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5819	System Access	2020-06-15 10:22:26	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5820	System Access	2020-06-15 14:44:01	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5821	System Access	2020-06-15 15:19:55	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5822	System Access	2020-06-18 10:55:11	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5823	User System Configuration	2020-06-18 10:56:39	Type : standalone Mode : stationary GPS : dgps
5824	System Configuration	2020-06-18 10:56:54	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5825	System Access	2020-06-18 11:05:17	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5826	System Access	2020-06-18 11:14:40	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5827	System Access	2020-06-19 16:12:49	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5828	User System Configuration	2020-06-19 16:13:03	Type : hummer Mode : stationary GPS : dgps
5829	User System Configuration	2020-06-19 16:13:10	Type : hummer Mode : stationary GPS : dgps
5830	User System Configuration	2020-06-19 16:13:19	Type : hummer Mode : stationary GPS : dgps
5831	System Configuration	2020-06-19 16:48:25	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5832	System Configuration	2020-06-19 16:49:10	Manual Override:false\tFalcon Enabled:false\tAuto Enabled:true\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5833	System Access	2020-06-22 11:22:37	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5834	System Access	2020-06-22 11:52:24	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5835	System Access	2020-06-22 11:59:00	Access: Login\tUser Id: admin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5836	System Access	2020-06-22 12:04:42	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5837	Cue	2020-06-22 12:05:45	MANUAL\tHUMMER\tCueId:H_1592807745391\tStartAngle:24\tStopAngle:25\tFrequency:95\tBandwidth:10.0\tTilt:0
5838	Cue	2020-06-22 12:06:35	Terminated\tMANUAL\tCueId:H_1592807745391\tPriority Event
5839	System Access	2020-06-22 13:49:54	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5840	System Access	2020-06-22 17:09:16	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5841	System Access	2020-06-23 10:51:25	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5842	User System Configuration	2020-06-23 10:56:54	Type : integrated Mode : stationary GPS : dgps
5843	User System Configuration	2020-06-23 10:56:55	Type : integrated Mode : stationary GPS : dgps
5844	User System Configuration	2020-06-23 10:57:05	Type : standalone Mode : stationary GPS : dgps
5845	System Configuration	2020-06-23 10:57:05	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5846	User System Configuration	2020-06-23 10:57:06	Type : standalone Mode : stationary GPS : dgps
5852	User System Configuration	2020-06-23 10:57:59	Type : hummer Mode : stationary GPS : dgps
5847	User System Configuration	2020-06-23 10:57:10	Type : standalone Mode : stationary GPS : dgps
5848	User System Configuration	2020-06-23 10:57:23	Type : standalone Mode : stationary GPS : dgps
5849	User System Configuration	2020-06-23 10:57:23	Type : standalone Mode : stationary GPS : dgps
5850	User System Configuration	2020-06-23 10:57:32	Type : standalone Mode : stationary GPS : dgps
5851	User System Configuration	2020-06-23 10:57:41	Type : standalone Mode : stationary GPS : dgps
5853	User System Configuration	2020-06-23 10:58:07	Type : hummer Mode : stationary GPS : dgps
5854	User System Configuration	2020-06-23 10:58:14	Type : standalone Mode : stationary GPS : dgps
5855	System Configuration	2020-06-23 10:58:14	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5856	User System Configuration	2020-06-23 10:58:37	Type : standalone Mode : stationary GPS : dgps
5857	System Access	2020-06-23 12:25:20	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5858	System Access	2020-06-25 19:41:06	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5859	System Access	2020-06-25 20:42:44	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
5860	User System Configuration	2020-06-25 20:57:07	Type : standalone Mode : stationary GPS : dgps
5861	User System Configuration	2020-06-25 21:09:12	Type : hummer Mode : stationary GPS : dgps
5862	User System Configuration	2020-06-25 21:10:25	Type : integrated Mode : stationary GPS : dgps
5863	User System Configuration	2020-06-25 21:12:15	Type : standalone Mode : stationary GPS : dgps
5864	System Configuration	2020-06-25 21:12:15	Manual Override:true\tFalcon Enabled:false\tAuto Enabled:false\tTimeout:11\tValidity:16\tSelf Timeout:4.0\tSelf Validity:1
5865	User System Configuration	2020-06-25 21:13:46	Type : standalone Mode : stationary GPS : dgps
5866	User System Configuration	2020-06-25 21:14:31	Type : standalone Mode : stationary GPS : dgps
5867	User System Configuration	2020-06-25 21:14:33	Type : standalone Mode : stationary GPS : dgps
5868	User System Configuration	2020-06-25 21:14:35	Type : standalone Mode : stationary GPS : dgps
5869	System Access	2020-06-25 22:47:32	Access: Login\tUser Id: superadmin\tType: AUTHENTICATION_SUCCESS\tClient IP: 127.0.0.1
\.


--
-- Data for Name: band_filter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.band_filter (id, start_frq, stop_frq, status) FROM stdin;
\.


--
-- Data for Name: bms_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bms_info (id, on_btn, reset_btn, pr_btn) FROM stdin;
\.


--
-- Data for Name: bmsconfig; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bmsconfig (id, ip, name, tag, value) FROM stdin;
43	1.1.1.1	load		2
44	1.1.1.1	periodicity		5
45	1.1.1.1	systemtime		1549356523891
46	1.1.1.2	load		2
47	1.1.1.2	periodicity		5
48	1.1.1.2	systemtime		1549368222688
49	1.11.3.4	load		2
50	1.11.3.4	periodicity		5
51	1.11.3.4	systemtime		1549368649790
52	123.123.123.123	load		2
53	123.123.123.123	periodicity		5
54	123.123.123.123	systemtime		1549369057482
55	123.123.123.124	load		2
56	123.123.123.124	periodicity		5
57	123.123.123.124	systemtime		1549369374718
61	192.168.7.3	load		2
62	192.168.7.3	periodicity		5
63	192.168.7.3	systemtime		1561358460046
64	192.168.7.2	load		2
65	192.168.7.2	periodicity		5
66	192.168.7.2	systemtime		1561358922019
67	192.168.4.6	load		2
68	192.168.4.6	periodicity		5
69	192.168.4.6	systemtime		1591265482917
\.


--
-- Data for Name: bmsstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bmsstatus (id, ip, bcv1, bcv2, bcv3, bcv4, bcv5, bcv6, bcv7, bcv8, bcv9, bcv10, bcv11, bcv12, bcv13, bcv14, btv, tbc, soc, btemp, alarmword, logtime, generationtime) FROM stdin;
\.


--
-- Data for Name: bmsstatus_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bmsstatus_history (id, ip, bcv1, bcv2, bcv3, bcv4, bcv5, bcv6, bcv7, bcv8, bcv9, bcv10, bcv11, bcv12, bcv13, bcv14, btv, tbc, soc, btemp, alarmword, logtime, generationtime) FROM stdin;
\.


--
-- Data for Name: calibration; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.calibration (ip_add, ant_id, ref_level, status) FROM stdin;
\.


--
-- Data for Name: color_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.color_code (id, startfreq, stopfreq, networktype, color, depl, epr, modulation, visible, profile) FROM stdin;
-3	9	500	RADIO	#ffffff	37	 	 	f	default
\.


--
-- Data for Name: commands; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.commands (id, node_id, tag, cmd, status) FROM stdin;
10	6	SYNCH	\N	t
5	6	PR	1,3	t
1	6	LON	1,1,1	t
2	6	LOFF	1,1,2	t
3	6	NO_RESET	1,2,0	t
4	6	RESET	1,2,1	t
6	6	GET_STATUS	2,5	t
7	6	GET_LOAD	2,1	t
8	6	GET_PR	2,3	t
9	6	GET_TIME	2,4	t
11	6	SET_TIME	1,4	t
\.


--
-- Data for Name: component_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.component_type (id, component, component_type, status) FROM stdin;
\.


--
-- Data for Name: config_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.config_data (ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code, distance, tx_power, fade_margin, calib_timer, ref_level, band_specific, band_filter) FROM stdin;
192.168.4.3	1	1	0	0	6000	-95	0	0	0000	0000	6	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	2000	30	0	0	\N	\N	\N
192.168.4.3	1	2	0	0	6000	-95	0	0	0000	0000	6	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	2000	30	0	0	\N	\N	\N
192.168.4.3	1	3	0	0	6000	-95	0	0	0000	0000	6	31	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	2000	30	0	0	\N	\N	\N
192.168.4.3	1	4	0	85	105	-95	0	0	0000	0000	6	41	1	1	1	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	2000	30	0	0	\N	\N	\N
\.


--
-- Data for Name: config_data_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.config_data_history (id, ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code, fade_margin, tx_power, distance, calib_timer, ref_level, band_specific, band_filter) FROM stdin;
1744	192.168.4.3	1	2	0	0	6000	-70	0	0	0000	0000	6	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1745	192.168.4.3	1	3	0	0	6000	-70	0	0	0000	0000	6	31	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1746	192.168.4.3	1	4	0	0	6000	-70	0	0	0000	0000	6	41	1	1	1	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1747	192.168.4.3	1	1	0	0	6000	-95	0	0	0000	0000	6	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1748	192.168.4.3	1	2	0	0	6000	-95	0	0	0000	0000	6	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1749	192.168.4.3	1	3	0	0	6000	-95	0	0	0000	0000	6	31	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1750	192.168.4.3	1	4	0	0	6000	-95	0	0	0000	0000	6	41	1	1	1	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1751	192.168.4.3	1	1	0	0	6000	-95	0	0	0000	0000	6	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1752	192.168.4.3	1	1	0	0	6000	-95	0	0	0000	0000	6	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1753	192.168.4.3	1	1	0	0	6000	-95	0	0	0000	0000	6	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1740	192.168.4.3	1	2	0	700	1500	-78	0	0	0000	0000	6	21	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1741	192.168.4.3	1	3	0	700	1500	-78	0	0	0000	0000	6	31	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1742	192.168.4.3	1	4	0	890	910	-75	0	0	0000	0000	6	41	1	1	1	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
1743	192.168.4.3	1	1	0	0	6000	-70	0	0	0000	0000	6	11	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	79	091	\N	\N	\N	\N	\N	\N	\N
\.


--
-- Data for Name: config_profile; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.config_profile (ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code, profile, id, ref_level, band_specific, band_filter) FROM stdin;
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1525416450125-13	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.338	1	EXECUTED	7:1b10bd27290a94090d2d14cd3c50e582	createTable tableName=role		\N	3.5.4	\N	\N	7181431168
1525416450125-16	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.352	2	MARK_RAN	7:a1c2a69723645b1266e5a886ed16c6cf	createTable tableName=users		\N	3.5.4	\N	\N	7181431168
create-user-table	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.414	3	EXECUTED	7:45f897e8aa701d86e82466d760b2b3ca	createTable tableName=user_role		\N	3.5.4	\N	\N	7181431168
add-foreign-key-user-role	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.492	4	EXECUTED	7:3e5e3f124febf0b3fba45e176e6dd3cb	addForeignKeyConstraint baseTableName=user_role, constraintName=fkj345gk1bovqvfame88rcx7yyx, referencedTableName=users; addForeignKeyConstraint baseTableName=user_role, constraintName=fka68196081fvovjhkek5m97n3y, referencedTableName=role		\N	3.5.4	\N	\N	7181431168
1525416450125-13	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.338	1	EXECUTED	7:1b10bd27290a94090d2d14cd3c50e582	createTable tableName=role		\N	3.5.4	\N	\N	7181431168
1525416450125-16	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.352	2	MARK_RAN	7:a1c2a69723645b1266e5a886ed16c6cf	createTable tableName=users		\N	3.5.4	\N	\N	7181431168
create-user-table	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.414	3	EXECUTED	7:45f897e8aa701d86e82466d760b2b3ca	createTable tableName=user_role		\N	3.5.4	\N	\N	7181431168
add-foreign-key-user-role	paras.vij (generated)	classpath:db/liquibase-changelog.xml	2018-09-17 16:20:31.492	4	EXECUTED	7:3e5e3f124febf0b3fba45e176e6dd3cb	addForeignKeyConstraint baseTableName=user_role, constraintName=fkj345gk1bovqvfame88rcx7yyx, referencedTableName=users; addForeignKeyConstraint baseTableName=user_role, constraintName=fka68196081fvovjhkek5m97n3y, referencedTableName=role		\N	3.5.4	\N	\N	7181431168
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: device_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.device_info (id, ip_add, name, color, lat, lon, is_active, state, starttime) FROM stdin;
79	192.168.4.3	SPS	#9e7e7e	28.486413	77.074798	false	Reachable	18/09/2019 09:34:24
\.


--
-- Data for Name: devices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.devices (id, node_name, status) FROM stdin;
1	FINLEY	t
4	FALCON	t
5	OXFAM	t
6	BMS	t
2	STRU	t
8	SMM	t
3	SPS	t
7	RF SWITCH	f
\.


--
-- Data for Name: event_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.event_type (id, event_node, event_type, status) FROM stdin;
\.


--
-- Data for Name: frequencyconfiguration_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.frequencyconfiguration_data (id, frequency, bandwidth, freqtype, profile) FROM stdin;
16	700	1	blacklist	default
17	800	1	whitelist	default
\.


--
-- Data for Name: gps_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gps_data (id, lat, lon, inserttime) FROM stdin;
\.


--
-- Data for Name: jmdata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jmdata (ip_add, "time", angle, lat, lon, jmpacketdatetime, created_at) FROM stdin;
10.100.207.117	2018-07-13 11:30:19	\N	28.486463000000001	77.075103999999996	\N	\N
10.100.207.117	2018-07-13 11:30:22	\N	28.486450999999999	77.075118000000003	\N	\N
10.100.207.117	2018-07-13 11:30:23	\N	28.486450999999999	77.075118000000003	\N	\N
10.100.207.117	2018-07-13 11:30:33	\N	28.486474999999999	77.075119000000001	\N	\N
10.100.207.117	2018-07-13 11:30:34	\N	28.486474999999999	77.075119000000001	\N	2018-07-13 11:30:34.041
10.100.207.117	2018-07-13 11:30:36	\N	28.486484000000001	77.075107000000003	\N	\N
10.100.207.117	2018-07-13 11:31:22	\N	28.486494	77.075120999999996	\N	\N
10.100.207.117	2018-07-13 11:30:37	\N	28.486478000000002	77.075113000000002	\N	\N
10.100.207.117	2018-07-13 11:30:43	\N	28.48649	77.075113999999999	\N	\N
10.100.207.117	2018-07-13 11:30:44	\N	28.486497	77.075108999999998	\N	\N
10.100.207.117	2018-07-13 11:30:45	\N	28.486497	77.075108999999998	\N	\N
10.100.207.117	2018-07-13 11:30:47	\N	28.486505999999999	77.075106000000005	\N	\N
10.100.207.117	2018-07-13 11:31:23	\N	28.486494	77.075120999999996	\N	\N
10.100.207.117	2018-07-13 11:30:48	\N	28.486509000000002	77.075102999999999	\N	\N
10.100.207.117	2018-07-13 11:30:49	\N	28.486509999999999	77.075100000000006	\N	\N
10.100.207.117	2018-07-13 11:30:52	\N	28.486526999999999	77.075102999999999	\N	\N
10.100.207.117	2018-07-13 11:30:53	\N	28.486526999999999	77.075102999999999	\N	\N
10.100.207.117	2018-07-13 11:30:54	\N	28.486528	77.075102000000001	\N	\N
10.100.207.117	2018-07-13 11:30:55	\N	28.486528	77.075102000000001	\N	\N
10.100.207.117	2018-07-13 11:30:56	\N	28.486522000000001	77.075114999999997	\N	\N
10.100.207.117	2018-07-13 11:30:57	\N	28.486522000000001	77.075114999999997	\N	\N
10.100.207.117	2018-07-13 11:30:58	\N	28.486519999999999	77.075113999999999	\N	\N
10.100.207.117	2018-07-13 11:31:26	\N	28.48648	77.075126999999995	\N	\N
10.100.207.117	2018-07-13 11:30:59	\N	28.486522000000001	77.075113000000002	\N	\N
10.100.207.117	2018-07-13 11:31:00	\N	28.486522000000001	77.075113000000002	\N	\N
10.100.207.117	2018-07-13 11:31:02	\N	28.486507	77.075119999999998	\N	\N
10.100.207.117	2018-07-13 11:31:03	\N	28.486501000000001	77.075130999999999	\N	\N
10.100.207.117	2018-07-13 11:31:04	\N	28.486501000000001	77.075130999999999	\N	\N
10.100.207.117	2018-07-13 11:31:05	\N	28.486491000000001	77.075141000000002	\N	\N
10.100.207.117	2018-07-13 11:31:06	\N	28.486491000000001	77.075141000000002	\N	\N
10.100.207.117	2018-07-13 11:31:30	\N	28.486463000000001	77.075130000000001	\N	\N
10.100.207.117	2018-07-13 11:31:07	\N	28.486476	77.075147000000001	\N	\N
10.100.207.117	2018-07-13 11:31:08	\N	28.486476	77.075147000000001	\N	\N
10.100.207.117	2018-07-13 11:31:10	\N	28.486481999999999	77.075147999999999	\N	\N
10.100.207.117	2018-07-13 11:31:11	\N	28.486481999999999	77.075147999999999	\N	\N
10.100.207.117	2018-07-13 11:31:13	\N	28.486497	77.075128000000007	\N	\N
10.100.207.117	2018-07-13 11:31:14	\N	28.486497	77.075128000000007	\N	\N
10.100.207.117	2018-07-13 11:31:17	\N	28.486508000000001	77.075130999999999	\N	\N
10.100.207.117	2018-07-13 11:31:18	\N	28.486508000000001	77.075130999999999	\N	\N
10.100.207.117	2018-07-13 11:31:32	\N	28.486464000000002	77.075120999999996	\N	\N
10.100.207.117	2018-07-13 11:31:47	\N	28.48638	77.075158999999999	\N	\N
10.100.207.117	2018-07-13 11:31:33	\N	28.486456	77.075120999999996	\N	\N
10.100.207.117	2018-07-13 11:31:34	\N	28.486456	77.075120999999996	\N	\N
10.100.207.117	2018-07-13 11:31:36	\N	28.486428	77.075128000000007	\N	\N
10.100.207.117	2018-07-13 11:31:37	\N	28.486407	77.075136000000001	\N	\N
10.100.207.117	2018-07-13 11:31:38	\N	28.486407	77.075136000000001	\N	\N
10.100.207.117	2018-07-13 11:31:40	\N	28.486412000000001	77.075131999999996	\N	\N
10.100.207.117	2018-07-13 11:31:59	\N	28.486395999999999	77.075145000000006	\N	\N
10.100.207.117	2018-07-13 11:31:48	\N	28.486381000000002	77.075156000000007	\N	\N
10.100.207.117	2018-07-13 11:31:41	\N	28.486414	77.075140000000005	\N	\N
10.100.207.117	2018-07-13 11:31:42	\N	28.486414	77.075140000000005	\N	\N
10.100.207.117	2018-07-13 11:31:43	\N	28.486405000000001	77.075149999999994	\N	\N
10.100.207.117	2018-07-13 11:31:44	\N	28.486398999999999	77.075153999999998	\N	\N
10.100.207.117	2018-07-13 11:31:49	\N	28.486388999999999	77.075146000000004	\N	\N
10.100.207.117	2018-07-13 11:31:45	\N	28.486381000000002	77.075152000000003	\N	\N
10.100.207.117	2018-07-13 11:31:46	\N	28.486381000000002	77.075152000000003	\N	\N
10.100.207.117	2018-07-13 11:31:50	\N	28.486388000000002	77.075142	\N	\N
10.100.207.117	2018-07-13 11:31:51	\N	28.48639	77.075142	\N	\N
10.100.207.117	2018-07-13 11:32:00	\N	28.486395999999999	77.075145000000006	\N	\N
10.100.207.117	2018-07-13 11:31:52	\N	28.486397	77.075137999999995	\N	\N
10.100.207.117	2018-07-13 11:31:53	\N	28.486398999999999	77.075135000000003	\N	\N
10.100.207.117	2018-07-13 11:31:54	\N	28.486398999999999	77.075135000000003	\N	\N
10.100.207.117	2018-07-13 11:31:56	\N	28.486398999999999	77.075138999999993	\N	\N
10.100.207.117	2018-07-13 11:31:57	\N	28.486398999999999	77.075138999999993	\N	\N
10.100.207.117	2018-07-13 11:32:01	\N	28.486404	77.075147000000001	\N	\N
10.100.207.117	2018-07-13 11:32:03	\N	28.486395000000002	77.075145000000006	\N	\N
10.100.207.117	2018-07-13 11:32:07	\N	28.486407	77.075123000000005	\N	\N
10.100.207.117	2018-07-13 11:32:04	\N	28.486402999999999	77.075131999999996	\N	\N
10.100.207.117	2018-07-13 11:32:05	\N	28.486411	77.075130000000001	\N	\N
10.100.207.117	2018-07-13 11:32:13	\N	28.486378999999999	77.075118000000003	\N	\N
10.100.207.117	2018-07-13 11:32:08	\N	28.4864	77.075125999999997	\N	\N
10.100.207.117	2018-07-13 11:32:09	\N	28.4864	77.075125999999997	\N	\N
10.100.207.117	2018-07-13 11:32:17	\N	28.486377999999998	77.075098999999994	\N	\N
10.100.207.117	2018-07-13 11:32:16	\N	28.486377999999998	77.075098999999994	\N	\N
10.100.207.117	2018-07-13 11:32:19	\N	28.486387000000001	77.075078000000005	\N	\N
10.100.207.117	2018-07-13 11:32:21	\N	28.486388000000002	77.075072000000006	\N	2018-07-13 11:32:21.002
10.100.207.117	2018-07-13 11:32:20	\N	28.486388000000002	77.075072000000006	\N	\N
10.100.207.117	2018-07-13 11:32:22	\N	28.486398000000001	77.075055000000006	\N	\N
10.100.207.117	2018-07-13 11:32:23	\N	28.486395999999999	77.075050000000005	\N	\N
10.100.207.117	2018-07-13 11:32:24	\N	28.486401000000001	77.075047999999995	\N	\N
10.100.207.117	2018-07-13 11:32:25	\N	28.486401000000001	77.075047999999995	\N	\N
10.100.207.117	2018-07-13 11:32:27	\N	28.486405999999999	77.075041999999996	\N	\N
10.100.207.117	2018-07-13 11:32:28	\N	28.486419000000001	77.075034000000002	\N	\N
10.100.207.117	2018-07-13 11:32:30	\N	28.486416999999999	77.075040000000001	\N	\N
10.100.207.117	2018-07-13 11:32:31	\N	28.486419000000001	77.075039000000004	\N	\N
10.100.207.117	2018-07-13 11:32:32	\N	28.486419000000001	77.075039000000004	\N	2018-07-13 11:32:32.086
10.100.207.117	2018-07-13 11:32:34	\N	28.486411	77.075056000000004	\N	\N
10.100.207.117	2018-07-13 11:32:35	\N	28.486411	77.075056000000004	\N	\N
10.100.207.117	2018-07-13 11:32:40	\N	28.486402999999999	77.075084000000004	\N	\N
10.100.207.117	2018-07-13 11:38:18	\N	28.486499999999999	77.075130000000001	\N	2018-07-13 11:38:19.074
10.100.207.117	2018-07-13 11:38:31	\N	28.486518	77.075131999999996	\N	2018-07-13 11:38:31.92
10.100.207.117	2018-07-13 11:38:23	\N	28.486502999999999	77.075131999999996	\N	2018-07-13 11:38:23.918
10.100.207.117	2018-07-13 11:38:32	\N	28.486515000000001	77.075129000000004	\N	2018-07-13 11:38:32.892
10.100.207.117	2018-07-13 11:38:19	\N	28.486502999999999	77.075130000000001	\N	2018-07-13 11:38:19.975
10.100.207.117	2018-07-13 11:38:33	\N	28.486515000000001	77.075129000000004	\N	2018-07-13 11:38:33.001
10.100.207.117	2018-07-13 11:38:25	\N	28.486499999999999	77.075136000000001	\N	2018-07-13 11:38:26
10.100.207.117	2018-07-13 11:38:20	\N	28.486505999999999	77.075130999999999	\N	2018-07-13 11:38:20.616
10.100.207.117	2018-07-13 11:38:34	\N	28.486504	77.075125	\N	2018-07-13 11:38:34.984
10.100.207.117	2018-07-13 11:38:26	\N	28.486512000000001	77.075136999999998	\N	2018-07-13 11:38:26.973
10.100.207.117	2018-07-13 11:38:35	\N	28.486504	77.075125	\N	2018-07-13 11:38:35.187
10.100.207.117	2018-07-13 11:38:22	\N	28.486499999999999	77.075130999999999	\N	2018-07-13 11:38:22.932
10.100.207.117	2018-07-13 11:38:37	\N	28.486484000000001	77.075113999999999	\N	2018-07-13 11:38:37.936
10.100.207.117	2018-07-13 11:38:28	\N	28.486525	77.075136999999998	\N	2018-07-13 11:38:28.963
10.100.207.117	2018-07-13 11:38:29	\N	28.486525	77.075136999999998	\N	2018-07-13 11:38:29.166
10.100.207.117	2018-07-13 11:38:38	\N	28.486484999999998	77.075113000000002	\N	2018-07-13 11:38:38.154
\.


--
-- Data for Name: jmdevice_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jmdevice_data (id, deviceid, "time", roll, tilt, pan) FROM stdin;
\.


--
-- Data for Name: jmdevicedata_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jmdevicedata_history (id, deviceid, "time", roll, tilt, pan) FROM stdin;
2	3	2019-03-19 09:46:38.818938	\N	\N	\N
3	4	2019-03-19 09:46:38.818938	\N	\N	\N
4	1	2019-03-19 09:46:38.818938	\N	\N	\N
5	1	2019-03-19 09:46:38.818938	\N	\N	\N
6	2	2019-03-19 09:46:38.818938	\N	\N	\N
7	3	2019-03-19 09:46:41.103964	\N	\N	\N
8	4	2019-03-19 09:46:41.103964	\N	\N	\N
9	1	2019-03-19 09:46:41.103964	\N	\N	\N
10	1	2019-03-19 09:46:41.103964	\N	\N	\N
11	2	2019-03-19 09:46:41.103964	\N	\N	\N
12	3	2019-03-19 09:46:42.90167	\N	\N	\N
13	4	2019-03-19 09:46:42.90167	\N	\N	\N
14	1	2019-03-19 09:46:42.90167	\N	\N	\N
15	1	2019-03-19 09:46:42.90167	\N	\N	\N
16	2	2019-03-19 09:46:42.90167	\N	\N	\N
\.


--
-- Data for Name: jmnode_mapping; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jmnode_mapping (deviceid, antennaid, sector) FROM stdin;
1	11	1
2	21	2
3	31	3
4	41	4
\.


--
-- Data for Name: led_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.led_status (ip_add, led_on, device_id) FROM stdin;
192.168.4.3	1	79
\.


--
-- Data for Name: managedobject_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.managedobject_type (id, managed_object, managed_object_id, status) FROM stdin;
\.


--
-- Data for Name: min_threshold; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.min_threshold (id, startfreq, stopfreq, power, attenuation) FROM stdin;
1	0	3300	-145	0
2	3300	4100	-140	0
3	4100	5000	-138	0
4	5000	6000	-128	0
\.


--
-- Data for Name: node_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.node_info (id, ip_add, type, status) FROM stdin;
30	192.168.3.2	FINLEY	Not Reachable
31	192.168.5.2	FALCON	Not Reachable
32	192.168.6.2	OXFAM	Not Reachable
\.


--
-- Data for Name: peak_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.peak_info (ip_add, power, frequency, "time", device_id, id, angle, currentconfigid, antennaid, alarm, jmdevice_id, type, sector, inserttime, technology, lat, lon) FROM stdin;
\.


--
-- Data for Name: priority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.priority (id, priority, name) FROM stdin;
1	1	MANUAL
2	2	OXFAM
4	3	FALCON
3	4	BLACKLIST
5	5	AUTO
\.


--
-- Data for Name: ptz_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ptz_info (device_ip, ptz_ip, ptz_offset, name, sector_offset) FROM stdin;
\.


--
-- Data for Name: ptz_position_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ptz_position_data (id, ptz_ip, pan_position, tilt_position, date_time, system_time) FROM stdin;
\.


--
-- Data for Name: reflevel_master; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reflevel_master (id, value) FROM stdin;
l1	l1
l2	l2
l3	l3
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (id, role, active, created_at, updated_at) FROM stdin;
1	Admin	1	2018-09-17 16:20:36.789	2018-09-17 16:20:36.789
2	User	1	2018-12-18 16:01:57.586	2018-12-18 16:01:57.586
3	SuperAdmin	1	2019-02-16 16:31:31.76	2019-02-16 16:31:31.76
\.


--
-- Data for Name: sensor_config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensor_config (sensornum, sensorenabled, sensorip, sensorport, rotatingsensor, omcip, rfswitchip, angle, beamwidth, gsmserverip, usestatus, sector) FROM stdin;
2	Enabled	192.168.4.5	1	No	192.168.4.2	10.100.206.1	90	60	10.100.207.134	Yes	2
4	Enabled	192.168.4.7	1	Yes	192.168.4.2	10.100.206.1	0	60	10.100.207.134	Yes	STRU
3	Enabled	192.168.4.6	1	No	192.168.4.2	10.100.206.1	150	60	10.100.207.134	Yes	3
1	Enabled	192.168.4.4	1	No	192.168.4.2	10.100.206.1	30	60	10.100.207.134	Yes	1
\.


--
-- Data for Name: severity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.severity (id, severity, severity_id, status) FROM stdin;
\.


--
-- Data for Name: switch_conf; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.switch_conf (id, sid, pathid, switchid, val, seq) FROM stdin;
1	1	1	1	11	1
2	1	2	1	10	1
5	1	1	4	10	2
6	1	2	4	11	2
10	1	5	7	11	5
11	1	6	10	11	6
13	2	1	2	11	3
14	2	2	2	10	3
17	2	1	5	10	4
18	2	2	5	11	4
21	2	6	8	11	9
24	2	5	11	11	10
25	3	1	3	11	7
26	3	2	3	10	7
29	3	1	6	10	8
30	3	2	6	11	8
33	3	6	9	11	11
36	3	5	12	11	12
38	4	8	13	11	13
39	4	9	13	10	13
41	4	1	14	11	14
42	4	2	14	10	14
45	4	1	15	10	15
46	4	2	15	11	15
27	3	3	3	00	7
28	3	4	3	01	7
22	2	5	8	00	9
23	2	6	11	00	10
47	4	3	15	01	15
48	4	4	15	00	15
43	4	3	14	00	14
44	4	4	14	01	14
4	1	4	1	01	1
3	1	3	1	00	1
8	1	4	4	00	2
7	1	3	4	01	2
9	1	6	7	00	5
12	1	5	10	00	6
40	4	10	13	00	13
16	2	4	2	01	3
15	2	3	2	00	3
20	2	4	5	00	4
19	2	3	5	11	4
32	3	4	6	00	8
31	3	3	6	01	8
37	4	7	13	01	13
35	3	6	12	00	12
34	3	5	9	00	11
\.


--
-- Data for Name: systemconfiguration_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.systemconfiguration_data (id, ugs, tmdas, timeout, validity, selftimeout, auto, selfvalidity, profile, range) FROM stdin;
5	1	0	11	16	4	0	1	default	\N
\.


--
-- Data for Name: task_priority; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.task_priority (ip_add, priority, device_name) FROM stdin;
\.


--
-- Data for Name: trigger_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trigger_data (ip_add, unit_no, product_name, model, mfg_date, client, location, country, device_id) FROM stdin;
192.168.4.3	4	SMS-IP	TRGL	09/09/2018	abc	GURGAON	091	79
\.


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role (id, user_id, role_id, created_at, updated_at) FROM stdin;
1	3	2	\N	\N
2	2	1	\N	\N
3	5	3	\N	\N
\.


--
-- Data for Name: user_setting; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_setting (id, type, mode, gps, gps_accuracy, opr_mode) FROM stdin;
2	standalone	stationary	dgps	50	\N
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, active, email, last_name, password, first_name, mobile, username, created_at, updated_at) FROM stdin;
3	1	user@vnl.in	VNL	$2a$10$l0jL3LloT.nrqXTC2riafe3usIxc/dqEEnE25oxDpb5HVNBa0VUVW	User	1234567890	user	2018-12-18 16:01:57.819	2018-12-18 16:01:57.819
2	1	admin@vnl.in	VNL	$2a$10$92lPrQKfALgGxFrloT4MEe13u1ike9z.mknpJmyywfG.CSwmlTEdW	Admin	1234567890	admin	2018-07-19 10:37:05.647	2019-01-18 13:20:14.483
5	1	superadmin@vnl.in	VNL	$2a$10$kKqU6e1YIYvrYTRLpZTTeundUbNmMj6Vav/NsiKrC7pxpOwkENrf6	SuperAdmin	1234567890	superadmin	2019-02-16 16:31:31.979	2019-02-16 16:31:31.979
\.


--
-- Name: alarm_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.alarm_info_id_seq', 876101, true);


--
-- Name: alarms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.alarms_id_seq', 1, false);


--
-- Name: audit_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.audit_log_id_seq', 5869, true);


--
-- Name: band_filter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.band_filter_id_seq', 1, false);


--
-- Name: bms_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bms_info_id_seq', 1, false);


--
-- Name: bmsconfig_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bmsconfig_id_seq', 69, true);


--
-- Name: bmsstatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bmsstatus_id_seq', 1, false);


--
-- Name: color_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.color_code_id_seq', 20, true);


--
-- Name: commands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.commands_id_seq', 17, true);


--
-- Name: component_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.component_type_id_seq', 1, false);


--
-- Name: config_data_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.config_data_history_id_seq', 1753, true);


--
-- Name: config_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.config_profile_id_seq', 3, true);


--
-- Name: device_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.device_info_id_seq', 79, true);


--
-- Name: devices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.devices_id_seq', 1, false);


--
-- Name: event_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.event_type_id_seq', 1, false);


--
-- Name: frequencyconfiguration_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.frequencyconfiguration_data_id_seq', 19, true);


--
-- Name: gps_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gps_data_id_seq', 29231, true);


--
-- Name: jmdevice_data_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jmdevice_data_history_id_seq', 16, true);


--
-- Name: jmdevice_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jmdevice_data_id_seq', 12, true);


--
-- Name: jmnode_mapping_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jmnode_mapping_id_seq', 6, true);


--
-- Name: managedobject_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.managedobject_type_id_seq', 1, false);


--
-- Name: min_threshold_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.min_threshold_id_seq', 1, false);


--
-- Name: node_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.node_info_id_seq', 33, true);


--
-- Name: peak_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.peak_info_id_seq', 569704158, true);


--
-- Name: priority_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.priority_id_seq', 1, false);


--
-- Name: ptz_position_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ptz_position_data_id_seq', 7185502, true);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 3, true);


--
-- Name: severity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.severity_id_seq', 1, false);


--
-- Name: switch_conf_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.switch_conf_id_seq', 1, false);


--
-- Name: systemconfiguration_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.systemconfiguration_data_id_seq', 9, true);


--
-- Name: user_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_role_id_seq', 3, true);


--
-- Name: user_setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_setting_id_seq', 2, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 5, true);


--
-- Name: alarm_info alarm_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alarm_info
    ADD CONSTRAINT alarm_info_pkey PRIMARY KEY (id);


--
-- Name: alarms alarms_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alarms
    ADD CONSTRAINT alarms_pkey PRIMARY KEY (id);


--
-- Name: bmsconfig bmsconfig_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bmsconfig
    ADD CONSTRAINT bmsconfig_pkey PRIMARY KEY (id);


--
-- Name: bmsstatus bmsstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bmsstatus
    ADD CONSTRAINT bmsstatus_pkey PRIMARY KEY (id);


--
-- Name: color_code color_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color_code
    ADD CONSTRAINT color_code_pkey PRIMARY KEY (id);


--
-- Name: commands commands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commands
    ADD CONSTRAINT commands_pkey PRIMARY KEY (id);


--
-- Name: component_type component_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component_type
    ADD CONSTRAINT component_type_pkey PRIMARY KEY (id);


--
-- Name: config_data_history config_data_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config_data_history
    ADD CONSTRAINT config_data_history_pkey PRIMARY KEY (id);


--
-- Name: config_data config_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config_data
    ADD CONSTRAINT config_data_pkey PRIMARY KEY (ip_add, room);


--
-- Name: config_profile config_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config_profile
    ADD CONSTRAINT config_profile_pkey PRIMARY KEY (id);


--
-- Name: device_info device_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.device_info
    ADD CONSTRAINT device_id_pkey PRIMARY KEY (id);


--
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- Name: event_type event_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.event_type
    ADD CONSTRAINT event_type_pkey PRIMARY KEY (id);


--
-- Name: frequencyconfiguration_data frequencyconfiguration_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.frequencyconfiguration_data
    ADD CONSTRAINT frequencyconfiguration_data_pkey PRIMARY KEY (id);


--
-- Name: gps_data gps_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gps_data
    ADD CONSTRAINT gps_data_pkey PRIMARY KEY (id);


--
-- Name: jmdata jmdata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jmdata
    ADD CONSTRAINT jmdata_pkey PRIMARY KEY ("time");


--
-- Name: jmdevicedata_history jmdevice_data_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jmdevicedata_history
    ADD CONSTRAINT jmdevice_data_history_pkey PRIMARY KEY (id);


--
-- Name: jmdevice_data jmdevice_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jmdevice_data
    ADD CONSTRAINT jmdevice_data_pkey PRIMARY KEY (id);


--
-- Name: jmnode_mapping jmnode_mapping_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jmnode_mapping
    ADD CONSTRAINT jmnode_mapping_pkey PRIMARY KEY (deviceid);


--
-- Name: led_status led_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.led_status
    ADD CONSTRAINT led_status_pkey PRIMARY KEY (ip_add);


--
-- Name: audit_log log_audit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.audit_log
    ADD CONSTRAINT log_audit_pkey PRIMARY KEY (id);


--
-- Name: managedobject_type managedobject_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.managedobject_type
    ADD CONSTRAINT managedobject_type_pkey PRIMARY KEY (id);


--
-- Name: min_threshold min_threshold_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.min_threshold
    ADD CONSTRAINT min_threshold_pkey PRIMARY KEY (id);


--
-- Name: node_info node_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.node_info
    ADD CONSTRAINT node_info_pkey PRIMARY KEY (id);


--
-- Name: peak_info peak_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peak_info
    ADD CONSTRAINT peak_info_pkey PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: priority priority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.priority
    ADD CONSTRAINT priority_pkey PRIMARY KEY (id);


--
-- Name: ptz_info ptz_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ptz_info
    ADD CONSTRAINT ptz_info_pkey PRIMARY KEY (device_ip);


--
-- Name: ptz_position_data ptz_position_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ptz_position_data
    ADD CONSTRAINT ptz_position_data_pkey PRIMARY KEY (id);


--
-- Name: reflevel_master reflevel_master_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reflevel_master
    ADD CONSTRAINT reflevel_master_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: sensor_config sensor_config_pkey1; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor_config
    ADD CONSTRAINT sensor_config_pkey1 PRIMARY KEY (sensornum, sensorport);


--
-- Name: severity severity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.severity
    ADD CONSTRAINT severity_pkey PRIMARY KEY (id);


--
-- Name: switch_conf switch_conf_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.switch_conf
    ADD CONSTRAINT switch_conf_pkey PRIMARY KEY (id);


--
-- Name: systemconfiguration_data systemconfiguration_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.systemconfiguration_data
    ADD CONSTRAINT systemconfiguration_data_pkey PRIMARY KEY (id);


--
-- Name: task_priority task_priority_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_priority
    ADD CONSTRAINT task_priority_pkey PRIMARY KEY (priority);


--
-- Name: trigger_data trigger_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trigger_data
    ADD CONSTRAINT trigger_data_pkey PRIMARY KEY (ip_add);


--
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);


--
-- Name: user_setting user_setting_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_setting
    ADD CONSTRAINT user_setting_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: fki_config_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_config_fkey ON public.config_data USING btree (device_id);


--
-- Name: fki_led_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_led_fkey ON public.led_status USING btree (device_id);


--
-- Name: fki_peak_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_peak_fkey ON public.peak_info USING btree (device_id);


--
-- Name: fki_trigger_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_trigger_fkey ON public.trigger_data USING btree (device_id);


--
-- Name: peak_info_currentconfigid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX peak_info_currentconfigid_idx ON public.peak_info USING btree (currentconfigid);


--
-- Name: peak_info_time; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX peak_info_time ON public.peak_info USING btree ("time");


--
-- Name: config_data insert_config_data_history_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER insert_config_data_history_trigger AFTER INSERT ON public.config_data FOR EACH ROW EXECUTE PROCEDURE public.insert_config_data();


--
-- Name: config_data insert_config_data_history_trigger1; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER insert_config_data_history_trigger1 AFTER UPDATE ON public.config_data FOR EACH ROW EXECUTE PROCEDURE public.insert_config_data();


--
-- Name: alarms move_alarm_to_history; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER move_alarm_to_history AFTER INSERT ON public.alarms FOR EACH ROW EXECUTE PROCEDURE public.move_alarm_to_history_table();


--
-- Name: alarms move_alarm_to_history_on_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER move_alarm_to_history_on_update AFTER UPDATE ON public.alarms FOR EACH ROW EXECUTE PROCEDURE public.move_alarm_to_history_table_on_update();


--
-- Name: bmsstatus move_bms_status_to_history; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER move_bms_status_to_history BEFORE INSERT ON public.bmsstatus FOR EACH ROW EXECUTE PROCEDURE public.move_bms_status_to_history_table();


--
-- Name: jmdevice_data move_jmdevice_data_to_history; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER move_jmdevice_data_to_history AFTER INSERT ON public.jmdevice_data FOR EACH ROW EXECUTE PROCEDURE public.move_jmdevice_data_to_history_table();


--
-- Name: device_info movegps; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER movegps BEFORE UPDATE ON public.device_info FOR EACH ROW EXECUTE PROCEDURE public.move_gps();


--
-- Name: bmsconfig unique_prop_bms; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER unique_prop_bms BEFORE INSERT ON public.bmsconfig FOR EACH ROW EXECUTE PROCEDURE public.delete_previous_prop();


--
-- Name: peak_info update_peak_info_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_peak_info_trigger AFTER INSERT ON public.peak_info FOR EACH ROW EXECUTE PROCEDURE public.update_peak_info();

ALTER TABLE public.peak_info DISABLE TRIGGER update_peak_info_trigger;


--
-- Name: config_data config_data_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config_data
    ADD CONSTRAINT config_data_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);


--
-- Name: user_role fka68196081fvovjhkek5m97n3y; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fka68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES public.role(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_role fkj345gk1bovqvfame88rcx7yyx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkj345gk1bovqvfame88rcx7yyx FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: led_status led_status_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.led_status
    ADD CONSTRAINT led_status_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);


--
-- Name: peak_info peak_info_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peak_info
    ADD CONSTRAINT peak_info_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);


--
-- Name: peak_info peak_info_jmdevice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peak_info
    ADD CONSTRAINT peak_info_jmdevice_id_fkey FOREIGN KEY (jmdevice_id) REFERENCES public.jmdevicedata_history(id);


--
-- Name: trigger_data trigger_data_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trigger_data
    ADD CONSTRAINT trigger_data_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);


--
-- PostgreSQL database dump complete
--

