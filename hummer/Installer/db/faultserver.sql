--
-- PostgreSQL database dump
--

-- Dumped from database version 11.8
-- Dumped by pg_dump version 11.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: clearbyseverityfunc(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clearbyseverityfunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
   	  delete from current where id = new.id and new.severity = 0;
      RETURN NEW;
   END;

$$;


ALTER FUNCTION public.clearbyseverityfunc() OWNER TO postgres;

--
-- Name: delete_by_id(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_by_id() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
		insert into history select * from current where id=old.id;
		update history set severity = 0 where id =  old.id;
      RETURN OLD;
   END;

$$;


ALTER FUNCTION public.delete_by_id() OWNER TO postgres;

--
-- Name: deletefunc(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.deletefunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
insert into history select * from current where component_id=old.component_id and managed_object_id=old.managed_object_id and event_id=old.event_id;
      RETURN OLD;
   END;

$$;


ALTER FUNCTION public.deletefunc() OWNER TO postgres;

--
-- Name: insertfunc(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.insertfunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
 delete from current where component_id=new.component_id and managed_object_id=new.managed_object_id  and event_id=new.event_id;
      INSERT INTO history(id,report,alarm,component_id, component_type, managed_object_id, managed_object_type,event_id, event_type, severity,event_description,generation_time, ip, log_time,acknowledgement) 
      VALUES (new.ID, new.report, new.alarm, new.component_id, new.component_type, new.managed_object_id, new.managed_object_type, new.event_id, new.event_type, new.severity, new.event_description, new.generation_time, new.ip, new.log_time, new.acknowledgement);
      RETURN NEW;
   END;

$$;


ALTER FUNCTION public.insertfunc() OWNER TO postgres;

--
-- Name: updatefunc(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.updatefunc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

   BEGIN
      INSERT INTO history(id,report,alarm,component_id, component_type, managed_object_id, managed_object_type,event_id, event_type, severity,event_description,generation_time, ip, log_time,acknowledgement) 
      VALUES (new.ID, new.report, new.alarm, new.component_id, new.component_type, new.managed_object_id, new.managed_object_type, new.event_id, new.event_type, new.severity, new.event_description, new.generation_time, new.ip, new.log_time, new.acknowledgement);

	delete from current where id = new.id  and new.severity = 0;
      
      RETURN NEW;
   END;

$$;


ALTER FUNCTION public.updatefunc() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: component_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.component_type (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.component_type OWNER TO postgres;

--
-- Name: current; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.current (
    id integer NOT NULL,
    report integer,
    alarm integer,
    component_id character varying(50),
    component_type integer,
    managed_object_id character varying(50),
    managed_object_type integer,
    event_id character varying(50),
    event_type integer,
    severity integer,
    event_description character varying(100),
    generation_time timestamp without time zone,
    ip character varying(24),
    log_time timestamp without time zone DEFAULT now(),
    acknowledgement boolean DEFAULT false
);


ALTER TABLE public.current OWNER TO postgres;

--
-- Name: current_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.current_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.current_id_seq OWNER TO postgres;

--
-- Name: current_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.current_id_seq OWNED BY public.current.id;


--
-- Name: history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.history (
    id numeric,
    report integer,
    alarm integer,
    component_id character varying(50),
    component_type integer,
    managed_object_id character varying(50),
    managed_object_type integer,
    event_id character varying(50),
    event_type integer,
    severity integer,
    event_description character varying(100),
    generation_time timestamp without time zone,
    ip character varying(24),
    log_time timestamp without time zone,
    acknowledgement boolean DEFAULT false
);


ALTER TABLE public.history OWNER TO postgres;

--
-- Name: manged_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.manged_type (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.manged_type OWNER TO postgres;

--
-- Name: severity; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.severity (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE public.severity OWNER TO postgres;

--
-- Name: view_history; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_history AS
 SELECT a.log_time AS logtime,
    a.ip AS node_ip,
    a.component_id,
    'SPS'::text AS component_type,
    'HUMMER'::text AS managed_object_type,
    b.name AS severity,
    a.event_description
   FROM public.history a,
    public.severity b
  WHERE (a.severity = b.id);


ALTER TABLE public.view_history OWNER TO postgres;

--
-- Name: current id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.current ALTER COLUMN id SET DEFAULT nextval('public.current_id_seq'::regclass);


--
-- Data for Name: component_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.component_type (id, name) FROM stdin;
\.


--
-- Data for Name: current; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.current (id, report, alarm, component_id, component_type, managed_object_id, managed_object_type, event_id, event_type, severity, event_description, generation_time, ip, log_time, acknowledgement) FROM stdin;
\.


--
-- Data for Name: history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.history (id, report, alarm, component_id, component_type, managed_object_id, managed_object_type, event_id, event_type, severity, event_description, generation_time, ip, log_time, acknowledgement) FROM stdin;
\.


--
-- Data for Name: manged_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.manged_type (id, name) FROM stdin;
\.


--
-- Data for Name: severity; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.severity (id, name) FROM stdin;
1	info
2	warning
3	minor
4	major
5	critical
0	clear
\.


--
-- Name: current_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.current_id_seq', 13147, true);


--
-- Name: component_type component_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component_type
    ADD CONSTRAINT component_type_pkey PRIMARY KEY (id);


--
-- Name: manged_type manged_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.manged_type
    ADD CONSTRAINT manged_type_pkey PRIMARY KEY (id);


--
-- Name: severity severity_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.severity
    ADD CONSTRAINT severity_pkey PRIMARY KEY (id);


--
-- Name: current clear_by_severity_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER clear_by_severity_trigger AFTER INSERT ON public.current FOR EACH ROW EXECUTE PROCEDURE public.clearbyseverityfunc();


--
-- Name: current insert_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER insert_trigger BEFORE INSERT ON public.current FOR EACH ROW EXECUTE PROCEDURE public.insertfunc();


--
-- Name: current update_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER update_trigger AFTER UPDATE ON public.current FOR EACH ROW EXECUTE PROCEDURE public.updatefunc();


--
-- PostgreSQL database dump complete
--

