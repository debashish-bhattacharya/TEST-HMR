#!/bin/sh
dir=$PWD
cd $dir
rm -rf target/*
export M2_HOME=/home/debu/Downloads/tst/buildTool/maven/apache-maven-3.8.6
export PATH=$M2_HOME/bin:$PATH
mvn clean
mvn package -Dmaven.test.skip=true

#Compiling FaultServer
cd $dir/faultserver
rm -rf target/*
mvn clean
mvn package -Dmaven.test.skip=true
